Yoi
====
A Rust graphics framework made as a learning exercise. It is basically focused on using Vulkan for the time being.
The general application structure is inspired by [Nannou](https://nannou.cc/); it is pretty much just ported from it with some minor alterations. Also partially inspired by [Processing](www.processing.org).

General Structure
===
* yoi-vk : contains the general framework for working with Vulkan
* vsketch : contains the general app setup code as well as provides some helper classes like a camera.
  * Like Processing, there is a `setup` and `draw` function where setup serves as a place to set things up and draw is run in a loop.
  * Like Nannou, you have some kind of state object that you use to pass information between the two functions. 
* sketches : contains some samples

Getting Started
=====
* Clone the repo. There are submodules not yet published on crates.io, so you'll have to make sure to clone with submodules.

Notes about codebase
===
* Note that I do not use Rust on a professional basis, so things you see will likely not be reflective of best practices or possibly impact performance. I generally try to keep things simple.
* This library still approaches things from a lower-level perspective so there is still some verboseness to things; if you are looking for something higher level, [Vulkano](https://github.com/vulkano-rs/vulkano) could be a good fit.
[wgpu](https://github.com/gfx-rs/wgpu) could also be a good alternative (uses Vulkan under the hood on compatible systems)

Debugging Notes
====
* For use of `GL_EXT_debug_printf`, it's easiest to just hop into Vulkan Configurator and enable the `Debug Printf Preset` under the validation settings. As of 8.8.2023, there are a number of features that also need to be
disabled in addition to enabling `VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT`

Unsafe code
===
Unsafe code is kept to a minimum as much as possible, limited for the most part to Vulkan specific calls at the moment. Most unsafe calls will occur
in the core Vulkan class (`yoi-vk/src/core.rs`) with some exceptions.

Shader Compilation
===
GLSL is the assumed language and `shaderc` is used to convert GLSL -> SPIR-V as it is included in the default Vulkan SDK install. Things are done on the fly.
It should not be difficult to insert your shader parser of choice.

Textures and Samplers
===
For simplicity, textures and samplers are assumed to work in conjunction and not be separate in the shader.

Examples
===
Running examples is like in most Rust projects

* `cd sketches`
* `cargo run --bin <filename of example>`

Big To-dos
===
* better docs (docgen seems to be working well enough for now)

Big thanks
===
This is largely built on the ideas of others, authors are noted whenever possible, a lot of this is built on the backs of their hard work.
If you see something of yours where there is no author listed, please feel free to let me know, and I'll be happy to note that in 
the file. I am pretty much everywhere online under the username @sortofsleepy.

Most of the work is adapted from [Sascha Willems Vulkan examples repository.](https://github.com/SaschaWillems/Vulkan)
