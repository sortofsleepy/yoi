#version 450

#define PI 3.14149

/// Particle object.
struct Particle {
    vec4 pos;
    vec4 vel;
    vec4 originalVel;
    vec4 meta;
};
layout(push_constant) uniform PushConsts {
    vec4 settings;
} consts;

layout(std140, binding=0) buffer particle {
    Particle particles[];
};

layout(local_size_x = 1024, local_size_y = 1, local_size_z = 1) in;
void main(){

    // Current SSBO index
    uint index = gl_GlobalInvocationID.x;

    float golden_angle = consts.settings.x;
    float spread = consts.settings.y;
    float time = consts.settings.z;

    // Read position and velocity
    Particle p = particles[index];

    /////////////////

    float phi = golden_angle * (PI / 180.0);
    float r = sin(time) * cos(time) + spread * sqrt(index);

    float theta = index * phi;

    float x = r * cos(theta);
    float y = r * sin(theta);

    vec2 target = vec2(x,y);

    float dx = x - p.pos.x;
    float dy = y - p.pos.y;

    p.pos.x += dx  * p.meta.x;
    p.pos.y += dy  * p.meta.x;
    p.pos.z += sin((index + time) * 0.005);


    // check for lifetime
    p.pos.a -= p.meta.x * 0.0005;

    if(particles[index].pos.a < 0){
        p.pos.a = 1.0;
        p.pos = vec4(0.0,0.0,0.0,1.);
    }

    ///////////////////

    // write output
    particles[index] = p;
}

/*

    float phi = golden_angle * (PI / 180.0);
    float r = sin(time) * cos(time) + spread * sqrt(index);

    float theta = index * phi;

    float x = r * cos(theta);
    float y = r * sin(theta);

    vec2 target = vec2(x,y);

    float dx = x - p.pos.x;
    float dy = y - p.pos.y;

    p.pos.x += dx * 0.05 * p.meta.x;
    p.pos.y += dy * 0.06 * p.meta.x;
    p.pos.z += sin((index + time) * 0.3) * 2.0;

    // check for lifetime
    p.pos.a -= p.meta.x * 0.005;

    if(particles[index].pos.a < 0){
        p.pos.a = 1.0;
        p.pos = vec4(0.0,0.0,0.0,1.);
    }
*/