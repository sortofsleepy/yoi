#version 450

layout (set = 0, binding = 1) uniform sampler2D tInput;

layout(location = 0) out vec4 outColor;
layout(location = 1) in float id;

void main(){
    vec2 resolution = vec2(1280.0);
    vec2 uv = gl_FragCoord.xy / resolution;


    if (id == 0.0){
        outColor = vec4(1.0, 0.0, 0.0, 1.0);
    } else {
        outColor = vec4(1.0, 0.0, 1.0, 1.0);
    }

}