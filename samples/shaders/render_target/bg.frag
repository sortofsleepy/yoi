#version 450
layout (set = 0, binding = 0) uniform sampler2D tInput;
layout(location = 0) out vec4 fragColor;
layout(location = 0) in vec2 uv;

#define PI 3.14149
#define DILATION_TYPE vec4
#define DILATION_SAMPLE_FNC(TEX, UV) SAMPLER_FNC(TEX, UV)
//-dilation.glsl


// https://www.shadertoy.com/view/lsKSWR
void main(){

    vec2 res = vec2(1280.0,920.0);
    vec2 pixel = 1.0 / res;
    vec2 st = gl_FragCoord.xy * pixel;
    vec4 d =  dilation(tInput, st, pixel, 32);
    float mix_ratio = step(0.2, st.x);

    float directions = 12.0;// BLUR DIRECTIONS (Default 16.0 - More is better but slower)
    float quality = 9.0;// BLUR QUALITY (Default 4.0 - More is better but slower)
    float size = 0.9;// BLUR SIZE (Radius)

    // slight blur ======

    vec4 color = d;
    float _step = 1.0 /quality;
    float radius = 0.009;
    // Blur calculations
    for (float d= -_step; d < PI; d += _step)
    {
        for (float i=0.3; i<=1.0; i+= (_step))
        {
            color += texture(tInput, uv - vec2(sin(d), cos(d))*radius*i);
        }
    }

    // Output to screen
    color /= quality * 8.0;



    vec4 col = (SAMPLER_FNC(tInput,uv) * color);

    //fragColor = col;
    fragColor = col;
}


