#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#define NUM_LIGHTS 5

//-light.glsl

layout (set = 0, binding = 0) uniform sampler2D albedo;
layout (set = 0, binding = 1) uniform sampler2D uNormalEmissive;
layout (set = 0, binding = 2) uniform sampler2D position;

layout(location = 0) out vec4 glFragColor;

void main(){
    vec2 uWindowSize = vec2(1280.0, 920.0);
    vec2 uv                    = gl_FragCoord.xy / uWindowSize;
    vec4 position            = texture(position, uv);

    // Do not draw background
    if (length(position.xyz) <= 0.0) {
       discard;
    }

    for (int i = 0;i < NUM_LIGHTS; ++i){
        // get the light associated with this position
        Light l = lights.uLights[i];

        vec3 L = l.position.xyz - position.xyz;
        float d = length(L);

        // Only draw fragment if it occurs inside the light volume
        if (d > l.meta.z) {
            discard;
        }
        L /= d;

        // Calculate lighting
        vec4 normalEmissive = texture(uNormalEmissive, uv);
        vec3 N = normalize(normalEmissive.xyz);
        vec3 V = normalize(-position.xyz);
        vec3 H = normalize(L + V);
        float NdotL = max(0.0, dot(N, L));
        float HdotN = max(0.0, dot(H, N));
        float Ks = pow(HdotN, 100.0);
        float att = 1.0 / pow((d / (1.0 - pow(d / l.meta.z, 2.0))) / l.meta.z + 1.0, 2.0);

        // calculate color
        vec4 Ia = l.ambient;
        Ia = vec4(1.0,0.0,0.0,1.0);
        vec4 Id = NdotL * l.diffuse * texture(albedo, uv);
        vec4 Is = Ks * l.specular;
        vec4 Ie = vec4(vec3(normalEmissive.w), 1.0);

        glFragColor = Ia + att * (Id + Is);
        glFragColor *= l.meta.x;

        glFragColor += Ie;
        glFragColor.a = 1.0;

    }


}

/*
  glFragColor = vec4(0.);

    for (int i = 0;i < NUM_LIGHTS; ++i){
        // get the light associated with this position
        Light l = lights.uLights[i];

        vec3 L = l.position.xyz - position.xyz;
        float d = length(L);

        // Only draw fragment if it occurs inside the light volume
        if (d > l.meta.y) {
            discard;
        }
        L /= d;

        // Calculate lighting
        vec4 normalEmissive = texture(uNormalEmissive, uv);
        vec3 N = normalize(normalEmissive.xyz);
        vec3 V = normalize(-position.xyz);
        vec3 H = normalize(L + V);
        float NdotL = max(0.0, dot(N, L));
        float HdotN = max(0.0, dot(H, N));
        float Ks = pow(HdotN, 100.0);
        float att = 1.0 / pow((d / (1.0 - pow(d / l.meta.y, 2.0))) / l.meta.y + 1.0, 2.0);

        // calculate color
        vec4 Ia = l.ambient;
        vec4 Id = NdotL * l.diffuse * texture(albedo, uv);
        vec4 Is = Ks * l.specular;
        vec4 Ie = vec4(vec3(normalEmissive.w), 1.0);

        glFragColor = Ia + att * (Id + Is);
        glFragColor *= l.meta.x;

        glFragColor += vec4(1.);
        glFragColor.a = 1.0;

    }


*/