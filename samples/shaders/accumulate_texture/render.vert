#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
layout(push_constant) uniform PushConsts {
    vec4 settings;
} consts;
layout(binding = 1) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} ubo;

layout(location = 0) in vec4 position;


vec3 rotateX(vec3 p, float theta){
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x, p.y * c - p.z * s, p.z * c + p.y * s);
}
vec3 rotateY(vec3 p, float theta) {
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x * c + p.z * s, p.y, p.z * c - p.x * s);
}

vec3 rotateZ(vec3 p, float theta) {
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x * c - p.y * s, p.y * c + p.x * s, p.z);
}
void main(){
    vec4 p = position;
    p.xyz = rotateX(p.xyz,sin(consts.settings.x * 0.02));
    p.xyz = rotateY(p.xyz,cos(consts.settings.x * 0.02));
    p.xyz = rotateZ(p.xyz,sin(consts.settings.x * 0.02));

    gl_Position = ubo.proj * ubo.view * p;
}