#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} ubo;

layout(push_constant) uniform PushConsts {
    float modelMatrixIndex;
} consts;

layout(binding = 1) buffer UniformBufferObject2 {
    mat4 model[3];
} ubo2;

layout(location = 0) in vec4 position;
layout(location = 2) in vec4 iColor;

layout (location = 0) out vec4 vColor;
layout (location = 1) out vec4 vPos;


// x_axis: Vec4(1.0, 0.0, 0.0, 0.0), y_axis: Vec4(0.0, 1.0, 0.0, 0.0), z_axis: Vec4(0.0, 0.0, 1.0, 0.0), w_axis: Vec4(0.0, 0.0, 0.0, 1.0)
#define PI 3.14149


mat4 rotationMatrix(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat4(oc * axis.x * axis.x + c, oc * axis.x * axis.y - axis.z * s, oc * axis.z * axis.x + axis.y * s, 0.0,
    oc * axis.x * axis.y + axis.z * s, oc * axis.y * axis.y + c, oc * axis.y * axis.z - axis.x * s, 0.0,
    oc * axis.z * axis.x - axis.y * s, oc * axis.y * axis.z + axis.x * s, oc * axis.z * axis.z + c, 0.0,
    0.0, 0.0, 0.0, 1.0);
}
float to_radians(float degrees){
    return degrees * (PI / 180.0);
}


void main(){
    mat4 rot = mat4(
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 1.0, 0.0,
    0.0, 0.0, 0.0, 1.0
    );

    rot = rotationMatrix(vec3(1., 0.0, 0.0), to_radians(45));
    rot = ubo2.model[int(consts.modelMatrixIndex)];

    vec4 pos = position;

    vColor = iColor;
    vPos = pos;
    gl_PointSize = 2.0;
    gl_Position = ubo.proj * ubo.view * rot * pos;
}