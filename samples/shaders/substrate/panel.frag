#version 450

layout(location = 0) out vec4 glFragColor;
layout (location = 0) in vec4 vColor;
layout (location = 1) in vec4 vPos;

void main(){

    glFragColor = vColor * normalize(vPos);
    //glFragColor = vec4(1.);
}