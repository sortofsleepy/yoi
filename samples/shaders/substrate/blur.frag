#version 460
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#define PI 6.28318530718

layout (set = 0, binding = 0) uniform sampler2D tInput;
layout(location = 0) out vec4 glFragColor;
layout(location = 0) in vec2 uv;

void main(){

    float directions = 12.0;// BLUR DIRECTIONS (Default 16.0 - More is better but slower)
    float quality = 9.0;// BLUR QUALITY (Default 4.0 - More is better but slower)
    float size = 0.5;// BLUR SIZE (Radius)

    vec4 color = texture(tInput, uv);
    float step = 1.0 /quality;
    float radius = 0.009;
    // Blur calculations
    for (float d= -step; d < PI; d += step)
    {
        for (float i=0.3; i<=1.0; i+= (step))
        {
            color += texture(tInput, uv - vec2(sin(d), cos(d))*radius*i);
        }
    }

    // Output to screen
    color /= quality * directions + 2.0;

    glFragColor = texture(tInput, uv) + (color + color);
    //glFragColor = texture(tInput, uv);


}