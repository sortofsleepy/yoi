#version 450
// A buffer containing multiples of the golden ratio used for random directions per pixel
// Note that we could just do fract(N * phi) where N is based on the frame number, but once N gets large we hit 
// https://blog.demofox.org/2017/10/31/animating-noise-for-integration-over-time/

//hash from https://www.shadertoy.com/view/4ssXzX

const vec2 iResolution = vec2(1280);

float pi = 3.14159265359;
float bbsm = 1739.;

layout(location = 0) out vec4 fragColor;

vec2 bbsopt(in vec2 a){
    return fract(a*a*(1./bbsm))*bbsm;
}
vec2 mod1024(in vec2 a){
    return fract(a*(1./1024.))*1024.;
}
vec4 hash(in vec2 pos){
    vec2 a0 = mod1024(pos*pi);
    vec2 a1 = bbsopt(a0);
    vec2 a2 = a1.yx + bbsopt(a1);
    vec2 a3 = a2.yx + bbsopt(a2);
    return fract((a2.xyxy + a3.xxyy + a1.xyyx)*(1./bbsm));
}



void main()
{
    vec2 uv = gl_FragCoord.xy/iResolution.xy;
    vec4 init = hash(gl_FragCoord.xy);
    fragColor = init;
    //fragColor = fract(texture(iChannel0,uv) + RAYS_PER_PIXEL/phi);
}