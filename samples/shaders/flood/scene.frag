#version 450

float tau = 6.283185307179586;
float hash(float p) { return fract(sin(p) * 43758.5453123);}

// TODO move to ubo
const vec2 iResolution = vec2(640.,480.);
#define EPSILON 0.5/iResolution.x
float sdSphere( vec2 p, float s )
{
    return length(p)-s;
}

float sdBox( vec2 p, vec2 b )
{
    vec2 d = abs(p) - b;
    return min(max(d.x,d.y),0.0) + length(max(d,0.0));
}

#define lightTime 0.0
#define LIGHT_DIST sdSphere(uv - vec2(sin(lightTime),cos(lightTime)*(sin(lightTime*1.2123341) + 1.)*0.5), 0.1)
#define BOX1 sdBox(uv - vec2(1.2,0), vec2(0.5,0.4))
#define LIGHT_DIST2 sdSphere(uv - vec2(cos(lightTime),sin(lightTime)*(sin(lightTime*1.2123341) + 1.)*0.5), 0.1)

float map(vec2 uv)
{
    float d = sdSphere(uv - vec2(sin(lightTime),cos(lightTime)*(sin(lightTime*1.2123341) + 1.)*0.5), 0.1);
    d = min(d,LIGHT_DIST2);

    d = min(d,sdBox(uv - vec2(1.2,0), vec2(0.05,0.4)));

    return d;
}

bool trace(vec2 origin, vec2 ray)
{
    float t = 0.;
    float dist;
    vec2 samplePoint;
    for(int i = 0; i < 32; i++)
    {
        samplePoint = origin + ray * t;

        vec2 uv = samplePoint;
        dist = map(uv);

        t += dist;

        if(dist < EPSILON)
        {
            return true;
        }
    }
    return false;
}


layout(location = 0) out vec4 glFragColor;
layout(location = 0) in vec2 vUv;
void main() {
    vec2 uv = gl_FragCoord.xy/iResolution.xy;
    //vec2 uv = vUv;
    vec2 originalUv = vUv;
    uv = uv * 2.0 - 1.0;
    float aspect = iResolution.x / iResolution.y;
    float invAspect = iResolution.y / iResolution.x;
    uv.x *= aspect;

    // Time varying pixel color
    vec4 col = vec4(0.0,0.0,0.0,0);

    bool hit = trace(uv,vec2(0.));

    if(hit){
        col += vec4(0.0,0.95,1.0,2.0);
    }


    // Output to screen
    glFragColor = col;
    //glFragColor = vec4(vUv,0.0,1.0);
}
