#version 450
layout (set = 0, binding = 0) uniform sampler2D tInput;
layout(location = 0) in vec2 vUv;
layout(location = 0) out vec4 glFragColor;

// TODO add uniform
const vec2 iResolution = vec2(1280.);
void main() {
    // translate uvs from the square voronoi buffer back to viewport size.
    vec2 uv = vUv;
    vec2 SCREEN_PIXEL_SIZE = vec2(1.0 / 1280.0);
    float u_dist_mod = 1.0;

    if(SCREEN_PIXEL_SIZE.x < SCREEN_PIXEL_SIZE.y)
    uv.y = ((uv.y - 0.5) * (SCREEN_PIXEL_SIZE.x/ SCREEN_PIXEL_SIZE.y)) + 0.5;
    else
    uv.x = ((uv.x - 0.5) * (SCREEN_PIXEL_SIZE.y/ SCREEN_PIXEL_SIZE.x)) + 0.5;

    // input is the voronoi output which stores in each pixel the UVs of the closest surface.
    // here we simply take that value, calculate the distance between the closest surface and this
    // pixel, and return that distance.
    vec4 tex = texture(tInput, uv);
    float dist = distance(tex.xy, uv);
    float mapped = clamp(dist * u_dist_mod, 0.0, 1.0);
    glFragColor = vec4(vec3(mapped), 1.0);
}
