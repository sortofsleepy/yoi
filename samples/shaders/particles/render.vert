#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} ubo;
layout(push_constant) uniform PushConsts {
    vec4 settings;
} consts;

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 iPosition;
layout(location = 2) in vec4 iScale;
layout(location = 3) in vec4 iColor;

layout(location = 0) out float vLife;
layout(location = 1) out vec4 vColor;

#include ../utils/rotate.glsl

void main(){


    vec4 p = position;

    p.xyz *= iScale.xyz;
    p.xyz = rotateX(p.xyz,sin(consts.settings.x * iScale.a));
    p.xyz = rotateY(p.xyz,cos(consts.settings.x * iScale.a));
    p.xyz = rotateZ(p.xyz,sin(consts.settings.x * iScale.a));

    vec3 pos = p.xyz + iPosition.xyz;



    vLife = iPosition.a;
    vColor = iColor;

    gl_Position = ubo.proj * ubo.view * vec4(pos,1.);
}