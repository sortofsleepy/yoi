#version 450

layout (set = 0, binding = 0) uniform sampler2D tInput;
layout (set = 0, binding = 1) uniform sampler2D tScene;
layout (set = 0, binding = 2) uniform sampler2D tNoise;
layout(location = 0) in vec2 vUv;
layout(location = 0) out vec4 glFragColor;

#define PI 3.14149
layout(push_constant) uniform PushConsts {
    vec4 settings;
} consts;

// TODO add uniform

const int u_max_raymarch_steps = 100;
const float u_rays_per_pixel = 100.0;
const float u_dist_mod = 1;
const float u_emission_multi = 1.0;

void get_surface(vec2 uv, out float emissive, out vec3 colour)
{
    vec4 emissive_data = texture(tScene, uv);
    emissive = max(emissive_data.r, max(emissive_data.g, emissive_data.b)) * u_emission_multi;
    colour = emissive_data.rgb;
}

bool raymarch(vec2 origin, vec2 dir, float aspect, out vec2 hit_pos)
{


    float current_dist = 0.0;
    for(int i = 0; i < u_max_raymarch_steps; i++)
    {
        vec2 sample_point = origin + dir * current_dist;
        sample_point.x /= aspect; // when we sample the distance field we need to convert back to uv space.

        // early exit if we hit the edge of the screen.
        if(sample_point.x > 1.0 || sample_point.x < 0.0 || sample_point.y > 1.0 || sample_point.y < 0.0)
        return false;

        float dist_to_surface = texture(tInput, sample_point).r / u_dist_mod;

        // we've hit a surface if distance field returns 0 or close to 0 (due to our distance field using a 16-bit float
        // the precision isn't enough to just check against 0).
        if(dist_to_surface < 0.0001)
        {
            hit_pos = sample_point;
            return true;
        }

        // if we don't hit a surface, continue marching along the ray.
        current_dist += dist_to_surface;
    }
    return false;
}

float random (vec2 st)
{
    return fract(sin(dot(st.xy, vec2(12.9898,78.233))) * 43758.5453123);
}

void main()
{
    const vec2 SCREEN_PIXEL_SIZE = vec2(1.0 / consts.settings.xy);
    float pixel_emis = 0.0;
    vec3 pixel_col = vec3(0.0);

    // convert from uv aspect to world aspect.
    vec2 uv = vUv;
    vec2 UV = vUv;
    float aspect = SCREEN_PIXEL_SIZE.y / SCREEN_PIXEL_SIZE.x;
    uv.x *= aspect;

    float TIME = 1.0;
    //float rand2pi = random(UV * vec2(TIME, -TIME)) * 2.0 * PI;
    float golden_angle = PI * 0.7639320225; // magic number that gives us a good ray distribution.

    vec4 noise = texture(tNoise,vUv);
    float tau = 6.283185307179586;
    float phi = 1.6180339887498948482045868343656381177203091798058;

    for(int i = 0; i < u_rays_per_pixel; i++)
    {

        float rand2pi = fract(noise.r + i * phi) * tau;

        // get our ray dir by taking the random angle and adding golden_angle * ray number.
        float cur_angle = rand2pi;
        vec2 ray_dir = normalize(vec2(cos(cur_angle), sin(cur_angle)));


        vec2 ray_origin = uv;

        vec2 hit_pos;
        bool hit = raymarch(ray_origin, ray_dir, aspect, hit_pos);
        if(hit)
        {
            float mat_emissive = 0.2;
            vec3 mat_colour = vec3(1.);
            get_surface(hit_pos, mat_emissive, mat_colour);

            pixel_emis += mat_emissive;
            pixel_col += mat_colour;
        }
    }

    pixel_col /= pixel_emis;
    pixel_emis /= float(u_rays_per_pixel);

    glFragColor = vec4(pixel_emis * pixel_col, 1.0);

}

/*
// cast our rays.
    for(int i = 0; i < u_rays_per_pixel; i++)
    {
        vec4 noise = texture(tNoise,vUv);
        float rand2pi = fract(noise.r + i * phi) * tau;

        // get our ray dir by taking the random angle and adding golden_angle * ray number.
        float cur_angle = rand2pi;
        vec2 ray_dir = normalize(vec2(cos(cur_angle), sin(cur_angle)));


        vec2 ray_origin = uv;

        vec2 hit_pos;
        bool hit = raymarch(ray_origin, ray_dir, aspect, hit_pos);
        if(hit)
        {
            float mat_emissive = 0.2;
            vec3 mat_colour = vec3(1.);
            //get_surface(hit_pos, mat_emissive, mat_colour);

            pixel_emis += mat_emissive;
            pixel_col += mat_colour;
        }
    }

    pixel_col /= pixel_emis;
    pixel_emis /= float(u_rays_per_pixel);

    glFragColor = vec4(pixel_emis * pixel_col, 1.0);
*/