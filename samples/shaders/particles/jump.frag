#version 450
layout (set = 0, binding = 0) uniform sampler2D tInput;
layout(binding = 1) uniform UniformBufferObject {
    vec4 settings;
} ubo;

layout(location = 0) out vec4 glFragColor;
layout(location = 0) in vec2 vUv;


// TODO move this into uniform
const float u_dist_mod = 1.0;
vec4 load0(vec2 p) {
    vec2 uv = (vec2(p)-0.5) / ubo.settings.yz;
    return texture(tInput, uv);
}

float pi = 3.14159265359;
float bbsm = 1739.;
vec2 bbsopt(in vec2 a){
    return fract(a*a*(1./bbsm))*bbsm;
}
vec2 mod1024(in vec2 a){
    return fract(a*(1./1024.))*1024.;
}
vec4 hash(in vec2 pos){
    vec2 a0 = mod1024(pos*pi);
    vec2 a1 = bbsopt(a0);
    vec2 a2 = a1.yx + bbsopt(a1);
    vec2 a3 = a2.yx + bbsopt(a2);
    return fract((a2.xyxy + a3.xxyy + a1.xyyx)*(1./bbsm));
}
layout(push_constant) uniform PushConsts {
    vec4 settings;
} consts;


void main(){

    float u_offset = ubo.settings.x;
    vec2 SCREEN_PIXEL_SIZE = vec2(consts.settings.xy);
    vec2 UV = vec2(vUv.x, 1.0 - vUv.y);
    UV = vUv;
    float closest_dist = 9999999.9;
    vec2 closest_pos = vec2(0.0);

    // insert jump flooding algorithm here.
    for (float x = -1.0; x <= 1.0; x += 1.0)
    {
        for (float y = -1.0; y <= 1.0; y += 1.0)
        {
            vec2 voffset = UV;
            voffset += vec2(x, y) * (u_offset/ SCREEN_PIXEL_SIZE);
            //voffset += vec2(x, y) * (1.0 / SCREEN_PIXEL_SIZE)* u_offset;

            vec2 pos = texture(tInput, voffset).xy;
            float dist = distance(pos.xy, UV.xy);

            if (pos.x != 0.0 && pos.y != 0.0 && dist < closest_dist)
            {
                closest_dist = dist;
                closest_pos = pos;
            }
        }
    }
    glFragColor = vec4(closest_pos, 0.0, 1.0);

}
