#version 450
layout (input_attachment_index = 0, binding = 1) uniform subpassInput inputPosition;
layout(location = 0) out vec4 glFragColor;

void main(){
	vec3 fragPos = subpassLoad(inputPosition).rgb;
    glFragColor = vec4(fragPos * 0.5, 1.0) + vec4(1.0,0.0,1.0,1.);
}