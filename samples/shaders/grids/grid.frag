#version 450
layout(location = 0) out vec4 glFragColor;
layout(location = 0) in vec4 vColor;
layout(location = 1) in vec3 v_normal;

layout(location = 2) in vec4 vLightPos;
layout(location = 3) in vec4 vLightColor;
layout(location = 4) in vec4 vLightMeta;



void main(){
    vec4 color = normalize(vColor);

    glFragColor = color;

    //glFragColor = vec4(1.0,0.0,0.0,1.0);
}