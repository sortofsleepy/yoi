#version 450

struct Light {
    vec4 pos;
    vec4 ambient_color;
    vec4 diffuse_color;
    vec4 specular_color;
    float falloff;
    float intensity;
    float radius;
    float volume;
};

layout(binding = 1) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} camera;

layout(std140, binding=0) uniform ubo {
    vec4 settings;
};

struct InstanceState {
    mat4 transform;
};

layout(binding=2) buffer trans {
    mat4 transforms[2601];// TODO make this not hard coded.
}mat;

layout(binding=4) uniform lightubo {
    Light lights[5];
};


layout(binding=3) uniform trans2 {
    mat4 trans;
}global;

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;
layout(location = 2) in vec4 iColor;
layout(location = 3) in vec4 iMeta;
layout(location = 4) in vec4 iOffsets;


layout(location = 0) out vec4 vColor;
layout(location = 1) out vec3 v_normal;

// light varyings
layout(location = 2) out vec4 vLightPos;
layout(location = 3) out vec4 vLightColor;
layout(location = 4) out vec4 vLightMeta;


void main(){

    vec4 pos = position;

    // individual transform for each instance.
    mat4 iTransform = mat.transforms[gl_InstanceIndex];

    float trans = sin(settings.x * 10.0 * iOffsets.x * iOffsets.y);
    // scale along z
    iTransform[2][2] += trans;

   // iTransform[3][1] -= trans;


    int lightIndex = int(iMeta.a);
    Light light = lights[lightIndex];


    v_normal = normal.rgb;
    vLightPos = light.pos;
    vLightColor = light.ambient_color;
    vLightMeta = vec4(0.01,0.0002, 0.0, 1.0);
    vColor = iColor;

    gl_Position = camera.proj * camera.view * global.trans * iTransform * pos;

}