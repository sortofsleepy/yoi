use ash::vk;
use ash::vk::{Format, Result, ShaderStageFlags};
use glam::{Mat4, Vec4};
use std::f32::consts::PI;
use vsketch::api::deferred::DeferredRenderer;
use vsketch::objects::light::Light;
use vsketch::objects::sphere_pack::PackSpheres;
use vsketch::shaders::loader::load_shader2;
use vsketch::utils::generate_descriptor_buffer_format_with_size;
use vsketch::*;
use yoi_geo::cube::CubeGeo;
use yoi_geo::icosahedron::IcosahedronGeo;
use yoi_geo::simpleshapes::SimpleShapes;
use yoi_geo::sphere::PrimitiveSphere;
use yoi_math::core::{calculate_normal_matrix, rand_vec4_range};
use yoi_math::matrix::Matrix4;
use yoi_math::{create_vec4, rand_float, rand_vec4};
use yoi_vk::*;

const WIDTH: u32 = 1280;
const HEIGHT: u32 = 920;

const NUM_SPHERE: u32 = 20;

const NUM_LIGHTS: usize = 5;

struct SceneState {
    descriptor: VkDescriptorBuffer,
    mesh: DisplayObject,
}

struct SceneStateDeferred {
    descriptor: VkDescriptorBuffer,
    mesh: Mesh,
}

struct GeometryState {
    descriptor: VkDescriptorBuffer,
    mesh: Mesh,
    light_mesh: Mesh,
}

#[derive(Copy, Clone)]
struct GlobalState {
    normal_matrix: Mat4,
}

struct DataSet {
    buffer: DataBuffer,
    data: Vec<Vec4>,
}

struct LightData {
    descriptor: UniformDescriptor,
    data: Vec<Light>,
}

struct State {
    deferred: DeferredRenderer,
    gbuffer: GeometryState,
    lbuffer: SceneStateDeferred,
    settings_buffer: DataBuffer,
    output: SceneState,
    settings: Vec4,
    geo_settings: DataSet,
}

/// Simple Deferred Rendering sample
/// note - normals are a bit borked so not totally working but the basic idea should be.
/// Adapted from Cinder's simple sample.
fn main() {
    VSketch::new()
        .set_size(WIDTH, HEIGHT)
        .add_setup(setup)
        .add_draw(update_env)
        .add_draw(update_geo)
        .add_draw(draw)
        .run();
}

fn setup(vk: &mut Vulkan, comp: &mut VkRenderCore) -> State {
    let camera = build_camera(vk, 0, -40.0, 60.0, 1280.0 / 920.0, 0.1, 10000.0);

    let settings = create_vec4();

    let mut settings_buffer = DataBuffer::create(vk, generate_descriptor_buffer_format::<Vec4>());
    settings_buffer.set_object_data(vk, settings);

    let lights = build_lights(vk, 5, 4);
    let global = build_global_uniforms(vk, &camera.camera.get_view(), 1);

    let mut deferred = DeferredRenderer::new(vk, WIDTH, HEIGHT);

    let geo_positions = build_geo_positions(vk);

    let geometry_stage = build_geometry_stage(
        vk,
        comp,
        &camera.descriptor,
        &mut deferred,
        &global,
        &geo_positions.buffer,
        &lights.data,
    );

    let lighting_stage = build_lighting_stage(
        vk,
        comp,
        &mut deferred,
        &lights.descriptor,
        &geo_positions.buffer,
    );

    let output = build_output_debug(vk, comp, &deferred);

    State {
        deferred,
        lbuffer: lighting_stage,
        gbuffer: geometry_stage,
        output,
        settings_buffer,
        settings,
        geo_settings: geo_positions,
    }
}

fn draw(vk: &mut Vulkan, comp: &mut VkRenderCore, model: &mut State) {
    let renderer = &mut comp.render;
    let meta = &mut comp.meta;

    let deferred = &mut model.deferred;

    // setup gbuffer variables
    let mesh = &mut model.gbuffer.mesh;
    let light_mesh = &mut model.gbuffer.light_mesh;
    let mesh_desc = &mut model.gbuffer.descriptor;

    // setup lbuffer variables
    let lmesh = &mut model.lbuffer.mesh;
    let lmesh_desc = &mut model.lbuffer.descriptor;

    // setup output variables
    let output = &mut model.output.mesh;
    let output_desc = &mut model.output.descriptor;

    renderer.record_commands_with_renderpass(vk, meta, |cb| {
        // render into gbuffer first
        {
            deferred.begin_gbuffer(vk, cb);
            mesh_desc.bind_descriptors(vk, cb, &deferred.get_gbuffer_pipeline_layout());
            mesh.draw(vk, *cb);
            light_mesh.draw(vk, *cb);
            deferred.end_gbuffer(vk, cb);
        }

        // render into lbuffer next
        {
            deferred.begin_lbuffer(vk, cb);
            lmesh_desc.bind_descriptors(vk, cb, &deferred.get_lbuffer_pipeline_layout());
            lmesh.draw(vk, *cb);
            deferred.end_lbuffer(vk, cb);
        }
    });

    // output everything to screen.
    renderer.record_commands(vk, meta, |cb| {
        output.render(vk, cb, Some(output_desc));
    });

    /////////////////////////////////////////////////////////////////////////////////////////
    let res = renderer.present(&vk, comp.meta.get_swapchain());

    // handle out of date swapchain
    if res == Result::ERROR_OUT_OF_DATE_KHR {
        let window_size = comp.window.inner_size();
        comp.meta
            .reset(vk, &comp.surface, [window_size.width, window_size.height]);
    }

    renderer.finish_present();
}

/// Update global variables like delta time.
fn update_env(vk: &mut Vulkan, comp: &mut VkRenderCore, model: &mut State) {
    let mut env = &mut comp.app_env;
    env.update();

    ////// UPDATE APP ENV ///////////////
    let mut settings = model.settings;
    settings.x = env.get_delta_time();
    let mut env_buff = &mut model.settings_buffer;
    env_buff.set_object_data(vk, settings);
}

/// Update geometry position data (rest is updated in-shader)
fn update_geo(vk: &mut Vulkan, comp: &mut VkRenderCore, model: &mut State) {
    let positions = &mut model.geo_settings.data;
    let mut buffer = &mut model.geo_settings.buffer;

    for data in &mut *positions {
        data.x += data.z;
        data.y += data.w;
    }

    buffer.set_data(vk, &positions);
}

fn build_post(vk: &mut Vulkan) {
    let mut fbo = RenderTarget::new(WIDTH, HEIGHT);
    fbo.compile(vk);
}

/// Builds lighting information
fn build_lights(vk: &Vulkan, num_lights: usize, binding: u32) -> LightData {
    let mut lights = vec![];

    for i in 0..num_lights {
        let mut l = Light::new()
            .ambient_color4(vec4(0.95, 1.0, 0.92, 1.0))
            .specular_color(1.0, 0.0, 1.0, 1.0)
            .diffuse_color(0.95, 1.0, 0.92, 1.0)
            .volume(400.0)
            .intensity(0.5);
        lights.push(l);
    }

    let mut fmt = generate_descriptor_buffer_format_with_size::<Light>(num_lights);

    let mut lbuffer = DataBuffer::create(vk, fmt);
    lbuffer.set_data(vk, &lights);

    let mut l_desc = UniformDescriptor::new();
    l_desc.set_buffer_data_with_address(vk, lbuffer.raw(), 0, fmt.size);
    l_desc.set_shader_stage(vk::ShaderStageFlags::FRAGMENT | vk::ShaderStageFlags::VERTEX);
    l_desc.set_shader_binding(binding);

    LightData {
        descriptor: l_desc,
        data: lights,
    }
}

/// Build necessities for the lighting stage.
fn build_lighting_stage(
    ctx: &Vulkan,
    comp: &VkRenderCore,
    dr: &mut DeferredRenderer,
    light_desc: &UniformDescriptor,
    pos: &DataBuffer,
) -> SceneStateDeferred {
    // setup a new camera cause we gotta bind the descriptor at a separate point.
    let camera = build_camera(ctx, 3, -40.0, 60.0, 1280.0 / 920.0, 0.1, 10000.0);

    // setup layers from gbuffer as descriptors since they'll need to be read during the lighting stage.
    let mut output_descriptors = VkDescriptorBuffer::new();
    output_descriptors.add_uniform_buffer_descriptor(camera.descriptor);
    output_descriptors.add_uniform_buffer_descriptor(*light_desc);
    dr.generate_lbuffer_descriptors(&mut output_descriptors);

    output_descriptors.build(ctx);

    ////// BUILD MESH TO REPRESENT LIGHTS ////////
    let geo = SimpleShapes::generate_cube();
    let size = 2.0;
    let geo = CubeGeo::new(size, size, size, 10.0, 10.0, 10.0);

    let mesh = Mesh::new()
        .add_attribute(ctx, 0, geo.positions)
        .add_attribute(ctx, 1, geo.normals)
        .add_instanced_attrib_buffer(2, pos, size_of::<Vec4>() as u32)
        .set_num_instances(NUM_LIGHTS as u32)
        .add_index_data(ctx, geo.indices);

    dr.add_mesh_to_lbuffer(&mesh);
    dr.set_lbuffer_shader(
        ctx,
        load_shader2("shaders/deferred_simple/lbuffer.vert").as_str(),
        load_shader2("shaders/deferred_simple/lbuffer.frag").as_str(),
    );

    dr.build_lighting_stage(
        ctx,
        &comp.meta,
        Some(output_descriptors.get_descriptor_layout()),
    );

    SceneStateDeferred {
        descriptor: output_descriptors,
        mesh,
    }
}

/// Builds global uniforms + descriptors for any values like the normal matrix.
fn build_global_uniforms(vk: &Vulkan, camera_view: &Mat4, binding: u32) -> UniformDescriptor {
    // TODO this isn't terribly accurate - need actual model matrix but this will do for now.
    let normal_matrix = calculate_normal_matrix(*camera_view, Mat4::IDENTITY);

    let mut fmt = generate_descriptor_buffer_format::<GlobalState>();
    fmt.size = get_byte_size::<GlobalState>();

    let mut global_buffer = DataBuffer::create(vk, fmt);
    global_buffer.set_object_data(vk, GlobalState { normal_matrix });

    let mut global_desc = UniformDescriptor::new();
    global_desc.set_buffer_data_with_address(vk, global_buffer.raw(), 0, fmt.size);
    global_desc.set_shader_binding(binding);

    global_desc
}

/// Build data used to update geometry
fn build_geo_positions(vk: &Vulkan) -> DataSet {
    let mut meta = vec![];

    for i in 0..NUM_SPHERE {
        let phi = (rand_float(0.0, 1.0) * 2.0) * PI;
        let theta = (rand_float(0.0, 1.0) * 2.0) * PI;

        let phi_speed = rand_float(-0.01, 0.01);
        let theta_speed = rand_float(-0.01, 0.01);

        meta.push(vec4(phi, theta, phi_speed, theta_speed));
    }

    let fmt = generate_descriptor_buffer_format_with_size::<Vec4>(NUM_SPHERE as usize);

    let mut mbuffer = DataBuffer::create(vk, fmt);
    mbuffer.set_data(vk, &meta);

    DataSet {
        buffer: mbuffer,
        data: meta,
    }
}

/// Builds geometry stage for rendering things.
fn build_geometry_stage(
    ctx: &Vulkan,
    comp: &VkRenderCore,
    camera: &UniformDescriptor,
    dr: &mut DeferredRenderer,
    global_desc: &UniformDescriptor,
    meta: &DataBuffer,
    light_info: &Vec<Light>,
) -> GeometryState {
    ////////////// BUILD MAIN SCENE OBJECTS ///////////////////
    let size = 2.0;
    let geo = CubeGeo::new(size, size, size, 10.0, 10.0, 10.0);

    let mut lightInfo = vec![];
    for i in 0..NUM_SPHERE {
        // y = radius for cubes to move through.
        lightInfo.push(vec4(0.0, 30.0, 4.0, 0.0));
    }

    let mesh = Mesh::new()
        .add_attribute(ctx, 0, geo.positions)
        .add_attribute(ctx, 1, geo.normals)
        .add_instanced_attrib_buffer(2, meta, size_of::<Vec4>() as u32)
        .add_instanced_attribute(ctx, 3, lightInfo)
        .set_num_instances(NUM_SPHERE)
        .add_index_data(ctx, geo.indices);

    dr.add_mesh_to_gbuffer(&mesh);

    ////////////// BUILD LIGHT REPRESENTATIONS ///////////////////
    let size = 2.0;
    let geo2 = CubeGeo::new(size, size, size, 10.0, 10.0, 10.0);
    let geo2 = IcosahedronGeo::create();
    let mut lightInfo2 = vec![];
    for i in 0..NUM_LIGHTS {
        // x = emissive
        // y = spherical radius
        // z = scale of representation
        lightInfo2.push(vec4(1.0, 50.0, light_info[i].get_radius(), 1.0));
    }

    let mesh2 = Mesh::new()
        .add_attribute(ctx, 0, geo2.positions)
        .add_attribute(ctx, 1, geo2.normals)
        .add_instanced_attrib_buffer(2, meta, size_of::<Vec4>() as u32)
        .add_instanced_attribute(ctx, 3, lightInfo2)
        .set_num_instances(NUM_LIGHTS as u32)
        .add_index_data(ctx, geo2.indices);

    dr.add_mesh_to_gbuffer(&mesh2);
    dr.set_gbuffer_shader(
        ctx,
        "shaders/deferred_simple/gbuffer.vert",
        "shaders/deferred_simple/gbuffer.frag",
    );

    let mut output_descriptors = VkDescriptorBuffer::new();
    output_descriptors.add_uniform_buffer_descriptor(*camera);
    output_descriptors.add_uniform_buffer_descriptor(*global_desc);
    output_descriptors.build(ctx);

    dr.build_geometry_stage(
        ctx,
        &comp.meta,
        Some(output_descriptors.get_descriptor_layout()),
    );

    GeometryState {
        descriptor: output_descriptors,
        mesh,
        light_mesh: mesh2,
    }
}

/// Build necessary components to render output from render targets.
fn build_output_debug(
    ctx: &Vulkan,
    comp: &VkRenderCore,
    output_rt: &DeferredRenderer,
) -> SceneState {
    let mut sampler = VkSampler::new();
    sampler.build(ctx);

    let mut tex = TextureSamplerDescriptor::new();
    tex.set_image(*output_rt.get_lbuffer_output(), *sampler.raw());
    //tex.set_image(*output_rt.get_gbuffer_output(0), *sampler.raw());
    tex.set_shader_binding(0);
    tex.set_shader_stage(ShaderStageFlags::FRAGMENT);

    let mut output_descriptors = VkDescriptorBuffer::new();
    output_descriptors.add_texture_descriptor(tex);
    output_descriptors.build(ctx);

    // build a fullscreen triangle for the background
    let geo = SimpleShapes::generate_fullscreen_triangle();

    let mut mesh = Mesh::new();
    mesh = mesh
        .add_attribute(ctx, 0, geo.positions)
        .set_num_vertices(3);

    let shader = RenderShader::new(PASSTHRU_VERTEX, PASSTHRU_FRAGMENT, ctx);

    // build a pipeline to render it
    let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
    pipeline.enable_depth_test();
    pipeline.add_descriptor_set_layout(output_descriptors.get_descriptor_layout());
    pipeline.compile_dynamic(
        ctx,
        &comp.meta,
        &shader,
        Format::B8G8R8A8_UNORM,
        Format::D32_SFLOAT,
    );

    SceneState {
        descriptor: output_descriptors,
        mesh: DisplayObject::new(vec![mesh], pipeline),
    }
}

fn build_geo(ctx: &Vulkan, camera: &UniformDescriptor, app: &VkApp) -> SceneState {
    let geo = PrimitiveSphere::create(1.0, 20);

    let mut a = PackSpheres::new();
    a.build();
    let spheres = a.get_shapes();

    let mut positions = vec![];
    let mut meta = vec![];
    for sphere in &spheres {
        let pos = sphere.get_position();
        positions.push(vec4(pos[0], pos[1], pos[2], 1.0));

        meta.push(vec4(sphere.get_radius(), 0.0, 0.0, 0.0));
    }

    /////////////////// BUILD GEOMETRY ////////////////////

    let num_instances = spheres.len();

    let m = Mesh::new()
        .add_attribute(ctx, 0, geo.positions)
        .add_attribute(ctx, 1, geo.normals)
        .add_index_data(ctx, geo.indices)
        .add_instanced_attribute(ctx, 2, positions)
        .add_instanced_attribute(ctx, 3, meta)
        .set_num_instances(num_instances as u32);

    let mut shader = RenderShader::new(
        "shaders/deferred_simple/sphere.vert",
        "shaders/deferred_simple/sphere.frag",
        ctx,
    );

    let mut ds = VkDescriptorBuffer::new();
    ds.add_uniform_buffer_descriptor(*camera);
    ds.build(ctx);

    let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
    pipeline.add_mesh(&m);
    pipeline.enable_depth_test();
    pipeline.add_descriptor_set_layout(ds.get_descriptor_layout());
    pipeline.compile(ctx, app, &shader);

    SceneState {
        descriptor: ds,
        mesh: DisplayObject::new(vec![m], pipeline),
    }
}
