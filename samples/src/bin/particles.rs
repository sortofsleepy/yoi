use ash::vk::*;
use glam::{vec4, Vec4};
use vsketch::utils::generate_push_constant_range;
use vsketch::{
    build_camera, generate_descriptor_buffer_format, get_byte_size, get_byte_size_of, to_rgb,
    DisplayObject, SceneState, VSketch, VkRenderCore, PASSTHRU_VERTEX, WARM2,
};
use yoi_geo::cube::CubeGeo;
use yoi_geo::simpleshapes::SimpleShapes;
use yoi_math::core::normalize_rgba_color;
use yoi_math::{create_vec4, rand_float, rand_int, rand_value, rand_vec4};
use yoi_vk::*;

struct State {
    particle_geo: SceneState,
    output: SceneState,
    targets: Vec<RenderTarget2>,
    particle_compute: ParticleData,
    graphics_semaphore: Semaphore,
    time: f32,
    stages: Vec<SceneState>,
    jump: JumpSteps,
}

const WIDTH: u32 = 900;
const HEIGHT: u32 = 900;

const WIN_WIDTH: u32 = 1280;
const WIN_HEIGHT: u32 = 1280;

// Looks good but....
//const TEXTURE_FMT: Format = Format::B8G8R8A8_UNORM;

// this format makes things a bit more interesting.
const TEXTURE_FMT: Format = Format::R8G8B8A8_SNORM;

const NUM_PARTICLES: u32 = 100;

struct ParticleData {
    pipeline: ComputePipeline,
    descriptor: VkDescriptorBuffer,
    particle_buffer: DataBuffer,
}

const SCENE: usize = 0;
const ENCODE: usize = 1;

const DISTANCE: usize = 2;

const GI: usize = 3;

#[derive(Copy, Clone)]
struct Particle {
    pos: Vec4,
    vel: Vec4,
    original_pos: Vec4,
    meta: Vec4,
}

/// A small port of Sam Bigos's Godot implementation of 2D Global illumination... which in part is also based on this
/// https://samuelbigos.github.io/posts/2dgi1-2d-global-illumination-in-godot.html
/// Also referenced, this shadertoy by @toocanzs
/// https://www.shadertoy.com/view/lltcRN
fn main() {
    VSketch::new()
        .set_size(WIN_WIDTH, WIN_HEIGHT)
        .add_setup(setup)
        .add_draw(draw)
        .run();
}

fn setup(vk: &mut Vulkan, comp: &mut VkRenderCore) -> State {
    let targets = build_render_targets(vk);

    let noise = build_noise_buffer(vk, &comp.meta);

    // generate noise
    build_noise(vk, &noise, comp);

    let graphics_semaphore = vk.generate_semaphore(SemaphoreCreateInfo {
        ..Default::default()
    });

    let camera = build_camera(
        vk,
        0,
        -60.0,
        60.0,
        (WIDTH as f32) / (HEIGHT as f32),
        0.1,
        1000.0,
    );

    let particle_compute = build_particle_data(vk, comp.render.get_command_buffer(0), 0);
    let particle_geo = build_particle_geo(
        vk,
        &comp.meta,
        &particle_compute.particle_buffer,
        &targets[0],
        camera.descriptor,
    );

    ///////

    // build the encode stage
    let encode = build_stage(
        vk,
        &comp.meta,
        &targets[ENCODE],
        "shaders/particles/encode.frag",
        vec![&targets[SCENE].get_color_view()],
    );

    let jump = build_jump(
        vk,
        &comp.meta,
        WIDTH,
        HEIGHT,
        TEXTURE_FMT,
        "shaders/particles/jump.frag",
        comp.render.get_command_buffer(0),
        &targets[ENCODE].get_attachment(),
    );
    let last = jump.targets.last().unwrap();

    let distance = build_stage(
        vk,
        &comp.meta,
        &targets[DISTANCE],
        "shaders/particles/distance.frag",
        vec![last.get_color_view()],
    );

    let gi = build_stage(
        vk,
        &comp.meta,
        &targets[GI],
        "shaders/particles/gi.frag",
        vec![
            &targets[DISTANCE].get_color_view(),
            &targets[SCENE].get_color_view(),
            &noise.rt.get_color_view_at(0),
        ],
    );
    ///////////
    let output = build_output(vk, comp, targets[GI].get_color_view());
    //let output = build_output(vk, comp, targets[SCENE].get_color_view());

    State {
        particle_geo,
        output,
        targets,
        particle_compute,
        graphics_semaphore,
        stages: vec![encode, distance, gi],
        jump,
        time: 0.0,
    }
}

fn draw(vk: &mut Vulkan, comp: &mut VkRenderCore, model: &mut State) {
    let renderer = &mut comp.render;
    let meta = &mut comp.meta;
    let targets = &model.targets;

    let mut env = &mut comp.app_env;
    env.update();
    let time = env.get_delta_time();

    // scene to render
    let particles = &model.particle_geo;

    // encode positions
    let encode = &model.stages[0];

    let jump = &model.jump;
    let distance = &model.stages[1];
    let gi = &model.stages[2];

    let output = &model.output;

    let compute = &model.particle_compute;
    let compute_desc = &compute.descriptor;
    let graphics_semaphore = &model.graphics_semaphore;

    let screen_res: Vec<Vec<Vec4>> = vec![
        vec![Vec4::new(WIDTH as f32, HEIGHT as f32, 1.0, 1.0)], //vec![Vec4::new(1280.0, 1280.0, 1.0, 1.0)]
    ];

    renderer.record_commands_with_renderpass(vk, meta, |cb| {
        // render the scene.
        {
            targets[SCENE].begin(vk, cb);
            targets[SCENE].clear_attachments(vk, cb, [0.0, 0.0, 0.0, 0.0]);
            let constants: Vec<Vec<Vec4>> = vec![vec![Vec4::new(time, 1.0, 1.0, 1.0)]];
            particles.render_with_push_constants(vk, cb, constants, ShaderStageFlags::VERTEX);
            targets[SCENE].end(vk, cb);
        }

        // build position encoding
        {
            targets[ENCODE].begin(vk, cb);
            encode.render_with_push_constants(
                vk,
                cb,
                screen_res.clone(),
                ShaderStageFlags::FRAGMENT,
            );
            targets[ENCODE].end(vk, cb);
        }

        // build jump flooding.
        {
            for i in 0..jump.targets.len() {
                jump.targets[i].begin(vk, cb);
                jump.steps[i].render_with_push_constants(
                    vk,
                    cb,
                    screen_res.clone(),
                    ShaderStageFlags::FRAGMENT,
                );
                jump.targets[i].end(vk, cb);
            }
        }

        {
            targets[DISTANCE].begin(vk, cb);
            distance.render_with_push_constants(
                vk,
                cb,
                screen_res.clone(),
                ShaderStageFlags::FRAGMENT,
            );
            targets[DISTANCE].end(vk, cb);
        }

        // encode the positions
        {
            targets[GI].begin(vk, cb);
            gi.render_with_push_constants(vk, cb, screen_res.clone(), ShaderStageFlags::FRAGMENT);
            targets[GI].end(vk, cb);
        }
    });

    renderer.record_commands(vk, meta, |cb| {
        output.render(vk, cb);
    });

    // Run compute
    {
        let constants: Vec<Vec4> = vec![
            //Vec4::new(time, time * 0.005, 1.0, 1.0)
            Vec4::new(time, time, 5.0, 1.0),
        ];
        compute
            .pipeline
            .run_with_push_constants(vk, compute_desc, constants);
    }

    vsketch::utils::present_to_screen_with_compute(vk, comp, &compute.pipeline, graphics_semaphore);
    model.time += 0.1;
}

pub fn present_to_screen_with_compute(
    vk: &Vulkan,
    comp: &mut VkRenderCore,
    pipeline: &ComputePipeline,
    graphics_semaphore: &Semaphore,
) {
    let renderer = &mut comp.render;

    let res = renderer.present_with_compute(
        vk,
        comp.meta.get_swapchain(),
        pipeline.get_semaphore(),
        graphics_semaphore,
    );

    pipeline.submit_compute(vk, &graphics_semaphore);
}

/// Builds render targets needed for things
fn build_render_targets(vk: &Vulkan) -> Vec<RenderTarget2> {
    let mut targets = vec![];
    for i in 0..4 {
        let mut rt = RenderTarget2::new(WIDTH, HEIGHT);
        rt.add_default_attachment(vk, Some(TEXTURE_FMT));
        rt.compile(vk);

        targets.push(rt);
    }

    targets
}

/// Build necessary components to render output from render targets.
fn build_output(ctx: &Vulkan, comp: &VkRenderCore, output_rt: &ImageView) -> SceneState {
    let mut sampler = VkSampler::new();
    sampler.build(ctx);

    let mut tex = TextureSamplerDescriptor::new();
    tex.set_image(*output_rt, *sampler.raw());
    tex.set_shader_binding(0);
    tex.set_shader_stage(ShaderStageFlags::FRAGMENT);

    let mut output_descriptors = VkDescriptorBuffer::new();
    output_descriptors.add_texture_descriptor(tex);
    output_descriptors.build(ctx);

    // build a fullscreen triangle for the background
    let geo = SimpleShapes::generate_fullscreen_triangle();

    let mut mesh = Mesh::new();
    mesh = mesh
        .add_attribute(ctx, 0, geo.positions)
        .set_num_vertices(3);

    let shader = RenderShader::new(PASSTHRU_VERTEX, "shaders/particles/output.frag", ctx);

    // build a pipeline to render it
    let mut pipeline = RenderPipeline::new();
    pipeline.add_descriptor_set_layout(output_descriptors.get_descriptor_layout());
    pipeline.compile_dynamic(
        ctx,
        &comp.meta,
        &shader,
        Format::B8G8R8A8_UNORM,
        Format::D32_SFLOAT,
    );

    SceneState {
        descriptor: Some(output_descriptors),
        scene: DisplayObject::new(vec![mesh], pipeline),
    }
}

/// Build the geometry for the particles.
fn build_particle_geo(
    vk: &Vulkan,
    app: &VkApp,
    instanced_positions: &DataBuffer,
    rt: &RenderTarget2,
    descriptor: UniformDescriptor,
) -> SceneState {
    let geo = CubeGeo::new(2.0, 2.0, 2.0, 2.0, 2.0, 2.0);

    let mut particle_color = vec![];
    let colors = to_rgb(WARM2.to_vec());
    let mut scale = vec![];

    for i in 0..NUM_PARTICLES {
        let mut scale_val = rand_float(1.0, 3.0);

        // every 60 particles is an occluder; everything else is an emitter.
        if i % 4 == 0 {
            scale_val = 3.0;
            particle_color.push(vec4(0.0, 0.0, 0.0, 1.0));
        } else {
            let color = normalize_rgba_color(colors.get_at(rand_value()));
            particle_color.push(vec4(color.x, color.y, color.z, 1.0));
        }

        // alpha will hold an offset to tweak the spin
        scale.push(vec4(scale_val, scale_val, scale_val, rand_float(0.0, 1.0)));
    }

    let mesh = Mesh::new()
        .add_attribute(vk, 0, geo.positions)
        .add_index_data(vk, geo.indices)
        .add_instanced_attrib_buffer(1, instanced_positions, get_byte_size::<Particle>() as u32)
        .add_instanced_attribute(vk, 2, scale)
        .add_instanced_attribute(vk, 3, particle_color)
        .set_num_instances(NUM_PARTICLES);

    let shader = RenderShader::new(
        "shaders/particles/render.vert",
        "shaders/particles/render.frag",
        vk,
    );

    let mut ds = VkDescriptorBuffer::new();
    ds.add_uniform_buffer_descriptor(descriptor);
    ds.build(vk);

    let range = generate_push_constant_range::<Vec4>(ShaderStageFlags::VERTEX, 0);

    let mut pipe = RenderPipeline::new();
    pipe.add_mesh(&mesh);
    pipe.add_push_constant(range);
    pipe.enable_alpha_blending();
    pipe.add_descriptor_set_layout(ds.get_descriptor_layout());
    pipe.compile_with_renderpass(vk, app, &shader, rt.get_renderpass().raw());

    SceneState {
        descriptor: Some(ds),
        scene: DisplayObject::new(vec![mesh], pipe),
    }
}

/// Build data for particles to update in a compute shader.
fn build_particle_data(vk: &Vulkan, cb: &CommandBuffer, binding: u32) -> ParticleData {
    let mut descriptor = VkDescriptorBuffer::new();
    let mut particles = vec![];

    for i in 0..NUM_PARTICLES {
        let mut pos = rand_vec4();

        // alpha will be used for lifetime
        pos.w = 1.0;

        particles.push(Particle {
            pos,
            vel: create_vec4(),
            original_pos: pos,
            // x = life decay
            meta: Vec4::new(rand_float(0.09, 0.2), rand_value() * 0.04, 0.0, 1.),
        })
    }

    let size = get_byte_size_of::<Particle>(particles.len());

    // setup staging buffer
    let mut stage_fmt = BufferFormat::default();
    stage_fmt.usage_flags = BufferUsageFlags::TRANSFER_SRC;
    stage_fmt.size = size;

    let mut stage_buffer = DataBuffer::create(vk, stage_fmt);
    stage_buffer.set_data(vk, &particles);

    // setup buffer to transfer into
    let mut particle_fmt = generate_descriptor_buffer_format::<Particle>();
    particle_fmt.size = size;
    particle_fmt.usage_flags = BufferUsageFlags::TRANSFER_DST
        | BufferUsageFlags::STORAGE_BUFFER
        | BufferUsageFlags::VERTEX_BUFFER
        | BufferUsageFlags::SHADER_DEVICE_ADDRESS;
    particle_fmt.mem_alloc_flags = MemoryAllocateFlags::DEVICE_ADDRESS;

    let particle_buffer = DataBuffer::create(vk, particle_fmt);

    copy_buffer_to_buffer(vk, &cb, stage_buffer.raw(), particle_buffer.raw(), size);

    let mut storage = StorageDescriptor::new();
    storage.set_shader_stage(ShaderStageFlags::COMPUTE);
    storage.set_buffer_data_with_address(vk, particle_buffer.raw(), 0, stage_fmt.size);
    storage.set_shader_binding(binding);

    descriptor.add_storage_descriptor(storage);

    descriptor.build(vk);

    ////// SETUP PUSH CONSTANTS ////

    let range = generate_push_constant_range::<Vec4>(ShaderStageFlags::COMPUTE, 0);

    let shader = ComputeShader::new("shaders/particles/compute.glsl", vk);

    //// BUILD PIPELINE ////
    let mut pipeline = ComputePipeline::new_with_descriptor_buffer(vk);
    pipeline.add_push_constant(range);
    pipeline.set_dispatch_size([NUM_PARTICLES, 1, 1]);
    pipeline.compile(
        vk,
        shader.generate_pipeline_info(),
        vec![descriptor.get_descriptor_layout()],
    );

    ParticleData {
        pipeline,
        descriptor,
        particle_buffer,
    }
}

/////////// JUMP FLOOD STUFF ///////////////////

/// Builds the scene.
fn build_stage(
    vk: &Vulkan,
    app: &VkApp,
    rt: &RenderTarget2,
    shader: &str,
    inputs: Vec<&ImageView>,
) -> SceneState {
    let shader = RenderShader::new(PASSTHRU_VERTEX, shader, vk);

    let mesh = Mesh::new().set_num_vertices(3);
    let mut pipe = RenderPipeline::new();
    let mut ds = VkDescriptorBuffer::new();
    let mut descriptor: Option<VkDescriptorBuffer> = None;

    if !inputs.is_empty() {
        let mut sampler = VkSampler::new();
        sampler.build(vk);

        for i in 0..inputs.len() {
            let mut tex_desc = TextureSamplerDescriptor::new();
            tex_desc.set_image(*inputs[i], *sampler.raw());
            tex_desc.set_shader_binding(i as u32);

            ds.add_texture_descriptor(tex_desc);
        }

        ds.build(vk);
        pipe.add_descriptor_set_layout(ds.get_descriptor_layout());

        descriptor = Some(ds);
    }

    let range = generate_push_constant_range::<Vec4>(ShaderStageFlags::FRAGMENT, 0);

    pipe.add_mesh(&mesh);
    pipe.add_push_constant(range);
    pipe.compile2(vk, app, &shader, Some(rt.get_renderpass().raw()));

    SceneState {
        scene: DisplayObject::new(vec![mesh], pipe),
        descriptor,
    }
}

struct NoiseState {
    rt: RenderTarget2,
    obj: DisplayObject,
}

fn build_noise_buffer(vk: &Vulkan, app: &VkApp) -> NoiseState {
    let mut rt = RenderTarget2::new(WIDTH, HEIGHT);
    rt.add_default_attachment(vk, Some(TEXTURE_FMT));
    rt.compile(vk);

    let shader = RenderShader::new(PASSTHRU_VERTEX, "shaders/particles/noise.frag", vk);

    let mesh = Mesh::new().set_num_vertices(3);
    let mut pipeline = RenderPipeline::new();
    pipeline.add_mesh(&mesh);
    pipeline.compile2(vk, app, &shader, Some(rt.get_renderpass().raw()));

    NoiseState {
        rt,
        obj: DisplayObject::new(vec![mesh], pipeline),
    }
}

/// builds a static noise texture.
fn build_noise(vk: &Vulkan, noise: &NoiseState, core: &mut VkRenderCore) {
    // get command buffer to use for building the noise texture
    let cb = core.render.bind_command_buffer(0, vk);
    noise.rt.begin(vk, cb);
    noise.obj.render(vk, cb, None);
    noise.rt.end(vk, cb);
    core.render.unbind_command_buffer(vk, cb);

    vk.submit_command_buffer(cb);
}

struct JumpSteps {
    targets: Vec<RenderTarget2>,
    steps: Vec<SceneState>,
}

fn build_jump(
    vk: &Vulkan,
    app: &VkApp,
    width: u32,
    height: u32,
    format: Format,
    shader: &str,
    cb: &CommandBuffer,
    input: &VkTexture,
) -> JumpSteps {
    let mut sampler = VkSampler::new();
    sampler.build(vk);

    let largest = width.max(height);

    let passes = (f32::ceil(f32::log2(largest as f32)) as u32);

    // build targets
    let mut targets = vec![];
    for i in 0..passes {
        let mut rt = RenderTarget2::new(width, height);
        rt.add_default_attachment(vk, Some(TEXTURE_FMT));
        rt.compile(vk);
        targets.push(rt);
    }

    let shader = RenderShader::new(PASSTHRU_VERTEX, shader, vk);

    let mut steps = vec![];
    for i in 0..passes {
        let mut offset = (passes - i - 1);
        let foffset = 2_f32.powf(offset as f32);

        let mut settings = create_vec4();

        // x = offset
        // y + z = Screen size
        settings.x = foffset;
        settings.y = WIDTH as f32;
        settings.z = HEIGHT as f32;

        let mut settings_buffer =
            DataBuffer::create(vk, generate_descriptor_buffer_format::<Vec4>());
        settings_buffer.set_object_data(vk, settings);

        // setup core objects
        let mut ds = VkDescriptorBuffer::new();
        let mesh = Mesh::new().set_num_vertices(3);

        // build pipeline
        let mut pipe = RenderPipeline::new();
        pipe.add_mesh(&mesh);

        // build texture descriptor
        let mut tex = TextureSamplerDescriptor::new();

        // if i = 0, input is seed, otherwise, input is previous pass output
        if i == 0 {
            tex.set_image(*input.get_image_view(), *sampler.raw());
        } else {
            let idx = (i as usize) - 1usize;
            tex.set_image(*targets[idx].get_color_view(), *sampler.raw());
        }

        ds.add_texture_descriptor(tex);

        // build settings
        let mut settings_desc = UniformDescriptor::new();
        settings_desc.set_shader_binding(1);
        settings_desc.set_shader_stage(ShaderStageFlags::FRAGMENT);
        settings_desc.set_buffer_data_with_address(
            vk,
            settings_buffer.raw(),
            0,
            get_byte_size::<Vec4>(),
        );
        ds.add_uniform_buffer_descriptor(settings_desc);

        ds.build(vk);
        let range = generate_push_constant_range::<Vec4>(ShaderStageFlags::FRAGMENT, 0);

        pipe.add_push_constant(range);
        pipe.add_descriptor_set_layout(ds.get_descriptor_layout());
        pipe.compile2(
            vk,
            app,
            &shader,
            Some(&targets[(i as usize)].get_renderpass().raw()),
        );

        steps.push(SceneState {
            descriptor: Some(ds),
            scene: DisplayObject::new(vec![mesh], pipe),
        })
    }
    JumpSteps { targets, steps }
}
