use ash::vk;
use ash::vk::*;
use glam::Vec4;
use image::imageops::FilterType;
use image::{imageops, EncodableLayout};
use vsketch::utils::generate_push_constant_range;
use vsketch::{build_camera, DisplayObject, SceneState, VSketch, VkRenderCore, PASSTHRU_VERTEX};
use yoi_geo::cube::CubeGeo;
use yoi_geo::simpleshapes::SimpleShapes;
use yoi_vk::*;

struct State {
    scene: SceneState,
    rp: RenderPass,
    framebuffers: Vec<Framebuffer>,
    time: f32,
    output: SceneState,
    tex: VkTexture,
    geo: SceneState,
}

const TEXTURE_FMT: Format = Format::B8G8R8A8_UNORM;
const WIDTH: u32 = 1280;
const HEIGHT: u32 = 1280;

/// This is a basic example of how to set up an accumulation texture / renderpass (aka a non-clearing FBO)
fn main() {
    VSketch::new()
        .set_size(WIDTH, HEIGHT)
        .add_setup(setup)
        .add_draw(draw)
        .run();
}

fn setup(vk: &mut Vulkan, comp: &mut VkRenderCore) -> State {
    let mut sampler = VkSampler::new();
    sampler.build(vk);

    let mut rt = RenderTarget2::new(WIDTH, HEIGHT);
    rt.add_default_attachment(vk, Some(TEXTURE_FMT));
    rt.compile(vk);

    ////////// BUILD RENDERPASS /////////////////

    let descriptions = &[AttachmentDescription {
        format: TEXTURE_FMT,
        samples: SampleCountFlags::TYPE_1,
        load_op: AttachmentLoadOp::LOAD,
        store_op: AttachmentStoreOp::STORE,
        stencil_load_op: AttachmentLoadOp::DONT_CARE,
        stencil_store_op: AttachmentStoreOp::DONT_CARE,
        initial_layout: ImageLayout::GENERAL,
        final_layout: ImageLayout::GENERAL,
        ..Default::default()
    }];

    let attachment_ref = vec![AttachmentReference {
        attachment: 0,
        layout: ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
    }];

    let subpass_desc = &[SubpassDescription::default()
        .pipeline_bind_point(PipelineBindPoint::GRAPHICS)
        .color_attachments(&attachment_ref[..])];

    let subpass_dependencies = &[SubpassDependency::default()
        .src_subpass(SUBPASS_EXTERNAL)
        .dst_subpass(0)
        .src_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
        .dst_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
        .src_access_mask(AccessFlags::empty())
        .dst_access_mask(AccessFlags::COLOR_ATTACHMENT_WRITE)
        .dependency_flags(DependencyFlags::empty())];

    let rp_info = vk::RenderPassCreateInfo::default()
        .attachments(&descriptions[..])
        .subpasses(&subpass_desc[..])
        .dependencies(&subpass_dependencies[..]);

    let rp = vk.generate_renderpass(rp_info).unwrap();

    ////////// BUILD FBO ////////////////
    let mut format = TextureFormat::default();
    format.texture_format = TEXTURE_FMT;
    format.extent.width = WIDTH as u32;
    format.extent.height = HEIGHT as u32;
    format.extent.depth = 1;
    format.mip_levels = 1;
    format.array_layers = 1;
    format.sample_count = SampleCountFlags::TYPE_1;
    format.tiling_mode = ImageTiling::OPTIMAL;
    format.image_usage_flags = ImageUsageFlags::COLOR_ATTACHMENT
        | ImageUsageFlags::SAMPLED
        | ImageUsageFlags::TRANSFER_SRC;
    format.memory_property_flags = MemoryPropertyFlags::DEVICE_LOCAL;

    let mut tex = VkTexture::new(vk, format);

    // need to transition the texture prior to use.
    tex.update_image_layout(comp.render.get_command_buffer(0), vk, ImageLayout::GENERAL);

    let attachment = &[*tex.get_image_view()];

    let create_info = FramebufferCreateInfo::default()
        .width(WIDTH)
        .height(HEIGHT)
        .render_pass(rp)
        .layers(1)
        .attachments(attachment);

    let fb = vk.generate_framebuffer(create_info);

    let camera = build_camera(
        vk,
        1,
        -40.0,
        60.0,
        (WIDTH as f32) / (HEIGHT as f32),
        0.1,
        10000.0,
    );

    let geo = build_debug_geo(vk, &comp.meta, &camera.descriptor, &rp);
    let scene = build_scene(vk, &comp.meta, &rt.get_renderpass().raw());
    let output = build_output(vk, comp, tex.get_image_view());

    State {
        scene,
        rp,
        framebuffers: vec![fb],
        time: 0.0,
        tex,
        output,
        geo,
    }
}

fn draw(vk: &mut Vulkan, comp: &mut VkRenderCore, model: &mut State) {
    let renderer = &mut comp.render;
    let meta = &mut comp.meta;
    let swapchain = meta.get_swapchain();

    let mut rp = &mut model.rp;
    let fb = &model.framebuffers;

    let scene = &model.scene;
    let geo = &model.geo;
    let output = &model.output;
    let tex = &model.tex;

    renderer.record_commands_with_renderpass_and_index(vk, |cb, i| {
        let fbo = model.framebuffers[0];

        let scissor = Rect2D {
            offset: Offset2D { x: 0, y: 0 },
            extent: Extent2D {
                width: WIDTH,
                height: HEIGHT,
            },
        };

        let clear_values = vec![ClearValue {
            color: ClearColorValue {
                float32: [1.0, 0.0, 0.0, 1.0],
            },
        }];

        let viewport = Viewport {
            x: 0.0,
            y: 0.0,
            width: WIDTH as f32,
            height: HEIGHT as f32,
            min_depth: 0.0,
            max_depth: 1.0,
        };

        let renderpass_info = vk::RenderPassBeginInfo::default()
            .render_area(scissor)
            .render_pass(*rp)
            .clear_values(&clear_values[..])
            .framebuffer(fbo);

        vk.set_viewport(*cb, 0, &[viewport]);
        vk.set_scissor(*cb, 0, &[scissor]);

        vk.start_renderpass(cb, &renderpass_info);

        let consts: Vec<Vec<Vec4>> = vec![vec![Vec4::new(model.time, 20.0, 0.0, 0.08)]];
        geo.render_with_push_constants(vk, cb, consts, ShaderStageFlags::VERTEX);
        vk.end_renderpass(cb);
    });

    renderer.record_commands(vk, meta, |cb| {
        output.render(vk, cb);
    });

    renderer.present(vk, swapchain);
    renderer.finish_present();

    model.time += 0.1;
}

fn build_debug_geo(
    vk: &Vulkan,
    app: &VkApp,
    camera_descriptor: &UniformDescriptor,
    rp: &RenderPass,
) -> SceneState {
    let size = 10.0;
    let geo = CubeGeo::new(size, size, size, 2.0, 2.0, 2.0);
    let shader = RenderShader::new(
        "shaders/accumulate_texture/render.vert",
        "shaders/accumulate_texture/render.frag",
        vk,
    );

    let mesh = Mesh::new()
        .add_attribute(vk, 0, geo.positions)
        .add_index_data(vk, geo.indices);

    let mut descriptor_buffer = VkDescriptorBuffer::new();
    descriptor_buffer.add_uniform_buffer_descriptor(*camera_descriptor);
    descriptor_buffer.build(vk);

    let mut pipeline = RenderPipeline::new();
    pipeline.add_descriptor_set_layout(descriptor_buffer.get_descriptor_layout());
    pipeline.add_mesh(&mesh);

    let push = generate_push_constant_range::<Vec4>(ShaderStageFlags::VERTEX, 0);
    pipeline.add_push_constant(push);
    pipeline.compile2(vk, app, &shader, Some(rp));

    let mut ds = DisplayObject::new(vec![mesh], pipeline);

    SceneState {
        descriptor: Some(descriptor_buffer),
        scene: ds,
    }
}

fn build_scene(vk: &Vulkan, app: &VkApp, rt: &RenderPass) -> SceneState {
    let mut sampler = VkSampler::new();
    sampler.build(vk);

    let mut mesh = Mesh::new().set_num_vertices(3);

    let shader = RenderShader::new(
        "shaders/accumulate_texture/scene.vert",
        "shaders/accumulate_texture/scene.frag",
        vk,
    );

    // build a pipeline to render it
    let mut pipeline = RenderPipeline::new();
    let push = generate_push_constant_range::<Vec4>(ShaderStageFlags::FRAGMENT, 0);
    pipeline.add_push_constant(push);
    pipeline.compile2(vk, app, &shader, Some(rt));

    SceneState {
        descriptor: None,
        scene: DisplayObject::new(vec![mesh], pipeline),
    }
}

fn build_noise(vk: &Vulkan) -> VkTexture {
    let noise = image::open("shaders/rp_research/debug.png")
        .unwrap()
        .into_rgba8();
    let mut resized = imageops::resize(&noise, WIDTH, HEIGHT, FilterType::Nearest);
    VkTexture::create_with_image(
        vk,
        resized.as_bytes(),
        [resized.width(), resized.height()],
        TextureFormat::default(),
    )
}

/// Build necessary components to render output from render targets.
fn build_output(ctx: &Vulkan, comp: &VkRenderCore, output_rt: &ImageView) -> SceneState {
    let mut sampler = VkSampler::new();
    sampler.build(ctx);

    let mut tex = TextureSamplerDescriptor::new();
    tex.set_image(*output_rt, *sampler.raw());
    tex.set_shader_binding(0);
    tex.set_shader_stage(ShaderStageFlags::FRAGMENT);

    let mut output_descriptors = VkDescriptorBuffer::new();
    output_descriptors.add_texture_descriptor(tex);
    output_descriptors.build(ctx);

    // build a fullscreen triangle for the background
    let geo = SimpleShapes::generate_fullscreen_triangle();

    let mut mesh = Mesh::new();
    mesh = mesh
        .add_attribute(ctx, 0, geo.positions)
        .set_num_vertices(3);

    let shader = RenderShader::new(PASSTHRU_VERTEX, "shaders/rp_research/output.frag", ctx);

    // build a pipeline to render it
    let mut pipeline = RenderPipeline::new();
    pipeline.add_descriptor_set_layout(output_descriptors.get_descriptor_layout());
    pipeline.compile_dynamic(
        ctx,
        &comp.meta,
        &shader,
        Format::B8G8R8A8_UNORM,
        Format::D32_SFLOAT,
    );

    SceneState {
        descriptor: Some(output_descriptors),
        scene: DisplayObject::new(vec![mesh], pipeline),
    }
}

/*
let renderer = &mut comp.render;
   let meta = &mut comp.meta;


   let tex = &model.tex;
   let targets = &model.targets;

   let scene = &model.scene;

   let time = model.time;
   let output = &mut model.output.scene;
   let odesc = &mut model.output.descriptor;
   renderer.record_commands_with_renderpass(vk, meta, |cb| {

       insert_image_memory_barrier(
           vk,
           cb,
           tex.get_image(),
           AccessFlags::empty(),
           AccessFlags::COLOR_ATTACHMENT_READ | AccessFlags::COLOR_ATTACHMENT_WRITE | AccessFlags::SHADER_READ | AccessFlags::SHADER_WRITE,
           ImageLayout::UNDEFINED,
           ImageLayout::GENERAL,
           generate_subresource_range(false),
           PipelineStageFlags::ALL_GRAPHICS,
           PipelineStageFlags::ALL_GRAPHICS,
       );

       targets[0].begin(vk, cb);

       let consts: Vec<Vec<Vec4>> = vec![
           vec![Vec4::new(time, 20.0, 0.0, 0.08)]
       ];
       scene.render_with_push_constants(vk, cb, consts, ShaderStageFlags::FRAGMENT);


       targets[0].end(vk, cb);



   });

   // render the output
   renderer.record_commands(vk, meta, |cb| {
       output.render(vk, cb, odesc.as_ref());
   });

   /////////////////////////////////////////////////////////////////////////////////////////

   renderer.present(vk, meta.get_swapchain());
   renderer.finish_present();

   model.time += 0.1;
*/
