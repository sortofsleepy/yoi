use yoi_vk::*;
use ash::vk::{DescriptorSetLayout, Format, MemoryAllocateFlags, PrimitiveTopology, Semaphore, SemaphoreCreateInfo, ShaderStageFlags};
use glam::Vec4;
use vsketch::*;
use yoi_geo::simpleshapes::SimpleShapes;
use yoi_math::{create_vec4, rand_vec4};
use yoi_vk::descriptors::descriptor_set::VkDescriptorSet;
use yoi_vk::descriptors::storage_descriptor::StorageDescriptor;

mod objects;

use crate::objects::meshline::MeshLine;

const NUM_PARTICLES: u32 = 1000;

const WIDTH: u32 = 1024;
const HEIGHT: u32 = 768;

struct State {
    bg_display_object: DisplayObject,
    descriptor: VkDescriptorBuffer,
    compute: ComputeData,
    graphices_done_semaphore: Semaphore,
}

struct CameraData {
    camera: PerspectiveCamera,
    descriptor: UniformDescriptor,
    buffer: DataBuffer,
}

struct ComputeData {
    pipeline: ComputePipeline,
    buffer: DataBuffer,
    debug: DisplayObject,
    descriptor: VkDescriptorSet,
}

#[derive(Copy, Clone, Debug)]
struct ParticleData {
    pos: Vec4,
    vel: Vec4,
    meta: Vec4,
}

/// A basic setup of how to integrate a compute pipeline.
fn main() {
    VSketch::new()
        .add_setup(setup)
        .add_draw(draw)
        .run();
}

fn setup(vk: &mut Vulkan, comp: &mut VkRenderCore) -> State {
    let aspect = (WIDTH as f32) / (HEIGHT as f32);

    // used to hold and wait for compute signaled when compute is complete.
    let graphices_done_semaphore = vk.generate_semaphore(SemaphoreCreateInfo {
        ..Default::default()
    });


    let bg_display_object = build_background(vk, comp);

    let camera = build_camera(vk, 0, aspect);

    let mut descriptor = VkDescriptorBuffer::new();
    descriptor.add_uniform_buffer_descriptor(camera.descriptor);
    descriptor.build(vk);

    let particles = build_particle_system(vk, comp, &descriptor.get_descriptor_layout());

    State {
        bg_display_object,
        descriptor,
        compute: particles,
        graphices_done_semaphore,
    }
}

fn draw(vk: &mut Vulkan, comp: &mut VkRenderCore, model: &mut State) {
    let renderer = &mut comp.render;
    let win = &mut comp.window;


    let mut compute_pipeline = &model.compute.pipeline;
    let compute_descriptor = &model.compute.descriptor;

    let mut bg = &model.bg_display_object;
    let render_descriptor = &model.descriptor;
    let mut particles = &model.compute.debug;

    renderer.record_commands(vk, &comp.meta, |cb| {
        bg.draw(vk, cb, None);

        particles.draw(vk, cb, Some(render_descriptor));
        //scene.draw(vk, cb, Some(&model.descriptor));
        //scene.draw_with_descriptor_set(vk, cb, Some(&model.descriptor_set));
        //scene.draw_with_descriptor_set(vk, cb, Some(&model.descriptor_set));
    });


    //////// PRESENT ////////////
    compute_pipeline.run_with_descriptor_set(vk, compute_descriptor);
    let _ = renderer.present_with_compute(
        vk,
        comp.meta.get_swapchain(),
        compute_pipeline.get_semaphore(),
        &model.graphices_done_semaphore,
    );

    compute_pipeline.submit_compute(vk, &model.graphices_done_semaphore);

    renderer.finish_present();
}

/// Builds camera information
fn build_camera(instance: &Vulkan, location: u32, aspect: f32) -> CameraData {
    // setup main camera
    let mut camera = PerspectiveCamera::create(50.0, aspect, 0.1, 10000.0);
    camera.translate(vec3(0.0, 0.0, -5.0));

    let mut fmt = BufferFormat::default().size(size_of::<CameraBufferState>() as u64);
    fmt.usage_flags = fmt.usage_flags | BufferUsageFlags::SHADER_DEVICE_ADDRESS;
    fmt.mem_alloc_flags = MemoryAllocateFlags::DEVICE_ADDRESS;


    let mut camera_buffer = DataBuffer::create(instance, fmt);
    camera_buffer.set_object_data(instance, camera.get_projection_view());

    let buffer_address = instance.get_buffer_device_address(camera_buffer.raw());

    let mut camera_desc = UniformDescriptor::new();
    camera_desc.set_shader_binding(location);
    camera_desc.set_buffer_data(camera_buffer.raw(), 0, size_of::<CameraBufferState>() as u64);
    camera_desc.set_buffer_data_with_address(camera_buffer.raw(), 0, size_of::<CameraBufferState>() as u64, buffer_address);

    CameraData {
        camera,
        descriptor: camera_desc,
        buffer: camera_buffer,
    }
}

fn build_scene(
    ctx: &Vulkan,
    comp: &VkRenderCore,
    descriptor_layout: &DescriptorSetLayout,
    position_buffer: &DataBuffer,
) -> MeshLine {
    let mut meshline = MeshLine::new();

    meshline.setup(
        ctx,
        &comp.meta,
        10,
        4,
        descriptor_layout,
        position_buffer,
    );

    meshline
}

fn build_particle_system(ctx: &Vulkan, comp: &VkRenderCore, descriptor_layout: &DescriptorSetLayout) -> ComputeData {
    let mut particles = vec![];

    for i in 0..NUM_PARTICLES {
        particles.push(ParticleData {
            pos: rand_vec4(),
            vel: rand_vec4(),

            // x = life
            meta: vec4(1.0, 0.0, 0.0, 1.0),
        })
    }

    // setup staging buffer
    let mut stage_fmt = BufferFormat::default();
    stage_fmt.usage_flags = BufferUsageFlags::TRANSFER_SRC;
    stage_fmt.size = (size_of::<ParticleData>() * (NUM_PARTICLES as usize)) as u64;

    let mut stage_buffer = DataBuffer::create(ctx, stage_fmt);
    stage_buffer.set_data(ctx, &particles);

    // setup particle buffer data - note we append the Vertex buffer flag so it can be used in the render mesh.
    let mut particle_fmt = BufferFormat::default();
    particle_fmt.usage_flags = BufferUsageFlags::STORAGE_BUFFER | BufferUsageFlags::TRANSFER_DST | BufferUsageFlags::VERTEX_BUFFER;
    particle_fmt.size = (size_of::<ParticleData>() * (NUM_PARTICLES as usize)) as u64;
    let mut particle_buffer = DataBuffer::create(ctx, particle_fmt);

    ctx.copy_buffer(
        comp.render.get_command_buffer(0),
        stage_buffer.raw(),
        particle_buffer.raw(),
        (size_of::<ParticleData>() * (NUM_PARTICLES as usize)) as u64,
    );

    ///////// BUILD COMPUTE ////////////
    let mut storage = StorageDescriptor::new();
    storage.set_shader_stage(ShaderStageFlags::COMPUTE);
    storage.set_buffer_debug(particle_buffer.raw(), 0, (size_of::<ParticleData>() * (NUM_PARTICLES as usize)) as u64);
    storage.set_shader_binding(1);

    // build descriptor set
    let mut ds = VkDescriptorSet::new(1);
    ds.add_storage_buffer_descriptor(storage);
    ds.build(ctx);

    let mut compute = ComputePipeline::new(ctx);
    let comp_shader = ComputeShader::new(
        "shaders/sketch/particles.comp",
        ctx,
    );

    compute.set_dispatch_size([(NUM_PARTICLES / 100), 1, 1]);
    let ds_layouts = vec![
        *ds.get_descriptor_layout()
    ];
    compute.compile(ctx, comp_shader.generate_pipeline_info(), ds_layouts);

    //////// BUILD RENDERING //////////////
    let shader = RenderShader::new(
        "shaders/sketch/particles.vert",
        "shaders/sketch/particles.frag",
        ctx,
    );

    let geo = SimpleShapes::generate_cube();

    let mut positions = vec![create_vec4(); NUM_PARTICLES as usize];

    let mut m = Mesh::new()
        .add_attribute(ctx, 0, positions)
        .add_index_data(ctx, geo.indices)
        .add_instanced_attrib_buffer(1, &particle_buffer, size_of::<ParticleData>() as u32)
        .set_num_instances(particles.len() as u32);

    let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
    pipeline.add_mesh(&m);
    pipeline.add_descriptor_set_layout(*descriptor_layout);
    pipeline.set_topology(PrimitiveTopology::POINT_LIST);
    pipeline.compile(ctx, &comp.meta, &shader);

    ComputeData {
        pipeline: compute,
        buffer: particle_buffer,
        descriptor: ds,
        debug: DisplayObject::new(vec![m], pipeline),
    }
}

fn build_background(ctx: &Vulkan, comp: &VkRenderCore) -> DisplayObject {

    // build a fullscreen triangle for the background
    let geo = SimpleShapes::generate_fullscreen_triangle();

    let mut mesh = Mesh::new();
    mesh = mesh.add_attribute(ctx, 0, geo.positions)
        .set_num_vertices(3);

    let shader = RenderShader::new(
        PASSTHRU_VERTEX,
        "shaders/sketch/bg.frag",
        ctx,
    );


    // build a pipeline to render it
    let mut pipeline = RenderPipeline::new_with_descriptor_buffer();

    pipeline.compile_dynamic(ctx, &comp.meta, &shader, Format::B8G8R8A8_UNORM, Format::D32_SFLOAT);

    DisplayObject::new(vec![mesh], pipeline)
}

