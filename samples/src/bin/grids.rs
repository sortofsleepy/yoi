use ash::vk;
use ash::vk::{Format, Handle, MemoryAllocateFlags, Result, ShaderStageFlags};
use glam::{Mat4, Vec4};
use vsketch::objects::light::Light;
use vsketch::shaders::loader::load_shader2;
use vsketch::utils::{
    generate_descriptor_buffer_format, generate_descriptor_buffer_format_with_size,
};
use vsketch::*;
use yoi_geo::cube::CubeGeo;
use yoi_geo::simpleshapes::SimpleShapes;
use yoi_math::core::to_radians;
use yoi_math::{create_vec4, rand_float, rand_vec4};
use yoi_vk::*;

mod objects;

const WIDTH: u32 = 1280;
const HEIGHT: u32 = 920;

const NUM_LIGHTS: usize = 5;

struct State {
    floor: SceneState,
    roof: SceneState,
    l_wall: SceneState,
    r_wall: SceneState,
    env_buffer: DataBuffer,
    settings: Vec4,
    scene_target: RenderTarget,
    output: SceneState,
}

struct SceneState {
    descriptor: VkDescriptorBuffer,
    scene: DisplayObject,
}

struct GridTransforms {
    i_transforms: StorageDescriptor,
    g_transform: UniformDescriptor,
}

/// Small sample to demonstrate some things
/// - RenderTargets
/// - Descriptor Buffers
/// - instancing
fn main() {
    VSketch::new()
        .set_size(WIDTH, HEIGHT)
        .add_setup(setup)
        .add_draw(draw)
        .run();
}

fn setup(vk: &mut Vulkan, comp: &mut VkRenderCore) -> State {
    let mut sampler = VkSampler::new();
    sampler.build(vk);

    let camera = build_camera(
        vk,
        1,
        -700.0,
        60.0,
        (WIDTH as f32) / (HEIGHT as f32),
        0.1,
        10000.0,
    );

    ////////////////////////////////////
    let res = 20;
    let grid = build_grid(WIDTH as f32, WIDTH as f32, res as f32);

    ///////// BUILD FLOOR /////////////////
    let fcolors = vec![
        vec4(
            0.08627450980392157,
            0.15294117647058825,
            0.29411764705882354,
            1.0,
        ),
        vec4(
            0.5607843137254902,
            0.01568627450980392,
            0.0392156862745098,
            1.0,
        ),
        vec4(
            0.5215686274509804,
            0.5764705882352941,
            0.6627450980392157,
            1.0,
        ),
        vec4(
            0.42745098039215684,
            0.07450980392156863,
            0.3568627450980392,
            1.0,
        ),
        vec4(
            0.9019607843137255,
            0.1607843137254902,
            0.5176470588235295,
            1.0,
        ),
        vec4(
            0.9058823529411765,
            0.6901960784313725,
            0.7803921568627451,
            1.0,
        ),
    ];

    let transforms = build_model_matrices(vk, &grid);
    let global_transform = build_floor_transform(vk);

    let floor_desc =
        build_transform_descriptors(vk, &transforms, &global_transform, 2, 3, grid.len());

    ///////// BUILD ROOF /////////////////
    let rcolors = vec![
        vec4(0.0, 0.2, 0.29411764705882354, 1.0),
        vec4(0.4, 0.6, 0.7333333333333333, 1.0),
        vec4(0.47843137254901963, 0.0, 0.06666666666666667, 1.0),
        vec4(
            0.7607843137254902,
            0.06666666666666667,
            0.13333333333333333,
            1.0,
        ),
        vec4(0.8980392156862745, 0.7490196078431373, 0.6, 1.0),
        vec4(
            0.996078431372549,
            0.9490196078431372,
            0.8470588235294118,
            1.0,
        ),
    ];

    let transforms_roof = build_model_matrices(vk, &grid);
    let global_transform_roof = build_roof_transform(vk);

    let roof_desc = build_transform_descriptors(
        vk,
        &transforms_roof,
        &global_transform_roof,
        2,
        3,
        grid.len(),
    );

    ///////// BUILD LEFT WALL /////////////////
    let lwcolors = vec![
        vec4(0.37254901960784315, 0.0, 1.0, 1.0),
        vec4(0.6235294117647059, 0.21568627450980393, 1.0, 1.0),
        vec4(0.0392156862745098, 0.6, 0.8, 1.0),
        vec4(0.09019607843137255, 0.9098039215686274, 1.0, 1.0),
        vec4(
            0.7529411764705882,
            0.7568627450980392,
            0.8117647058823529,
            1.0,
        ),
        vec4(
            0.9411764705882353,
            0.9490196078431372,
            0.9725490196078431,
            1.0,
        ),
    ];
    let transforms_lwall = build_model_matrices(vk, &grid);
    let global_transform_lwall = build_left_wall_transform(vk);

    let lwall_desc = build_transform_descriptors(
        vk,
        &transforms_lwall,
        &global_transform_lwall,
        2,
        3,
        grid.len(),
    );

    ///////// BUILD RIGHT WALL /////////////////
    let rwcolors = vec![
        vec4(
            0.8823529411764706,
            0.09019607843137255,
            0.43529411764705883,
            1.0,
        ),
        vec4(0.08235294117647059, 0.3058823529411765, 0.4, 1.0),
        vec4(
            0.1843137254901961,
            0.4549019607843137,
            0.6196078431372549,
            1.0,
        ),
        vec4(
            0.3176470588235294,
            0.6235294117647059,
            0.8627450980392157,
            1.0,
        ),
        vec4(
            0.5843137254901961,
            0.6588235294117647,
            0.7333333333333333,
            1.0,
        ),
        vec4(
            0.8274509803921568,
            0.8745098039215686,
            0.9176470588235294,
            1.0,
        ),
    ];
    let transforms_rwall = build_model_matrices(vk, &grid);
    let global_transform_rwall = build_right_wall_transform(vk);

    let rwall_desc = build_transform_descriptors(
        vk,
        &transforms_rwall,
        &global_transform_rwall,
        2,
        3,
        grid.len(),
    );

    /////////////////////////////////
    let settings = create_vec4();

    let mut settings_buffer = DataBuffer::create(vk, generate_descriptor_buffer_format::<Vec4>());
    settings_buffer.set_object_data(vk, settings);

    let mut env_desc = UniformDescriptor::new();
    env_desc.set_shader_binding(0);
    env_desc.set_buffer_data_with_address(vk, settings_buffer.raw(), 0, size_of::<Vec4>() as u64);

    let scene_target = build_offscreen(vk);

    // build output geometry
    let output = build_output(vk, comp, &scene_target, &sampler.raw());

    // build lights
    let lights = build_lights(vk, 4);

    //////////////////////////////
    let floor = build_grid_scene(
        vk,
        &comp.meta,
        &scene_target,
        &camera.descriptor,
        &env_desc,
        &lights,
        &grid,
        &floor_desc,
        &fcolors,
    );

    let roof = build_grid_scene(
        vk,
        &comp.meta,
        &scene_target,
        &camera.descriptor,
        &env_desc,
        &lights,
        &grid,
        &roof_desc,
        &rcolors,
    );

    let l_wall = build_grid_scene(
        vk,
        &comp.meta,
        &scene_target,
        &camera.descriptor,
        &env_desc,
        &lights,
        &grid,
        &lwall_desc,
        &lwcolors,
    );

    let r_wall = build_grid_scene(
        vk,
        &comp.meta,
        &scene_target,
        &camera.descriptor,
        &env_desc,
        &lights,
        &grid,
        &rwall_desc,
        &rwcolors,
    );

    State {
        floor,
        roof,
        l_wall,
        r_wall,
        env_buffer: settings_buffer,
        settings,
        scene_target,
        output,
    }
}

fn draw(vk: &mut Vulkan, comp: &mut VkRenderCore, model: &mut State) {
    let renderer = &mut comp.render;
    let meta = &mut comp.meta;
    let mut env = &mut comp.app_env;
    env.update();

    ////// UPDATE APP ENV ///////////////
    let mut settings = model.settings;
    settings.x = env.get_delta_time();
    let mut env_buff = &mut model.env_buffer;
    env_buff.set_object_data(vk, settings);

    let floor = &mut model.floor.scene;
    let roof = &mut model.roof.scene;
    let l_wall = &mut model.l_wall.scene;
    let r_wall = &mut model.r_wall.scene;

    let db = &mut model.floor.descriptor;
    let db2 = &mut model.roof.descriptor;
    let db3 = &mut model.l_wall.descriptor;
    let db4 = &mut model.r_wall.descriptor;

    let scene_rt = &mut model.scene_target;

    let output = &mut model.output.scene;
    let odesc = &mut model.output.descriptor;

    renderer.record_commands_with_renderpass(vk, meta, |cb| {
        scene_rt.begin(vk, cb);

        l_wall.render(vk, cb, Some(db3));
        r_wall.render(vk, cb, Some(db4));

        floor.render(vk, cb, Some(db));
        roof.render(vk, cb, Some(db2));

        scene_rt.end(vk, cb);
    });

    renderer.record_commands(vk, meta, |cb| {
        //output.draw(vk, cb, Some(odesc));
        output.render(vk, cb, Some(&odesc));
    });

    /////////////////////////////////////////////////////////////////////////////////////////
    let res = renderer.present(&vk, comp.meta.get_swapchain());

    // handle out of date swapchain
    if res == Result::ERROR_OUT_OF_DATE_KHR {
        let window_size = comp.window.inner_size();
        comp.meta
            .reset(vk, &comp.surface, [window_size.width, window_size.height]);
    }

    renderer.finish_present();
}

/// Builds the main scene objects
fn build_grid_scene(
    ctx: &Vulkan,
    app: &VkApp,
    rt: &RenderTarget,
    camera_descriptor: &UniformDescriptor,
    env_desc: &UniformDescriptor,
    lights_desc: &UniformDescriptor,
    grid: &Vec<Vec4>,
    transforms: &GridTransforms,
    color_set: &Vec<Vec4>,
) -> SceneState {
    let res = 20;

    let mut descriptor_buffer = VkDescriptorBuffer::new();
    descriptor_buffer.add_uniform_buffer_descriptor(*camera_descriptor);
    descriptor_buffer.add_uniform_buffer_descriptor(*env_desc);
    descriptor_buffer.add_storage_descriptor(transforms.i_transforms);
    descriptor_buffer.add_uniform_buffer_descriptor(transforms.g_transform);
    descriptor_buffer.add_uniform_buffer_descriptor(*lights_desc);
    descriptor_buffer.build(ctx);

    ///////////////////////////
    let size = res as f32;
    let num = grid.len();
    let mut lin = GradientLinear::new(color_set.clone());

    let geo = CubeGeo::new(size, size, size, 2.0, 2.0, 2.0);

    //let geo = PlaneGeometry::create(size, size, 2.0, 2.0);
    let shader = RenderShader::new(
        "shaders/grids/grid.vert",
        load_shader2("shaders/grids/grid.frag").as_str(),
        ctx,
    );

    let mut colors = vec![];
    let mut meta = vec![];
    let mut offsets = vec![];

    for i in 0..num {
        let idx = rand_float(0.0, 1.0);
        colors.push(lin.get_at(idx));

        // build offsets for scaling
        offsets.push(rand_vec4());

        // stores grid position + index of light to blend with.
        meta.push(vec4(
            grid[i].x,
            grid[i].y,
            grid[i].z,
            rand_float(0.0, NUM_LIGHTS as f32),
        ));
    }

    let num_instances = meta.len() as u32;
    let mut mesh = Mesh::new();
    mesh = mesh
        .add_attribute(ctx, 0, geo.positions)
        .add_attribute(ctx, 1, geo.normals)
        .add_instanced_attribute(ctx, 2, colors)
        .add_instanced_attribute(ctx, 3, meta)
        .add_instanced_attribute(ctx, 4, offsets)
        .add_index_data(ctx, geo.indices)
        .set_num_instances(num_instances);

    let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
    pipeline.add_mesh(&mesh);
    pipeline.enable_depth_test();
    pipeline.add_descriptor_set_layout(descriptor_buffer.get_descriptor_layout());
    pipeline.compile_with_renderpass(ctx, app, &shader, rt.get_renderpass().raw());

    let mut ds = DisplayObject::new(vec![mesh], pipeline);
    ds.depth_test = true;

    SceneState {
        descriptor: descriptor_buffer,
        scene: ds,
    }
}

/// Builds descriptors for the instance and global transform buffers used in each grid.
fn build_transform_descriptors(
    vk: &Vulkan,
    transforms: &DataBuffer,
    global: &DataBuffer,
    transforms_binding: u32,
    global_binding: u32,
    grid_len: usize,
) -> GridTransforms {
    let mut transforms_desc = StorageDescriptor::new();
    transforms_desc.set_shader_stage(ShaderStageFlags::VERTEX);
    transforms_desc.set_buffer_data_with_address(
        vk,
        transforms.raw(),
        0,
        get_byte_size_of::<Mat4>(grid_len),
    );
    transforms_desc.set_shader_binding(transforms_binding);

    let mut global_desc = UniformDescriptor::new();
    global_desc.set_shader_stage(ShaderStageFlags::VERTEX);
    global_desc.set_buffer_data_with_address(vk, global.raw(), 0, get_byte_size::<Mat4>());
    global_desc.set_shader_binding(global_binding);

    GridTransforms {
        i_transforms: transforms_desc,
        g_transform: global_desc,
    }
}

/// Builds model matrices for each instance.
fn build_model_matrices(ctx: &Vulkan, grid: &Vec<Vec4>) -> DataBuffer {
    let num = grid.len();
    let mut transform_fmt = BufferFormat::default();
    transform_fmt.mem_alloc_flags = MemoryAllocateFlags::DEVICE_ADDRESS;
    transform_fmt.usage_flags =
        BufferUsageFlags::STORAGE_BUFFER | BufferUsageFlags::SHADER_DEVICE_ADDRESS;
    transform_fmt.size = (size_of::<Mat4>() * num) as u64;
    let mut transforms = DataBuffer::create(ctx, transform_fmt);

    //// Build individual transforms ////
    let mut tmp = vec![];
    for i in 0..num {
        let pos = grid[i];
        let m = Mat4::IDENTITY;
        let trans = Mat4::from_translation(vec3(pos.x, pos.y, pos.z));

        // scale
        let scale = Mat4::from_scale(vec3(1.0, 1.0, rand_float(1.0, 8.0)));

        tmp.push((m * scale * trans));
    }

    transforms.set_data(ctx, &tmp);

    transforms
}

/// Builds global transform for transforming the whole floor element.
fn build_floor_transform(ctx: &Vulkan) -> DataBuffer {
    let mut global_fmt = generate_descriptor_buffer_format::<Mat4>();
    let mut global_transforms = DataBuffer::create(ctx, global_fmt);

    let mut m = Mat4::IDENTITY;

    m = m
        * Mat4::from_translation(vec3(0.0, 200.0, 0.0))
        * Mat4::from_rotation_x(to_radians(-70.0))
        * Mat4::from_rotation_z(to_radians(45.0));

    global_transforms.set_object_data(ctx, m);
    global_transforms
}

/// Builds global transform for transforming the whole roof element.
fn build_roof_transform(ctx: &Vulkan) -> DataBuffer {
    let mut global_fmt = generate_descriptor_buffer_format::<Mat4>();
    let mut global_transforms = DataBuffer::create(ctx, global_fmt);

    let mut m = Mat4::IDENTITY;

    m = m
        * Mat4::from_translation(vec3(0.0, -400.0, 0.0))
        * Mat4::from_rotation_x(to_radians(90.0))
        * Mat4::from_rotation_z(to_radians(45.0));
    global_transforms.set_object_data(ctx, m);
    global_transforms
}

/// Builds global transform for transforming the whole left wall element.
fn build_left_wall_transform(ctx: &Vulkan) -> DataBuffer {
    let mut global_fmt = generate_descriptor_buffer_format::<Mat4>();
    let mut global_transforms = DataBuffer::create(ctx, global_fmt);

    let mut m = Mat4::IDENTITY;

    m = m
        * Mat4::from_scale(vec3(1.2, 1.2, 1.2))
        * Mat4::from_translation(vec3(-400.0, 200.0, 700.0))
        * Mat4::from_rotation_x(to_radians(0.0))
        * Mat4::from_rotation_y(to_radians(-45.0))
        * Mat4::from_rotation_z(to_radians(90.0));
    global_transforms.set_object_data(ctx, m);
    global_transforms
}

/// Builds global transform for transforming the whole right wall element.
fn build_right_wall_transform(ctx: &Vulkan) -> DataBuffer {
    let mut global_fmt = generate_descriptor_buffer_format::<Mat4>();
    let mut global_transforms = DataBuffer::create(ctx, global_fmt);

    let mut m = Mat4::IDENTITY;

    m = m
        * Mat4::from_scale(vec3(1.2, 1.2, 1.2))
        * Mat4::from_translation(vec3(400.0, 200.0, 700.0))
        * Mat4::from_rotation_x(to_radians(0.0))
        * Mat4::from_rotation_y(to_radians(45.0))
        * Mat4::from_rotation_z(to_radians(0.0));
    global_transforms.set_object_data(ctx, m);
    global_transforms
}

/// Build grid for positions.
fn build_grid(width: f32, height: f32, res: f32) -> Vec<Vec4> {
    let cols = width / res;
    let rows = height / res;

    let mut pos = vec![];
    for i in 0..(cols as usize) {
        for j in 0..(rows as usize) {
            let mut x = (j as f32) * res;
            let mut y = (i as f32) * res;
            pos.push(vec4(
                x - ((WIDTH as f32) / 2.0),
                y - ((WIDTH as f32) / 2.0),
                0.0,
                1.0,
            ));
        }
    }

    pos
}

/// Build render target to take in the general scene.
fn build_offscreen(ctx: &Vulkan) -> RenderTarget {
    let mut rt = RenderTarget::new(WIDTH, HEIGHT);
    rt.compile(ctx);

    rt
}

/// Build necessary components to render output from render targets.
fn build_output(
    ctx: &Vulkan,
    comp: &VkRenderCore,
    output_rt: &RenderTarget,
    sampler: &vk::Sampler,
) -> SceneState {
    let mut tex = TextureSamplerDescriptor::new();
    tex.set_image(*output_rt.get_color_view(), *sampler);
    tex.set_shader_binding(0);
    tex.set_shader_stage(ShaderStageFlags::FRAGMENT);

    let mut output_descriptors = VkDescriptorBuffer::new();
    output_descriptors.add_texture_descriptor(tex);
    output_descriptors.build(ctx);

    // build a fullscreen triangle for the background
    let geo = SimpleShapes::generate_fullscreen_triangle();

    let mut mesh = Mesh::new();
    mesh = mesh
        .add_attribute(ctx, 0, geo.positions)
        .set_num_vertices(3);

    let shader = RenderShader::new(
        PASSTHRU_VERTEX,
        load_shader2("shaders/grids/bg.frag").as_str(),
        //PASSTHRU_FRAGMENT,
        ctx,
    );

    // build a pipeline to render it
    let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
    pipeline.add_descriptor_set_layout(output_descriptors.get_descriptor_layout());
    pipeline.compile_dynamic(
        ctx,
        &comp.meta,
        &shader,
        Format::B8G8R8A8_UNORM,
        Format::D32_SFLOAT,
    );

    SceneState {
        descriptor: output_descriptors,
        scene: DisplayObject::new(vec![mesh], pipeline),
    }
}

/// Generates light information. Returns descriptor with data.
fn build_lights(vk: &Vulkan, binding: u32) -> UniformDescriptor {
    let gl = GradientLinear::new_from_hex(&SEASIDE);
    let mut lights = vec![];
    for i in 0..NUM_LIGHTS {
        let mut l = Light::new();

        l.position(rand_float(-1.0, 1.0), rand_float(-1.0, 1.0), 0.0);
        l.volume(rand_float(1.0, 60.0));
        l.intensity(0.001);
        l.falloff(rand_float(0.1, 0.2));

        // build colors
        let acol = gl.get_at(rand_float(0.1, NUM_LIGHTS as f32));
        let scol = gl.get_at(rand_float(0.1, NUM_LIGHTS as f32));
        l.ambient_color4(acol);
        l.specular_color4(scol);

        lights.push(Light::new())
    }

    let mut light_fmt = generate_descriptor_buffer_format_with_size::<Light>(NUM_LIGHTS);
    let mut light_buffer = DataBuffer::create(vk, light_fmt);
    light_buffer.set_data(vk, &lights);

    let mut desc = UniformDescriptor::new();
    desc.set_buffer_data_with_address(
        vk,
        light_buffer.raw(),
        0,
        get_byte_size_of::<Light>(NUM_LIGHTS),
    );
    desc.set_shader_binding(binding);
    desc.set_shader_stage(ShaderStageFlags::VERTEX);

    desc
}
