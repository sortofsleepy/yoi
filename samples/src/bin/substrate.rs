use crate::objects::substrate_panel::SubstratePanel;
use ash::vk::{DescriptorSetLayout, PrimitiveTopology, PushConstantRange, ShaderStageFlags};
use glam::{Mat4, Vec4};
use vsketch::core::simplesettings::SimpleSettings;
use vsketch::shaders::common::load_file;
use vsketch::utils::present_to_screen;
use vsketch::*;
use yoi_geo::simpleshapes::SimpleShapes;
use yoi_math::core::to_radians;
use yoi_vk::*;

mod objects;

struct SubstrateObject {
    engines: Vec<SubstratePanel>,
    obj: DisplayObject,
}

struct SubstrateObj {
    engine: SubstratePanel,
    obj: DisplayObject,
}

const WIDTH: u32 = 1280;
const HEIGHT: u32 = 1280;
const DIM_X: usize = 300;
const DIM_Y: usize = 300;
const MAX_NUM: usize = 50;

// Max buffer size to determine how many points to draw on the screen.
const MAX_POINTS: usize = 300000;

struct BloomSet {
    render_plane: DisplayObject,
    scene_rt: RenderTarget,
    blur_rt: RenderTarget,
    output_plane: DisplayObject,
}

struct State {
    geo_descriptors: VkDescriptorBuffer,
    processing: BloomSet,
    settings: SimpleSettings,
    geo: SubstrateObject,
}

/// A rendition of "Substrate" by Jared Tarbell.
/// http://www.complexification.net/gallery/machines/substrate/
///
/// Still some work to be done but the basics are there.
fn main() {
    VSketch::new()
        .set_size(WIDTH, HEIGHT)
        .add_setup(setup)
        .add_draw(draw)
        .run();
}

fn setup(vk: &mut Vulkan, comp: &mut VkRenderCore) -> State {
    let mut sampler = VkSampler::new();
    sampler.build(vk);

    let settings = SimpleSettings::new(vk, 2);

    let processing = build_fbos(vk, &comp.meta, &sampler);

    let descriptors = build_descriptors(vk, &settings);

    let geo = build_geometry(
        vk,
        &comp.meta,
        &descriptors.get_descriptor_layout(),
        &processing.scene_rt,
    );

    State {
        geo_descriptors: descriptors,
        processing,
        settings,
        geo,
    }
}

fn draw(vk: &mut Vulkan, comp: &mut VkRenderCore, model: &mut State) {
    let renderer = &mut comp.render;
    let meta = &mut comp.meta;
    let settings = &mut model.settings;

    comp.app_env.update();
    settings.update_time(vk, comp.app_env.get_delta_time());

    let descriptor = &mut model.geo_descriptors;

    let deng = &mut model.geo.engines;
    let dobj = &mut model.geo.obj;

    for i in 0..deng.len() {
        deng[i].update(vk);
        dobj.update_vertex_count(i, deng[i].get_num_instances() as u32);
    }

    renderer.record_commands(vk, meta, |cb| {
        let constants: Vec<Vec<f32>> = vec![vec![0.0], vec![1.0], vec![2.0]];

        dobj.render_with_constants(vk, cb, Some(descriptor), constants);
    });

    present_to_screen(vk, comp);
}

/// Build descriptor buffer for everything.
fn build_descriptors(vk: &Vulkan, settings: &SimpleSettings) -> VkDescriptorBuffer {
    let transforms = build_transforms(vk);

    let camera = build_camera(
        vk,
        0,
        -1040.0,
        60.0,
        (WIDTH as f32) / (HEIGHT as f32),
        0.1,
        100000.0,
    );

    let mut t_desc = StorageDescriptor::new();
    t_desc.set_shader_binding(1);
    t_desc.set_shader_stage(ShaderStageFlags::VERTEX);
    t_desc.set_buffer_data_with_address(vk, &transforms.raw(), 0, get_byte_size_of::<Mat4>(3));

    let mut descriptor = VkDescriptorBuffer::new();
    descriptor.add_uniform_buffer_descriptor(camera.descriptor);
    descriptor.add_uniform_buffer_descriptor(*settings.get_descriptor());
    descriptor.add_storage_descriptor(t_desc);

    descriptor.build(vk);

    descriptor
}

fn build_geometry(
    vk: &Vulkan,
    app: &VkApp,
    ds_layout: &DescriptorSetLayout,
    scene_rt: &RenderTarget,
) -> SubstrateObject {
    // build colors
    let mut colors = build_colors(MAX_POINTS);

    let shader = RenderShader::new(
        "shaders/substrate/panel.vert",
        "shaders/substrate/panel.frag",
        vk,
    );

    let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
    let mut engines = vec![];
    let mut meshes = vec![];

    for i in 0..3 {
        let mut engine = SubstratePanel::new(vk, DIM_X, DIM_Y);
        let pos_buffer = engine.get_position_buffer();

        let mesh = Mesh::new()
            .add_attrib_buffer(0, pos_buffer, get_byte_size::<Vec4>() as u32)
            .add_instanced_attribute(vk, 2, colors[0].clone());

        pipeline.add_mesh(&mesh);
        meshes.push(mesh);
        engines.push(engine);
    }

    // add push contant to store index of transform to look up
    let mut range = PushConstantRange::default()
        .stage_flags(ShaderStageFlags::VERTEX)
        .size(size_of::<f32>() as u32)
        .offset(0);

    pipeline.add_push_constant(range);

    pipeline.set_topology(PrimitiveTopology::POINT_LIST);
    pipeline.add_descriptor_set_layout(*ds_layout);
    pipeline.compile(vk, app, &shader);

    SubstrateObject {
        engines,
        obj: DisplayObject::new(meshes, pipeline),
    }
}

fn build_transforms(vk: &Vulkan) -> DataBuffer {
    let half_height = ((DIM_Y as f32) / 2.0);

    let ypos = (-half_height / 2.0) + 50.0;

    // Build rotations for each wall
    let mut transforms: Vec<Mat4> = vec![];

    // front wall
    {
        let mut matrix = Mat4::from_translation(vec3(0.0, ypos, 0.0));
        matrix *= Mat4::from_rotation_x(to_radians(180.0));
        //matrix *= Mat4::from_rotation_y(to_radians(-15.0));
        matrix *= Mat4::from_rotation_z(to_radians(45.0));
        transforms.push(matrix);
    }

    // right wall
    {
        let mut matrix = Mat4::from_translation(vec3(0.0, (-half_height / 2.0) + 50.0, 0.0));
        matrix *= Mat4::from_rotation_x(to_radians(-2.0));
        matrix *= Mat4::from_rotation_y(to_radians(0.0));
        matrix *= Mat4::from_rotation_z(to_radians(-45.0));
        transforms.push(matrix);
    }

    // left wall
    {
        let mut matrix = Mat4::from_translation(vec3(0.0, (-half_height / 2.0) + 50.0, 0.0));
        matrix *= Mat4::from_rotation_x(to_radians(0.0));
        matrix *= Mat4::from_rotation_y(to_radians(0.0));
        matrix *= Mat4::from_rotation_z(to_radians(135.0));
        transforms.push(matrix);
    }

    let mut fmt = generate_descriptor_buffer_format::<Mat4>();
    fmt.size = get_byte_size_of::<Mat4>(3);

    let mut buffer = DataBuffer::create(vk, fmt);
    buffer.set_data(vk, &transforms);

    buffer
}

/// Builds colors for everything.
fn build_colors(max_points: usize) -> Vec<Vec<Vec4>> {
    let mut color_sets = vec![NATURAL, CIRCUS, SEASIDE];
    let mut gradients = vec![];

    for i in 0..3 {
        let color_set = color_sets[i];

        let rgbs = color_set.map(|c| {
            let val = hex_to_rgb(c).expect("something went wrong");
            vec4(val[0] as f32, val[1] as f32, val[2] as f32, 1.0)
        });

        gradients.push(GradientLinear::new(rgbs.to_vec()));
    }

    // Get a random color from each color set to assign to each possible point.
    let mut point_sets = vec![];

    for i in 0..3 {
        let mut set = vec![];
        for a in 0..max_points {
            let max = if a == 0 { 1.0 } else { a as f32 };
            set.push(gradients[i].get_at_normalized(a as f32));
        }

        point_sets.push(set);
    }

    point_sets
}

// TODO - maybe re-add post processing but leaving out for now.
fn build_fbos(vk: &Vulkan, app: &VkApp, sampler: &VkSampler) -> BloomSet {
    let geo = SimpleShapes::generate_fullscreen_triangle();
    let mut scene = RenderTarget::new(WIDTH, HEIGHT);
    scene.compile(vk);

    let mut blur = RenderTarget::new(WIDTH, HEIGHT);
    blur.compile(vk);

    // build input plane
    let mut input_desc = TextureSamplerDescriptor::new();
    input_desc.set_image(*scene.get_color_view(), *sampler.raw());

    let mut output_desc = TextureSamplerDescriptor::new();
    output_desc.set_image(*blur.get_color_view(), *sampler.raw());

    ///// BUILD RENDER PLANE TO PUT SCENE INTO BLUR FBO //////
    let mut render_descriptor = VkDescriptorBuffer::new();
    render_descriptor.add_texture_descriptor(input_desc);
    render_descriptor.build(vk);

    let render_shader = RenderShader::new(
        PASSTHRU_VERTEX,
        load_file("shaders/substrate/blur.frag").as_str(),
        vk,
    );

    let render_mesh = Mesh::new()
        .add_attribute(vk, 0, geo.positions)
        .set_num_vertices(3);

    let mut render_pipeline = RenderPipeline::new_with_descriptor_buffer();
    render_pipeline.add_mesh(&render_mesh);
    render_pipeline.add_descriptor_set_layout(render_descriptor.get_descriptor_layout());
    render_pipeline.compile(vk, app, &render_shader);

    ///// BUILD OUTPUT PLANE  //////
    let mut output_descriptor = VkDescriptorBuffer::new();
    output_descriptor.add_texture_descriptor(output_desc);
    output_descriptor.build(vk);

    let output_shader = RenderShader::new(PASSTHRU_VERTEX, PASSTHRU_FRAGMENT, vk);

    let geo = SimpleShapes::generate_fullscreen_triangle();
    let output_mesh = Mesh::new()
        .add_attribute(vk, 0, geo.positions)
        .set_num_vertices(3);

    let mut output_pipeline = RenderPipeline::new_with_descriptor_buffer();
    output_pipeline.add_mesh(&output_mesh);
    output_pipeline.add_descriptor_set_layout(output_descriptor.get_descriptor_layout());
    output_pipeline.compile(vk, app, &output_shader);

    BloomSet {
        render_plane: DisplayObject::new(vec![render_mesh], render_pipeline),
        scene_rt: scene,
        blur_rt: blur,
        output_plane: DisplayObject::new(vec![output_mesh], output_pipeline),
    }
}
