use ash::vk::{CommandBuffer, ImageView, Sampler, ShaderStageFlags};
use vsketch::api::api::DisplayObjectSet;
use vsketch::{DisplayObject, PASSTHRU_VERTEX};
use yoi_geo::simpleshapes::SimpleShapes;
use yoi_vk::{
    Descriptor, Mesh, RenderPipeline, RenderShader, RenderTarget, TextureSamplerDescriptor,
    UniformDescriptor, VkApp, VkDescriptorBuffer, VkObject, Vulkan,
};

pub struct PingPong {
    index: usize,
    initialized: bool,
    buffers: Vec<RenderTarget>,
    planes: Vec<DisplayObjectSet>,
    descriptor: VkDescriptorBuffer,
    uniforms: Vec<UniformDescriptor>,
    textures: Vec<TextureSamplerDescriptor>,
}

impl PingPong {
    pub fn new(vk: &Vulkan, width: u32, height: u32) -> Self {
        let mut rt1 = RenderTarget::new(width, height);
        rt1.compile(vk);

        let mut rt2 = RenderTarget::new(width, height);
        rt2.compile(vk);

        PingPong {
            index: 0,
            initialized: false,
            descriptor: VkDescriptorBuffer::new(),
            buffers: vec![rt1, rt2],
            planes: vec![],
            uniforms: vec![],
            textures: vec![],
        }
    }

    pub fn add_uniform_descriptor(&mut self, desc: UniformDescriptor) {
        self.uniforms.push(desc);
    }

    pub fn add_texture_descriptor(&mut self, desc: TextureSamplerDescriptor) {
        self.textures.push(desc);
    }

    /// initializes the ping pong pass.
    pub fn init(
        &mut self,
        ctx: &Vulkan,
        app: &VkApp,
        sampler: &Sampler,
        fragment: &str,
        input: &ImageView,
    ) {
        let mut uniforms = &mut self.uniforms;
        let textures = &self.textures;

        // compile planes to render for each offscreen pass.
        for i in 0..2 {
            // setup main input - binding 0
            let mut input_tex = TextureSamplerDescriptor::new();
            input_tex.set_image(*input, *sampler);

            // setup input from previous pass - binding 1
            let mut tex = TextureSamplerDescriptor::new();
            if i == 0 {
                tex.set_image(*self.buffers[1].get_color_view(), *sampler);
            } else {
                tex.set_image(*self.buffers[0].get_color_view(), *sampler);
            }

            tex.set_shader_binding(1);

            // build render plane
            let plane = SimpleShapes::generate_fullscreen_triangle();
            let mesh = Mesh::new()
                .add_attribute(ctx, 0, plane.positions)
                .set_num_vertices(3);

            // setup shader
            let shader = RenderShader::new(PASSTHRU_VERTEX, fragment, ctx);

            // setup descriptors
            let mut descriptor = VkDescriptorBuffer::new();
            descriptor.add_texture_descriptor(input_tex);
            descriptor.add_texture_descriptor(tex);

            for uni in uniforms.iter_mut() {
                uni.set_shader_stage(ShaderStageFlags::FRAGMENT);
                descriptor.add_uniform_buffer_descriptor(*uni);
            }

            for t in textures {
                descriptor.add_texture_descriptor(*t);
            }

            descriptor.build(ctx);

            // build pipeline.
            let mut pipeline = RenderPipeline::new();
            pipeline.add_mesh(&mesh);
            pipeline.add_descriptor_set_layout(descriptor.get_descriptor_layout());
            pipeline.compile_with_renderpass(
                ctx,
                app,
                &shader,
                self.buffers[i].get_renderpass().raw(),
            );

            self.planes.push(DisplayObjectSet {
                object: DisplayObject::new(vec![mesh], pipeline),
                descriptor,
            });
        }

        self.initialized = true;
    }

    /// Updates the buffers
    pub fn update(&self, vk: &Vulkan, cb: &CommandBuffer) {
        if !self.initialized {
            return;
        }
        let index = self.index;

        let current = &self.buffers[index];
        let plane = &self.planes[index].object;
        let descriptor = &self.planes[index].descriptor;

        current.begin(vk, cb);
        plane.render(vk, cb, Some(descriptor));
        current.end(vk, cb);
    }

    /// Swap buffer index to use.
    pub fn swap(&mut self) {
        self.index = 1 - self.index;
    }
}
