use ash::vk;
use ash::vk::*;
use glam::Vec4;
use vsketch::utils::generate_push_constant_range;
use vsketch::{DisplayObject, SceneState, VSketch, VkRenderCore, PASSTHRU_VERTEX};
use yoi_geo::simpleshapes::SimpleShapes;
use yoi_vk::descriptors::renderpass_descriptor::RenderPassDescriptor;
use yoi_vk::*;

struct State {
    scene: SceneState,
    rp: RenderPass,
    framebuffers: Vec<Framebuffer>,
    time: f32,
}

const WIDTH: u32 = 1280;
const HEIGHT: u32 = 1280;

/// A very simple example of how to use render passes for rendering output.
/// Also shows off push constants as well.
fn main() {
    VSketch::new()
        .set_size(WIDTH, HEIGHT)
        .add_setup(setup)
        .add_draw(draw)
        .run();
}

fn setup(vk: &mut Vulkan, comp: &mut VkRenderCore) -> State {
    let mut sampler = VkSampler::new();
    sampler.build(vk);

    ////////// BUILD RENDERPASS /////////////////
    let mut color_desc = RenderPassDescriptor::default();
    color_desc.color_initial_layout = ImageLayout::GENERAL;

    let descriptions = &[AttachmentDescription {
        flags: Default::default(),
        format: color_desc.color_format,
        load_op: color_desc.load_op,
        store_op: color_desc.store_op,
        samples: vk::SampleCountFlags::TYPE_1,
        stencil_load_op: color_desc.stencil_load_op,
        stencil_store_op: color_desc.stencil_store_op,
        initial_layout: ImageLayout::UNDEFINED,
        final_layout: color_desc.color_final_layout,

        ..Default::default()
    }];

    let attachment_ref = vec![AttachmentReference {
        attachment: 0,
        layout: ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
    }];

    let subpass_desc = &[SubpassDescription::default()
        .pipeline_bind_point(PipelineBindPoint::GRAPHICS)
        .color_attachments(&attachment_ref[..])];

    let rp_info = vk::RenderPassCreateInfo::default()
        .attachments(&descriptions[..])
        .subpasses(&subpass_desc[..]);

    let rp = vk.generate_renderpass(rp_info).unwrap();
    ////////// BUILD FBO ////////////////

    let mut framebuffers = vec![];
    let swapchain = comp.meta.get_swapchain();

    for i in 0..3 {
        let attachments = &swapchain.get_image_views()[i];
        let attachment = &[*attachments];

        let create_info = FramebufferCreateInfo::default()
            .width(WIDTH)
            .height(HEIGHT)
            .render_pass(rp)
            .layers(1)
            .attachments(attachment);

        let fb = vk.generate_framebuffer(create_info);

        framebuffers.push(fb);
    }

    let scene = build_scene(vk, &comp.meta, &rp);

    State {
        scene,
        rp,
        framebuffers,
        time: 0.0,
    }
}

fn draw(vk: &mut Vulkan, comp: &mut VkRenderCore, model: &mut State) {
    let renderer = &mut comp.render;
    let meta = &mut comp.meta;
    let swapchain = meta.get_swapchain();

    let mut rp = &mut model.rp;
    let fb = &model.framebuffers;

    let scene = &model.scene;

    renderer.record_commands_with_renderpass_and_index(vk, |cb, i| {
        let fbo = model.framebuffers[i];

        let scissor = Rect2D {
            offset: Offset2D { x: 0, y: 0 },
            extent: Extent2D {
                width: WIDTH,
                height: HEIGHT,
            },
        };

        let clear_values = vec![ClearValue {
            color: ClearColorValue {
                float32: [0.0, 0.0, 0.0, 1.0],
            },
        }];

        let viewport = Viewport {
            x: 0.0,
            y: 0.0,
            width: WIDTH as f32,
            height: HEIGHT as f32,
            min_depth: 0.0,
            max_depth: 1.0,
        };

        let render_area = vk::Rect2D {
            offset: vk::Offset2D { x: 0, y: 0 },
            extent: swapchain.get_extent(),
        };

        let renderpass_info = vk::RenderPassBeginInfo {
            render_pass: *rp,
            framebuffer: fbo,
            render_area,
            clear_value_count: 3,
            p_clear_values: clear_values.as_ptr(),
            ..Default::default()
        };

        vk.set_viewport(*cb, 0, &[viewport]);
        vk.set_scissor(*cb, 0, &[scissor]);

        vk.start_renderpass(cb, &renderpass_info);

        let consts: Vec<Vec<Vec4>> = vec![vec![Vec4::new(model.time, 20.0, 0.0, 0.08)]];
        scene.render_with_push_constants(vk, cb, consts, ShaderStageFlags::FRAGMENT);

        vk.end_renderpass(cb);
    });

    renderer.present(vk, swapchain);
    renderer.finish_present();

    model.time += 0.1;
}

fn build_scene(vk: &Vulkan, app: &VkApp, rt: &RenderPass) -> SceneState {
    let mut sampler = VkSampler::new();
    sampler.build(vk);

    let mut mesh = Mesh::new().set_num_vertices(3);

    let shader = RenderShader::new(
        "shaders/renderpass/scene.vert",
        "shaders/renderpass/scene.frag",
        vk,
    );

    // build a pipeline to render it
    let mut pipeline = RenderPipeline::new();
    let push = generate_push_constant_range::<Vec4>(ShaderStageFlags::FRAGMENT, 0);

    pipeline.add_push_constant(push);
    //pipeline.compile2(vk, app, &shader, Some(rt.get_renderpass().raw()));
    pipeline.compile2(vk, app, &shader, Some(rt));

    SceneState {
        descriptor: None,
        scene: DisplayObject::new(vec![mesh], pipeline),
    }
}

/// Build necessary components to render output from render targets.
fn build_output(ctx: &Vulkan, comp: &VkRenderCore, output_rt: &RenderTarget2) -> SceneState {
    let mut sampler = VkSampler::new();
    sampler.build(ctx);

    let mut tex = TextureSamplerDescriptor::new();
    tex.set_image(*output_rt.get_color_view(), *sampler.raw());
    tex.set_shader_binding(0);
    tex.set_shader_stage(ShaderStageFlags::FRAGMENT);

    let mut output_descriptors = VkDescriptorBuffer::new();
    output_descriptors.add_texture_descriptor(tex);
    output_descriptors.build(ctx);

    // build a fullscreen triangle for the background
    let geo = SimpleShapes::generate_fullscreen_triangle();

    let mut mesh = Mesh::new();
    mesh = mesh
        .add_attribute(ctx, 0, geo.positions)
        .set_num_vertices(3);

    let shader = RenderShader::new(PASSTHRU_VERTEX, "shaders/flood/output.frag", ctx);

    // build a pipeline to render it
    let mut pipeline = RenderPipeline::new();
    pipeline.add_descriptor_set_layout(output_descriptors.get_descriptor_layout());
    pipeline.compile_dynamic(
        ctx,
        &comp.meta,
        &shader,
        Format::B8G8R8A8_UNORM,
        Format::D32_SFLOAT,
    );

    SceneState {
        descriptor: Some(output_descriptors),
        scene: DisplayObject::new(vec![mesh], pipeline),
    }
}

/*
let renderer = &mut comp.render;
   let meta = &mut comp.meta;


   let tex = &model.tex;
   let targets = &model.targets;

   let scene = &model.scene;

   let time = model.time;
   let output = &mut model.output.scene;
   let odesc = &mut model.output.descriptor;
   renderer.record_commands_with_renderpass(vk, meta, |cb| {

       insert_image_memory_barrier(
           vk,
           cb,
           tex.get_image(),
           AccessFlags::empty(),
           AccessFlags::COLOR_ATTACHMENT_READ | AccessFlags::COLOR_ATTACHMENT_WRITE | AccessFlags::SHADER_READ | AccessFlags::SHADER_WRITE,
           ImageLayout::UNDEFINED,
           ImageLayout::GENERAL,
           generate_subresource_range(false),
           PipelineStageFlags::ALL_GRAPHICS,
           PipelineStageFlags::ALL_GRAPHICS,
       );

       targets[0].begin(vk, cb);

       let consts: Vec<Vec<Vec4>> = vec![
           vec![Vec4::new(time, 20.0, 0.0, 0.08)]
       ];
       scene.render_with_push_constants(vk, cb, consts, ShaderStageFlags::FRAGMENT);


       targets[0].end(vk, cb);



   });

   // render the output
   renderer.record_commands(vk, meta, |cb| {
       output.render(vk, cb, odesc.as_ref());
   });

   /////////////////////////////////////////////////////////////////////////////////////////

   renderer.present(vk, meta.get_swapchain());
   renderer.finish_present();

   model.time += 0.1;
*/
