use ash::vk::{
    DescriptorSetLayout, MemoryAllocateFlags, Sampler, Semaphore, SemaphoreCreateInfo,
    ShaderStageFlags,
};
use glam::Vec4;
use vsketch::api::api::{ComputeObject, DisplayObjectSet};
use vsketch::utils::{generate_push_constant_range, present_to_screen_with_compute, set_clear};
use vsketch::*;
use yoi_geo::csphere::Sphere;
use yoi_geo::planegeometry::PlaneGeometry;
use yoi_geo::simpleshapes::SimpleShapes;
use yoi_math::core::normalize_rgba_color;
use yoi_math::{rand_float, rand_value, rand_vec4};
use yoi_vk::*;

mod objects;

const WIDTH: u32 = 1280;
const HEIGHT: u32 = 1280;

/// Settings for the simulation
#[derive(Copy, Clone)]
struct Settings {
    // x = golden_angle
    // y = spread
    settings: Vec4,
}

// Phyllotaxis
// Based on work by Deborah Fowler
// https://www.deborahrfowler.com/WebGL/WebGLExamples/Phyllotaxis/phylloWebgl.html

const NUM_PARTICLES: usize = 10000;

struct ComputeStack {
    pipeline: ComputePipeline,
    particle_data: DataBuffer,
}

/// Particle object that makes up the simulation.
#[derive(Copy, Clone)]
struct Particle {
    pub pos: Vec4,
    pub vel: Vec4,
    pub original_vel: Vec4,
    pub meta: Vec4,
}

struct State {
    particles: DisplayObject,
    descriptor: VkDescriptorBuffer,
    compute: ComputeObject,
    graphics_semaphore: Semaphore,
    output: Vec<DisplayObjectSet>,
    edge_output: DisplayObjectSet,
    final_composite: DisplayObjectSet,
    rt: Vec<RenderTarget>,
    clear_color: Vec4,
}

fn main() {
    VSketch::new()
        .set_size(WIDTH, HEIGHT)
        .add_setup(setup)
        .add_draw(draw)
        .run();
}

fn setup(vk: &mut Vulkan, comp: &mut VkRenderCore) -> State {
    let cb = generate_command_buffer(vk);
    let descriptors = build_descriptors(vk);

    let mut sampler = VkSampler::new();
    sampler.build(&vk);

    let post = build_post(vk);

    let graphics_semaphore = vk.generate_semaphore(SemaphoreCreateInfo {
        ..Default::default()
    });

    let compute = build_compute(vk);
    let geo = build_geo(
        vk,
        &comp.meta,
        &descriptors.get_descriptor_layout(),
        &compute.buffers[0],
        &post[0],
    );

    let output_scene = build_output(
        vk,
        &comp.meta,
        sampler.raw(),
        &post[0].get_color_view(),
        &post[1],
    );
    let edge_output = build_edge_output(
        vk,
        &comp.meta,
        sampler.raw(),
        &post[1].get_color_view(),
        &post[2],
    );
    let final_comp = build_final_composite(
        vk,
        &comp.meta,
        sampler.raw(),
        &post[0].get_color_view(),
        &post[2].get_color_view(),
    );
    let pallet = build_colors();

    let mut clear = pallet.get_at(rand_float(0.0, 6.0));
    clear = normalize_rgba_color(clear);
    State {
        particles: geo,
        descriptor: descriptors,
        compute,
        graphics_semaphore,
        output: output_scene,
        edge_output,
        final_composite: final_comp,
        rt: post,
        clear_color: clear,
    }
}

fn draw(vk: &mut Vulkan, comp: &mut VkRenderCore, model: &mut State) {
    let renderer = &mut comp.render;
    let meta = &mut comp.meta;
    let mut env = &mut comp.app_env;
    env.update();
    let time = env.get_delta_time();

    let main_descriptors = &mut model.descriptor;

    // particles
    let particles = &mut model.particles;

    // compute pipeline
    let compute_pipeline = &mut model.compute.pipeline;
    let compute_descriptor = &mut model.compute.descriptor;

    // semaphore
    let graphics_semaphore = &mut model.graphics_semaphore;

    // outputs
    let outputs = &mut model.output;

    let targets = &mut model.rt;
    let scene = &targets[0];
    let pre_blur_comp = &targets[1];
    let edge_comp = &targets[2];

    let edge_output = &mut model.edge_output;
    let final_comp = &mut model.final_composite;

    let clear_color = &mut model.clear_color;
    // Render scene
    renderer.record_commands_with_renderpass(vk, meta, |cb| {
        {
            scene.begin(vk, cb);
            set_clear(
                vk,
                cb,
                [clear_color.x, clear_color.y, clear_color.z, 1.0],
                Extent2D {
                    width: 1280,
                    height: 1280,
                },
            );
            particles.render(vk, cb, Some(main_descriptors));
            scene.end(vk, cb);
        }

        {
            pre_blur_comp.begin(vk, cb);

            // render the background
            outputs[0]
                .object
                .render(vk, cb, Some(&outputs[0].descriptor));

            // render the panels.
            // x = time
            // y = max scale
            // z = id; 0 = middle, 1 = foreground
            // w = offset

            let constants: Vec<Vec<Vec4>> = vec![
                vec![Vec4::new(time, 20.0, 0.0, 0.08)],
                vec![Vec4::new(time, 30.5, 1.0, 1.2)],
            ];
            outputs[1].object.render_with_constants(
                vk,
                cb,
                Some(&outputs[1].descriptor),
                constants,
            );

            pre_blur_comp.end(vk, cb);
        }

        {
            edge_comp.begin(vk, cb);
            edge_output
                .object
                .render(vk, cb, Some(&edge_output.descriptor));
            edge_comp.end(vk, cb);
        }
    });

    // render output
    renderer.record_commands(vk, meta, |cb| {
        final_comp
            .object
            .render(vk, cb, Some(&final_comp.descriptor))
    });

    // Run compute
    {
        let constants: Vec<Vec4> = vec![Vec4::new(90.5, 1.0, env.get_delta_time(), 1.0)];
        compute_pipeline.run_with_push_constants(vk, compute_descriptor, constants);
    }

    present_to_screen_with_compute(vk, comp, compute_pipeline, graphics_semaphore);
}

/// Build descriptors
fn build_descriptors(vk: &Vulkan) -> VkDescriptorBuffer {
    let camera = build_camera(
        vk,
        0,
        -205.5,
        60.0,
        (WIDTH as f32) / (HEIGHT as f32),
        0.1,
        1000.0,
    );

    let mut descriptor = VkDescriptorBuffer::new();
    descriptor.add_uniform_buffer_descriptor(camera.descriptor);

    descriptor.build(vk);

    descriptor
}

/// Builds the final composite
fn build_final_composite(
    vk: &Vulkan,
    app: &VkApp,
    sampler: &Sampler,
    original_image: &ImageView,
    final_texture: &ImageView,
) -> DisplayObjectSet {
    let geo = SimpleShapes::generate_fullscreen_triangle();
    let shader = RenderShader::new(PASSTHRU_VERTEX, "shaders/phylo/final_composite.frag", vk);

    let mesh = Mesh::new()
        .add_attribute(vk, 0, geo.positions)
        .set_num_vertices(3);

    let mut tex = TextureSamplerDescriptor::new();
    tex.set_image(*original_image, *sampler);
    tex.set_shader_binding(0);

    let mut tex2 = TextureSamplerDescriptor::new();
    tex2.set_image(*final_texture, *sampler);
    tex2.set_shader_binding(1);

    let mut descriptor = VkDescriptorBuffer::new();
    descriptor.add_texture_descriptor(tex);
    descriptor.add_texture_descriptor(tex2);
    descriptor.build(vk);

    let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
    pipeline.add_mesh(&mesh);
    pipeline.enable_alpha_blending();
    pipeline.add_descriptor_set_layout(descriptor.get_descriptor_layout());
    pipeline.compile(vk, app, &shader);

    DisplayObjectSet {
        object: DisplayObject::new(vec![mesh], pipeline),
        descriptor,
    }
}

fn build_edge_output(
    vk: &Vulkan,
    app: &VkApp,
    sampler: &Sampler,
    final_texture: &ImageView,
    renderpass: &RenderTarget,
) -> DisplayObjectSet {
    let geo = SimpleShapes::generate_fullscreen_triangle();
    let shader = RenderShader::new(PASSTHRU_VERTEX, "shaders/phylo/edge_process.glsl", vk);

    let mesh = Mesh::new()
        .add_attribute(vk, 0, geo.positions)
        .set_num_vertices(3);

    let mut tex = TextureSamplerDescriptor::new();
    tex.set_image(*final_texture, *sampler);
    tex.set_shader_binding(0);

    let mut descriptor = VkDescriptorBuffer::new();
    descriptor.add_texture_descriptor(tex);
    descriptor.build(vk);

    let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
    pipeline.add_mesh(&mesh);
    pipeline.enable_additive_blending();
    pipeline.add_descriptor_set_layout(descriptor.get_descriptor_layout());
    //pipeline.compile(vk, app, &shader);
    pipeline.compile_with_renderpass(vk, app, &shader, renderpass.get_renderpass().raw());

    DisplayObjectSet {
        object: DisplayObject::new(vec![mesh], pipeline),
        descriptor,
    }
}

/// Builds display geometry
fn build_geo(
    vk: &Vulkan,
    app: &VkApp,
    descriptors: &DescriptorSetLayout,
    position: &DataBuffer,
    scene_target: &RenderTarget,
) -> DisplayObject {
    let geo = SimpleShapes::generate_cube();
    let mut meta = vec![];
    let mut colors = vec![];
    let pallet = build_colors();

    for i in 0..NUM_PARTICLES {
        meta.push(vec4(
            rand_float(0.1, 0.3),
            rand_float(0.5, 1.2),
            rand_float(1.0, 4.0),
            0.0,
        ));

        colors.push(pallet.get_at(rand_value()));
    }

    let mesh = Mesh::new()
        .add_attribute(vk, 0, geo.positions)
        .add_index_data(vk, geo.indices)
        .add_instanced_attribute(vk, 2, meta)
        .add_instanced_attribute(vk, 3, colors)
        .add_instanced_attrib_buffer(1, position, get_byte_size::<Particle>() as u32)
        .set_num_instances(NUM_PARTICLES as u32);

    let shader = RenderShader::new("shaders/phylo/render.vert", "shaders/phylo/render.frag", vk);

    let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
    pipeline.add_mesh(&mesh);
    pipeline.add_descriptor_set_layout(*descriptors);

    pipeline.enable_depth_test();
    pipeline.enable_depth_write();
    pipeline.compile_with_renderpass(vk, app, &shader, scene_target.get_renderpass().raw());

    DisplayObject::new(vec![mesh], pipeline)
}

/// Build geometry for final output
fn build_output(
    vk: &Vulkan,
    app: &VkApp,
    sampler: &Sampler,
    scene_texture: &ImageView,
    renderpass: &RenderTarget,
) -> Vec<DisplayObjectSet> {
    let mut output = vec![];

    let camera = build_camera(
        vk,
        0,
        -100.0,
        60.0,
        (WIDTH as f32) / (HEIGHT as f32),
        0.1,
        1000.0,
    );

    let shader = RenderShader::new("shaders/phylo/layer.vert", "shaders/phylo/layer.frag", vk);

    // Build full screen background
    {
        let geo = SimpleShapes::generate_fullscreen_triangle();
        let shader = RenderShader::new(PASSTHRU_VERTEX, PASSTHRU_FRAGMENT, vk);

        let mesh = Mesh::new()
            .add_attribute(vk, 0, geo.positions)
            .set_num_vertices(3);

        let mut tex = TextureSamplerDescriptor::new();
        tex.set_image(*scene_texture, *sampler);
        tex.set_shader_binding(0);

        let mut descriptor = VkDescriptorBuffer::new();
        descriptor.add_texture_descriptor(tex);
        descriptor.build(vk);

        let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
        pipeline.add_mesh(&mesh);
        pipeline.enable_additive_blending();
        pipeline.add_descriptor_set_layout(descriptor.get_descriptor_layout());
        pipeline.compile_with_renderpass(vk, app, &shader, renderpass.get_renderpass().raw());

        output.push(DisplayObjectSet {
            object: DisplayObject::new(vec![mesh], pipeline),
            descriptor,
        })
    }

    {
        let geo = PlaneGeometry::create((WIDTH / 15) as f32, (HEIGHT / 15) as f32, 4.0, 4.0);
        let pos = geo.positions.clone();
        let uvs = geo.uvs;
        let len = pos.len();
        let mesh = Mesh::new()
            .add_attribute(vk, 0, pos)
            .add_index_data(vk, geo.indices)
            .set_num_vertices(len as u32);

        let geo = PlaneGeometry::create(
            ((WIDTH / 15) / 2) as f32,
            ((HEIGHT / 15) / 2) as f32,
            4.0,
            4.0,
        );
        let pos = geo.positions.clone();

        let len = pos.len();
        let mesh2 = Mesh::new()
            .add_attribute(vk, 0, pos)
            .add_index_data(vk, geo.indices)
            .set_num_vertices(len as u32);

        let mut tex = TextureSamplerDescriptor::new();
        tex.set_image(*scene_texture, *sampler);
        tex.set_shader_binding(1);

        let mut descriptor = VkDescriptorBuffer::new();
        descriptor.add_uniform_buffer_descriptor(camera.descriptor);
        descriptor.add_texture_descriptor(tex);
        descriptor.build(vk);

        let push = generate_push_constant_range::<Vec4>(ShaderStageFlags::VERTEX, 0);

        let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
        pipeline.add_mesh(&mesh);
        pipeline.add_mesh(&mesh2);
        pipeline.enable_depth_write();
        pipeline.enable_depth_test();
        pipeline.enable_additive_blending();
        pipeline.add_push_constant(push);
        pipeline.add_descriptor_set_layout(descriptor.get_descriptor_layout());
        pipeline.compile_with_renderpass(vk, app, &shader, renderpass.get_renderpass().raw());

        output.push(DisplayObjectSet {
            object: DisplayObject::new(vec![mesh, mesh2], pipeline),
            descriptor,
        })
    }

    output
}

fn build_colors() -> GradientLinear {
    let rgbs = WARM2.map(|c| {
        let val = hex_to_rgb(c).expect("something went wrong");
        vec4(val[0] as f32, val[1] as f32, val[2] as f32, 1.0)
    });

    GradientLinear::new(rgbs.to_vec())
}

/// Sets up the compute pipeline
fn build_compute(vk: &Vulkan) -> ComputeObject {
    let cb = generate_command_buffer(&vk);

    let mut descriptor = VkDescriptorBuffer::new();

    //// BUILD PARTICLES /////
    let mut particles: Vec<Particle> = Vec::<Particle>::new();

    for i in 0..NUM_PARTICLES {
        let mut vel = rand_vec4();

        particles.push(Particle {
            pos: Vec4::new(0.0, 0.0, 0.0, 1.0),
            vel,
            original_vel: vel,
            // x = life decay
            meta: Vec4::new(rand_float(0.09, 0.2), rand_value() * 0.04, 0.0, 1.),
        })
    }

    let size = get_byte_size_of::<Particle>(particles.len());

    // setup staging buffer
    let mut stage_fmt = BufferFormat::default();
    stage_fmt.usage_flags = BufferUsageFlags::TRANSFER_SRC;
    stage_fmt.size = size;

    let mut stage_buffer = DataBuffer::create(vk, stage_fmt);
    stage_buffer.set_data(vk, &particles);

    // setup buffer to transfer into
    let mut particle_fmt = generate_descriptor_buffer_format::<Particle>();
    particle_fmt.size = size;
    particle_fmt.usage_flags = BufferUsageFlags::TRANSFER_DST
        | BufferUsageFlags::STORAGE_BUFFER
        | BufferUsageFlags::VERTEX_BUFFER
        | BufferUsageFlags::SHADER_DEVICE_ADDRESS;
    particle_fmt.mem_alloc_flags = MemoryAllocateFlags::DEVICE_ADDRESS;

    let particle_buffer = DataBuffer::create(vk, particle_fmt);

    copy_buffer_to_buffer(vk, &cb, stage_buffer.raw(), particle_buffer.raw(), size);

    //// SETUP DESCRIPTOR FOR DATA ////
    let mut storage = StorageDescriptor::new();
    storage.set_shader_stage(ShaderStageFlags::COMPUTE);
    // storage.set_buffer_debug(particle_buffer.raw(), 0, (size_of::<Particle>() * NUM_PARTICLES) as u64);
    storage.set_buffer_data_with_address(vk, particle_buffer.raw(), 0, stage_fmt.size);
    storage.set_shader_binding(0);

    descriptor.add_storage_descriptor(storage);

    descriptor.build(vk);

    ////// SETUP PUSH CONSTANTS ////

    let range = generate_push_constant_range::<Vec4>(ShaderStageFlags::COMPUTE, 0);

    let shader = ComputeShader::new("shaders/phylo/compute.glsl", vk);

    //// BUILD PIPELINE ////
    let mut pipeline = ComputePipeline::new_with_descriptor_buffer(vk);
    pipeline.add_push_constant(range);
    pipeline.set_dispatch_size([(NUM_PARTICLES / 100) as u32, 1, 1]);
    pipeline.compile(
        vk,
        shader.generate_pipeline_info(),
        vec![descriptor.get_descriptor_layout()],
    );

    ComputeObject {
        pipeline,
        descriptor,
        buffers: vec![particle_buffer],
    }
}

/// Builds RenderTargets to use for post processing.
/// Currently
/// - 1 for storing scene (particles)
/// - 1 to store composite
/// - 1 to post process the edges
/// - 1 to output the final final image.
pub fn build_post(vk: &Vulkan) -> Vec<RenderTarget> {
    let mut targets = vec![];

    for i in 0..3 {
        let mut rt = RenderTarget::new(WIDTH, HEIGHT);
        rt.compile(vk);
        targets.push(rt)
    }

    targets
}
