pub const ROTATE_X: &str = "vec3 rotateX(vec3 p, float theta){
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x, p.y * c - p.z * s, p.z * c + p.y * s);
}";

pub const ROTATE_Y: &str = "
vec3 rotateY(vec3 p, float theta) {
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x * c + p.z * s, p.y, p.z * c - p.x * s);
}";

pub const ROTATE_Z: &str = "
vec3 rotateZ(vec3 p, float theta) {
  float s = sin(theta);
  float c = cos(theta);
  return vec3(p.x * c - p.y * s, p.y * c + p.x * s, p.z);
}";

pub const ROTATE_MATRIX_MAT3:&str = "\
mat3 rotationMatrix(vec3 axis, float angle) {
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat3(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s, oc * axis.z * axis.x + axis.y * s,
                oc * axis.x * axis.y + axis.z * s,   oc * axis.y * axis.y + c,         oc * axis.y * axis.z - axis.x * s,
                oc * axis.z * axis.x - axis.y * s,   oc * axis.y * axis.z + axis.x * s, oc * axis.z * axis.z + c);
}
";
