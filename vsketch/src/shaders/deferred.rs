pub fn generate_gbuffer_vert() {
    "";
}

pub const LIGHT_STRUCT: &str = "struct Light
{
	vec4	ambient;
	vec4	diffuse;
	vec4	specular;
	vec3	position;
	float	intensity;
	float	radius;
	float	volume;
	uint	pad0;
	uint	pad1;
};";

/// Default gbuffer vertex shader
pub const DEFAULT_GBUFFER_VERT: &str = "#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} ubo;


layout(binding = 1) uniform NormalObject {
    mat4 normalMatrix;
} normal_mat;

layout(binding = 2) buffer NormalObject {
    Light lights[];
}lgts;


layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;

layout(location = 0) in vec4 vPos;
layout(location = 1) in vec4 vNormal;


void main(){

    vNormal = normal_mat.normalMatrix * normal;
    gl_Position = ubo.proj * ubo.view *  position;
}";

/// Default gbuffer fragment shader
pub const DEFAULT_GBUFFER_FRAG: &str = "#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) out vec4 oAlbedo;
layout (location = 1) out vec4 oNormalEmissive;
layout (location = 2) out vec4 oPosition;

void main(){

    oAlbedo = vec4(1.0,0.0,0.0,1.0);
    oNormalEmissive = vec4(1.0,1.0,0.0,1.0);
    oPosition = vec4(1.0,0.0,1.0,1.0);


}";
