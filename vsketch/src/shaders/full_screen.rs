/// a pass-thru vertex shader used for rendering a large full screen triangle.
/// Pass-thru shader for rendering a full screen quad.
pub const PASSTHRU_VERTEX: &str = "#version 450
layout(location = 0) out vec2 uv;
out gl_PerVertex
{
	vec4 gl_Position;
};
void main()
{
    vec2 scale = vec2(0.5);
    //vec4 pos = vec4(vec2((gl_VertexIndex << 1) & 2, gl_VertexIndex & 2) * 3.0f - 1.0f, 0.0f, 1.0f);
    uv = vec2((gl_VertexIndex << 1) & 2, gl_VertexIndex & 2);
    gl_Position = vec4(uv * 2.0f + -1.0f, 0.0f, 1.0f);
}";

/// A pass-thru fragment shader - for use when you want to show a full screen image.
pub const PASSTHRU_FRAGMENT: &str = "#version 450
layout (set = 0, binding = 0) uniform sampler2D tInput;
layout(location = 0) out vec4 glFragColor;
layout(location = 0) in vec2 uv;

void main()
{
    vec4 color = texture(tInput,uv);
    glFragColor = color;
    //glFragColor = vec4(vec2(uv.x, 1.0 - uv.y),0.0,1.0);


}";
