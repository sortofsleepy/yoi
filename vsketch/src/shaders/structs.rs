const LIGHTS: &str = "\
struct Light {
    vec4 pos;
    vec4 ambient_color;
    vec4 diffuse_color;
    vec4 specular_color;
    float falloff;
    float intensity;
    float radius;
    float volume;
};
";
