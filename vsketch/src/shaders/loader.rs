use crate::shaders::common::load_file;
use crate::shaders::rotate_components::{ROTATE_X, ROTATE_Y, ROTATE_Z};
use crate::{simplex_3d, CURL_NOISE, RAND_HELPERS};
use std::env;
use std::path::Path;

struct ShaderFunc {
    name: String,
    func: String,
}

/// Reads a shader file and inserts content indicated by the specified preprocessor directives.
/// Include symbol is "//- <path to file>". You can also use #include
/// The way it works is that things are read line-by-line. If a preprocessor directive is encountered,
/// it attempts to process it by either loading the specified file or by inserting one of the included source code.
pub fn load_shader2(path: &str) -> String {
    let parent = Path::new(".");
    let mut output = String::new();

    let helpers: Vec<ShaderFunc> = vec![
        ShaderFunc {
            name: String::from("simplex_3d"),
            func: simplex_3d().to_string(),
        },
        ShaderFunc {
            name: String::from("rand_helpers"),
            func: RAND_HELPERS.to_string(),
        },
        ShaderFunc {
            name: String::from("curl_noise"),
            func: CURL_NOISE.to_string(),
        },
        ShaderFunc {
            name: String::from("rotate_x"),
            func: ROTATE_X.to_string(),
        },
        ShaderFunc {
            name: String::from("rotate_y"),
            func: ROTATE_Y.to_string(),
        },
        ShaderFunc {
            name: String::from("rotate_z"),
            func: ROTATE_Z.to_string(),
        },
    ];
    gather_includes(path, parent, &mut output, &helpers);

    output
}

/// Recursively iterates through a file looking for #include statements and appending the contents line-by-line to
/// [output]. Paths passed to the statements should be relative to the initial shader path you pass in.
fn gather_includes(path: &str, current_dir: &Path, output: &mut String, helpers: &Vec<ShaderFunc>) {
    let resolved_path = current_dir.join(path);
    let resolved_path = resolved_path.canonicalize().unwrap();

    let content = load_file(resolved_path.to_str().unwrap());

    let mut lines = content.lines();
    while let Some(line) = lines.next() {
        if line.starts_with("#include") || line.starts_with("//-") {
            let included_path = line
                .trim_start_matches("//-")
                .trim()
                .trim_start_matches("#include")
                .trim()
                .replace("\"", "");
            let dir = resolved_path.parent().unwrap();

            let mut found = false;
            for helper in helpers {
                if line.contains(helper.name.as_str()) {
                    found = true;
                    output.push_str(helper.func.as_str());
                    output.push_str("\n");
                }
            }

            if !found {
                gather_includes(included_path.as_str(), dir, output, helpers);
            }
        } else {
            output.push_str(line);
            output.push_str("\n");
        }
    }
}

/// Reads a shader file and inserts content indicated by the specified preprocessor directives.
/// Include symbol is "//- <path to file>"
/// The way it works is that things are read line-by-line. If a preprocessor directive is encountered,
/// it attempts to process it by either loading the specified file or by inserting one of the included source code.
#[deprecated]
pub fn load_shader(path: &str) -> String {
    let mut file = load_file(path);

    let mut lines = Vec::new();

    // Go through each line and check for special lookups; if line contains
    // "//-<filepath>" or "#include filepath", replace that with the contents of that file.
    // "//-<constant name>" replace that line with the contents of the the constant values in this package.
    file.lines().for_each(|line| {
        let funcs = vec![
            ShaderFunc {
                name: String::from("simplex_3d"),
                func: simplex_3d().to_string(),
            },
            ShaderFunc {
                name: String::from("rand_helpers"),
                func: RAND_HELPERS.to_string(),
            },
            ShaderFunc {
                name: String::from("curl_noise"),
                func: CURL_NOISE.to_string(),
            },
            ShaderFunc {
                name: String::from("rotate_x"),
                func: ROTATE_X.to_string(),
            },
            ShaderFunc {
                name: String::from("rotate_y"),
                func: ROTATE_Y.to_string(),
            },
            ShaderFunc {
                name: String::from("rotate_z"),
                func: ROTATE_Z.to_string(),
            },
        ];

        /// Look for default bundled sources
        let mut found = false;
        for func in funcs {
            let mut delimiter = String::from("//-");
            delimiter += &*func.name;

            if line.contains((&delimiter)) {
                lines.push(func.func);

                found = true;
                break;
            }
        }

        if !found {
            // check to see if delimiter exists and is a path
            let mut delimiter = String::from("//-");
            let mut delimiter2 = String::from("#include ");

            // if delimiter 1 is found is found, attempt to load file
            if line.contains((&delimiter)) {
                // extract the path
                let mut path = line.replace(delimiter.as_str(), "").replace("\"", "");
                let s_path = Path::new(path.as_str());

                // load file if it exists, if not, we just pass over things.
                if s_path.exists() {
                    let mut source = String::new();
                    source = load_file(path.as_str());
                    lines.push(source);
                }
            } else if line.contains((&delimiter2)) {
                // extract the path
                let mut path = line.replace(delimiter2.as_str(), "");
                let s_path = Path::new(path.as_str());

                // load file if it exists, if not, we just pass over things.
                if s_path.exists() {
                    let mut source = String::new();
                    source = load_file(path.as_str());
                    lines.push(source);
                }
            } else {
                // push the next line.
                lines.push(line.to_string());
            }
        }
    });

    lines.join("\n")
}
