use std::fs;
use std::path::Path;

/// common functions most shaders use.
pub const COMMON_UTILS: &str = "vec3 mod289(vec3 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 mod289(vec4 x) {
  return x - floor(x * (1.0 / 289.0)) * 289.0;
}

vec4 permute(vec4 x) {
     return mod289(((x*34.0)+1.0)*x);
}

vec4 taylorInvSqrt(vec4 r)
{
  return 1.79284291400159 - 0.85373472095314 * r;
}
";

/// the starting version declaration
pub fn build_shader_start(version: u32, extensions: &str) -> String {
    format!("#version {} \n {}", version, extensions)
}

pub fn load_file(src: &str) -> String {
    let s = format!("Something went wrong trying to read file at {}", src);
    fs::read_to_string(src).expect(s.as_str())
}

/// allows you to compose several shader sources together. You can either pass in
/// the full source or a path to the source to bundle.
/// Includes the shader declaration + helpful extensions.
pub fn build_shader_source(sources: Vec<&str>) -> String {
    let mut final_sources = vec![String::from(
        "#version 450
        #extension GL_ARB_separate_shader_objects : enable
        #extension GL_ARB_shading_language_420pack : enable",
    )];

    for src in sources {
        let path = Path::new(src);

        if path.exists() {
            final_sources.push(load_file(src));
        } else {
            final_sources.push(String::from(src));
        }
    }

    final_sources.join("\n")
}

/// allows you to compose several shader sources together with a set of uniform declarations. You can either pass in
/// the full source or a path to the source to bundle.
/// Includes the shader declaration + helpful extensions.
pub fn build_shader_source_with_uniforms(sources: Vec<&str>, uniforms: Vec<String>) -> String {
    let mut final_sources = vec![String::from(
        "#version 450
        #extension GL_ARB_separate_shader_objects : enable
        #extension GL_ARB_shading_language_420pack : enable",
    )];

    final_sources.extend(uniforms);

    for src in sources {
        let path = Path::new(src);

        if path.exists() {
            final_sources.push(load_file(src));
        } else {
            final_sources.push(String::from(src));
        }
    }

    final_sources.join("\n")
}

pub fn generate_shader_uniform(binding: usize, uniform_name: &str, uniform_body: &str) -> String {
    let rnd_name = "02498029480";
    format!(
        "layout(binding = {}) uniform  {}\n{} {};",
        binding, rnd_name, uniform_body, uniform_name
    )
}

/// Recursively iterates through a file looking for #include statements and appending the contents line-by-line to
/// [output]
pub fn resolve_includes(path: &str, current_dir: &Path, output: &mut String) {
    let resolved_path = current_dir.join(path);
    let resolved_path = resolved_path.canonicalize().unwrap();

    let content = load_file(resolved_path.to_str().unwrap());

    let mut lines = content.lines();
    while let Some(line) = lines.next() {
        if line.starts_with("#include") {
            let included_path = line.trim_start_matches("#include").trim().replace("\"", "");
            let dir = resolved_path.parent().unwrap();
            resolve_includes(included_path.as_str(), dir, output);
        } else {
            output.push_str(line);
            output.push_str("\n");
        }
    }
}
