// based on https://github.com/mattdesl/pack-spheres/blob/master/index.js

use glam::Vec3;
use std::cmp::max;
use yoi_math::rand_float;

pub struct PackSpheres {
    bounds: f32,
    pack_attempts: usize,
    max_count: usize,
    dimensions: usize,
    max_radius: f32,
    min_radius: f32,
    radius_growth: f32,
    max_growth_steps: f32,
    padding: f32,
    shapes: Vec<Shape>,
}

#[derive(Copy, Clone, Debug)]
pub struct Shape {
    pub(crate) min_radius: f32,
    pub(crate) radius_growth: f32,
    pub(crate) max_radius: f32,
    pub(crate) max_growth_steps: f32,
    pub(crate) padding: f32,
    pub(crate) radius: f32,
    pub(crate) position: [f32; 3],
    pub(crate) built: bool,
}

impl Shape {
    pub fn get_position(self) -> [f32; 3] {
        self.position
    }

    pub fn get_radius(self) -> f32 {
        self.radius
    }
}

impl Default for Shape {
    fn default() -> Self {
        Shape {
            min_radius: -1.0,
            radius_growth: 0.0,
            max_radius: 0.0,
            max_growth_steps: 0.0,
            padding: 0.0,
            radius: 0.0,
            position: [-1.0, -1.0, -1.0],
            built: false,
        }
    }
}

impl PackSpheres {
    pub fn new() -> Self {
        PackSpheres {
            bounds: 1.0,
            pack_attempts: 500,
            dimensions: 3,
            max_count: 1000,
            max_radius: 0.5,
            min_radius: 0.01,
            radius_growth: 0.01,
            padding: 0.0,
            max_growth_steps: f32::INFINITY,
            shapes: vec![],
        }
    }

    pub fn build(&mut self) {
        for i in 0..self.max_count {
            let result = self.pack();
            if result.built {
                self.shapes.push(result)
            }
        }
    }

    pub fn get_shapes(self) -> Vec<Shape> {
        self.shapes.clone()
    }

    pub fn pack(&mut self) -> Shape {
        let mut shape = Shape::default();
        for i in 0..self.pack_attempts {
            shape = self.place();
            if shape.built {
                break;
            }
        }

        if !shape.built {
            return shape;
        }

        let mut radius = shape.min_radius;
        if shape.radius_growth > 0.0 {
            let mut count = 0.0;

            while radius < shape.max_radius && count < shape.max_growth_steps {
                let new_radius = radius + shape.radius_growth;

                if self.reject(shape.position, new_radius, shape.padding) {
                    break;
                }
                radius = new_radius;
                count += 1.0;
            }
        }

        shape.radius = shape.max_radius.min(radius);

        shape
    }

    pub fn place(&mut self) -> Shape {
        let max_radius = self.max_radius;
        let radius_growth = self.radius_growth;
        let max_growth_steps = self.max_growth_steps;
        let position = self.sample();
        let radius = self.min_radius;
        let padding = self.padding;

        if self.reject(position, radius, padding) {
            return Shape::default();
        }

        return Shape {
            built: true,
            max_growth_steps,
            min_radius: radius,
            max_radius,
            radius_growth,
            position,
            padding,
            radius,
        };
    }

    fn reject(&mut self, position: [f32; 3], radius: f32, padding: f32) -> bool {
        if self.outside(position, radius) {
            return true;
        }

        return self.shapes.iter().any(|x| {
            return PackSpheres::collision(
                position,
                radius + padding,
                x.position,
                x.radius + x.padding,
            );
        });
    }

    fn outside(&self, position: [f32; 3], radius: f32) -> bool {
        let max_bound = self.bounds.abs();

        for i in 0..position.len() {
            let component = position[i];

            if (component + radius).abs() >= max_bound || (component - radius).abs() >= max_bound {
                return true;
            }
        }

        return false;
    }

    fn sample(&self) -> [f32; 3] {
        let mut p = [0.0; 3];
        for i in 0..self.dimensions {
            p[i] = ((rand_float(0.0, 1.0) * 2.0 - 1.0) * self.bounds);
        }
        p
    }

    fn dist_sq(a: [f32; 3], b: [f32; 3]) -> f32 {
        let mut sum = 0.0;
        for i in 0..3 {
            let delta = a[i] - b[i];
            sum += delta * delta;
        }

        sum
    }

    fn mag(self, a: [f32; 3]) -> f32 {
        let mut sum = 0.0;
        for i in 0..3 {
            let d = a[i];
            sum += d * d;
        }

        sum
    }

    fn collision(point_a: [f32; 3], radius_a: f32, point_b: [f32; 3], radius_b: f32) -> bool {
        let radius = radius_a + radius_b;
        let radius_sq = radius * radius;
        PackSpheres::dist_sq(point_a, point_b) < radius_sq
    }
}
