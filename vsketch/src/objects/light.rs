use crate::get_byte_size;
use glam::{vec4, Vec4};
use yoi_math::{create_vec4, rand_vec4};
use yoi_vk::{BufferFormat, DataBuffer, Vulkan};

#[derive(Copy, Clone)]
pub struct Light {
    pub position: Vec4,
    pub ambient_color: Vec4,
    pub diffuse_color: Vec4,
    pub specular_color: Vec4,
    pub meta: Vec4,
    //pub intensity: f32,
    //pub radius: f32,
    //pub volume: f32,
    //pub falloff: f32,
}

impl Default for Light {
    fn default() -> Self {
        Light {
            position: create_vec4(),
            ambient_color: Vec4::new(1.0, 0.0, 1.0, 1.0),
            diffuse_color: Vec4::new(1.0, 0.0, 1.0, 1.0),
            specular_color: Vec4::new(0.0, 1.0, 1.0, 1.0),
            meta: Vec4::new(1.0, 1.0, 1.0, 0.0),
            // Volume refers to the area that the light affects.
            // Radius is more for visualization purposes.

            //intensity: 1.0,
            //radius: 1.0,
            //volume: 1.0,
            //falloff: 0.0,
        }
    }
}

impl Light {
    pub fn new() -> Self {
        Light {
            position: create_vec4(),
            ambient_color: Vec4::new(1.0, 0.0, 1.0, 1.0),
            diffuse_color: Vec4::new(1.0, 0.0, 1.0, 1.0),
            specular_color: Vec4::new(0.0, 1.0, 1.0, 1.0),
            meta: Vec4::new(1.0, 1.0, 1.0, 0.0),
            // Volume refers to the area that the light affects.
            // Radius is more for visualization purposes.

            //intensity: 1.0,
            //radius: 1.0,
            //volume: 1.0,
            //falloff: 0.0,
        }
    }

    /// Returns the position of the light object.
    pub fn get_position(&self) -> &Vec4 {
        &self.position
    }

    /// Gets light volume
    pub fn get_volume(&self) -> f32 {
        self.meta.z
    }

    pub fn volume(mut self, volume: f32) -> Self {
        self.meta.z = volume;
        self
    }

    pub fn radius(mut self, radius: f32) -> Self {
        self.meta.y = radius;
        self
    }

    pub fn get_radius(&self) -> f32 {
        self.meta.y
    }

    pub fn intensity(mut self, intense: f32) -> Self {
        self.meta.x = intense;
        self
    }

    pub fn falloff(mut self, falloff: f32) -> Self {
        self.meta.w = falloff;
        self
    }

    //pub fn get_intensity(&self) -> f32 {
    //    self.volume
    //}

    pub fn ambient_color(mut self, r: f32, g: f32, b: f32, a: f32) -> Self {
        self.ambient_color.x = r;
        self.ambient_color.y = g;
        self.ambient_color.z = b;
        self.ambient_color.w = a;

        self
    }

    /// Same as [ambient_color] but accepts vec4
    pub fn ambient_color4(mut self, color: Vec4) -> Self {
        self.ambient_color.x = color.x;
        self.ambient_color.y = color.y;
        self.ambient_color.z = color.z;
        self.ambient_color.w = color.w;

        self
    }

    pub fn get_ambient_color(&self) -> &Vec4 {
        &self.ambient_color
    }

    pub fn specular_color(mut self, r: f32, g: f32, b: f32, a: f32) -> Self {
        self.specular_color.x = r;
        self.specular_color.y = g;
        self.specular_color.z = b;
        self.specular_color.w = a;

        self
    }

    /// Same as [specular_color] but accepts vec4
    pub fn specular_color4(mut self, color: Vec4) -> Self {
        self.specular_color.x = color.x;
        self.specular_color.y = color.y;
        self.specular_color.z = color.z;
        self.specular_color.w = color.w;

        self
    }

    pub fn get_specular_color(&self) -> &Vec4 {
        &self.specular_color
    }

    pub fn diffuse_color(mut self, r: f32, g: f32, b: f32, a: f32) -> Self {
        self.diffuse_color.x = r;
        self.diffuse_color.y = g;
        self.diffuse_color.z = b;
        self.diffuse_color.w = a;

        self
    }

    /// Same as [specular_color] but accepts vec4
    pub fn diffuse_color4(mut self, color: Vec4) -> Self {
        self.diffuse_color.x = color.x;
        self.diffuse_color.y = color.y;
        self.diffuse_color.z = color.z;
        self.diffuse_color.w = color.w;

        self
    }

    pub fn get_diffuse_color(&self) -> &Vec4 {
        &self.diffuse_color
    }

    pub fn position(mut self, x: f32, y: f32, z: f32) -> Self {
        self.position.x = x;
        self.position.y = y;
        self.position.z = z;
        self
    }

    pub fn debug_meta(&mut self) {
        self.meta = vec4(1.0, 1.0, 0.0, 1.0);
    }
}

/// Generates a [DataBuffer] for the material.
pub fn generate_light_buffer(vk: &Vulkan, mat: Light) -> DataBuffer {
    let mut fmt = BufferFormat::default();
    fmt.size = get_byte_size::<Light>();

    let mut buffer = DataBuffer::create(vk, fmt);
    buffer.set_object_data(vk, mat);

    buffer
}
