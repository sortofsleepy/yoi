use crate::{get_byte_size, get_byte_size_of};
use glam::Vec4;
use winit::event::DeviceEvent::Button;
use yoi_math::{create_vec4, rand_vec4};
use yoi_vk::{BufferFormat, DataBuffer, Vulkan};

#[derive(Copy, Clone)]
pub struct Material {
    ambient_color: Vec4,
    diffuse_color: Vec4,
    emission_color: Vec4,
    specular_color: Vec4,

    // holds random props of the material like shine
    aspects: Vec4,
}

impl Material {
    pub fn new() -> Self {
        Material {
            ambient_color: rand_vec4(),
            diffuse_color: rand_vec4(),
            emission_color: rand_vec4(),
            specular_color: rand_vec4(),
            aspects: create_vec4(),
        }
    }

    pub fn set_diffuse(mut self, c: Vec4) -> Self {
        self.diffuse_color = c;
        self
    }

    pub fn set_emission(mut self, c: Vec4) -> Self {
        self.emission_color = c;
        self
    }

    pub fn set_specular(mut self, c: Vec4) -> Self {
        self.specular_color = c;
        self
    }

    pub fn set_shine(mut self, shine: Vec4) -> Self {
        self.aspects = shine;
        self
    }
}

/// Generates a [DataBuffer] for the material.
pub fn generate_material_buffer(vk: &Vulkan, mat: Material) -> DataBuffer {
    let mut fmt = BufferFormat::default();
    fmt.size = get_byte_size::<Material>();

    let mut buffer = DataBuffer::create(vk, fmt);
    buffer.set_object_data(vk, mat);

    buffer
}
