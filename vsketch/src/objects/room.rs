use crate::DisplayObject;
use ash::vk;
use glam::{vec2, vec3, vec4, Vec3};
use std::mem::size_of;
use yoi_vk::{
    BufferFormat, DataBuffer, Descriptor, Mesh, RenderPipeline, RenderShader, UniformDescriptor,
    VkApp, VkDescriptorBuffer, VkObject, Vulkan,
};

#[derive(Copy, Clone)]
pub struct RoomSettings {
    pub eye_dir: Vec3,
    pub power: f32,
    pub room_dimensions: Vec3,
    pub light_power: f32,
}

/// Room object from @flight404's Eyeo 2012 talk
/// https://github.com/flight404/Eyeo2012
pub struct MeshRoom {
    settings: RoomSettings,
    mesh: Option<DisplayObject>,
}

impl MeshRoom {
    pub fn new() -> Self {
        MeshRoom {
            settings: RoomSettings {
                eye_dir: vec3(0.0, 0.0, 0.0),
                power: 0.03,
                room_dimensions: vec3(350.0, 200.0, 350.0),
                light_power: 3.2,
            },
            mesh: None,
        }
    }

    pub fn setup(&mut self, instance: &Vulkan, camera: &UniformDescriptor, comp: &VkApp) {
        let x = 1.0f32;
        let y = 1.0f32;
        let z = 1.0f32;

        let box_shader = RenderShader::new(ROOM_VERTEX, ROOM_FRAGMENT, instance);

        let verts = vec![
            vec4(-x, -y, -z, 1.0),
            vec4(-x, -y, z, 1.0),
            vec4(x, -y, z, 1.0),
            vec4(x, -y, -z, 1.0),
            vec4(-x, y, -z, 1.0),
            vec4(-x, y, z, 1.0),
            vec4(x, y, z, 1.0),
            vec4(x, y, -z, 1.0),
        ];

        let v_indices = vec![
            [0, 1, 3],
            [1, 2, 3], // floor
            [4, 7, 5],
            [7, 6, 5], // ceiling
            [0, 4, 1],
            [4, 5, 1], // left
            [2, 6, 3],
            [6, 7, 3], // right
            [1, 5, 2],
            [5, 6, 2], // back
            [3, 7, 0],
            [7, 4, 0], // front
        ];

        let v_normals = vec![
            vec4(0.0, -1.0, 0.0, 1.0), // floor
            vec4(0.0, 1.0, 0.0, 1.0),  // ceiling
            vec4(1.0, 0.0, 0.0, 1.0),  // left
            vec4(-1.0, 0.0, 0.0, 1.0), // right
            vec4(0.0, 0.0, -1.0, 1.0), // back
            vec4(0.0, 0.0, 1.0, 1.0),  // front
        ];

        let v_tex_coords = vec![
            vec2(0.0, 0.0),
            vec2(0.0, 1.0),
            vec2(1.0, 1.0),
            vec2(1.0, 0.0),
        ];

        let t_indices = vec![
            [0, 1, 3],
            [1, 2, 3], // floor
            [0, 1, 3],
            [1, 2, 3], // ceiling
            [0, 1, 3],
            [1, 2, 3], // left
            [0, 1, 3],
            [1, 2, 3], // right
            [0, 1, 3],
            [1, 2, 3], // back
            [0, 1, 3],
            [1, 2, 3], // front
        ];

        let mut positions = vec![];
        let mut indices = vec![];
        let mut normals = vec![];
        let mut uvs = vec![];

        for i in 0..12 {
            positions.push(verts[v_indices[i][0]]);
            positions.push(verts[v_indices[i][1]]);
            positions.push(verts[v_indices[i][2]]);

            normals.push(v_normals[i / 2]);
            normals.push(v_normals[i / 2]);
            normals.push(v_normals[i / 2]);

            uvs.push(v_tex_coords[t_indices[i][0]]);
            uvs.push(v_tex_coords[t_indices[i][1]]);
            uvs.push(v_tex_coords[t_indices[i][2]]);
        }

        for i in 0..36 {
            indices.push(i);
        }
        let mut room_buffer = DataBuffer::create(
            instance,
            BufferFormat::default().size(size_of::<RoomSettings>() as u64),
        );
        room_buffer.set_object_data(instance, self.settings);
        let buffer_address = instance.get_buffer_device_address(room_buffer.raw());

        let mut room_settings = UniformDescriptor::new();
        room_settings.set_shader_binding(1);
        room_settings
            .set_shader_stage(vk::ShaderStageFlags::VERTEX | vk::ShaderStageFlags::FRAGMENT);
        //room_settings.set_buffer_data(room_buffer.get_vulkan_object(), 0, size_of::<RoomSettings>() as u64);
        room_settings.set_buffer_data_with_address(
            instance,
            room_buffer.raw(),
            0,
            size_of::<RoomSettings>() as u64,
        );

        let mut mesh = Mesh::new();
        mesh = mesh
            .add_attribute(instance, 0, positions)
            .add_attribute(instance, 1, normals)
            .add_index_data(instance, indices);

        let mut descriptor = VkDescriptorBuffer::new();
        descriptor.add_uniform_buffer_descriptor(*camera);
        descriptor.add_uniform_buffer_descriptor(room_settings);
        descriptor.build(instance);

        let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
        pipeline.add_mesh(&mesh);
        pipeline.add_descriptor_set_layout(descriptor.get_descriptor_layout());
        pipeline.compile(instance, &comp, &box_shader);

        self.mesh = Some(DisplayObject::new(vec![mesh], pipeline));
    }
}

pub const ROOM_VERTEX: &str = "#version 450

out gl_PerVertex
{
    vec4 gl_Position;
};

layout(binding = 0) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} ubo;


layout(binding = 1) uniform RoomSettings{
    vec3 eyePos;
    float power;
    vec3 dimensions;
    float lightPower;
} room;

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;


layout(location = 0) out vec4 vNormal;
layout(location = 1) out vec3 vEyeDir;
layout(location = 2) out vec3 vPos;


void main()
{
    vec4 pos = position;

    pos.xyz *= room.dimensions;

    vPos = pos.xyz;
    vNormal = normal;
    vEyeDir = normalize(room.eyePos - position.xyz);



    gl_Position = ubo.proj * ubo.view * vec4(pos.xyz,1.);
    //gl_Position =  vec4(position.xyz,1.);
}";

pub const ROOM_FRAGMENT: &str = "#version 450

layout(location = 0) out vec4 glFragColor;

layout(binding = 1) uniform RoomSettings{
    vec3 eyePos;
    float power;
    vec3 roomDims;
    float lightPower;
} room;

layout(location = 0) in vec4 vNormal;
layout(location = 1) in vec3 vEyeDir;
layout(location = 2) in vec3 vPos;

void main(){
    float aoLight = 1.0 - length( vPos ) * ( 0.0015 + ( room.power * 0.005 ) );
    float ceiling		= 0.0;
    if( vNormal.y < -0.5 ) ceiling = 1.0;

    float yPer = clamp( -vPos.y / room.roomDims.y, 0.0, 1.0 );
    float ceilingGlow	= pow( yPer, 2.0 ) * 0.25;
    ceilingGlow			+= pow( yPer, 200.0 );
    ceilingGlow			+= pow( max( yPer - 0.7, 0.0 ), 3.0 ) * 4.0;
    vec3 finalColor		= vec3( aoLight + ( ceiling + ceilingGlow * 0.5 ) * room.lightPower );

    glFragColor = vec4(finalColor * vec3(1.0,1.0,0.0),1.);
}";
