use crate::core::Timer;

pub struct AppEnv {
    resolution: [f32; 2],
    time: Timer,
}

impl AppEnv {
    pub fn new(resolution: [f32; 2]) -> AppEnv {
        AppEnv {
            resolution,
            time: Timer::new(),
        }
    }

    pub fn update_resolution(&mut self, resolution: [u32; 2]) {
        self.resolution[0] = resolution[0] as f32;
        self.resolution[1] = resolution[1] as f32;
    }

    /// Updates metadata + data buffer
    pub fn update(&mut self) {
        self.time.update();
    }

    /// Returns delta time
    pub fn get_delta_time(&mut self) -> f32 {
        self.time.get_time()
    }
}
