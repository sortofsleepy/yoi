use crate::generate_descriptor_buffer_format;
use glam::Vec4;
use std::mem::size_of;
use yoi_math::create_vec4;
use yoi_vk::{DataBuffer, Descriptor, UniformDescriptor, VkObject, Vulkan};

/// Provides an interface for managing simple global settings like delta time and
/// window resolution for the purposes of adding those values to a shader.
///
/// Provides buffer and descriptor as well
pub struct SimpleSettings {
    settings: Vec4,
    buffer: DataBuffer,
    descriptor: UniformDescriptor,
}

impl SimpleSettings {
    pub fn new(vk: &Vulkan, binding: u32) -> Self {
        let settings = create_vec4();

        let mut settings_buffer =
            DataBuffer::create(vk, generate_descriptor_buffer_format::<Vec4>());
        settings_buffer.set_object_data(vk, settings);

        let mut env_desc = UniformDescriptor::new();
        env_desc.set_shader_binding(binding);
        env_desc.set_buffer_data_with_address(
            vk,
            settings_buffer.raw(),
            0,
            size_of::<Vec4>() as u64,
        );

        SimpleSettings {
            settings,
            buffer: settings_buffer,
            descriptor: env_desc,
        }
    }

    /// Gets the descriptor
    pub fn get_descriptor(&self) -> &UniformDescriptor {
        &self.descriptor
    }

    /// Updates delta time value in buffer.
    pub fn update_time(&mut self, vk: &Vulkan, time: f32) {
        self.settings.x = time;
        self.buffer.set_object_data(vk, self.settings);
    }
}
