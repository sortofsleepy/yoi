use yoi_vk::{Mesh, RenderPipeline, RenderShader, VkApp, VkTexture, Vulkan};

pub struct PostProcess {
    scene: Option<VkTexture>,
    shader: RenderShader,
    mesh: Mesh,
    pipeline: RenderPipeline,
}

impl PostProcess {
    pub fn create(instance: &Vulkan, vertex: &str, fragment: &str) -> Self {
        PostProcess {
            scene: None,
            shader: RenderShader::new(vertex, fragment, instance),
            mesh: Mesh::new(),
            pipeline: RenderPipeline::new(),
        }
    }

    pub fn compile(&mut self, instance: &Vulkan, app: &VkApp) {
        let shader = &self.shader;

        self.pipeline.compile(instance, app, shader);
    }
}
