use winit::{
    dpi::LogicalSize,
    event::{Event, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::{Window, WindowBuilder},
};

use ash::vk::{ImageAspectFlags, Result, SurfaceKHR};

use crate::core::appenv::AppEnv;
use crate::core::window_data::WindowData;
use raw_window_handle::{HasRawDisplayHandle, HasRawWindowHandle};
use yoi_vk::*;

// functional reference for functions that should receive a Vulkan instance and current state.
pub type InstanceStateFn<State> = fn(&mut Vulkan, &mut VkRenderCore, &mut State);
pub type SetupFn<State> = fn(&mut Vulkan, &mut VkRenderCore) -> State;
pub type ResizeFn<State> = fn(&mut Vulkan, &mut State, [u32; 2]) -> State;

/// Used during "run" function to collect all the necessary info for running the app.
pub struct VkRenderCore {
    // the Vulkan related metadata(ie viewport settings, etc)
    pub meta: VkApp,
    pub surface: SurfaceKHR,
    pub render: VkRenderer,
    pub window: Window,

    // general application environment metadata
    pub app_env: AppEnv,
}

impl VkRenderCore {
    /// Checks a [vk::Result] object to see if it is an "out-of-date" result.
    pub fn out_of_date_khr(&self, result: Result) -> bool {
        if result == Result::ERROR_OUT_OF_DATE_KHR {
            return true;
        }
        return false;
    }
    pub fn resize(&mut self, instance: &Vulkan, result: Result) {
        if self.out_of_date_khr(result) {
            let window_size = self.window.inner_size();
            self.meta.reset(
                instance,
                &self.surface,
                [window_size.width, window_size.height],
            );
        }
    }
}

/// A "sketch" object - encompasses the whole application.
pub struct VSketch<A = ()> {
    windows: Vec<WindowData>,
    setup: Vec<SetupFn<A>>,
    resize: Vec<ResizeFn<A>>,
    draw: Vec<InstanceStateFn<A>>,
    frames_in_flight: usize,
    enable_ray_trace: bool,
}

impl<'a, A> VSketch<A>
where
    A: 'static,
{
    pub fn new() -> Self {
        VSketch {
            windows: vec![WindowData::default()],
            setup: vec![],
            draw: vec![],
            resize: vec![],
            frames_in_flight: 3,
            enable_ray_trace: false,
        }
    }

    #[allow(unused)]
    pub fn new_sketch(width: u32, height: u32, title: String) -> Self {
        VSketch {
            windows: vec![WindowData {
                window_size: LogicalSize::new(width, height),
                title,
                window_position: Default::default(),
                always_on_top: false,
            }],
            enable_ray_trace: false,
            resize: vec![],
            setup: vec![],
            draw: vec![],
            frames_in_flight: 3,
        }
    }

    /// set window size
    pub fn set_size(mut self, width: u32, height: u32) -> Self {
        self.windows[0].window_size.width = width;
        self.windows[0].window_size.height = height;
        self
    }

    /// set window size of specified window
    pub fn set_size_of(mut self, width: u32, height: u32, idx: usize) -> Self {
        self.windows[idx].window_size.width = width;
        self.windows[idx].window_size.height = height;
        self
    }

    /// Turns on ray-tracing in the vulkan instance.
    pub fn enable_ray_tracing(mut self) -> Self {
        self.enable_ray_trace = true;
        self
    }

    /// sets up the setup function
    pub fn add_setup(mut self, evt: SetupFn<A>) -> Self {
        self.setup.push(evt);
        self
    }

    /// sets up the resize function
    pub fn add_resize(mut self, evt: ResizeFn<A>) -> Self {
        self.resize.push(evt);
        self
    }

    /// adds a draw function to be run in a loop.
    pub fn add_draw(mut self, evt: InstanceStateFn<A>) -> Self {
        self.draw.push(evt);
        self
    }

    /// adds a new window to the sketch
    #[allow(unused)]
    pub fn add_window(mut self, window: WindowData) -> Self {
        self.windows.push(window);
        self
    }

    #[allow(unused)]
    /// Kicks off the sketch. by default we assume all windows to share the same instance.
    pub fn run(&mut self) {
        if self.windows.len() < 1 {
            eprintln!("No windows were specified!");
            return;
        }

        let e_loop = EventLoop::new();

        // build instance. First build tmp window to get surface extensions
        let tmp_window = WindowBuilder::new()
            .with_title("tmp")
            .with_inner_size(LogicalSize::new(1, 1))
            .build(&e_loop)
            .unwrap();

        let mut info = VkAppInfo {
            window_extensions: ash_window::enumerate_required_extensions(
                tmp_window.raw_display_handle(),
            )
            .unwrap()
            .to_vec(),
            enable_descriptor_buffer: true, // TODO maybe just enable by default
            enable_ray_trace: self.enable_ray_trace,
            ..Default::default()
        };

        // drop tmp window
        std::mem::drop(tmp_window);

        // build instance with info
        // TODO not sure if this ought to be ref counted, leaving alone for now since it makes things easier to work with.
        let mut instance = Vulkan::new(info);

        // start to loop through and build windows
        let mut windows: Vec<VkRenderCore> = vec![];

        let mut i = 0;
        // build the windows and setup the surfaces
        for win in self.windows.iter() {
            let window = WindowBuilder::new()
                .with_title(win.title.as_str())
                .with_position(win.window_position)
                .with_inner_size(win.window_size)
                .build(&e_loop)
                .unwrap();

            let dim = window.inner_size();
            let surface = unsafe {
                ash_window::create_surface(
                    instance.get_entry(),
                    instance.get_vk_instance(),
                    window.raw_display_handle(),
                    window.raw_window_handle(),
                    None,
                )
                .unwrap()
            };

            let win_size = window.inner_size();

            // run setup
            // TODO might re-think this but for multi-window it might make sense to have separate instances?
            instance.setup(&surface);

            // build debug support if in debug
            #[cfg(debug_assertions)]
            {
                instance.build_debug();
            }

            // build default sampler
            let mut sampler = VkSampler::new();
            sampler.build(&instance);

            // setup swapchain for each window.
            let mut swapchain = VkSwapChain::new();
            swapchain.init(&instance, &surface);

            // build default depth attachment
            let mut dtfmt = generate_depth_attachment_format(win_size.width, win_size.height);
            dtfmt.image_subresource_range.aspect_mask = ImageAspectFlags::DEPTH;

            let depth_tex = VkTexture::new(&instance, dtfmt);

            // build renderer for window
            let mut render = VkRenderer::new();
            render.set_max_frames_in_flight(self.frames_in_flight);
            render.init(&instance, &swapchain);

            let mut meta = VkApp::new(
                dim.width as f32,
                dim.height as f32,
                swapchain.get_extent(),
                swapchain,
                depth_tex,
            );

            let app_env = AppEnv::new([
                window.inner_size().width as f32,
                window.inner_size().height as f32,
            ]);

            windows.push(VkRenderCore {
                meta,
                surface,
                window,
                render,
                app_env,
            });

            i += 1;
        }

        let setup_funcs = self.setup.clone();
        let draw_funcs = self.draw.clone();
        let resize_funcs = self.resize.clone();

        let mut model_states = vec![];

        for mut win in windows.iter_mut() {
            for sf in &setup_funcs {
                model_states.push(sf(&mut instance, &mut win))
            }
        }

        e_loop.run(move |event, _, control_flow| {
            *control_flow = ControlFlow::Poll;

            match event {
                Event::RedrawRequested(id) => {
                    let mut i = 0;
                    for mut win in windows.iter_mut() {
                        if win.window.id() == id {
                            // run through all the draw functions added to sketch.
                            for df in &draw_funcs {
                                // TODO just trying the following out here - may move back to draw later.

                                // update app env
                                win.app_env.update();

                                df(&mut instance, &mut win, &mut model_states[i]);
                            }

                            i += 1;
                        }
                    }
                }

                Event::MainEventsCleared => {
                    // Make sure to re-quest a redraw - triggers loop
                    for win in windows.iter_mut() {
                        win.window.request_redraw()
                    }
                }

                Event::WindowEvent { event, window_id } => {
                    match event {
                        // Handle resize events
                        WindowEvent::Resized(size) => {
                            let mut i = 0;
                            for win in windows.iter_mut() {
                                if win.window.id() == window_id && win.window.has_focus() {
                                    for re in &resize_funcs {
                                        re(
                                            &mut instance,
                                            &mut model_states[i],
                                            [size.width, size.height],
                                        );
                                    }
                                }

                                i += 1;
                            }
                        }

                        // Handle focus events.
                        WindowEvent::Focused { .. } => {
                            for win in windows.iter_mut() {
                                // find the window we just focused on an re-initialize things
                                // TODO not sure if this might cause issues later - make sure to come back and check.
                                if win.window.id() == window_id && win.window.has_focus() {
                                    //  win.meta.reset();
                                    //  win.meta.re_init_swapchain(&instance, &win.surface);
                                    instance.setup(&win.surface);
                                }
                            }
                        }

                        WindowEvent::CloseRequested => {
                            // TODO currently changing control flow closes both windows which is probably fine
                            //  but it could be nice to close one window at a time.
                            for win in windows.iter_mut() {
                                if win.window.id() == window_id {
                                    *control_flow = ControlFlow::Exit
                                }
                            }
                        }
                        _ => {}
                    }
                }
                _ => {}
            }
        });
    }
}
