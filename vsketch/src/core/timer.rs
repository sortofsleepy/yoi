use std::mem::size_of;
use std::time::Instant;

#[derive(Copy, Clone)]
pub struct TimeData {
    time: f32,
}

/// Ported from @flight404's Eyeo 2012 code
#[derive(Copy, Clone)]
pub struct Timer {
    time: f32,
    time_elapsed: f32,
    time_multi: f32,
    time_since_last_frame: f32,
    tick_timer: f32,
    tick: bool,
    sys: Instant,
}

impl Timer {
    pub fn new() -> Self {
        let mut timer = Timer {
            time: 0.0,
            time_elapsed: 0.0,
            time_multi: 60.0,
            time_since_last_frame: 0.0,
            tick_timer: 0.0,
            tick: false,
            sys: Instant::now(),
        };

        timer
    }

    pub fn get_data(&self) -> TimeData {
        TimeData { time: self.time }
    }

    pub fn get_time(&self) -> f32 {
        self.time
    }

    /// Get byte size.
    pub fn get_byte_size(&self) -> u64 {
        size_of::<Timer>() as u64
    }

    pub fn update(&mut self) {
        let prev_time = self.time;
        self.time = self.sys.elapsed().as_secs_f32();

        let dt = self.time - prev_time;

        self.time_since_last_frame = dt * self.time_multi;
        self.time_elapsed += self.time_since_last_frame;

        self.tick_timer += self.time_since_last_frame;
        self.tick = false;

        if self.tick_timer > 1.0 {
            self.tick = true;
            self.tick_timer = 0.0;
        }
    }
}
