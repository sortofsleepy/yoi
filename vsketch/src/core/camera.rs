use glam::{Mat4, Vec3};
use std::f32::consts::PI;

const DEG2RAD: f32 = 0.017453292519943295;

/// Default structure to make it simpler to send projection + view matrices.
#[derive(Copy, Clone)]
pub struct CameraBufferState {
    pub proj: Mat4,
    pub view: Mat4,
}

#[allow(unused)]
fn to_radians(deg: f32) -> f32 {
    let v = PI / 180.0;
    deg * v
}

/// The basics of a perspective projection camera.
pub struct PerspectiveCamera {
    aspect: f32,
    center: Vec3,
    eye: Vec3,
    far: f32,
    near: f32,
    fov: f32,
    #[allow(unused)]
    position: Vec3,
    projection_matrix: Mat4,
    target: Vec3,
    up: Vec3,
    view_matrix: Mat4,
}

impl PerspectiveCamera {
    #[allow(unused)]
    pub fn create(fov: f32, aspect: f32, near: f32, far: f32) -> Self {
        let mut persp = PerspectiveCamera {
            aspect,
            center: Vec3::new(0.0, 0.0, 0.0),
            eye: Vec3::new(0.0, 0.0, 1.0),
            far,
            near,
            fov,
            position: Vec3::new(0.0, 0.0, 0.0),
            projection_matrix: Mat4::IDENTITY,
            target: Vec3::new(0.0, 0.0, 0.0),
            up: Vec3::new(0.0, 0.9, 0.0),
            view_matrix: Mat4::IDENTITY,
        };

        persp.perspective();

        persp
    }

    #[allow(unused)]
    /// returns a [CameraBufferState] struct to make it easier to attach camera info to buffer.
    pub fn get_projection_view(&self) -> CameraBufferState {
        CameraBufferState {
            proj: self.projection_matrix,
            view: self.view_matrix,
        }
    }

    #[allow(unused)]
    /// Returns projection matrix.
    pub fn get_projection(&self) -> Mat4 {
        self.projection_matrix
    }

    #[allow(unused)]
    /// Returns view matrix
    pub fn get_view(&self) -> Mat4 {
        self.view_matrix
    }

    #[allow(unused)]
    pub fn set_target(mut self, target: Vec3) -> Self {
        self.target = target;
        self
    }

    #[allow(unused)]
    pub fn set_up(mut self, up: Vec3) -> Self {
        self.up = up;
        self
    }

    #[allow(unused)]
    pub fn set_eye(&mut self, eye: Vec3) {
        self.eye = eye;
    }

    #[allow(unused)]
    /// Updates view matrix with current eye, center and up settings.
    pub fn look_at(&mut self) {
        self.view_matrix = Mat4::look_at_rh(self.eye, self.center, self.up);
    }

    #[allow(unused)]
    /// Returns inverse projection matrix
    pub fn get_inverse_projection(&self) -> Mat4 {
        self.projection_matrix.inverse()
    }

    #[allow(unused)]
    /// Moves camera.
    pub fn translate(&mut self, pos: Vec3) {
        self.eye.x = pos.x;
        self.eye.y = pos.y;

        // maintain current z position if z happens to be 0
        self.eye.z = if pos.z == 0.0 { self.eye.z } else { pos.z };

        // reset view
        self.view_matrix = Mat4::IDENTITY;
        self.view_matrix = Mat4::look_at_rh(self.eye, self.center, self.up);

        // compensates things so positive x = moving to the right.
        // TODO not sure why this is happening.
        self.view_matrix.x_axis.x *= -1.0;
        self.view_matrix.x_axis.y *= -1.0;
        self.view_matrix.x_axis.z *= -1.0;
    }

    #[allow(unused)]
    /// Sets the perspective projection based on new settings.
    pub fn set_perspective(&mut self, fov: f32, aspect: f32, near: f32, far: f32) {
        self.aspect = aspect;
        self.fov = fov;
        self.near = near;
        self.far = far;
        self.perspective();
    }

    #[allow(unused)]
    /// Sets projection matrix based on current values.
    pub fn perspective(&mut self) {
        let near = self.near;
        let far = self.far;
        let aspect = self.aspect;

        let top = (near * ((DEG2RAD * 0.5 * self.fov).tan())) / self.eye.z;
        let height = 2.0 * top;
        let width = aspect * height;
        let left = -0.5 * width;
        let right = left + width;
        let bottom = top - height;

        let x = 2.0 * near / (right - left);
        let y = 2.0 * near / (top - bottom);
        let a = (right + left) / (right - left);
        let b = (top + bottom) / (top - bottom);
        let c = -(far + near) / (far - near);
        let d = (-2.0 * far * near) / (far - near);

        let mut te = Mat4::IDENTITY;
        te.x_axis.x = x;
        te.y_axis.x = 0.0;
        te.z_axis.x = a;

        te.w_axis.x = 0.0;
        te.x_axis.y = 0.0;
        te.y_axis.y = y;

        te.z_axis.y = b;
        te.w_axis.y = 0.0;
        te.x_axis.z = 0.0;

        te.y_axis.z = 0.0;
        te.z_axis.z = c;
        te.w_axis.z = d;

        te.x_axis.w = 0.0;
        te.y_axis.w = 0.0;
        te.z_axis.w = -1.0;
        te.w_axis.w = 0.0;

        self.projection_matrix = te;
    }
}
