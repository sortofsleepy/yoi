use crate::WARM2;
use glam::{vec4, Vec4};

/// hue to rgb converter
pub fn hue2rgb(p: f32, q: f32, _t: f32) -> f32 {
    let mut t = _t;

    if t < 0.0 {
        t += 1.0;
    }

    if t > 1.0 {
        t += 1.0;
    }

    if t < 1.0 / 6.0 {
        return p + (q - p) * 6.0 * t;
    }

    if t < 1.0 / 2.0 {
        return q;
    }

    if t < 2.0 / 3.0 {
        return p + (q - p) * 6.0 * (2.0 / 3.0 - t);
    }

    p
}

/// Normalize a RGB color that was specified from 0-255
pub fn normalize_color(val: &mut Vec4) {
    *val /= 255.0;
}

////// HEX TO RGB //////
// adapted from colorsys package
// https://github.com/emgyrz/colorsys.rs/blob/master/src/converters/hex_to_rgb.rs
const HASH: u8 = b'#';

#[derive(Debug)]
pub struct HexError {
    pub message: String,
}

/// Converts hex string to rgb
pub fn hex_to_rgb(s: &str) -> Result<[u32; 3], HexError> {
    from_hex(s.as_bytes()).map_err(|_| HexError {
        message: format!("Cannot convert"),
    })
}

pub fn hex_digit_to_rgb(num: u32) -> [u32; 3] {
    let r = num >> 16;
    let g = (num >> 8) & 0x00FF;
    let b = num & 0x0000_00FF;

    [r, g, b]
}

pub fn from_hex(s: &[u8]) -> Result<[u32; 3], ()> {
    let mut buff: [u8; 6] = [0; 6];
    let mut buff_len = 0;

    for b in s {
        if !b.is_ascii() || buff_len == 6 {
            return Err(());
        }
        let bl = b.to_ascii_lowercase();
        if bl == HASH {
            continue;
        }
        if bl.is_ascii_hexdigit() {
            buff[buff_len] = bl;
            buff_len += 1;
        }
    }
    if buff_len == 3 {
        buff = [buff[0], buff[0], buff[1], buff[1], buff[2], buff[2]];
    }

    let hex_str = core::str::from_utf8(&buff).map_err(|_| ())?;
    let hex_digit = u32::from_str_radix(hex_str, 16).map_err(|_| ())?;
    Ok(hex_digit_to_rgb(hex_digit))
}

// adapted from https://github.com/spite/codevember-2021/blob/main/modules/gradient-linear.js

pub struct GradientLinear {
    colors: Vec<Vec4>,
}

impl GradientLinear {
    pub fn new(colors: Vec<Vec4>) -> Self {
        GradientLinear { colors }
    }

    /// Same as new but allows you to pass in a slice of &str hex strings. See [crate::color::pallets::WARM] as an example.
    /// Will convert hex strings to rgb between 0-255
    pub fn new_from_hex(colors: &[&str]) -> Self {
        let mut col = vec![];
        for color in colors.iter() {
            let val = hex_to_rgb(color).expect("something went wrong");
            col.push(vec4(val[0] as f32, val[1] as f32, val[2] as f32, 1.0));
        }

        GradientLinear { colors: col }
    }
    /// Returns a value from the gradient based on the value you pass in.
    pub fn get_at(&self, val: f32) -> Vec4 {
        let t = val.clamp(0.0, 1.0);
        let len = self.colors.len() as f32;

        let from = (t * len * 0.9999).floor();
        let to = (from + 1.0).clamp(0.0, len - 1.0);

        let fc = self.colors[from as usize];
        let ft = self.colors[to as usize];

        let p = (t - from / len) / (1.0 / len);

        let r = mix(fc.x, ft.x, p);
        let g = mix(fc.y, ft.y, p);
        let b = mix(fc.z, ft.z, p);

        Vec4::new(r, g, b, 1.)
    }

    /// Same as [get_at] but normalizes the value ahead of time to be suitable for the shader.
    pub fn get_at_normalized(&self, val: f32) -> Vec4 {
        let t = val.clamp(0.0, 1.0);
        let len = self.colors.len() as f32;

        let from = (t * len * 0.9999).floor();
        let to = (from + 1.0).clamp(0.0, len - 1.0);

        let fc = self.colors[from as usize];
        let ft = self.colors[to as usize];

        let p = (t - from / len) / (1.0 / len);

        let r = mix(fc.x, ft.x, p);
        let g = mix(fc.y, ft.y, p);
        let b = mix(fc.z, ft.z, p);

        Vec4::new(r / 255.0, g / 255.0, b / 255.0, 1.0)
    }
}

pub fn mix(x: f32, y: f32, a: f32) -> f32 {
    if a <= 0.0 {
        return x;
    }

    if a >= 1.0 {
        return y;
    }

    x + a * (y - x)
}

/// Builds a color map of hex color strings into a [GradientLinear]
///
/// * `color_map` - a Vec of color values to convert from hex to rgb
pub fn to_rgb(color_map: Vec<&str>) -> GradientLinear {
    let col = color_map.into_iter();
    let rgbs = col
        .map(|c| {
            let val = hex_to_rgb(c).expect("something went wrong");
            vec4(val[0] as f32, val[1] as f32, val[2] as f32, 1.0)
        })
        .collect();
    GradientLinear::new(rgbs)
}
