pub mod api;
pub mod color;
pub mod core;
pub mod objects;
pub mod shaders;
pub mod utils;

pub use glam::{vec3, vec4};

pub use crate::{
    api::api::{DataSet, DisplayObject, SceneState},
    color::{pallets::*, utils::*},
    core::appenv::AppEnv,
    core::camera::{CameraBufferState, PerspectiveCamera},
    core::vsketch::{VSketch, VkRenderCore},
    core::window_data::WindowData,
    objects::{light::Light, material},
    shaders::{
        common::{build_shader_start, resolve_includes},
        curl::CURL_NOISE,
        full_screen::{PASSTHRU_FRAGMENT, PASSTHRU_VERTEX},
        random::RAND_HELPERS,
        simplex_3d::simplex_3d,
    },
    utils::{
        build_background, build_camera, build_output_panel, calculate_normal_matrix,
        generate_descriptor_buffer_format, generate_descriptor_buffer_format_with_size,
        get_byte_size, get_byte_size_of, get_default_render_formats, CameraData,
    },
};
