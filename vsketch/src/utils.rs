use crate::{
    CameraBufferState, DisplayObject, PerspectiveCamera, SceneState, VkRenderCore,
    PASSTHRU_FRAGMENT, PASSTHRU_VERTEX,
};
use ash::vk;
use ash::vk::{
    BufferUsageFlags, Format, MemoryAllocateFlags, PushConstantRange, ShaderStageFlags, SurfaceKHR,
};
use ash::vk::{
    ClearAttachment, ClearColorValue, ClearRect, ClearValue, CommandBuffer, Extent2D,
    ImageAspectFlags, ImageView, Offset2D, Rect2D, Result, Semaphore, SemaphoreCreateInfo,
};
use glam::{vec3, Mat4};
use std::mem::size_of;
use yoi_geo::simpleshapes::SimpleShapes;
use yoi_vk::descriptors::uniform_descriptor::UniformDescriptor;
use yoi_vk::{
    BufferFormat, ComputePipeline, DataBuffer, Descriptor, Mesh, RenderPipeline, RenderShader,
    TextureSamplerDescriptor, VkApp, VkDescriptorBuffer, VkObject, VkRenderer, VkSampler, Vulkan,
};

pub struct CameraData {
    pub camera: PerspectiveCamera,
    pub descriptor: UniformDescriptor,
    pub buffer: DataBuffer,
}

/// Builds a perspective camera with the specified settings and a default translation.
/// Will return the camera object, it's descriptor ana a buffer for the camera information.
pub fn build_camera(
    instance: &Vulkan,
    location: u32,
    zoom: f32,
    fov: f32,
    aspect: f32,
    near: f32,
    far: f32,
) -> CameraData {
    // setup main camera
    let mut camera = PerspectiveCamera::create(fov, aspect, near, far);
    camera.translate(vec3(0.0, 0.0, zoom));

    let mut fmt = generate_descriptor_buffer_format::<CameraBufferState>();

    let mut camera_buffer = DataBuffer::create(instance, fmt);
    camera_buffer.set_object_data(instance, camera.get_projection_view());

    let buffer_address = instance.get_buffer_device_address(camera_buffer.raw());

    let mut camera_desc = UniformDescriptor::new();
    camera_desc.set_shader_binding(location);
    camera_desc.set_buffer_data(
        camera_buffer.raw(),
        0,
        size_of::<CameraBufferState>() as u64,
    );
    camera_desc.set_buffer_data_with_address(
        instance,
        camera_buffer.raw(),
        0,
        size_of::<CameraBufferState>() as u64,
    );

    CameraData {
        camera,
        descriptor: camera_desc,
        buffer: camera_buffer,
    }
}

/// Builds a full screen triangle background mesh - useful for post processing or just as a general
/// purpose background.
pub fn build_background(
    ctx: &Vulkan,
    comp: &VkRenderCore,
    descriptor_layouts: Vec<vk::DescriptorSetLayout>,
) -> DisplayObject {
    // build a fullscreen triangle for the background
    let geo = SimpleShapes::generate_fullscreen_triangle();

    let mut mesh = Mesh::new();
    mesh = mesh
        .add_attribute(ctx, 0, geo.positions)
        .set_num_vertices(3);

    let shader = RenderShader::new(PASSTHRU_VERTEX, PASSTHRU_FRAGMENT, ctx);

    // build a pipeline to render it
    let mut pipeline = RenderPipeline::new_with_descriptor_buffer();

    for ds in descriptor_layouts {
        pipeline.add_descriptor_set_layout(ds);
    }

    pipeline.compile_dynamic(
        ctx,
        &comp.meta,
        &shader,
        Format::B8G8R8A8_UNORM,
        Format::D32_SFLOAT,
    );

    DisplayObject::new(vec![mesh], pipeline)
}

/// Helper to generate descriptor buffer compatible [BufferFormat]
/// Make sure to specify object type in order to generate the appropriate size for the buffer.
pub fn generate_descriptor_buffer_format<T>() -> BufferFormat {
    let mut fmt = BufferFormat::default().size(size_of::<T>() as u64);
    fmt.usage_flags = fmt.usage_flags | BufferUsageFlags::SHADER_DEVICE_ADDRESS;
    fmt.mem_alloc_flags = MemoryAllocateFlags::DEVICE_ADDRESS;

    fmt
}

/// Helper to generate descriptor buffer compatible [BufferFormat] that lets you specify a size.
pub fn generate_descriptor_buffer_format_with_size<T>(size: usize) -> BufferFormat {
    let mut fmt = BufferFormat::default().size((size_of::<T>() * size) as u64);
    fmt.usage_flags = fmt.usage_flags
        | BufferUsageFlags::STORAGE_BUFFER
        | BufferUsageFlags::SHADER_DEVICE_ADDRESS;
    fmt.mem_alloc_flags = MemoryAllocateFlags::DEVICE_ADDRESS;

    fmt
}

pub fn generate_push_constant_range<T>(stage: ShaderStageFlags, offset: u32) -> PushConstantRange {
    PushConstantRange::default()
        .offset(offset)
        .size(size_of::<T>() as u32)
        .stage_flags(stage)
}

pub fn resize(instance: &Vulkan, meta: &mut VkApp, surface: &SurfaceKHR, window_size: [u32; 2]) {
    meta.reset(&instance, surface, window_size);
    /*
      if res == Result::ERROR_OUT_OF_DATE_KHR {
                                   let window_size = win.window.inner_size();
                                   win.meta.reset(&instance, &win.surface, [window_size.width, window_size.height]);
                               }

                               win.render.finish_present();
    */
}

/// Returns the size in bytes of a datatype as a u64/DeviceSize
pub fn get_byte_size<T>() -> u64 {
    return size_of::<T>() as u64;
}

/// Returns the size in bytes of a container type as a u64/DeviceSize
pub fn get_byte_size_of<T>(len: usize) -> u64 {
    return (size_of::<T>() * len) as u64;
}

/// Helper to present to screen. Also handles rebuilding of swapchain.
pub fn present_to_screen(vk: &Vulkan, comp: &mut VkRenderCore) {
    let renderer = &mut comp.render;
    let res = renderer.present(&vk, comp.meta.get_swapchain());

    // handle out of date swapchain
    if res == Result::ERROR_OUT_OF_DATE_KHR {
        let window_size = comp.window.inner_size();
        comp.meta
            .reset(vk, &comp.surface, [window_size.width, window_size.height]);
    }

    renderer.finish_present();
}

pub fn present_to_screen_with_compute(
    vk: &Vulkan,
    comp: &mut VkRenderCore,
    pipeline: &ComputePipeline,
    graphics_semaphore: &Semaphore,
) {
    let renderer = &mut comp.render;

    let res = renderer.present_with_compute(
        vk,
        comp.meta.get_swapchain(),
        pipeline.get_semaphore(),
        graphics_semaphore,
    );

    pipeline.submit_compute(vk, &graphics_semaphore);
}

/// Meant to run after a renderpass has begun. This makes the assumption you're only clearing the first layer.
pub fn set_clear(vk: &Vulkan, cb: &CommandBuffer, color: [f32; 4], size: Extent2D) {
    let clear = ClearAttachment::default()
        .clear_value(ClearValue {
            color: ClearColorValue { float32: color },
        })
        .color_attachment(0)
        .aspect_mask(ImageAspectFlags::COLOR);

    let rect = Rect2D::default()
        .offset(Offset2D { x: 0, y: 0 })
        .extent(size);

    let clear_rect = ClearRect::default().layer_count(1).rect(rect);

    vk.clear_attachment(*cb, clear, clear_rect);
}

/// Builds a mesh and pipeline for rendering a texture.
///
/// * ctx - the [Vulkan] context to use
/// * app - the [VkApp] instance to use
/// * output - the [ImageView] to use
/// * formats - the image formats to use during dynamic rendering; first slot is for the color, second is for the depth.
/// Generally [Format::B8G8R8A8_UNORM] and [Format::D32_SFLOAT] should be find in most systems.
pub fn build_output_panel(
    ctx: &Vulkan,
    app: &VkApp,
    output: &ImageView,
    formats: (Format, Format),
) -> SceneState {
    let mut sampler = VkSampler::new();
    sampler.build(ctx);

    let mut tex = TextureSamplerDescriptor::new();
    tex.set_image(*output, *sampler.raw());
    tex.set_shader_binding(0);
    tex.set_shader_stage(ShaderStageFlags::FRAGMENT);

    let mut output_descriptors = VkDescriptorBuffer::new();
    output_descriptors.add_texture_descriptor(tex);
    output_descriptors.build(ctx);

    // build a fullscreen triangle for the background
    let geo = SimpleShapes::generate_fullscreen_triangle();

    let mut mesh = Mesh::new();
    mesh = mesh
        .add_attribute(ctx, 0, geo.positions)
        .set_num_vertices(3);

    let shader = RenderShader::new(PASSTHRU_VERTEX, PASSTHRU_FRAGMENT, ctx);

    // build a pipeline to render it
    let mut pipeline = RenderPipeline::new();
    pipeline.add_descriptor_set_layout(output_descriptors.get_descriptor_layout());
    pipeline.compile_dynamic(ctx, &app, &shader, formats.0, formats.1);

    SceneState {
        descriptor: Some(output_descriptors),
        scene: DisplayObject::new(vec![mesh], pipeline),
    }
}

/// Returns the default color and depth formats that should be suitable for most applications
pub fn get_default_render_formats() -> (Format, Format) {
    (Format::B8G8R8A8_UNORM, Format::D32_SFLOAT)
}

/// Calculates a normal matrix
pub fn calculate_normal_matrix(model: Mat4, view: Mat4) -> Mat4 {
    let model_view = model * view;
    let inverse_mv = model_view.inverse();

    inverse_mv.transpose()
}
