use ash::vk::{CommandBuffer, ShaderStageFlags, Viewport};
use glam::Vec4;
use std::mem::{size_of};
use yoi_vk::descriptors::descriptor_set::VkDescriptorSet;
use yoi_vk::{
    ComputePipeline, DataBuffer, Mesh, RenderPipeline,  VkDescriptorBuffer, Vulkan
};

/// A general type for a piece of geometry consisting of a [DisplayObject] and an
/// optional [VkDescriptorBuffer]
/// Provides a helper to quickly render the geometry.
pub struct SceneState {
    pub descriptor: Option<VkDescriptorBuffer>,
    pub scene: DisplayObject,
}

/// A representative structure for a buffer of data that we might want to manipulate later.
pub struct DataSet {
    pub buffer: DataBuffer,
    pub data: Vec<Vec4>,
}

impl SceneState {
    /// General render function Will automatically supply descriptor buffer if set.
    pub fn render(&self, vk: &Vulkan, cb: &CommandBuffer) {
        self.scene.render(vk, cb, self.descriptor.as_ref());
    }

    /// Same as [SceneState::render] but allows you to pass in a [Vec] of push constant values. This assumes your
    /// pipeline was setup with push constants in mind.
    pub fn render_with_push_constants<T>(
        &self,
        vk: &Vulkan,
        cb: &CommandBuffer,
        push_constants: Vec<Vec<T>>,
        shader_stage_flags: ShaderStageFlags,
    ) {
        self.scene.render_with_constants2(
            vk,
            cb,
            self.descriptor.as_ref(),
            push_constants,
            shader_stage_flags,
        )
    }
}

pub struct ComputeObject {
    pub pipeline: ComputePipeline,
    pub descriptor: VkDescriptorBuffer,
    pub buffers: Vec<DataBuffer>,
}

/// Additional common options when rendering things.
pub struct RenderOpts<T> {
    descriptor: Option<VkDescriptorBuffer>,
    push_constants: Vec<T>,
    push_constant_stage: ShaderStageFlags,
}

impl<T> RenderOpts<T> {
    pub fn new() -> Self {
        RenderOpts {
            descriptor: None,
            push_constants: vec![],
            push_constant_stage: ShaderStageFlags::VERTEX,
        }
    }
}

pub struct DisplayObjectSet {
    pub object: DisplayObject,
    pub descriptor: VkDescriptorBuffer,
}

/// A wrapper around a collection of [yoi_vk::Mesh] objects and a [yoi_vk::RenderPipeline] object to try and help
/// simplify rendering a collection of objects that all utilize the same pipeline. Also provides some helper methods as well.
/// Assumes dynamic rendering is enabled in your Vulkan instance.
pub struct DisplayObject {
    meshes: Vec<Mesh>,
    pipeline: RenderPipeline,
    pub depth_write: bool,
    pub depth_test: bool,
}

impl DisplayObject {
    pub fn new(meshes: Vec<Mesh>, pipeline: RenderPipeline) -> DisplayObject {
        DisplayObject {
            meshes,
            pipeline,
            depth_write: false,
            depth_test: false,
        }
    }

    pub fn set_viewport(&self, vk: &Vulkan, cb: CommandBuffer, vp: Viewport) {
        vk.set_viewport(cb, 0, &[vp])
    }

    /// Renders the meshes. Assumes command buffer has been bound for recording.
    /// Accepts Descriptor Buffer and Push Constants.
    ///
    /// For Push Constants, each nested vector is in reference to one vkCmdPushConstants call. Pass in either a typed vector or make
    /// sure to add the type to the function call.
    pub fn render_with_constants<T>(
        &self,
        vk: &Vulkan,
        cb: &CommandBuffer,
        descriptor_buffer: Option<&VkDescriptorBuffer>,
        push_constants: Vec<Vec<T>>,
    ) {
        let meshes = &self.meshes;
        let pipeline = &self.pipeline;

        pipeline.bind_pipeline(vk, *cb);

        // enable depth write
        if self.pipeline.is_depth_test_enabled() {
            vk.set_command_buffer_depth_test(cb, true);
        }

        if self.pipeline.is_depth_write_enabled() {
            vk.set_command_buffer_depth_write(cb, true);
        }

        if descriptor_buffer.is_some() {
            descriptor_buffer.as_ref().unwrap().bind_descriptors(
                vk,
                cb,
                &pipeline.get_pipeline_layout(),
            );
        }

        for i in 0..meshes.len() {
            if !push_constants.is_empty() {
                unsafe {
                    let dat = &push_constants[i];
                    let slice =
                        std::slice::from_raw_parts(dat.as_ptr() as *const u8, size_of::<T>());
                    vk.set_push_constants(
                        cb,
                        &pipeline.get_pipeline_layout(),
                        ShaderStageFlags::VERTEX,
                        0,
                        slice,
                    );
                }
            }

            meshes[i].render(vk, *cb);
        }
    }

    /// Renders the meshes. Assumes command buffer has been bound for recording.
    /// Accepts Descriptor Buffer and Push Constants.
    ///
    /// For Push Constants, each nested vector is in reference to one vkCmdPushConstants call. Pass in either a typed vector or make
    /// sure to add the type to the function call.
    pub fn render_with_constants2<T>(
        &self,
        vk: &Vulkan,
        cb: &CommandBuffer,
        descriptor_buffer: Option<&VkDescriptorBuffer>,
        push_constants: Vec<Vec<T>>,
        shader: ShaderStageFlags,
    ) {
        let meshes = &self.meshes;
        let pipeline = &self.pipeline;

        pipeline.bind_pipeline(vk, *cb);

        // enable depth write
        if self.pipeline.is_depth_test_enabled() {
            vk.set_command_buffer_depth_test(cb, true);
        }

        if self.pipeline.is_depth_write_enabled() {
            vk.set_command_buffer_depth_write(cb, true);
        }

        if descriptor_buffer.is_some() {
            descriptor_buffer.as_ref().unwrap().bind_descriptors(
                vk,
                cb,
                &pipeline.get_pipeline_layout(),
            );
        }

        for i in 0..meshes.len() {
            if !push_constants.is_empty() {
                unsafe {
                    let dat = &push_constants[i];
                    let slice =
                        std::slice::from_raw_parts(dat.as_ptr() as *const u8, size_of::<T>());
                    vk.set_push_constants(cb, &pipeline.get_pipeline_layout(), shader, 0, slice);
                }
            }

            meshes[i].render(vk, *cb);
        }
    }
    /// Renders the meshes. Assumes command buffer has been bound for recording.
    /// Accepts Descriptor Buffer and Push Constants.
    ///
    /// For Push Constants, each nested vector is in reference to one vkCmdPushConstants call. Pass in either a typed vector or make
    /// sure to add the type to the function call.
    pub fn render(
        &self,
        vk: &Vulkan,
        cb: &CommandBuffer,
        descriptor_buffer: Option<&VkDescriptorBuffer>,
    ) {
        let meshes = &self.meshes;
        let pipeline = &self.pipeline;

        pipeline.bind_pipeline(vk, *cb);

        // enable depth write
        if self.pipeline.is_depth_test_enabled() {
            vk.set_command_buffer_depth_test(cb, true);
        }

        if self.pipeline.is_depth_write_enabled() {
            vk.set_command_buffer_depth_write(cb, true);
        }

        if descriptor_buffer.is_some() {
            descriptor_buffer.as_ref().unwrap().bind_descriptors(
                vk,
                cb,
                &pipeline.get_pipeline_layout(),
            );
        }

        for i in 0..meshes.len() {
            meshes[i].render(vk, *cb);
        }
    }

    /// Updates the instance count of a mesh.
    pub fn update_instance_count(&mut self, mesh_index: usize, num_instances: u32) {
        let mesh = self.meshes.get_mut(mesh_index).unwrap();
        mesh.update_num_instances(num_instances);
    }

    pub fn update_vertex_count(&mut self, mesh_index: usize, num_vertices: u32) {
        let mesh = self.meshes.get_mut(mesh_index).unwrap();
        mesh.update_num_verts(num_vertices);
    }

    pub fn update_attribute<T: std::marker::Copy>(
        &mut self,
        vk: &Vulkan,
        mesh_index: usize,
        location: u32,
        data: &[T],
    ) {
        let mesh = self.meshes.get_mut(mesh_index).unwrap();
        mesh.update_attribute(location, vk, data);
    }

    pub fn draw_with_descriptor_set(
        &self,
        vk: &Vulkan,
        cb: &CommandBuffer,
        descriptor_buffer: Option<&VkDescriptorSet>,
    ) {
        let meshes = &self.meshes;
        let pipeline = &self.pipeline;

        pipeline.bind_pipeline(vk, *cb);

        // enable depth write
        if self.pipeline.is_depth_test_enabled() {
            vk.set_command_buffer_depth_test(cb, true);
        }

        if self.pipeline.is_depth_write_enabled() {
            vk.set_command_buffer_depth_write(cb, true);
        }

        if descriptor_buffer.is_some() {
            descriptor_buffer.as_ref().unwrap().bind_descriptors(
                vk,
                cb,
                pipeline.get_pipeline_layout(),
                0,
            );
        }

        for m in meshes {
            m.render(vk, *cb);
        }
    }
}
