use crate::shaders::deferred::{DEFAULT_GBUFFER_FRAG, DEFAULT_GBUFFER_VERT};
use crate::{PASSTHRU_VERTEX};
use ash::vk;
use ash::vk::{CommandBuffer, CullModeFlags, DescriptorSetLayout, ImageView};
use yoi_vk::{
    Descriptor, Mesh, RenderPipeline, RenderShader, RenderTarget, TextureSamplerDescriptor, VkApp,
    VkDescriptorBuffer, VkObject, VkRenderPass, VkSampler, Vulkan,
};

/// A basic deferred rendering setup
#[deprecated]
pub struct DeferredRenderer {
    gbuffer: RenderTarget,
    lbuffer: RenderTarget,
    geometry_pipeline: RenderPipeline,
    lighting_pipeline: RenderPipeline,
    gbuffer_shader: Option<RenderShader>,
    lbuffer_shader: Option<RenderShader>,
    gbuffer_descriptors: Vec<TextureSamplerDescriptor>,
    lbuffer_descriptors: Vec<TextureSamplerDescriptor>,
    sampler: vk::Sampler,
}

impl DeferredRenderer {
    pub fn new(ctx: &Vulkan, width: u32, height: u32) -> Self {
        let mut gbuffer = RenderTarget::new(width, height);

        // generate attachments for albedo , emission and position.
        gbuffer.generate_attachment(ctx);
        gbuffer.generate_attachment(ctx);
        gbuffer.generate_attachment(ctx);

        let mut sampler = VkSampler::new();
        sampler.build(ctx);

        DeferredRenderer {
            gbuffer,
            lbuffer: RenderTarget::new(width, height),
            geometry_pipeline: RenderPipeline::new_with_descriptor_buffer(),
            lighting_pipeline: RenderPipeline::new_with_descriptor_buffer(),
            gbuffer_shader: None,
            lbuffer_shader: None,
            gbuffer_descriptors: vec![],
            lbuffer_descriptors: vec![],
            sampler: *sampler.raw(),
        }
    }

    pub fn set_gbuffer_shader(&mut self, vk: &Vulkan, vertex: &str, fragment: &str) {
        self.gbuffer_shader = Some(RenderShader::new(vertex, fragment, vk));
    }

    pub fn set_lbuffer_shader(&mut self, vk: &Vulkan, vertex: &str, fragment: &str) {
        self.lbuffer_shader = Some(RenderShader::new(vertex, fragment, vk));
    }

    pub fn add_mesh_to_gbuffer(&mut self, m: &Mesh) {
        self.geometry_pipeline.add_mesh(m);
    }

    pub fn get_gbuffer_renderpass(&self) -> &VkRenderPass {
        self.gbuffer.get_renderpass()
    }

    pub fn build_geometry_stage(
        &mut self,
        vk: &Vulkan,
        app: &VkApp,
        gbuffer_descriptor: Option<DescriptorSetLayout>,
    ) {
        //////////////// BUILD GEOMETRY STAGE ///////////////////////
        if !self.gbuffer_shader.is_some() {
            // load default shader
            self.gbuffer_shader = Some(RenderShader::new(
                DEFAULT_GBUFFER_VERT,
                DEFAULT_GBUFFER_FRAG,
                vk,
            ));
        }

        /// TODO maybe gate this behind another check to account for custom attachment states.
        for i in 0..self.gbuffer.get_num_attachments() {
            self.geometry_pipeline
                .add_default_color_attachment_blend_state();
        }

        // apply descriptor to gbuffer if available.
        if gbuffer_descriptor.is_some() {
            self.geometry_pipeline
                .add_descriptor_set_layout(gbuffer_descriptor.unwrap());
        }

        self.geometry_pipeline.set_cull_mode(CullModeFlags::BACK);
        self.geometry_pipeline.enable_depth_test();
        self.geometry_pipeline.enable_depth_write();
        self.gbuffer.compile(vk);
        self.geometry_pipeline.compile_with_renderpass(
            vk,
            app,
            self.gbuffer_shader.as_ref().unwrap(),
            self.gbuffer.get_renderpass().raw(),
        );
    }

    /// returns the [RenderPipeline] object associated with the Geometry buffer stage.
    pub fn get_geometry_pipeline(&self) -> &RenderPipeline {
        &self.geometry_pipeline
    }

    ////////////////// GEOMETRY BUFFER STAGE ////////////////////

    /// Binds geometry buffer and pipeline for rendering
    pub fn begin_gbuffer(&self, vk: &Vulkan, cb: &CommandBuffer) {
        self.gbuffer.begin(vk, cb);
        self.geometry_pipeline.bind_pipeline(vk, *cb);
        vk.set_command_buffer_depth_test(cb, true);
        vk.set_command_buffer_depth_write(cb, true);
    }

    /// Ends geometry buffer rendering by ending the renderpass associated with the geometry buffer.
    pub fn end_gbuffer(&self, vk: &Vulkan, cb: &CommandBuffer) {
        self.gbuffer.end(vk, cb);
    }

    pub fn get_gbuffer_pipeline_layout(&self) -> vk::PipelineLayout {
        self.geometry_pipeline.get_pipeline_layout()
    }

    pub fn get_gbuffer_output(&self, idx: usize) -> &ImageView {
        self.gbuffer.get_color_view_at(idx)
    }

    /// Generates a descriptor for a gbuffer attachment.
    pub fn generate_gbuffer_descriptor(
        self,
        attachment: usize,
        shader_binding: u32,
        sampler: &vk::Sampler,
    ) -> TextureSamplerDescriptor {
        let mut desc = TextureSamplerDescriptor::new();
        desc.set_image(*self.gbuffer.get_color_view_at(attachment), *sampler);
        desc.set_shader_binding(shader_binding);
        desc
    }

    ////////////////// LIGHT BUFFER STAGE ////////////////////

    pub fn get_lbuffer_output(&self) -> &ImageView {
        self.lbuffer.get_color_view_at(0)
    }

    /// Binds geometry buffer and pipeline for rendering
    pub fn begin_lbuffer(&self, vk: &Vulkan, cb: &CommandBuffer) {
        self.lbuffer.begin(vk, cb);
        self.lighting_pipeline.bind_pipeline(vk, *cb);
    }

    /// Ends geometry buffer rendering by ending the renderpass associated with the geometry buffer.
    pub fn end_lbuffer(&self, vk: &Vulkan, cb: &CommandBuffer) {
        self.lbuffer.end(vk, cb);
    }

    /// Generate texture descriptors for all the gbuffer layers to use in the lbuffer
    pub fn generate_lbuffer_descriptors(&mut self, desc: &mut VkDescriptorBuffer) {
        for i in 0..self.gbuffer.get_num_attachments() {
            let attachment = self.gbuffer.get_color_view_at(i);

            let mut tex = TextureSamplerDescriptor::new();
            tex.set_shader_binding(i as u32);
            tex.set_image(*attachment, self.sampler);

            desc.add_texture_descriptor(tex);
        }
    }

    pub fn add_mesh_to_lbuffer(&mut self, m: &Mesh) {
        self.lighting_pipeline.add_mesh(m);
    }

    /// Builds the lighting stage.
    pub fn build_lighting_stage(
        &mut self,
        vk: &Vulkan,
        app: &VkApp,
        lbuffer_descriptor: Option<DescriptorSetLayout>,
    ) {
        ///////////// BUILD LIGHTING STAGE /////////////////
        if !self.lbuffer_shader.is_some() {
            // load default shader
            self.lbuffer_shader = Some(
                // TODO replace with default lbuffer
                RenderShader::new(PASSTHRU_VERTEX, DEFAULT_GBUFFER_FRAG, vk),
            );
        }

        // add descriptor layout if passed
        if lbuffer_descriptor.is_some() {
            self.lighting_pipeline
                .add_descriptor_set_layout(lbuffer_descriptor.unwrap());
        }

        self.lighting_pipeline.set_cull_mode(CullModeFlags::FRONT);
        self.lighting_pipeline.enable_additive_blending();
        self.lbuffer.compile(vk);

        self.lighting_pipeline.compile_with_renderpass(
            vk,
            app,
            self.lbuffer_shader.as_ref().unwrap(),
            self.lbuffer.get_renderpass().raw(),
        );
    }

    pub fn get_lbuffer_pipeline_layout(&self) -> vk::PipelineLayout {
        self.lighting_pipeline.get_pipeline_layout()
    }
}
