use crate::DisplayObject;
use ash::vk::{CommandBuffer, Sampler};
use glam::Vec4;
use std::mem::size_of;
use yoi_geo::cube::CubeGeo;
use yoi_vk::{
    DataBuffer, Descriptor, Mesh, RenderPipeline, RenderShader, RenderTarget,
    TextureSamplerDescriptor, VkApp, VkDescriptorBuffer, VkObject, VkSampler, Vulkan,
};

pub struct DObjectLayer {
    scene: DisplayObject,
    descriptor_buffer: Option<VkDescriptorBuffer>,
}

pub struct DeferredEngine {
    geometry: Vec<DObjectLayer>,
    lighting_geometry: Vec<DObjectLayer>,
    gbuffer: RenderTarget,
    lbuffer: RenderTarget,
    sampler: Sampler,
    geometry_depth_test: bool,
    geometry_depth_write: bool,
}

impl DeferredEngine {
    pub fn new(ctx: &Vulkan, width: u32, height: u32) -> Self {
        let mut sampler = VkSampler::new();
        sampler.build(ctx);

        DeferredEngine {
            gbuffer: RenderTarget::new(width, height),
            lbuffer: RenderTarget::new(width, height),
            geometry: vec![],
            lighting_geometry: vec![],
            sampler: *sampler.raw(),
            geometry_depth_test: false,
            geometry_depth_write: false,
        }
    }

    pub fn compile(&mut self, ctx: &Vulkan) {
        self.gbuffer.compile(ctx);
        self.lbuffer.compile(ctx);
    }

    /// Adds an attachment state to the
    pub fn add_attachment(&mut self, ctx: &Vulkan, num_attachments: usize) {
        for i in 0..num_attachments {
            self.gbuffer.generate_attachment(ctx);
        }
    }

    pub fn get_num_attachments(&self) -> usize {
        self.gbuffer.get_num_attachments()
    }

    pub fn add_geometry_layer(mut self, geo: DObjectLayer) -> Self {
        self.geometry.push(geo);
        self
    }

    pub fn begin_gbuffer(&self, vk: &Vulkan, cb: &CommandBuffer) {
        self.gbuffer.begin(vk, cb);

        if self.geometry_depth_test {
            vk.set_command_buffer_depth_test(cb, true);
        }

        if self.geometry_depth_write {
            vk.set_command_buffer_depth_write(cb, true);
        }

        for geo in &self.geometry {
            if geo.descriptor_buffer.is_some() {
                geo.scene
                    .render(vk, cb, Some(geo.descriptor_buffer.as_ref().unwrap()));
            } else {
                geo.scene.render(vk, cb, None);
            }
        }
    }

    pub fn end_gbuffer(&self, vk: &Vulkan, cb: &CommandBuffer) {
        self.gbuffer.end(vk, cb);
    }

    pub fn generate_gbuffer_descriptor(
        self,
        attachment: usize,
        shader_binding: u32,
        sampler: &Sampler,
    ) -> TextureSamplerDescriptor {
        let mut desc = TextureSamplerDescriptor::new();
        desc.set_image(*self.gbuffer.get_color_view_at(attachment), *sampler);
        desc.set_shader_binding(shader_binding);
        desc
    }

    //////////////

    /// Builds some default lighting geometry.
    /// In addition to the first 2 args, pass in the
    /// - number of lights
    /// - a [DataBuffer] object containing the positional data for the lights
    /// - and the shader to use for the lighting stage.
    ///
    /// Note this makes the assumption you're using static lights and/or manually calculating the position of the lighting so
    /// this does not take a [VkDescriptorBuffer]
    pub fn setup_default_lighting_geo(
        &mut self,
        ctx: &Vulkan,
        app: &VkApp,
        num_lights: usize,
        light_positions: &DataBuffer,
        shader: &RenderShader,
    ) {
        let mut pipeline = RenderPipeline::new_with_descriptor_buffer();

        let mut meshes = vec![];
        for i in 0..num_lights {
            let geo = CubeGeo::new(2.0, 2.0, 2.0, 10.0, 10.0, 10.0);
            let mesh = Mesh::new()
                .add_attribute(ctx, 0, geo.positions)
                .add_attribute(ctx, 1, geo.normals)
                .add_instanced_attrib_buffer(2, light_positions, size_of::<Vec4>() as u32);

            pipeline.add_mesh(&mesh);
            meshes.push(mesh)
        }

        pipeline.compile_with_renderpass(ctx, app, shader, self.lbuffer.get_renderpass().raw());
        let scene = DisplayObject::new(meshes, pipeline);

        self.lighting_geometry.push(DObjectLayer {
            scene,
            descriptor_buffer: None,
        })
    }

    /// Adds a layer to use during the lighting stage.
    pub fn add_lighting_geometry(mut self, geo: DObjectLayer) -> Self {
        self.lighting_geometry.push(geo);
        self
    }

    pub fn begin_lbuffer(&self, vk: &Vulkan, cb: &CommandBuffer) {
        self.lbuffer.begin(vk, cb);

        for lgeo in &self.lighting_geometry {
            if lgeo.descriptor_buffer.is_some() {
                lgeo.scene
                    .render(vk, cb, Some(lgeo.descriptor_buffer.as_ref().unwrap()));
            } else {
                lgeo.scene.render(vk, cb, None);
            }
        }
    }
}
