vsketch
===

This crate contains the code necessary to start up a Vulkan based sketch in conjunction with `yoi-vk` as well as other 
useful helpers that might be interesting to integrate into work.


API
===
This includes the definition for the `DisplayObject` struct. Possibly other rendering related things will get added
down the line.

Core
===
This contains some core components that might be used for any sketch including
* A struct for maintaining information about the app like window resolution and delta time along with a `DataBuffer` object
to push it to a shader.
* A camera struct
* (NOT WORKING YET) a post processing setup
* a timing class
* the sketch object used to initialize an application
* as well as a `WindowData` object to maintain information about the current window being rendered to.

Objects
===
* Basic components possibly useful for sketches

Shaders
===
* Shader helpers and components 