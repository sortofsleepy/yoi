#version 450


layout(location = 0) in float vLife;
layout(location = 0) out vec4 outColor;
layout(location = 1) in vec4 vColor;

void main(){
    outColor = vec4(vColor.rgb, 1.5);
}