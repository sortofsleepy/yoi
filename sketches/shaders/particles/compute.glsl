#version 450

#define PI 3.14149

/// Particle object.
struct Particle {
    vec4 pos;
    vec4 vel;
    vec4 original_pos;
    vec4 meta;
};

layout(push_constant) uniform PushConsts {
    vec4 settings;
} consts;

layout(std140, binding=0) buffer particle {
    Particle particles[];
};

layout(local_size_x = 1024, local_size_y = 1, local_size_z = 1) in;

#include ../utils/rand_utils.glsl
#include ../utils/rand.glsl
#include ../utils/curl.glsl

const float NOISE_POSITION_SCALE = 1.5;
const float NOISE_TIME_SCALE =  1.0 / 4000.0;
const float NOISE_SCALE = 0.0007;
const float BASE_SPEED = 0.02;


// from https://github.com/dli/flow/blob/02884f0231fd2d9b2218be963788e3bd67e500c8/flow.js#L140
vec3 buildNoise(vec3 pos, float time){
    vec4 xNoisePotentialDerivatives = vec4(0.0);
    vec4 yNoisePotentialDerivatives = vec4(0.0);
    vec4 zNoisePotentialDerivatives = vec4(0.0);

    vec3 noisePosition = pos * NOISE_POSITION_SCALE;
    float noiseTime = time * NOISE_TIME_SCALE;


    for (int i = 0; i < 3; ++i){
        float scale = (1.0 / 2.0) * pow(2.0, float(i));

        float noiseScale = 2.0;
        xNoisePotentialDerivatives += simplexNoiseDerivatives(vec4(noisePosition * pow(2.0, float(i)), noiseTime)) * noiseScale * scale;
        yNoisePotentialDerivatives += simplexNoiseDerivatives(vec4(noisePosition * pow(2.0, float(i)), noiseTime)) * noiseScale * scale;
        zNoisePotentialDerivatives += simplexNoiseDerivatives(vec4(noisePosition * pow(2.0, float(i)), noiseTime)) * noiseScale * 2.0 * scale;

        //yNoisePotentialDerivatives += simplexNoiseDerivatives(vec4((noisePosition + vec3(123.4, 129845.6, -1239.1)) * pow(2.0, float(i)), noiseTime)) * noiseScale * scale;
        //zNoisePotentialDerivatives += simplexNoiseDerivatives(vec4((noisePosition + vec3(-9519.0, 9051.0, -123.0)) * pow(2.0, float(i)), noiseTime)) * noiseScale * scale;
    }

    vec3 noiseVelocity = vec3(
        zNoisePotentialDerivatives[1] - yNoisePotentialDerivatives[2],
        xNoisePotentialDerivatives[2] - zNoisePotentialDerivatives[0],
        yNoisePotentialDerivatives[0] - xNoisePotentialDerivatives[1]
    ) * NOISE_SCALE;

    noiseVelocity.x -= 0.02;

    vec3 velocity = vec3(BASE_SPEED);
    vec3 totalVelocity = velocity + noiseVelocity;


    return totalVelocity * time;

}

void main(){

    // Current SSBO index
    uint index = gl_GlobalInvocationID.x;

    // Read position and velocity
    Particle p = particles[index];

    ////////////
    float time = consts.settings.x * 0.005;
    float golden_angle = consts.settings.y;
    float spread = consts.settings.z;

    /////////////////

    float phi = golden_angle * (PI / 180.0);
    float r = sin(time) * cos(time) + spread * sqrt(index);

    float theta = index * phi;

    float x = r * cos(theta);
    float y = r * sin(theta);

    vec2 target = vec2(x,y);

    float dx = x - p.pos.x;
    float dy = y - p.pos.y;

    p.vel.x += dx  * p.meta.x;
    p.vel.y += dy  * p.meta.x;
    p.vel.z += sin((index + time) * 0.005);

    p.vel *= 0.06;

    p.pos.xyz += p.vel.xyz;

    ////////////////////

    // check for lifetime
    p.pos.a -= p.meta.x * 0.01;

    if (particles[index].pos.a < 0){
        p.pos.a = 1.0;
        p.pos = p.original_pos;
        p.vel = vec4(0.);
    }
    ///////////////////

    // write output
    particles[index] = p;

}

/*
IDEA 1
 // Current SSBO index
    uint index = gl_GlobalInvocationID.x;

    // Read position and velocity
    Particle p = particles[index];

    p.vel.xyz = buildNoise(p.pos.xyz, 1.2);
    //p.vel *= .67;
    p.vel.xy *= 1.2;

    p.pos.xyz += p.vel.xyz;

    // check for lifetime
    p.pos.a -= p.meta.x * 0.008;

    if (particles[index].pos.a < 0){
        p.pos.a = 1.0;
        p.pos = p.original_pos;
        p.vel = vec4(0.);
    }
    ///////////////////

    // write output
    particles[index] = p;
*/

/*
 IDEA 2
 p.vel.xyz = buildNoise(p.pos.xyz, (consts.settings.x * 0.05));


    vec3 curl = curlNoise(p.pos.xyz * 0.01);

    p.vel.xyz += curl;


    p.pos.xy += p.vel.xy;


*/