#version 450
layout (set = 0, binding = 0) uniform sampler2D tInput;
layout(location = 0) out vec4 glFragColor;
layout(location = 0) in vec2 uv;
vec3 lin_to_srgb(vec4 color)
{
    vec3 x = color.rgb * 12.92;
    vec3 y = 1.055 * pow(clamp(color.rgb, 0.0, 1.0), vec3(0.4166667)) - 0.055;
    vec3 clr = color.rgb;
    clr.r = (color.r < 0.0031308) ? x.r : y.r;
    clr.g = (color.g < 0.0031308) ? x.g : y.g;
    clr.b = (color.b < 0.0031308) ? x.b : y.b;
    return clr.rgb;
}

void main()
{
    vec4 color = texture(tInput,uv);
    glFragColor = vec4(lin_to_srgb(color), 1.0);
}