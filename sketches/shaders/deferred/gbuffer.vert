#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} ubo;


layout(binding = 1) uniform GlobalObject {
    mat4 normalMatrix;
} global;


layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;
//layout(location = 2) in vec4 iMeta;
//layout(location = 3) in vec4 iLightInfo;


layout(location = 0) out vec4 vPos;
layout(location = 1) out vec4 vNormal;
//layout(location = 2) out vec4 vLightInfo;

vec3 rotateX(vec3 p, float theta){
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x, p.y * c - p.z * s, p.z * c + p.y * s);
}

vec3 rotateY(vec3 p, float theta) {
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x * c + p.z * s, p.y, p.z * c - p.x * s);
}

vec3 rotateZ(vec3 p, float theta) {
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x * c - p.y * s, p.y * c + p.x * s, p.z);
}

void main(){
    vec4 p = position;


    /*
     p.x *= iLightInfo.z;
    p.y *= iLightInfo.z;
    p.z *= iLightInfo.z;
    */

    //float radius = iLightInfo.y;

    /*
        float radius = 1.0;
    float x = cos(iMeta.y) * sin(iMeta.x) * radius;
    float y = sin(iMeta.y) * sin(iMeta.x) * radius;
    float z = cos(iMeta.x) * radius;

    vec4 iPos = vec4(x, y, z, 1.);

    p.xyz = rotateX(p.xyz, iMeta.x);
    p.xyz = rotateY(p.xyz, iMeta.y);
    p.xyz = rotateY(p.xyz, x);
    */

    ///vec4 pos = p + iPos;
    vec4 pos = p;

    vNormal = global.normalMatrix * normal;
    vPos = ubo.view * pos;
    //vLightInfo = iLightInfo;
    gl_Position = ubo.proj * ubo.view *  pos;
}