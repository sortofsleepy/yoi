#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(location = 0) out vec4 oAlbedo;
layout (location = 1) out vec4 oPosition;
layout (location = 2) out vec4 oNormal;
//layout (location = 1) out vec4 oNormalEmissive;
//layout (location = 2) out vec4 oPosition;

layout(location = 0) in vec4 vPos;
layout(location = 1) in vec4 vNormal;
//layout(location = 2) in vec4 vLightInfo;
void main(){

    oAlbedo = vec4(vNormal.x,0.0,0.0,1.);
    oPosition = vec4(vNormal.x, 1.0, 0.0, 1.0);
    oNormal = vec4(1.0);
    //oNormalEmissive = vec4(normalize(vNormal.xyz),vLightInfo.x);
    //oPosition = vec4(vNormal.x,1.0,0.0, 1.0);
}
