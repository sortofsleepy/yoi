#version 450
layout (input_attachment_index = 0, binding = 0) uniform subpassInput inputPosition;
layout(location = 0) out vec4 glFragColor;

void main(){
    vec3 fragPos = subpassLoad(inputPosition).rgb;
    glFragColor = vec4(fragPos, 1.0);
}