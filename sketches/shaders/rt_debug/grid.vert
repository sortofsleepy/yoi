#version 450

struct Light {
    vec4 pos;
    vec4 ambient_color;
    vec4 diffuse_color;
    vec4 specular_color;
    float falloff;
    float intensity;
    float radius;
    float volume;
};

layout(binding = 1) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} camera;

layout(std140, binding=0) uniform ubo {
    vec4 settings;
};

struct InstanceState {
    mat4 transform;
};

layout(binding=2) buffer trans {
    mat4 transforms[2601];// TODO make this not hard coded.
}mat;

layout(binding=3) uniform trans2 {
    mat4 trans;
}global;

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;
layout(location = 2) in vec4 iColor;




void main(){

    vec4 pos = position;

    // individual transform for each instance.
    mat4 iTransform = mat.transforms[gl_InstanceIndex];

    float trans = sin(settings.x * 10.0);
    // scale along z
    iTransform[2][2] += trans;

   // iTransform[3][1] -= trans;


    gl_Position = camera.proj * camera.view * global.trans * iTransform * pos;

}