#version 450

layout(location = 0) out vec4 glFragColor;

layout(location = 0) in float vLife;

void main(){

    glFragColor = vec4(1.0, 1.0, 0.0, vLife);
}