#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} ubo;

layout(location = 0) in vec4 position;
layout(location = 2) in vec2 uv;
layout(location = 1) in vec4 iPosition;
layout(location = 3) in vec4 iPrevPos;

layout(location = 0) out float vLife;

#define PI 3.14149

mat4 rotationMatrix(vec3 axis, float angle) {
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat4(oc * axis.x * axis.x + c, oc * axis.x * axis.y - axis.z * s, oc * axis.z * axis.x + axis.y * s, 0.0,
    oc * axis.x * axis.y + axis.z * s, oc * axis.y * axis.y + c, oc * axis.y * axis.z - axis.x * s, 0.0,
    oc * axis.z * axis.x - axis.y * s, oc * axis.y * axis.z + axis.x * s, oc * axis.z * axis.z + c, 0.0,
    0.0, 0.0, 0.0, 1.0);
}

vec3 rotate(vec3 v, vec3 axis, float angle) {
    mat4 m = rotationMatrix(axis, angle);
    return (m * vec4(v, 1.0)).xyz;
}

vec2 rotate(vec2 v, float a) {
    float s = sin(a);
    float c = cos(a);
    mat2 m = mat2(c, s, -s, c);
    return m * v;
}
vec3 align(vec3 pos, vec3 dir) {
    vec3 initDir = vec3(1.0,0.0, 0.0);
    vec3 axis = cross(dir, initDir);
    float angle = acos(dot(dir, initDir));
    return rotate(pos, axis, angle);
}

void main(){

    gl_PointSize = 10.0;
    vec4 p = position * vec4(10.);

    float radius = 2.0;
    float ease = 0.04;

    float scale = clamp(iPosition.a, 0.0, 1.0);
    scale = sin(scale * PI) * mix(1.0, 2.0, 0.3);

    vec3 v = vec3(radius - scale,radius + scale,0.0);
    float theta = -uv.y * PI * 2.0 * -PI;
    //v.yz += rotate(v.yz, theta);
    v += rotate(v,vec3(1.0,1.0,1.0),theta);

    ////////////
    vec3 dir = iPosition.xyz - p.xyz;
    dir = normalize(dir);

    v = align(v, dir);
    v += iPosition.xyz;

    vLife = iPosition.a;
    gl_Position = ubo.proj * ubo.view *  vec4(v, 1.);

}


/*
    vec4 pos = position + iPosition;



    float radius = 15.0;
    float scale = clamp(iPosition.a, 0.0, 1.0);
    scale = sin(scale * PI) * mix(1.0, 2.0, 0.3);

    vec3 v = vec3(0.0, radius + scale, 0.0);
    float theta = -uv.y * PI * 2.0;
    v.yz += rotate(v.yz, theta);
    //v += rotate(v,vec3(1.0),theta);

    vec3 dir = normalize(iPosition.xyz - position.xyz);
    v = align(v, normalize(pos.xyz));
    //v += dir;

    vec3 posOffset = iPosition.xyz;
    v += posOffset;

*/