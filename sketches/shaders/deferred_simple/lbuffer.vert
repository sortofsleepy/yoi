#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable
#define NUM_LIGHTS 5

//-light.glsl

layout(binding = 3) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} ubo;


layout(location = 0) in vec4 position;
layout(location = 2) in vec4 iMeta;

void main(){

    float radius = 50.0;

    float x = cos(iMeta.y) * sin(iMeta.x) * radius;
    float y = sin(iMeta.y) * sin(iMeta.x) * radius;
    float z = cos(iMeta.x) * radius;
//
    vec4 iPos = vec4(x, y, z, 1.);

    vec4 p = position;
    p.xyz *= lights.uLights[gl_InstanceIndex].meta.z;
    gl_Position = ubo.proj * ubo.view * p;
}