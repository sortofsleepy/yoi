#if !defined ( NUM_LIGHTS )
#define NUM_LIGHTS 1
#endif

#if !defined (LIGHT_UNIFORM_BINDING)
#define LIGHT_UNIFORM_BINDING 4
#endif

struct Light
{
    vec4  position;
    vec4  ambient;
    vec4  diffuse;
    vec4  specular;

    // meta follows the pattern below.
    vec4 meta;

/*
float intensity;
float radius;
float volume;
float falloff;
*/
};

layout(binding = 4) uniform Lightss
{
    Light uLights[ NUM_LIGHTS ];
}lights;