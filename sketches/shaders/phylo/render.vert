#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} ubo;

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 iPosition;
layout(location = 2) in vec4 meta;
layout(location = 3) in vec4 color;

layout(location = 0) out float vLife;
layout(location = 1) out vec4 vColor;

vec3 rotateX(vec3 p, float theta){
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x, p.y * c - p.z * s, p.z * c + p.y * s);
}
vec3 rotateY(vec3 p, float theta) {
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x * c + p.z * s, p.y, p.z * c - p.x * s);
}

vec3 rotateZ(vec3 p, float theta) {
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x * c - p.y * s, p.y * c + p.x * s, p.z);
}

void main(){
    vec4 p = position;

    p.xyz *= meta.z;

    p.xyz = rotateX(p.xyz,iPosition.z * 0.004 * meta.y);
    p.xyz = rotateY(p.xyz,iPosition.z * 0.004 * meta.y);
    p.xyz = rotateZ(p.xyz,iPosition.z * 0.004 * meta.y);

    vec4 pos = p + iPosition;

    pos.xyz = rotateZ(pos.xyz, iPosition.z *0.01);

    vLife = iPosition.a;
    vColor = color;

    gl_Position = ubo.proj * ubo.view * pos;
}