#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4 proj;
    mat4 view;
} ubo;

layout(push_constant) uniform PushConsts {
// x = time
// y = scale max
    vec4 settings;
} consts;

vec3 rotateZ(vec3 p, float theta) {
    float s = sin(theta);
    float c = cos(theta);
    return vec3(p.x * c - p.y * s, p.y * c + p.x * s, p.z);
}

#define PI 3.14149

float toRadian(float deg){
    return (PI / 180.0) * deg;
}

layout(location = 0) in vec4 position;

layout(location = 0) out vec2 vUv;
layout(location = 1) out float id;


void main(){

    id = consts.settings.z;

    vec4 p = position;

    // shift foreground element forward a bit.
    if (id == 1.0){
        p.z -= 20.0;
        p.xyz = rotateZ(p.xyz, toRadian(45.0));
    } else {
        //p.xyz *= 1.0 + consts.settings.y * consts.settings.a * abs(sin(consts.settings.x));
        p.xyz = rotateZ(p.xyz, toRadian(45.0) );
    }

    gl_Position = ubo.proj * ubo.view *  p;
}