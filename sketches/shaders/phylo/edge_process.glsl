#version 450
layout (set = 0, binding = 0) uniform sampler2D tInput;
layout(location = 0) out vec4 fragColor;


#define PI 3.14149

/// Edge detection shader by @jmk
/// https://www.shadertoy.com/view/Mdf3zr

/// blur from
/// https://www.shadertoy.com/view/lsKSWR

float lookup(vec2 p, float dx, float dy, sampler2D tex, vec2 resolution)
{
    float d = sin(5.0)*0.5 + 1.5;// kernel offset

    vec2 uv = (p.xy + vec2(dx * d, dy * d)) / resolution.xy;
    vec4 c = texture(tex, uv.xy);

    // return as luma
    return 0.2126*c.r + 0.7152*c.g + 0.0722*c.b;
}

vec4 sobel(sampler2D tex, vec2 resolution){
    vec2 p = gl_FragCoord.xy;

    // simple sobel edge detection
    float gx = 0.0;
    gx += -1.0 * lookup(p, -1.0, -1.0, tex, resolution);
    gx += -2.0 * lookup(p, -1.0, 0.0, tex, resolution);
    gx += -1.0 * lookup(p, -1.0, 1.0, tex, resolution);
    gx +=  1.0 * lookup(p, 1.0, -1.0, tex, resolution);
    gx +=  2.0 * lookup(p, 1.0, 0.0, tex, resolution);
    gx +=  1.0 * lookup(p, 1.0, 1.0, tex, resolution);

    float gy = 0.0;
    gy += -1.0 * lookup(p, -1.0, -1.0, tex, resolution);
    gy += -2.0 * lookup(p, 0.0, -1.0, tex, resolution);
    gy += -1.0 * lookup(p, 1.0, -1.0, tex, resolution);
    gy +=  1.0 * lookup(p, -1.0, 1.0, tex, resolution);
    gy +=  2.0 * lookup(p, 0.0, 1.0, tex, resolution);
    gy +=  1.0 * lookup(p, 1.0, 1.0, tex, resolution);

    // hack: use g^2 to conceal noise in the video
    float g = gx*gx + gy*gy;

    vec4 col = texture(tex, p / resolution.xy);
    col += vec4(0.0, g, g, 1.0);

    return vec4(pow(g, 4.0));
}

vec4 sobel(sampler2D tex, vec2 resolution, vec4 color){
    vec2 p = gl_FragCoord.xy;

    // simple sobel edge detection
    float gx = 0.0;
    gx += -1.0 * lookup(p, -1.0, -1.0, tex, resolution);
    gx += -2.0 * lookup(p, -1.0, 0.0, tex, resolution);
    gx += -1.0 * lookup(p, -1.0, 1.0, tex, resolution);
    gx +=  1.0 * lookup(p, 1.0, -1.0, tex, resolution);
    gx +=  2.0 * lookup(p, 1.0, 0.0, tex, resolution);
    gx +=  1.0 * lookup(p, 1.0, 1.0, tex, resolution);

    float gy = 0.0;
    gy += -1.0 * lookup(p, -1.0, -1.0, tex, resolution);
    gy += -2.0 * lookup(p, 0.0, -1.0, tex, resolution);
    gy += -1.0 * lookup(p, 1.0, -1.0, tex, resolution);
    gy +=  1.0 * lookup(p, -1.0, 1.0, tex, resolution);
    gy +=  2.0 * lookup(p, 0.0, 1.0, tex, resolution);
    gy +=  1.0 * lookup(p, 1.0, 1.0, tex, resolution);

    // hack: use g^2 to conceal noise in the video
    float g = gx*gx + gy*gy;

    vec4 col = color;
    col += vec4(0.0, g, g, 1.0);

    return vec4(pow(g, 4.0));
}

void main() {

    vec2 resolution =  vec2(1280.);
    vec2 uv = gl_FragCoord.xy / resolution;
    vec4 original = texture(tInput, uv);

    vec2 pixel = 1.0 / resolution;
    vec2 st = gl_FragCoord.xy * pixel;

    float directions = 25.0;// BLUR DIRECTIONS (Default 16.0 - More is better but slower)
    float quality = 5.0;// BLUR QUALITY (Default 4.0 - More is better but slower)

    // blur edges
    vec4 color = vec4(0.);
    float _step = 1.0 /quality;
    float radius = 0.007;
    // Blur calculations
    for (float d= -_step; d < PI; d += _step)
    {
        for (float i=0.3; i<=1.0; i+= (_step))
        {
            vec4 sob = sobel(tInput, resolution);
            vec4 orig = texture(tInput, uv - vec2(sin(d), cos(d))*radius*i);
            color += orig + sob;
        }
    }
    // Output to screen
    color /= quality * 10.0;

    //////////

    fragColor = color;

}
