#version 450

layout (set = 0, binding = 0) uniform sampler2D tInput;

layout(location = 0) out vec4 outColor;

void main(){
    vec2 resolution = vec2(1280.0);
    vec2 uv = gl_FragCoord.xy / resolution;
    vec4 orig =  texture(tInput, uv);
    outColor = orig;

}