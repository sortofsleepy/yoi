#version 450
layout (set = 0, binding = 0) uniform sampler2D tOriginal;
layout (set = 0, binding = 1) uniform sampler2D tEdges;
layout(location = 0) out vec4 glFragColor;

// vignette idea from
// https://www.shadertoy.com/view/lsKSWR



float lookup(vec2 p, float dx, float dy, sampler2D tex, vec2 resolution)
{
    float d = sin(5.0)*0.5 + 1.5;// kernel offset

    vec2 uv = (p.xy + vec2(dx * d, dy * d)) / resolution.xy;
    vec4 c = texture(tex, uv.xy);

    // return as luma
    return 0.2126*c.r + 0.7152*c.g + 0.0722*c.b;
}

vec4 sobel(sampler2D tex, vec2 resolution){
    vec2 p = gl_FragCoord.xy;

    // simple sobel edge detection
    float gx = 0.0;
    gx += -1.0 * lookup(p, -1.0, -1.0, tex, resolution);
    gx += -2.0 * lookup(p, -1.0, 0.0, tex, resolution);
    gx += -1.0 * lookup(p, -1.0, 1.0, tex, resolution);
    gx +=  1.0 * lookup(p, 1.0, -1.0, tex, resolution);
    gx +=  2.0 * lookup(p, 1.0, 0.0, tex, resolution);
    gx +=  1.0 * lookup(p, 1.0, 1.0, tex, resolution);

    float gy = 0.0;
    gy += -1.0 * lookup(p, -1.0, -1.0, tex, resolution);
    gy += -2.0 * lookup(p, 0.0, -1.0, tex, resolution);
    gy += -1.0 * lookup(p, 1.0, -1.0, tex, resolution);
    gy +=  1.0 * lookup(p, -1.0, 1.0, tex, resolution);
    gy +=  2.0 * lookup(p, 0.0, 1.0, tex, resolution);
    gy +=  1.0 * lookup(p, 1.0, 1.0, tex, resolution);

    // hack: use g^2 to conceal noise in the video
    float g = gx*gx + gy*gy;

    vec4 col = texture(tex, p / resolution.xy);
    col += vec4(0.0, g, g, 1.0);

    return vec4(pow(g, 4.0));
}

vec4 sobel(sampler2D tex, vec2 resolution, vec4 color){
    vec2 p = gl_FragCoord.xy;

    // simple sobel edge detection
    float gx = 0.0;
    gx += -1.0 * lookup(p, -1.0, -1.0, tex, resolution);
    gx += -2.0 * lookup(p, -1.0, 0.0, tex, resolution);
    gx += -1.0 * lookup(p, -1.0, 1.0, tex, resolution);
    gx +=  1.0 * lookup(p, 1.0, -1.0, tex, resolution);
    gx +=  2.0 * lookup(p, 1.0, 0.0, tex, resolution);
    gx +=  1.0 * lookup(p, 1.0, 1.0, tex, resolution);

    float gy = 0.0;
    gy += -1.0 * lookup(p, -1.0, -1.0, tex, resolution);
    gy += -2.0 * lookup(p, 0.0, -1.0, tex, resolution);
    gy += -1.0 * lookup(p, 1.0, -1.0, tex, resolution);
    gy +=  1.0 * lookup(p, -1.0, 1.0, tex, resolution);
    gy +=  2.0 * lookup(p, 0.0, 1.0, tex, resolution);
    gy +=  1.0 * lookup(p, 1.0, 1.0, tex, resolution);

    // hack: use g^2 to conceal noise in the video
    float g = gx*gx + gy*gy;

    vec4 col = color;
    col += vec4(0.0, g, g, 1.0);

    return vec4(pow(g, 4.0));
}

void main(){
    vec2 uv = gl_FragCoord.xy / vec2(1280.0);
    vec4 scene = texture(tOriginal, uv);
    vec4 edges = texture(tEdges, uv);


    vec2 vuv = uv *=  1.0 - uv.yx;
    float vig = vuv.x*vuv.y * 8.0;
    //vig = pow(vig, 0.15);

    glFragColor = (scene * vig) * edges;

    //glFragColor = edges;
}