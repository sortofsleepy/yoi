#version 450

layout (set = 0, binding = 0) uniform sampler2D tInput;
layout(location = 0) in vec2 vUv;
layout(location = 0) out vec4 glFragColor;

// TODO add uniform
const vec2 iResolution = vec2(1280.);

void main() {

    // init
    vec2 UV = vUv;
    vec4 scene_col = texture(tInput, UV);

    // translate uvs from rectangular input texture to square voronoi texture.
    ivec2 tex_size = textureSize(tInput, 0);
    vec2 uv = UV;
    if(tex_size.x > tex_size.y)
    uv.y = ((uv.y - 0.5) * (float(tex_size.x) / float(tex_size.y))) + 0.5;
    else
    uv.x = ((uv.x - 0.5) * (float(tex_size.y) / float(tex_size.x))) + 0.5;

    // for the voronoi seed texture we just store the UV of the pixel if the pixel is part
    // of an object (emissive or occluding), or black otherwise.
    glFragColor = vec4(UV.x * scene_col.a, UV.y * scene_col.a, 0.0, 1.0);
}
