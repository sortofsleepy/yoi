#version 450
layout(push_constant) uniform PushConsts {
    vec4 settings;
} consts;
layout(location = 0) out vec2 uv;
out gl_PerVertex
{
    vec4 gl_Position;
};
void main()
{
    vec2 scale = vec2(0.5);
    //vec4 pos = vec4(vec2((gl_VertexIndex << 1) & 2, gl_VertexIndex & 2) * 3.0f - 1.0f, 0.0f, 1.0f);
    uv = vec2((gl_VertexIndex << 1) & 2, gl_VertexIndex & 2);
    gl_Position = vec4(uv * 2.0f + -1.0f, 0.0f, 1.0f);
}