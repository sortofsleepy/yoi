use std::f32::consts::PI;
use std::mem::size_of;
use std::rc::Rc;
use glam::{Vec4, vec4};
use yoi_vk::*;
use yoi_math::*;

#[derive(Copy, Clone)]
struct Crack {
    x: f32,
    y: f32,
    t: f32,
}

impl Crack {
    pub fn new() -> Self {
        Crack {
            x: 0.0,
            y: 0.0,
            t: 0.0,
        }
    }
}


const MAX_NUM: usize = 50;

// Max buffer size to determine how many points to draw on the screen.
const MAX_POINTS: usize = 300000;

pub struct SubstratePanel {
    cracks: Vec<Crack>,
    positions: Vec<Vec4>,
    position_buffer: DataBuffer,
    grid: Vec<i32>,
    count: usize,
    dimensions: (usize, usize),
}

impl SubstratePanel {
    pub fn new(instance: &Vulkan, width: usize, height: usize) -> Self {
        let positions: Vec<Vec4> = Vec::new();

        let mut grid: Vec<i32> = vec![0; width * height];

        // current count of cracks
        let mut count: usize = 0;

        // initialize cracks vector
        let mut cracks: Vec<Crack> = Vec::new();

        // erase grid - will also make random seeds.
        erase_grid(&mut grid, (width, height));

        // initialize just 3 cracks
        for _i in 0..3 {
            make_crack(&mut cracks, &mut count, &mut grid, (width, height));
        }

        // Build position buffer to hold all possible instances.
        let mut position_buffer = DataBuffer::create(instance, BufferFormat::default().size((size_of::<Vec4>() * MAX_POINTS) as u64));
        position_buffer.set_data(instance, &positions);


        SubstratePanel {
            cracks,
            positions,
            position_buffer,
            grid,
            count,
            dimensions: (width, height),
        }
    }

    pub fn update(&mut self, instance: &Vulkan) {
        // Move all the cracks
        for i in 0..self.count {
            move_crack(i, &mut self.cracks, &mut self.positions, &mut self.grid, &mut self.count, self.dimensions);
        }

        // update instance position buffer
        self.position_buffer.set_data(instance, &self.positions);

    }

    /// Returns the number of instances to send to your geometry
    pub fn get_num_instances(&self) -> usize {
        self.positions.len()
    }

    /// Returns the buffer that is storing the instanced positions to render.
    pub fn get_position_buffer(&self) -> &DataBuffer {
        &self.position_buffer
    }
}


/// Erase cracks and seed grid.
fn erase_grid(grid: &mut Vec<i32>, dimensions: (usize, usize)) {
    // erase grid
    for y in 0..dimensions.1 {
        for x in 0..dimensions.0 {
            grid[y * dimensions.0 + x] = 50000;
        }
    }

    // make random seeds
    for _k in 0..16 {
        let i = (rand_float(0.0, (dimensions.0 * dimensions.1 - 1) as f32)) as usize;
        grid[i] = (rand_float(0.0, 360.0)) as i32;
    }
}

/// Initializes crack with starting values.
fn find_crack_start(crack: &mut Crack, grid: &mut Vec<i32>, dimensions: (usize, usize)) {
    // pick random point
    let mut px: usize = 0;
    let mut py: usize = 0;

    // shift until crack found
    let mut found = false;
    let mut timeout = 0;

    while !found || timeout < 1000 {
        px = (rand_float(0.0, dimensions.0 as f32)) as usize;
        py = (rand_float(0.0, dimensions.1 as f32)) as usize;

        if grid[py * dimensions.0 + px] < 10000 {
            found = true;
            break;
        }

        timeout += 1;

        if timeout > 1000 {
            break;
        }
    }

    if found {

        // start crack
        let mut a = grid[py * dimensions.0 + px];

        if rand_float(0.0, 100.0) < 50.0 {
            a -= 90 + (rand_float(-2.0, 2.1)) as i32;
        } else {
            a += 90 + (rand_float(-2.0, 2.1)) as i32;
        }

        crack.x = px as f32;
        crack.y = py as f32;
        crack.t = a as f32;

        crack.x += 2.61 * (crack.t * PI / 180.0).cos();
        crack.y += 2.61 * (crack.t * PI / 180.0).sin();
    }
}

/// Moves a crack
fn move_crack(i: usize, cracks: &mut Vec<Crack>, positions: &mut Vec<Vec4>, grid: &mut Vec<i32>, count: &mut usize, dimensions: (usize, usize)) {
    cracks[i].x += 0.42 * (cracks[i].t * PI / 180.0).cos();
    cracks[i].y += 0.42 * (cracks[i].t * PI / 180.0).sin();

    // bound check
    let z = 0.33;
    let cx = (cracks[i].x + rand_float(-z, z)) as i32;
    let cy = (cracks[i].y + rand_float(-z, z)) as i32;

    let x = cracks[i].x + rand_float(-z, z);
    let y = cracks[i].y + rand_float(-z, z);

    if positions.len() < MAX_POINTS {
        let pos = vec4(x, y, 0.0, 1.0);
        positions.push(pos);
    }

    if cx >= 0 && cx < dimensions.0 as i32 && cy >= 0 && cy < dimensions.1 as i32 {
        let crack_t = cracks[i].t as i32;
        let index = (cy * (dimensions.0 as i32) + cx) as usize;
        if grid[index] > 10000 || ((grid[index] - crack_t).abs() < 5) {
            grid[index] = crack_t as i32;
        } else if (grid[index] - crack_t).abs() > 2 {
            find_crack_start(&mut cracks[i], grid, dimensions);
            make_crack(cracks, count, grid, dimensions);
        }
    } else {
        find_crack_start(&mut cracks[i], grid, dimensions);
        make_crack(cracks, count, grid, dimensions);
    }
}

/// creates a new crack by initializing an instance, finding the start point, creating a new position and
/// saving to Vec objects.
fn make_crack(cracks: &mut Vec<Crack>, count: &mut usize, grid: &mut Vec<i32>, dimensions: (usize, usize)) {
    if *count < MAX_NUM {
        let mut crack = Crack::new();
        find_crack_start(&mut crack, grid, dimensions);
        cracks.push(crack);
        *count += 1;
    }
}
