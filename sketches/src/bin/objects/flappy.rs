use std::mem::size_of;
use ash::vk::{CommandBuffer, DescriptorSetLayout, PrimitiveTopology};
use glam::{vec2, vec4, Vec4};
use yoi_vk::{DataBuffer, Mesh, RenderPipeline, RenderShader, VkApp, VkDescriptorBuffer, Vulkan};

// A kind of un-successful attempt @ replicating this
// https://github.com/yiwenl/flappyes/blob/gh-pages/experiments/trails02/src/js/DrawTrails.js
// The MRT output and the fact that textures are used complicates things.

pub struct MeshLine {
    mesh: Option<Mesh>,
    pipeline: RenderPipeline,
}

#[derive(Copy, Clone)]
struct ParticleData {
    pos: Vec4,
    vel: Vec4,
    orig_vel: Vec4,
    meta: Vec4,
}

impl MeshLine {
    pub fn new() -> Self {
        MeshLine {
            mesh: None,
            pipeline: RenderPipeline::new_with_descriptor_buffer(),
        }
    }

    pub fn setup(
        &mut self,
        ctx: &Vulkan,
        app: &VkApp,
        length: usize,
        numsides: usize,
        descriptor_layout: &DescriptorSetLayout,
        position_buff: &DataBuffer,
        num_particles: u32,
        topology:PrimitiveTopology
    ) {
        let mut positions = vec![];
        let mut uvs = vec![];
        let mut indices = vec![];

        let mut count = 0;
        for i in 0..length {
            for j in 0..numsides {
                positions.push(vec4(i as f32, j as f32, 0f32, 1f32));
                positions.push(vec4((i + 1) as f32, j as f32, 0f32, 1f32));
                positions.push(vec4((i + 1) as f32, (j + 1) as f32, 0f32, 1f32));
                positions.push(vec4(i as f32, (j + 1) as f32, 0f32, 1f32));

                uvs.push(vec2((i as f32) / (length as f32), (j as f32) / (numsides as f32)));
                uvs.push(vec2(((i + 1) as f32) / (length as f32), (j as f32) / (numsides as f32)));
                uvs.push(vec2(((i + 1) as f32) / (length as f32), ((j + 1) as f32) / (numsides as f32)));
                uvs.push(vec2((i as f32) / (length as f32), ((j + 1) as f32) / (numsides as f32)));

                indices.push(count * 4 + 0);
                indices.push(count * 4 + 1);
                indices.push(count * 4 + 2);
                indices.push(count * 4 + 0);
                indices.push(count * 4 + 2);
                indices.push(count * 4 + 3);

                count += 1;
            }
        }

        let shader = RenderShader::new(
            "shaders/flappy/particles.vert",
            "shaders/flappy/particles.frag",
            ctx,
        );

        let len = positions.len();
        let mesh = Mesh::new()
            .add_attribute(ctx, 0, positions)
            .add_attribute(ctx, 2, uvs)
            .add_instanced_attrib_buffer(1, position_buff, size_of::<ParticleData>() as u32)
            .add_instanced_attrib_buffer(3, position_buff, (size_of::<ParticleData>() * 4 * 3) as u32)
            .set_num_instances(len as u32)
            .add_index_data(ctx, indices);

        //mesh.set_num_instances(num_particles);

        self.pipeline.set_topology(topology);
        self.pipeline.add_mesh(&mesh);
        self.pipeline.enable_alpha_blending();
        self.pipeline.add_descriptor_set_layout(*descriptor_layout);
        self.pipeline.compile(ctx, app, &shader);
        self.mesh = Some(mesh);
    }

    pub fn render(
        &self,
        ctx: &Vulkan,
        cb: &CommandBuffer,
        descriptor_buffer: &VkDescriptorBuffer,
    ) {
        let mesh = self.mesh.as_ref().unwrap();
        let pipeline = &self.pipeline;

        pipeline.bind_pipeline(ctx, *cb);

        descriptor_buffer.bind_descriptors(ctx, cb, &pipeline.get_pipeline_layout());

        mesh.draw(ctx, *cb);
    }
}