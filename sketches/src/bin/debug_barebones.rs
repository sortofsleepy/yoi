use ash::vk::{DescriptorSetLayout, ImageAspectFlags};
use raw_window_handle::{HasRawDisplayHandle, HasRawWindowHandle};
use vsketch::shaders::common::load_file;
use vsketch::utils::present_to_screen;
use vsketch::*;
use winit::dpi::LogicalSize;
use winit::event::Event;
use winit::event::WindowEvent;
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::WindowBuilder;
use yoi_geo::cube::CubeGeo;
use yoi_vk::*;

const WIDTH: u32 = 1280;
const HEIGHT: u32 = 920;

fn main() {
    let e_loop = EventLoop::new();

    // build instance. First build tmp window to get surface extensions
    let window = WindowBuilder::new()
        .with_title("tmp")
        .with_inner_size(LogicalSize::new(WIDTH, HEIGHT))
        .build(&e_loop)
        .unwrap();

    let mut started = false;

    let mut info = VkAppInfo {
        window_extensions: ash_window::enumerate_required_extensions(window.raw_display_handle())
            .unwrap()
            .to_vec(),
        enable_descriptor_buffer: true, // TODO maybe just enable by default
        enable_ray_trace: false,
        enable_dynamic_render: false,
        ..Default::default()
    };

    // build instance with info
    // TODO not sure if this ought to be ref counted, leaving alone for now since it makes things easier to work with.
    let mut instance = Vulkan::new(info);

    let surface = unsafe {
        ash_window::create_surface(
            instance.get_entry(),
            instance.get_vk_instance(),
            window.raw_display_handle(),
            window.raw_window_handle(),
            None,
        )
        .unwrap()
    };

    instance.build_debug();

    // run setup
    // TODO might re-think this but for multi-window it might make sense to have separate instances?
    instance.setup(&surface);

    e_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        match event {
            Event::MainEventsCleared => {}

            Event::Resumed => {}

            Event::WindowEvent { event, window_id } => {
                match event {
                    WindowEvent::KeyboardInput { .. } => {}

                    // Handle resize events
                    WindowEvent::Resized(size) => {}

                    // Handle focus events.
                    WindowEvent::Focused { .. } => {}

                    WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                    _ => {}
                }
            }
            _ => {}
        }
    });
}

fn build_obj(ctx: &Vulkan, app: &VkApp, ds_layout: DescriptorSetLayout) -> DisplayObject {
    let size = 2.0;
    let geo = CubeGeo::new(size, size, size, 10.0, 10.0, 10.0);

    let mesh = Mesh::new().set_num_vertices(3);

    let shader = RenderShader::new_debug(PASSTHRU_VERTEX, PASSTHRU_FRAGMENT, ctx);

    let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
    pipeline.add_mesh(&mesh);

    pipeline.compile(ctx, app, &shader);
    pipeline.add_label(ctx, String::from("boop"));
    DisplayObject::new(vec![mesh], pipeline)
}
