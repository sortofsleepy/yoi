use std::cell::{RefCell, RefMut};
use std::f32::consts::PI;
use std::rc::Rc;
use std::sync::Arc;
use glam::Mat4;
use winit::event::VirtualKeyCode::S;
use yoi_math::rand_float;

/// Attempt to try passing a Vector around but issue lies on line 23
#[derive(Clone, Copy)]
pub struct Square {
    pub nest_level: f32,
    pub count: i32,
    pub kids: [usize; 2],
    pub has_kids: bool,
    pub split_dir: f32,
}

impl Square {
    pub fn new(squares: &mut RefCell<Vec<Square>>) -> Self {
        let sq = Square::default();
        squares.borrow_mut().push(sq);
        squares.borrow_mut()[0] // this returns a "new" object, not the one currently in the RefCell
    }

    pub fn set_level(&mut self, squares: &mut RefCell<Vec<Square>>) {
        self.nest_level = rand_float(0.0, 20.0);

        if self.nest_level > 10.0 {
            self.subdivide(squares)
        }
    }

    pub fn subdivide(&mut self, squares: &mut RefCell<Vec<Square>>) {

        self.split_dir = if rand_float(0.0, 1.0) > 0.5 {
            1.0
        } else {
            0.0
        };


        let mut a = Square::new(squares);
        let mut b = Square::new(squares);

        // if we
        a.set_level(squares);
        b.set_level(squares);

        let len = squares.borrow().len() - 1;
        //println!("Size is {}, a index should be {}, b index should be {}", len, len - 2, len - 1);
        self.kids[0] = len - 2;
        self.kids[1] = len - 1;
        self.has_kids = true;
    }

    pub fn update(&mut self, squares: &mut RefCell<Vec<Square>>) {
        if self.has_kids {

            let mut list = squares.borrow_mut();



            if self.split_dir == 0.0 {

                {
                    let a = &mut list[self.kids[0]];
                    //println!("A Has kids, {}",a.nest_level);
                }
                {
                    let b = &mut list[self.kids[1]];
                    //println!("B Has kids, {}",b.nest_level);
                }
            } else {

            }
        }
    }
}

impl Default for Square {
    fn default() -> Self {
        Square {
            nest_level: 0.0,
            count: 0,
            kids: [0, 0],
            has_kids: false,
            split_dir: 0.0,
        }
    }
}


fn main() {
    let mut squares: RefCell<Vec<Square>> = RefCell::new(vec![]);

    let mut sq = Square::new(&mut squares);
    sq.set_level(&mut squares);
    sq.update(&mut squares);

}
