use ash::vk;
use ash::vk::{AccessFlags, AttachmentDescription, AttachmentLoadOp, AttachmentReference, AttachmentStoreOp, DependencyFlags, DescriptorSetLayout, DescriptorType, Format, Framebuffer, FramebufferCreateInfo, ImageLayout, ImageTiling, ImageUsageFlags, MemoryPropertyFlags, PipelineBindPoint, PipelineStageFlags, RenderPass, SampleCountFlags, SUBPASS_EXTERNAL, SubpassContents, SubpassDependency};
use vsketch::{DisplayObject, PASSTHRU_FRAGMENT, PASSTHRU_VERTEX, VkRenderCore, VSketch};
use vsketch::api::api::DisplayObjectSet;
use yoi_geo::simpleshapes::SimpleShapes;
use yoi_vk::*;
use yoi_vk::render::renderpass2::{SubpassDescriptor, SubpassInput};

struct State {
    rp: RenderPass2,
    fb: Vec<Framebuffer>,
    geo1: DisplayObject,
    geo2: DisplayObject,
    ds: VkDescriptorBuffer,
}

const WIDTH: u32 = 1280;
const HEIGHT: u32 = 1280;


fn main() {
    VSketch::new()
        .set_size(WIDTH, HEIGHT)
        .add_setup(setup)
        .add_draw(draw)
        .run();
}

fn setup(vk: &mut Vulkan, comp: &mut VkRenderCore) -> State {

    // build texture used for second framebuffer attachment
    let mut format = TextureFormat::default();
    format.texture_format = comp.meta.get_swapchain().get_color_format();
    format.extent.width = WIDTH;
    format.extent.height = HEIGHT;
    format.extent.depth = 1;
    format.mip_levels = 1;
    format.array_layers = 1;
    format.sample_count = SampleCountFlags::TYPE_1;
    format.tiling_mode = ImageTiling::OPTIMAL;
    format.image_usage_flags = ImageUsageFlags::COLOR_ATTACHMENT | ImageUsageFlags::SAMPLED | ImageUsageFlags::TRANSFER_SRC;
    format.memory_property_flags = MemoryPropertyFlags::DEVICE_LOCAL;

    let texture = VkTexture::new(vk, format);

    // build descriptor
    let mut sampler = VkSampler::new();
    sampler.build(vk);

    let mut ds = VkDescriptorBuffer::new();
    let mut tex = TextureSamplerDescriptor::new();
    tex.descriptor_type = DescriptorType::INPUT_ATTACHMENT;
    tex.set_image(*texture.get_image_view(), *sampler.raw());

    ds.add_texture_descriptor(tex);

    ds.build(vk);


    let rp = build_renderpass(vk, comp.meta.get_swapchain().get_color_format());
    let fb = build_framebuffer(vk, rp.raw(), comp.meta.get_swapchain(), &texture);

    let geo1 = build_geo(vk, &comp.meta, rp.raw());
    let geo2 = build_geo2(vk, &comp.meta, rp.raw(), ds.get_descriptor_layout());

    State {
        rp,
        fb,
        geo1,
        geo2,
        ds,
    }
}

fn draw(vk: &mut Vulkan, comp: &mut VkRenderCore, model: &mut State) {
    let renderer = &mut comp.render;
    let meta = &mut comp.meta;
    let swapchain = meta.get_swapchain();

    let mut rp = &mut model.rp;
    let mut fb = &mut model.fb;

    let mut geo1 = &mut model.geo1;
    let mut geo2 = &mut model.geo2;

    let ds = &mut model.ds;

    renderer.record_commands_with_renderpass2(vk, meta, |cb, i| {
        rp.begin(vk, cb, &fb[i]);


        geo1.render(vk, cb, None);

        vk.next_subpass(cb, SubpassContents::INLINE);
        geo2.render(vk, cb, Some(ds));

        rp.end(vk, cb);
    });


    renderer.present(vk, swapchain);
    renderer.finish_present();
}

fn build_output(vk:&Vulkan, app:&VkApp, rp:&RenderTarget2, tex:&ImageView) -> DisplayObjectSet {
    let mut sampler = VkSampler::new();
    sampler.build(vk);

    let geo = SimpleShapes::generate_fullscreen_triangle();
    let shader = RenderShader::new(
        PASSTHRU_VERTEX,
        PASSTHRU_FRAGMENT,
        vk,
    );

    let mesh = Mesh::new()
        .add_attribute(vk, 0, geo.positions)
        .set_num_vertices(3);

    let mut tex = TextureSamplerDescriptor::new();
    tex.set_image(*tex, *sampler);
    tex.set_shader_binding(0);

    let mut descriptor = VkDescriptorBuffer::new();
    descriptor.add_texture_descriptor(tex);
    descriptor.build(vk);

    let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
    pipeline.add_mesh(&mesh);
    pipeline.enable_additive_blending();
    pipeline.add_descriptor_set_layout(descriptor.get_descriptor_layout());
    pipeline.compile_with_renderpass(vk, app, &shader, rp.get_renderpass().raw());


    DisplayObjectSet {
        object: DisplayObject::new(vec![mesh], pipeline),
        descriptor,
    }
}

fn build_geo(ctx: &Vulkan, app: &VkApp, rp: &RenderPass) -> DisplayObject {
    let m = Mesh::new()
        .set_num_vertices(3);

    let shader = RenderShader::new(
        PASSTHRU_VERTEX,
        "shaders/subpass-tests/debug.frag",
        ctx,
    );

    let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
    pipeline.add_mesh(&m);
    pipeline.add_default_color_attachment_blend_state();
    pipeline.add_default_color_attachment_blend_state();
    pipeline.compile2(ctx, app, &shader, Some(rp));

    DisplayObject::new(vec![m], pipeline)
}

fn build_geo2(ctx: &Vulkan, app: &VkApp, rp: &RenderPass, ds: DescriptorSetLayout) -> DisplayObject {
    let m = Mesh::new()
        .set_num_vertices(3);

    let mut shader = RenderShader::new(
        PASSTHRU_VERTEX,
        "shaders/subpass-tests/debug2.frag",
        ctx,
    );


    let mut pipeline = RenderPipeline::new();
    pipeline.add_mesh(&m);
    pipeline.add_descriptor_set_layout(ds);
    pipeline.set_subpass_index(1);
    pipeline.add_default_color_attachment_blend_state();
    pipeline.add_default_color_attachment_blend_state();
    pipeline.compile2(ctx, app, &shader, Some(rp));

    DisplayObject::new(vec![m], pipeline)
}

/// Build framebuffers
fn build_framebuffer(vk: &Vulkan, rp: &RenderPass, swapchain: &VkSwapChain, texture: &VkTexture) -> Vec<Framebuffer> {
    let mut framebuffers = vec![];

    for i in 0..3 {
        let attachments = &swapchain.get_image_views()[i];
        let attachment = &[*attachments, *texture.get_image_view()];

        let create_info = FramebufferCreateInfo::default()
            .width(WIDTH)
            .height(HEIGHT)
            .render_pass(*rp)
            .layers(1)
            .attachments(attachment);

        let fb = vk.generate_framebuffer(create_info);

        framebuffers.push(fb);
    }

    framebuffers
}

/// build subpass-tests
fn build_renderpass(vk: &mut Vulkan, swapchain_fmt: Format) -> RenderPass2 {
    let mut rp = RenderPass2::new();

    // add attachment descriptions
    rp.add_attachment_description(
        AttachmentDescription {
            format: swapchain_fmt,
            samples: SampleCountFlags::TYPE_1,
            load_op: vk::AttachmentLoadOp::CLEAR,
            store_op: vk::AttachmentStoreOp::STORE,
            stencil_load_op: AttachmentLoadOp::DONT_CARE,
            stencil_store_op: AttachmentStoreOp::DONT_CARE,
            initial_layout: ImageLayout::UNDEFINED,
            final_layout: vk::ImageLayout::PRESENT_SRC_KHR,
            ..Default::default()
        }
    );

    rp.add_attachment_description(
        AttachmentDescription {
            format: swapchain_fmt,
            samples: SampleCountFlags::TYPE_1,
            load_op: vk::AttachmentLoadOp::CLEAR,
            store_op: vk::AttachmentStoreOp::STORE,
            stencil_load_op: AttachmentLoadOp::DONT_CARE,
            stencil_store_op: AttachmentStoreOp::DONT_CARE,
            initial_layout: ImageLayout::UNDEFINED,
            final_layout: vk::ImageLayout::PRESENT_SRC_KHR,
            ..Default::default()
        }
    );

    rp.add_attachment_reference(
        vk::AttachmentReference {
            attachment: 0,
            layout: ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
        }
    );

    rp.add_attachment_reference(
        vk::AttachmentReference {
            attachment: 1,
            layout: ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
        }
    );
    rp.add_subpass_descriptor(
        SubpassDescriptor {
            pipeline_bind_point: PipelineBindPoint::GRAPHICS
        }
    );
    rp.add_subpass_descriptor(
        SubpassDescriptor {
            pipeline_bind_point: PipelineBindPoint::GRAPHICS
        }
    );
    rp.add_subpass_dependency(
        SubpassDependency::default()

            .src_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
            .dst_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
            .src_access_mask(AccessFlags::empty())
            .dst_access_mask(AccessFlags::COLOR_ATTACHMENT_WRITE)
            .dependency_flags(DependencyFlags::empty())
    );
    rp.add_subpass_dependency(
        SubpassDependency::default()
            .src_subpass(0)
            .dst_subpass(1)
            .src_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
            .dst_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
            .src_access_mask(AccessFlags::empty())
            .dst_access_mask(AccessFlags::COLOR_ATTACHMENT_WRITE)
            .dependency_flags(DependencyFlags::empty())
    );

    rp.add_clear_value(vk::ClearValue {
        color: vk::ClearColorValue {
            float32: [0.0, 0.0, 0.0, 1.0]
        },
    });

    rp.add_clear_value(vk::ClearValue {
        color: vk::ClearColorValue {
            float32: [0.0, 0.0, 0.0, 1.0]
        },
    });

    rp.add_input_attachment_ref(SubpassInput {
        subpass_index: 1,
        refs: vec![
            vk::AttachmentReference {
                attachment: 0,
                layout: ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            }
        ],
    });
    rp.compile(vk);

    rp
}