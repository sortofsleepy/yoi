use std::f32::consts::PI;
use std::mem::size_of;
use ash::vk::{BufferUsageFlags, MemoryAllocateFlags};
use glam::{Mat4, Quat, vec3, Vec3, vec4, Vec4};
use vsketch::DisplayObject;
use vsketch::shaders::common::build_shader_source_with_uniforms;
use vsketch::shaders::loader::load_shader;
use yoi_geo::cube::CubeGeo;
use yoi_math::{Perlin, NoiseFn, rand_float};
use yoi_vk::{BufferFormat, DataBuffer, Descriptor, Mesh, RenderPipeline, RenderShader, UniformDescriptor, VkApp, VkDescriptorBuffer, VkObject, Vulkan};

#[derive(Copy, Clone)]
struct TransformsBuffers {
    global_transform: DataBuffer,
    transforms: DataBuffer,
}


#[derive(Clone)]
pub struct Mondrian {
    padding: f32,
    scale: f32,
    depth: f32,
    max_depth: f32,
    probability: f32,
    squares: Vec<Square>,
    leaf_squares: Vec<Square>,
    square: Option<Square>,
    perlin: Perlin,
    global_transform: Mat4,
    buffers: Option<TransformsBuffers>,
    buffer_descriptors: Vec<UniformDescriptor>,
}

impl Mondrian {
    pub fn create(ctx: &Vulkan) -> Self {
        let m = Mondrian {
            square: None,
            global_transform: Mat4::IDENTITY,
            perlin: Perlin::new(rand_float(0.0, 4.0) as u32),
            squares: vec![],
            leaf_squares: vec![],
            padding: 0.0,
            scale: rand_float(1.0, 3.0),
            depth: rand_float(1.0, 6.0),
            max_depth: rand_float(10.0, 15.0).round(),
            probability: rand_float(0.5, 0.9),
            buffers: None,
            buffer_descriptors: vec![],
        };

        let mut md = m.clone();

        md.init(ctx);

        md
    }

    pub fn update_system(&mut self, ctx: &Vulkan, time: f32) {
        let mut transform_tmp = Mat4::IDENTITY;
        let mut final_transform = Mat4::IDENTITY;
        let mut transforms = &mut self.buffers.unwrap().transforms;


        let mut sq = self.square.as_mut().unwrap();
        for mut s in self.squares.iter_mut() {
            s.split_pct = 0.5 + 0.4 * (self.perlin.get([2.0, 2.0]) as f32);
        }

        let size = 2f32;

        sq.update(0.0, 0.0, size, size, 0.0);

        let mut i = 0;
        for s in self.leaf_squares.iter_mut() {
            let offset = s.z;
            let transform = s.transform;

            let scale = vec3(
                s.w - 0.2 * self.padding,
                s.h - 0.2 * self.padding,
                self.depth * s.depth * offset,
            );

            let position = vec3(
                s.x - 0.5 * size,
                s.y - 0.5 * size,
                0.0,
            );


            Mondrian::build_transform(position, scale, &mut transform_tmp);
            final_transform = transform_tmp * transform;

            let offset = size_of::<Mat4>() * i;
            transforms.set_object_data_at_offset(ctx, final_transform, (offset) as u64);
            i += 1;
        }
    }

    pub fn set_position_and_rotation(&mut self, pos: Vec3, rot_axis: Vec3, rot_deg: f32) {
        let rot = Quat::from_axis_angle(rot_axis, rot_deg);
        self.global_transform = Mat4::from_rotation_translation(rot, pos)
        // TODO translate back
    }

    fn build_transform(position: Vec3, scale: Vec3, te: &mut Mat4) {
        let quat = vec4(0.0, 0.0, 0.0, 1.0);
        let x = quat.x;
        let y = quat.y;
        let z = quat.z;
        let w = quat.w;

        let x2 = x + x;
        let y2 = y + y;
        let z2 = z + z;

        let xx = x * x2;
        let xy = x * y2;
        let xz = x * z2;

        let yy = y * y2;
        let yz = y * z2;
        let zz = z * z2;

        let wx = w * x2;
        let wy = w * y2;
        let wz = w * z2;

        let sx = scale.x;
        let sy = scale.y;
        let sz = scale.z;

        te.x_axis.x = (1.0 - (yy + zz)) * sx;
        te.x_axis.y = (xy + wz) * sx;
        te.x_axis.z = (xz - wy) * sx;
        te.x_axis.w = 0.0;

        te.y_axis.x = (xy - wz) * sy;
        te.y_axis.y = (1.0 - (xx + zz)) * sy;
        te.y_axis.z = (yz + wx) * sy;
        te.y_axis.w = 0.0;

        te.z_axis.x = (xz + wy) * sz;
        te.z_axis.y = (yz - wx) * sz;
        te.z_axis.z = (1.0 - (xx + yy)) * sz;
        te.z_axis.w = 0.0;

        te.w_axis.x = position.x;
        te.w_axis.y = position.y;
        te.w_axis.z = position.z;
        te.w_axis.w = 1.0;
    }

    /// init function
    fn init(&mut self, ctx: &Vulkan) {
        let mut sq = Square::new(&mut self.squares);
        sq.split_pct = 0.5;
        sq.set_level(0.0, &mut self.squares, self.probability, self.max_depth);
        sq.update(0.0, 0.0, 1.0, 1.0, 1.0);

        self.leaf_squares = self.squares
            .iter()
            .filter(|&sq| sq.children.len() == 0)
            .cloned()
            .collect();

        self.square = Some(sq);

        self.build_transform_data(ctx);
        self.buffer_descriptors = self.build_descriptors(ctx, 2, 3);
    }

    /// Builds descriptors for the buffers
    fn build_descriptors(&self, ctx: &Vulkan, global_loc: u32, transforms_loc: u32) -> Vec<UniformDescriptor> {
        // build global transform descriptor
        let gbuffer = self.buffers.as_ref().unwrap().global_transform.raw();
        let mut global_transform_desc = UniformDescriptor::new();
        let g_buffer_address = ctx.get_buffer_device_address(gbuffer);
        global_transform_desc.set_buffer_data_with_address(gbuffer, 0, size_of::<Mat4>() as u64, g_buffer_address);
        global_transform_desc.set_shader_binding(global_loc);


        // build transforms descriptor
        let tbuffer = self.buffers.as_ref().unwrap().global_transform.raw();
        let mut transforms_desc = UniformDescriptor::new();
        let t_buffer_address = ctx.get_buffer_device_address(tbuffer);
        transforms_desc.set_buffer_data_with_address(tbuffer, 0, (size_of::<Mat4>() * self.leaf_squares.len()) as u64, t_buffer_address);
        transforms_desc.set_shader_binding(transforms_loc);

        vec![global_transform_desc, transforms_desc]
    }

    /// Builds transform buffers
    fn build_transform_data(&mut self, ctx: &Vulkan) {

        // build transform data
        let mut transform_fmt = BufferFormat::default();
        transform_fmt.usage_flags = transform_fmt.usage_flags | BufferUsageFlags::SHADER_DEVICE_ADDRESS;
        transform_fmt.mem_alloc_flags = MemoryAllocateFlags::DEVICE_ADDRESS;
        transform_fmt.size = (size_of::<Mat4>() * self.leaf_squares.len()) as u64;
        let mut transforms = DataBuffer::create(ctx, transform_fmt);

        let mut global_fmt = BufferFormat::default();
        global_fmt.usage_flags = transform_fmt.usage_flags | BufferUsageFlags::SHADER_DEVICE_ADDRESS;
        global_fmt.mem_alloc_flags = MemoryAllocateFlags::DEVICE_ADDRESS;
        global_fmt.size = (size_of::<Mat4>()) as u64;

        let mut global_transform = DataBuffer::create(ctx, global_fmt);

        //// Build individual transforms ////
        let num_instances = self.leaf_squares.len();
        let mut tmp = vec![];
        for i in 0..num_instances {
            let m = Mat4::from_translation(vec3(0.5, 0.5, 0.05));
            tmp.push(m);
            self.leaf_squares[i].transform = m;
        }


        //// Set the data ////
        transforms.set_data(ctx, &tmp);
        global_transform.set_object_data(ctx, self.global_transform);

        self.buffers = Some(TransformsBuffers {
            transforms,
            global_transform,
        })
    }

    pub fn build_geometry(&self, ctx: &Vulkan, app: &VkApp, camera_descriptor: &UniformDescriptor, colors: Vec<Vec4>) -> DisplayObject {
        let geo = CubeGeo::new(1.0, 1.0, 0.1, 1.0, 1.0, 1.0);

        let mondrian_vertex = build_shader_source_with_uniforms(
            vec![
                "shaders/sketch/mondrian.vert"
            ],
            vec![
                format!("layout(binding = 3) uniform SqTransforms{{ mat4 transforms[{}];}} squareTransforms;", self.leaf_squares.len())
            ],
        );

        let shader = RenderShader::new(
            mondrian_vertex.as_str(),
            load_shader("shaders/sketch/mondrian.frag").as_str(),
            ctx,
        );

        /////////////////////////
        // build global transform descriptor
        let gbuffer = self.buffers.as_ref().unwrap().global_transform.raw();
        let mut global_transform_desc = UniformDescriptor::new();

        let g_buffer_address = ctx.get_buffer_device_address(gbuffer);
        global_transform_desc.set_buffer_data_with_address(gbuffer, 0, size_of::<Mat4>() as u64, g_buffer_address);
        global_transform_desc.set_shader_binding(2);


        // build transforms descriptor
        let tbuffer = self.buffers.as_ref().unwrap().transforms.raw();
        let mut transforms_desc = UniformDescriptor::new();

        let t_buffer_address = ctx.get_buffer_device_address(tbuffer);
        transforms_desc.set_buffer_data_with_address(tbuffer, 0, (size_of::<Mat4>() * self.leaf_squares.len()) as u64, t_buffer_address);
        transforms_desc.set_shader_binding(3);

        let mut descriptor_buffer = VkDescriptorBuffer::new();
        descriptor_buffer.add_uniform_buffer_descriptor(*camera_descriptor);
        descriptor_buffer.add_uniform_buffer_descriptor(transforms_desc);
        descriptor_buffer.add_uniform_buffer_descriptor(global_transform_desc);
        descriptor_buffer.build(ctx);

        /////////////////////
        let mut mesh = Mesh::new();
        mesh = mesh.add_attribute(ctx, 0, geo.positions)
            .set_num_instances(self.leaf_squares.len() as u32)
            .add_attribute(ctx, 1, colors)
            .add_index_data(ctx, geo.indices);

        //////// BUILD TRANSFORMS //////////


        let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
        pipeline.add_mesh(&mesh);
        pipeline.add_descriptor_set_layout(descriptor_buffer.get_descriptor_layout());
        pipeline.compile(ctx, app, &shader);


        DisplayObject::new(vec![mesh], pipeline)
    }
}

//////////// SQUARE ////////////////////

#[derive(Clone)]
pub struct Square {
    children: Vec<Square>,
    nest_level: f32,
    split_dir: f32,
    split_pct: f32,
    width: f32,
    height: f32,
    depth: f32,
    z_offset: f32,
    speed: f32,
    phase: f32,
    transform: Mat4,
    w: f32,
    h: f32,
    x: f32,
    y: f32,
    z: f32,
    d: f32,
}

impl Square {
    pub fn new(squares: &mut Vec<Square>) -> Self {
        let sq = Square {
            speed: rand_float(0.1, 0.9),
            children: vec![],
            nest_level: 0.0,
            split_dir: 0.0,
            split_pct: 0.5,
            width: 1.0,
            height: 1.0,
            depth: rand_float(0.1, 0.9),
            z_offset: 0.0,
            phase: rand_float(0.0, PI * 2.0),
            transform: Mat4::IDENTITY,
            w: 0.0,
            h: 0.0,
            x: 0.0,
            y: 0.0,
            z: 0.0,
            d: 0.0,
        };

        squares.push(sq.clone());

        sq
    }

    pub fn set_level(&mut self, level: f32, squares: &mut Vec<Square>, probability: f32, max_depth: f32) {
        self.nest_level = level;

        let p = (rand_float(0.0, 1.0)).max(1.0 - self.nest_level / max_depth);

        if self.nest_level < max_depth &&
            p >= probability &&
            squares.len() < 2000 {
            self.subdivide(squares, probability, max_depth);
        }
    }

    pub fn subdivide(&mut self, squares: &mut Vec<Square>, probability: f32, max_depth: f32) {
        self.split_dir = if rand_float(0.0, 1.0) > 0.5 {
            1.0
        } else {
            0.0
        };

        self.split_pct = 0.5;

        let mut a = Square::new(squares);
        let mut b = Square::new(squares);

        a.set_level(self.nest_level + 1.0, squares, probability, max_depth);
        b.set_level(self.nest_level + 1.0, squares, probability, max_depth);

        self.children.push(a);
        self.children.push(b);

        self.resize();
    }

    pub fn resize(&mut self) {
        if self.children.is_empty() {
            return;
        }
        let mut kids = self.children.clone();

        if self.split_dir == 0.0 {
            kids[0].width = self.split_pct;
            kids[0].height = 1.0;
            kids[1].width = (1.0 - self.split_pct);
            kids[1].height = 1.0;
        } else {
            kids[0].width = 1.0;
            kids[0].height = self.split_pct;
            kids[1].width = 1.0;
            kids[1].height = (1.0 - self.split_pct);
        }
        self.children = kids;
    }

    pub fn update(&mut self, x: f32, y: f32, w: f32, h: f32, d: f32) {
        self.w = w * self.width;
        self.h = h * self.height;
        self.x = x;
        self.y = y;
        self.d = d + self.depth;

        self.z_offset += 0.1 * d * 0.2;
        self.z = self.z_offset.sin() * 90.0;

        self.z *= 0.05;

        if !self.children.is_empty() {
            let mut kids = self.children.clone();

            let dw = self.w * self.split_pct;
            let dh = self.h * self.split_pct;

            if self.split_dir == 0.0 {
                kids[0].update(self.x, self.y, self.w, self.h, self.d);
                kids[1].update(self.x + dw, self.y, self.w, self.h, self.d);
            } else {
                kids[0].update(self.x, self.y, self.w, self.h, self.d);
                kids[1].update(self.x, self.y + dh, self.w, self.h, self.d);
            }
        }
    }
}