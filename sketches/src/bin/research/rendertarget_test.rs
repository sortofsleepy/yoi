use ash::vk;
use ash::vk::{Format, Handle, ShaderStageFlags};
use vsketch::*;
use yoi_geo::cube::CubeGeo;
use yoi_geo::simpleshapes::SimpleShapes;
use yoi_vk::*;

mod objects;

const WIDTH: u32 = 1280;
const HEIGHT: u32 = 920;

struct State {
    scene_target: RenderTarget2,
    output: SceneState,
    floor: SceneState,
}

struct SceneState {
    descriptor: VkDescriptorBuffer,
    scene: DisplayObject,
}

/// Render Target testing
fn main() {
    VSketch::new()
        .set_size(WIDTH, HEIGHT)
        .add_setup(setup)
        .add_draw(draw)
        .run();
}

fn setup(vk: &mut Vulkan, comp: &mut VkRenderCore) -> State {
    let mut sampler = VkSampler::new();
    sampler.build(vk);

    let camera = build_camera(
        vk,
        1,
        -40.0,
        60.0,
        (WIDTH as f32) / (HEIGHT as f32),
        0.1,
        10000.0,
    );

    let mut scene_target = RenderTarget2::new(WIDTH, HEIGHT);
    scene_target.add_default_attachment(vk, Some(Format::B8G8R8A8_UNORM));
    scene_target.compile(vk);

    // build output geometry
    let output = build_output(vk, comp, &scene_target, &sampler.raw());

    //////////////////////////////
    let floor = build_debug(
        vk,
        &comp.meta,
        &scene_target,
        &camera.descriptor,
    );

    State {
        floor,
        scene_target,
        output,
    }
}

fn draw(vk: &mut Vulkan, comp: &mut VkRenderCore, model: &mut State) {
    let renderer = &mut comp.render;
    let meta = &mut comp.meta;

    let floor = &mut model.floor.scene;
    let db = &mut model.floor.descriptor;

    let scene_rt = &mut model.scene_target;

    let output = &mut model.output.scene;
    let odesc = &mut model.output.descriptor;

    renderer.record_commands_with_renderpass(vk, meta, |cb| {
        scene_rt.begin(vk, cb);
        floor.render(vk, cb, Some(db));
        scene_rt.end(vk, cb);
    });

    renderer.record_commands(vk, meta, |cb| {
        output.render(vk,cb,Some(&odesc));
    });

    /////////////////////////////////////////////////////////////////////////////////////////

    renderer.present(vk, meta.get_swapchain());
    renderer.finish_present();
}

fn build_debug(
    vk: &Vulkan,
    app: &VkApp,
    rt: &RenderTarget2,
    camera_descriptor: &UniformDescriptor,
) -> SceneState {
    let size = 25.0;
    let geo = CubeGeo::new(size, size, size, 2.0, 2.0, 2.0);

    //let geo = PlaneGeometry::create(size, size, 2.0, 2.0);
    let shader = RenderShader::new(
        "shaders/rt_debug/render.vert",

        //PASSTHRU_VERTEX,
        "shaders/rt_debug/render.frag",
        vk,
    );

    let mesh = Mesh::new().add_attribute(vk, 0, geo.positions)
        .add_index_data(vk, geo.indices);

    let mut descriptor_buffer = VkDescriptorBuffer::new();
    descriptor_buffer.add_uniform_buffer_descriptor(*camera_descriptor);
    descriptor_buffer.build(vk);

    let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
    pipeline.add_mesh(&mesh);
    pipeline.add_descriptor_set_layout(descriptor_buffer.get_descriptor_layout());

    pipeline.compile_with_renderpass(vk, app, &shader, rt.get_renderpass().raw());

    let mut ds = DisplayObject::new(vec![mesh], pipeline);

    SceneState {
        descriptor: descriptor_buffer,
        scene: ds,
    }
}

/// Build necessary components to render output from render targets.
fn build_output(ctx: &Vulkan, comp: &VkRenderCore, output_rt: &RenderTarget2, sampler: &vk::Sampler) -> SceneState {
    let mut tex = TextureSamplerDescriptor::new();
    tex.set_image(*output_rt.get_color_view(), *sampler);
    tex.set_shader_binding(0);
    tex.set_shader_stage(ShaderStageFlags::FRAGMENT);

    let mut output_descriptors = VkDescriptorBuffer::new();
    output_descriptors.add_texture_descriptor(tex);
    output_descriptors.build(ctx);

    // build a fullscreen triangle for the background
    let geo = SimpleShapes::generate_fullscreen_triangle();

    let mut mesh = Mesh::new();
    mesh = mesh.add_attribute(ctx, 0, geo.positions)
        .set_num_vertices(3);

    let shader = RenderShader::new(
        PASSTHRU_VERTEX,
        //load_shader2("shaders/rt_debug/bg.frag").as_str(),
        PASSTHRU_FRAGMENT,
        ctx,
    );

    // build a pipeline to render it
    let mut pipeline = RenderPipeline::new();
    pipeline.add_descriptor_set_layout(output_descriptors.get_descriptor_layout());
    pipeline.compile_dynamic(ctx, &comp.meta, &shader, Format::B8G8R8A8_UNORM, Format::D32_SFLOAT);

    SceneState {
        descriptor: output_descriptors,
        scene: DisplayObject::new(vec![mesh], pipeline),
    }
}


