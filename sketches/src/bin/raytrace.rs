use ash::extensions::khr::AccelerationStructure;
use ash::vk;
use ash::vk::{
    AccelerationStructureBuildRangeInfoKHR, AccelerationStructureGeometryKHR,
    AccelerationStructureGeometryTrianglesDataKHR, AccelerationStructureInstanceKHR,
    AccelerationStructureReferenceKHR, BuildAccelerationStructureFlagsKHR, DescriptorSetLayout,
    Format, GeometryFlagsKHR, GeometryTypeKHR, IndexType, MemoryAllocateFlags, MemoryPropertyFlags,
    Packed24_8, TransformMatrixKHR,
};
use glam::Vec4;
use vsketch::core::simplesettings::SimpleSettings;
use vsketch::utils::present_to_screen;
use vsketch::*;
use winit::event::Ime::Disabled;
use yoi_geo::csphere::Sphere;
use yoi_geo::simpleshapes::SimpleShapes;
use yoi_math::create_vec4;
use yoi_vk::raytracing::rt::RayTracing;
use yoi_vk::raytracing::types::BlasInput;
use yoi_vk::*;

struct State {
    db: VkDescriptorBuffer,
    settings: SimpleSettings,
}

const WIDTH: u32 = 1280;

const HEIGHT: u32 = 920;

struct Geo {
    vertex_buffer: DataBuffer,
    index_buffer: DataBuffer,
}

/// Based on the mini sample from NVidia
/// https://nvpro-samples.github.io/vk_mini_path_tracer/index.html
/// https://github.com/nvpro-samples/nvpro_core/tree/master/nvvk
/// https://github.com/nvpro-samples/vk_mini_path_tracer/tree/main/vk_mini_path_tracer
fn main() {
    VSketch::new()
        .enable_ray_tracing()
        .set_size(WIDTH, HEIGHT)
        .add_setup(setup)
        .add_draw(draw)
        .run();
}

fn setup(vk: &mut Vulkan, comp: &mut VkRenderCore) -> State {
    let camera = build_camera(
        vk,
        0,
        -40.0,
        60.0,
        (WIDTH as f32) / (HEIGHT as f32),
        0.1,
        1000.0,
    );

    let settings = SimpleSettings::new(vk, 1);

    let device_size = WIDTH * HEIGHT * 3 * (size_of::<f32>() as u32);

    // build command buffer for general purpose tasks we need to do
    let cb = generate_command_buffer(vk);

    // init RayTracing helpers
    let mut rt = RayTracing::new(vk);

    //
    let buff = generate_storage_buffer(vk, device_size.clone() as usize);
    fill_buffer(vk, device_size as usize, &cb, &buff);

    let mut db = VkDescriptorBuffer::new();
    db.add_uniform_buffer_descriptor(camera.descriptor);
    db.add_uniform_buffer_descriptor(*settings.get_descriptor());
    db.build(vk);

    let geo = build_geo(vk);
    build_blas(vk, &geo, &mut rt);

    State { db, settings }
}

fn draw(vk: &mut Vulkan, comp: &mut VkRenderCore, model: &mut State) {
    let renderer = &mut comp.render;
    let meta = &mut comp.meta;
    let settings = &mut model.settings;
    let env = &mut comp.app_env;

    /*
     env.update();
    settings.update_time(vk, env.get_delta_time());

    let db = &mut model.db;

    renderer.record_commands(vk, meta, |cb| {});

    /////////////////////////////////////////////////////////////////////////////////////////

    present_to_screen(vk, comp);
     */
}

fn load_compute(vk: &mut Vulkan, ds: &VkDescriptorSet) -> ComputePipeline {
    let compute_shader = ComputeShader::new("shaders/path_mini/raytrace.glsl", vk);
    let mut pipeline = ComputePipeline::new(vk);
    //pipeline.compile(vk, compute_shader.generate_pipeline_info());
    pipeline.compile_with_descriptors(
        vk,
        compute_shader.generate_pipeline_info(),
        *ds.get_descriptor_layout(),
    );

    pipeline
}

/// Builds buffer for storage.
/// https://github.com/nvpro-samples/vk_mini_path_tracer/blob/main/checkpoints/3_memory/main.cpp
fn build_buffer(ctx: &Vulkan, cb: &CommandBuffer) -> DataBuffer {
    // build buffer
    let device_size = WIDTH * HEIGHT * 3 * (size_of::<f32>() as u32);

    let mut fmt = BufferFormat::default();
    fmt.size = device_size as u64;
    fmt.usage_flags = BufferUsageFlags::STORAGE_BUFFER | BufferUsageFlags::TRANSFER_DST;
    fmt.mem_prop_flags = MemoryPropertyFlags::HOST_VISIBLE
        | MemoryPropertyFlags::HOST_CACHED
        | MemoryPropertyFlags::HOST_COHERENT;

    let buff = DataBuffer::create(ctx, fmt);

    buff
}

fn build_geo(vk: &Vulkan) -> Geo {
    let cube = SimpleShapes::generate_cube();

    let flags = BufferUsageFlags::SHADER_DEVICE_ADDRESS
        | BufferUsageFlags::STORAGE_BUFFER
        | BufferUsageFlags::ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_KHR;

    // build vertex buffer
    let mut vfmt = BufferFormat::default();
    vfmt.size = (size_of::<Vec4>() * cube.positions.len()) as u64;
    vfmt.usage_flags = flags;
    vfmt.mem_alloc_flags = MemoryAllocateFlags::DEVICE_ADDRESS;

    let mut vertex_buffer = DataBuffer::create(vk, vfmt);
    vertex_buffer.set_data(vk, &cube.positions);

    // Build index buffer
    let mut ifmt = BufferFormat::default();
    ifmt.size = (size_of::<u32>() * cube.indices.len()) as u64;
    ifmt.usage_flags = flags;
    ifmt.mem_alloc_flags = MemoryAllocateFlags::DEVICE_ADDRESS;

    let mut index_buffer = DataBuffer::create(vk, ifmt);
    index_buffer.set_data(vk, &cube.indices);

    Geo {
        vertex_buffer,
        index_buffer,
    }
}

fn build_blas(vk: &Vulkan, geo: &Geo, rt: &mut RayTracing) {
    let mut blas = BlasInput::new();
    let mut blasses = vec![];

    let cube = SimpleShapes::generate_cube();

    let vertex_buffer_address = rt.get_buffer_address(geo.vertex_buffer.raw());
    let index_buffer_address = rt.get_buffer_address(geo.index_buffer.raw());

    // Specify where the builder can find the vertices and indices for triangles, and their formats:
    let mut triangles = vk::AccelerationStructureGeometryTrianglesDataKHR::default();
    triangles.vertex_format = vk::Format::R32G32B32A32_SFLOAT;
    triangles.vertex_data.device_address = vertex_buffer_address;
    triangles.vertex_stride = (size_of::<Vec4>()) as u64;
    triangles.max_vertex = ((cube.positions.len() * 4) - 1) as u32;
    triangles.index_type = vk::IndexType::UINT32;
    triangles.index_data.device_address = index_buffer_address;
    triangles.transform_data.device_address = 0;

    // Create a VkAccelerationStructureGeometryKHR object that says it handles opaque triangles and points to the above:
    let mut geometry = vk::AccelerationStructureGeometryKHR::default();
    geometry.geometry.triangles = triangles;
    geometry.geometry_type = vk::GeometryTypeKHR::TRIANGLES;
    geometry.flags = vk::GeometryFlagsKHR::OPAQUE;

    blas.as_geometry.push(geometry);

    // Create offset info that allows us to say how many triangles and vertices to read
    let mut offsetInfo = vk::AccelerationStructureBuildRangeInfoKHR::default();
    offsetInfo.first_vertex = 0;
    offsetInfo.primitive_count = (cube.indices.len()) as u32;
    offsetInfo.primitive_offset = 0;
    offsetInfo.transform_offset = 0;

    blas.as_build_offset_info.push(offsetInfo);

    blasses.push(blas);

    let blas = rt.build_BLAS(
        vk,
        blasses,
        vk::BuildAccelerationStructureFlagsKHR::PREFER_FAST_TRACE,
    );

    /*
       let instances: Vec<AccelerationStructureInstanceKHR> = vec![];
    {
        let mut matrix: [[f32; 4]; 3] = [[0.0; 4]; 3];


        matrix[0][0] = 1.0;

        let mvec = matrix.concat();

        let mut fmatrix: [f32; 12] = [0.0; 12];

        for i in 0..mvec.len() {
            fmatrix[i] = mvec[i]
        }

        let mut instances = vec![];
        let mut instance = AccelerationStructureInstanceKHR {
            transform: TransformMatrixKHR {
                matrix: fmatrix
            },
            instance_custom_index_and_mask: Packed24_8::new(
                0,
                0xFF,
            ),
            instance_shader_binding_table_record_offset_and_flags: Packed24_8::new(
                0,
                0b1, // not sure how to convert flag to the following
                //vk::GeometryInstanceFlagsKHR::TRIANGLE_FACING_CULL_DISABLE,
            ),
            acceleration_structure_reference: AccelerationStructureReferenceKHR {
                device_handle: rt.get_blas_device_address(&blas, 0)
            },
        };

        instances.push(instance)
    }
     */
}

pub fn build_TLAS(vk: &Vulkan) {}

/*

fn debug_build_geo(vk: &Vulkan, comp: &VkApp, ds_layout: &DescriptorSetLayout) -> DisplayObject {
    let geo = Sphere::create_with_size(10.0, 100);

    let mesh = Mesh::new()
        .add_attribute(vk, 0, geo.positions)
        .add_index_data(vk, geo.indices);

    let shader = RenderShader::new(
        "shaders/sketch/debug.vert",
        "shaders/sketch/debug.frag",
        vk,
    );


    let mut pipeline = RenderPipeline::new_with_descriptor_buffer();
    pipeline.add_mesh(&mesh);
    pipeline.add_descriptor_set_layout(*ds_layout);
    pipeline.compile(vk, comp, &shader);

    DisplayObject::new(vec![mesh], pipeline)
}
 */
