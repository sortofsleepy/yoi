use ash::vk::*;
use glam::Vec4;
use image::imageops::FilterType;
use image::io::Reader as ImageReader;
use image::{imageops, EncodableLayout};
use vsketch::utils::generate_push_constant_range;
use vsketch::*;
use yoi_geo::cube::CubeGeo;
use yoi_geo::simpleshapes::SimpleShapes;
use yoi_math::create_vec4;
use yoi_vk::*;

struct State {
    output: SceneState,
    targets: Vec<RenderTarget2>,
    stages: Vec<SceneState>,
    jump: JumpSteps,
    time: f32,
}

const WIDTH: u32 = 1024;
const HEIGHT: u32 = 1024;

const WIN_WIDTH: u32 = 1280;
const WIN_HEIGHT: u32 = 1280;

//const TEXTURE_FMT: Format = Format::R16G16B16A16_SFLOAT;
const TEXTURE_FMT: Format = Format::B8G8R8A8_UNORM;

const SCENE: usize = 0;
const ENCODE: usize = 1;
const DISTANCE: usize = 2;
const GI: usize = 3;

struct JumpSteps {
    targets: Vec<RenderTarget2>,
    steps: Vec<SceneState>,
}

struct NoiseState {
    rt: RenderTarget2,
    obj: DisplayObject,
}
/// A small port of Sam Bigos's Godot implementation of 2D Global illumination...
/// https://samuelbigos.github.io/posts/2dgi1-2d-global-illumination-in-godot.html
/// which in part is also based on this shadertoy by @toocanzs
/// https://www.shadertoy.com/view/lltcRN

fn main() {
    VSketch::new()
        .set_size(WIN_WIDTH, WIN_HEIGHT)
        .add_setup(setup)
        .add_draw(draw)
        .run();
}

fn setup(vk: &mut Vulkan, comp: &mut VkRenderCore) -> State {
    let targets = build_targets(vk, WIDTH, HEIGHT, TEXTURE_FMT);

    // build the scene.
    let scene = build_stage(
        vk,
        &comp.meta,
        &targets[SCENE],
        "shaders/flood/scene.frag",
        vec![],
    );

    // build the encode stage
    let encode = build_stage(
        vk,
        &comp.meta,
        &targets[ENCODE],
        "shaders/flood/encode.frag",
        vec![&targets[SCENE].get_color_view()],
    );

    let jump = build_jump(
        vk,
        &comp.meta,
        WIDTH,
        HEIGHT,
        TEXTURE_FMT,
        "shaders/flood/jump.frag",
        comp.render.get_command_buffer(0),
        &targets[ENCODE].get_attachment(),
    );

    let last = jump.targets.last().unwrap();

    let distance = build_stage(
        vk,
        &comp.meta,
        &targets[DISTANCE],
        "shaders/flood/distance.frag",
        vec![last.get_color_view()],
    );

    let noise = build_noise_buffer(vk, &comp.meta);

    // generate noise
    build_noise(vk, &noise, comp);

    let gi = build_stage(
        vk,
        &comp.meta,
        &targets[GI],
        "shaders/flood/gi.frag",
        vec![
            &targets[DISTANCE].get_color_view(),
            &targets[SCENE].get_color_view(),
            &noise.rt.get_color_view_at(0),
        ],
    );

    let output = build_output(vk, comp, &targets[GI].get_color_view());

    State {
        stages: vec![scene, encode, distance, gi],
        jump,
        output,
        targets,
        time: 0.0,
    }
}

fn draw(vk: &mut Vulkan, comp: &mut VkRenderCore, model: &mut State) {
    let renderer = &mut comp.render;
    let meta = &mut comp.meta;

    let targets = &model.targets;

    let scene = &model.stages[SCENE];
    let encode = &model.stages[ENCODE];
    let jump = &model.jump;
    let distance = &model.stages[DISTANCE];
    let gi = &model.stages[GI];

    let output = &model.output;

    renderer.record_commands_with_renderpass(vk, meta, |cb| {
        // render the scene
        {
            targets[SCENE].begin(vk, cb);
            scene.render(vk, cb);
            targets[SCENE].end(vk, cb);
        }

        // encode the positions
        {
            targets[ENCODE].begin(vk, cb);
            encode.render(vk, cb);
            targets[ENCODE].end(vk, cb);
        }

        {
            for i in 0..jump.targets.len() {
                jump.targets[i].begin(vk, cb);
                jump.steps[i].render(vk, cb);
                jump.targets[i].end(vk, cb);
            }
        }
        // encode the positions
        {
            targets[DISTANCE].begin(vk, cb);
            distance.render(vk, cb);
            targets[DISTANCE].end(vk, cb);
        }

        // encode the positions
        {
            targets[GI].begin(vk, cb);
            gi.render(vk, cb);
            targets[GI].end(vk, cb);
        }
    });

    renderer.record_commands(vk, meta, |cb| {
        output.render(vk, cb);
    });

    renderer.present(vk, meta.get_swapchain());
    renderer.finish_present();

    model.time += 0.1;
}

/// builds a static noise texture.
fn build_noise(vk: &Vulkan, noise: &NoiseState, core: &mut VkRenderCore) {
    // get command buffer to use for building the noise texture
    let cb = core.render.bind_command_buffer(0, vk);
    noise.rt.begin(vk, cb);
    noise.obj.render(vk, cb, None);
    noise.rt.end(vk, cb);
    core.render.unbind_command_buffer(vk, cb);

    vk.submit_command_buffer(cb);
}

/// Build necessary components to render output from render targets.
fn build_output(ctx: &Vulkan, comp: &VkRenderCore, output_rt: &ImageView) -> SceneState {
    let mut sampler = VkSampler::new();
    sampler.build(ctx);

    let mut tex = TextureSamplerDescriptor::new();
    tex.set_image(*output_rt, *sampler.raw());
    tex.set_shader_binding(0);
    tex.set_shader_stage(ShaderStageFlags::FRAGMENT);

    let mut output_descriptors = VkDescriptorBuffer::new();
    output_descriptors.add_texture_descriptor(tex);
    output_descriptors.build(ctx);

    // build a fullscreen triangle for the background
    let geo = SimpleShapes::generate_fullscreen_triangle();

    let mut mesh = Mesh::new();
    mesh = mesh
        .add_attribute(ctx, 0, geo.positions)
        .set_num_vertices(3);

    let shader = RenderShader::new(PASSTHRU_VERTEX, "shaders/flood/output.frag", ctx);

    // build a pipeline to render it
    let mut pipeline = RenderPipeline::new();
    pipeline.add_descriptor_set_layout(output_descriptors.get_descriptor_layout());
    pipeline.compile_dynamic(
        ctx,
        &comp.meta,
        &shader,
        Format::B8G8R8A8_UNORM,
        Format::D32_SFLOAT,
    );

    SceneState {
        descriptor: Some(output_descriptors),
        scene: DisplayObject::new(vec![mesh], pipeline),
    }
}

/// Builds the general targets we'll need. Note that these will clear themselves on each frame.
fn build_targets(
    vk: &Vulkan,
    width: u32,
    height: u32,
    texture_format: Format,
) -> Vec<RenderTarget2> {
    let mut rts = vec![];

    for i in 0..4 {
        let mut rt = RenderTarget2::new(width, height);
        rt.add_default_attachment(vk, Some(texture_format));
        rt.compile(vk);
        rts.push(rt);
    }

    rts
}

/// Builds the scene.
fn build_stage(
    vk: &Vulkan,
    app: &VkApp,
    rt: &RenderTarget2,
    shader: &str,
    inputs: Vec<&ImageView>,
) -> SceneState {
    let shader = RenderShader::new(PASSTHRU_VERTEX, shader, vk);

    let mesh = Mesh::new().set_num_vertices(3);
    let mut pipe = RenderPipeline::new();
    let mut ds = VkDescriptorBuffer::new();
    let mut descriptor: Option<VkDescriptorBuffer> = None;

    if !inputs.is_empty() {
        let mut sampler = VkSampler::new();
        sampler.build(vk);

        for i in 0..inputs.len() {
            let mut tex_desc = TextureSamplerDescriptor::new();
            tex_desc.set_image(*inputs[i], *sampler.raw());
            tex_desc.set_shader_binding(i as u32);

            ds.add_texture_descriptor(tex_desc);
        }

        ds.build(vk);
        pipe.add_descriptor_set_layout(ds.get_descriptor_layout());

        descriptor = Some(ds);
    }

    pipe.add_mesh(&mesh);
    pipe.compile2(vk, app, &shader, Some(rt.get_renderpass().raw()));

    SceneState {
        scene: DisplayObject::new(vec![mesh], pipe),
        descriptor,
    }
}

fn build_noise_buffer(vk: &Vulkan, app: &VkApp) -> NoiseState {
    let mut rt = RenderTarget2::new(WIDTH, HEIGHT);
    rt.add_default_attachment(vk, Some(TEXTURE_FMT));
    rt.compile(vk);

    let shader = RenderShader::new(PASSTHRU_VERTEX, "shaders/flood/noise.frag", vk);

    let mesh = Mesh::new().set_num_vertices(3);
    let mut pipeline = RenderPipeline::new();
    pipeline.add_mesh(&mesh);
    pipeline.compile2(vk, app, &shader, Some(rt.get_renderpass().raw()));

    NoiseState {
        rt,
        obj: DisplayObject::new(vec![mesh], pipeline),
    }
}

fn build_jump(
    vk: &Vulkan,
    app: &VkApp,
    width: u32,
    height: u32,
    format: Format,
    shader: &str,
    cb: &CommandBuffer,
    input: &VkTexture,
) -> JumpSteps {
    let mut sampler = VkSampler::new();
    sampler.build(vk);

    let largest = width.max(height);

    let passes = (f32::ceil(f32::log2(largest as f32)) as u32);

    // build targets
    let mut targets = vec![];
    for i in 0..passes {
        let mut rt = RenderTarget2::new(width, height);
        rt.add_default_attachment(vk, Some(TEXTURE_FMT));
        rt.compile(vk);
        targets.push(rt);
    }

    let shader = RenderShader::new(PASSTHRU_VERTEX, shader, vk);

    let mut steps = vec![];
    for i in 0..passes {
        let mut offset = (passes - i - 1);
        let foffset = 2_f32.powf(offset as f32);

        let mut settings = create_vec4();

        // x = offset
        // y + z = Screen size
        settings.x = foffset;
        settings.y = WIDTH as f32;
        settings.z = HEIGHT as f32;

        let mut settings_buffer =
            DataBuffer::create(vk, generate_descriptor_buffer_format::<Vec4>());
        settings_buffer.set_object_data(vk, settings);

        // setup core objects
        let mut ds = VkDescriptorBuffer::new();
        let mesh = Mesh::new().set_num_vertices(3);

        // build pipeline
        let mut pipe = RenderPipeline::new();
        pipe.add_mesh(&mesh);

        // build texture descriptor
        let mut tex = TextureSamplerDescriptor::new();

        // if i = 0, input is seed, otherwise, input is previous pass output
        if i == 0 {
            tex.set_image(*input.get_image_view(), *sampler.raw());
        } else {
            let idx = (i as usize) - 1usize;
            tex.set_image(*targets[idx].get_color_view(), *sampler.raw());
        }

        ds.add_texture_descriptor(tex);

        // build settings
        let mut settings_desc = UniformDescriptor::new();
        settings_desc.set_shader_binding(1);
        settings_desc.set_shader_stage(ShaderStageFlags::FRAGMENT);
        settings_desc.set_buffer_data_with_address(
            vk,
            settings_buffer.raw(),
            0,
            get_byte_size::<Vec4>(),
        );
        ds.add_uniform_buffer_descriptor(settings_desc);

        ds.build(vk);

        pipe.add_descriptor_set_layout(ds.get_descriptor_layout());
        pipe.compile2(
            vk,
            app,
            &shader,
            Some(&targets[(i as usize)].get_renderpass().raw()),
        );

        steps.push(SceneState {
            descriptor: Some(ds),
            scene: DisplayObject::new(vec![mesh], pipe),
        })
    }
    JumpSteps { targets, steps }
}
