use crate::lighting::deferred::DeferredRenderer;
use ash::vk::*;
use glam::{vec4, Mat4, Vec4};
use std::f32::consts::PI;
use vsketch::*;
use yoi_geo::cube::CubeGeo;
use yoi_math::rand_float;
use yoi_vk::*;

mod lighting;

struct DRScene {
    meshes: Mesh,
    ds: VkDescriptorBuffer,
}

struct State {
    scene: DRScene,
    output: SceneState,
    engine: DeferredRenderer,
    global_ds: VkDescriptorBuffer,
}

struct LightData {
    lights: Vec<Light>,
    desc: StorageDescriptor,
    buffer: DataBuffer,
}

const WIDTH: u32 = 1280;
const HEIGHT: u32 = 1280;

const NUM_ITEMS: usize = 10;
const NUM_LIGHTS: usize = 5;

fn main() {
    VSketch::new()
        .set_size(WIDTH, HEIGHT)
        .add_setup(setup)
        .add_draw(draw)
        .run();
}

fn setup(vk: &mut Vulkan, comp: &mut VkRenderCore) -> State {
    let mut engine = DeferredRenderer::create([WIDTH, HEIGHT], Format::B8G8R8A8_UNORM);
    engine.setup(vk);
    engine.add_default_color_blend_state();

    let camera = build_camera(
        vk,
        0,
        -60.0,
        60.0,
        (WIDTH / HEIGHT) as f32,
        0.1,
        10000.0,
    );

    let mut global_ds = VkDescriptorBuffer::new();
    global_ds.add_uniform_buffer_descriptor(camera.descriptor);
    global_ds.build(vk);

    let mut global_sampler = VkSampler::new();
    global_sampler.build(vk);

    let positions = build_geo_positions(vk);
    let scene = build_gbuffer_scene(vk, &comp.meta, &camera, &positions.buffer, &mut engine);

    // build composition components
    let compose = build_composition_scene(
        vk,
        &comp.meta,
        &global_sampler,
        &mut engine,
    );

    let output = build_output_panel(
        vk,
        &comp.meta,
        &engine.get_output(2),
        get_default_render_formats(),
    );

    State {
        global_ds,
        scene,
        output,
        engine,
    }
}

fn draw(vk: &mut Vulkan, comp: &mut VkRenderCore, model: &mut State) {
    let renderer = &mut comp.render;
    let meta = &mut comp.meta;

    let engine = &model.engine;
    let scene = &model.scene;
    let descriptor = &model.scene.ds;

    let output = &mut model.output;

    renderer.record_commands_with_renderpass(vk, meta, |cb| {
        engine.begin(vk, cb, Some(&descriptor));
        engine.bind_gbuffer(vk, cb);
        scene.meshes.render(vk, *cb);
        vk.next_subpass(cb, SubpassContents::INLINE);
        engine.end(vk, cb);
    });

    renderer.record_commands(vk, meta, |cb| {
        output.render(vk, cb);
    });

    let _ = renderer.present(vk, meta.get_swapchain());
    renderer.finish_present();
}

/// Build data used to update geometry
fn build_geo_positions(vk: &Vulkan) -> DataSet {
    let mut meta = vec![];

    for i in 0..NUM_ITEMS {
        let phi = (rand_float(0.0, 1.0) * 2.0) * PI;
        let theta = (rand_float(0.0, 1.0) * 2.0) * PI;

        let phi_speed = rand_float(-0.01, 0.01);
        let theta_speed = rand_float(-0.01, 0.01);

        meta.push(vec4(phi, theta, phi_speed, theta_speed));
    }

    let fmt = generate_descriptor_buffer_format_with_size::<Vec4>(NUM_ITEMS as usize);
    let mut mbuffer = DataBuffer::create(vk, fmt);
    mbuffer.set_data(vk, &meta);

    DataSet {
        buffer: mbuffer,
        data: meta,
    }
}

/// Builds the scene
fn build_gbuffer_scene(
    vk: &Vulkan,
    app: &VkApp,
    camera: &CameraData,
    positions: &DataBuffer,
    dr: &mut DeferredRenderer,
) -> DRScene {
    let gbuffer_shader = RenderShader::new(
        "shaders/deferred/gbuffer.vert",
        "shaders/deferred/gbuffer.frag",
        vk,
    );

    ////////////// BUILD MAIN SCENE OBJECTS ///////////////////
    let size = 10.0;
    let geo = CubeGeo::new(size, size, size, 2.0, 2.0, 2.0);

    let mesh = Mesh::new()
        .add_attribute(vk, 0, geo.positions)
        .add_attribute(vk, 1, geo.normals)
        .set_num_instances(NUM_ITEMS as u32)
        .add_index_data(vk, geo.indices);

    dr.get_geometry_pipeline().add_mesh(&mesh);

    // make dummy normal matrix using the current camera.
    let model_matrix = Mat4::IDENTITY; // dummy identity matrix.
    let view = camera.camera.get_view();

    let normal_matrix = calculate_normal_matrix(model_matrix, view);

    let fmt = generate_descriptor_buffer_format::<Mat4>();
    let mut model_buffer = DataBuffer::create(vk, fmt);
    model_buffer.set_object_data(vk, &normal_matrix);

    let mut desc = UniformDescriptor::new();
    desc.set_shader_binding(1);
    desc.set_buffer_data_with_address(vk, model_buffer.raw(), 0, get_byte_size::<Mat4>());

    ////////////// BUILD LIGHT SCENE OBJECTS ///////////////////

    /////// SETUP DESCRIPTORS AND COMPILE //////
    let mut ds = VkDescriptorBuffer::new();
    ds.add_uniform_buffer_descriptor(camera.descriptor);
    ds.add_uniform_buffer_descriptor(desc);

    ds.build(vk);

    dr.get_geometry_pipeline()
        .add_descriptor_set_layout(ds.get_descriptor_layout());

    dr.compile_gbuffer(vk, app, &gbuffer_shader);

    DRScene { meshes: mesh, ds }
}

/// Builds the necessary components for the lighting / composition pass
fn build_composition_scene(
    vk: &Vulkan,
    app: &VkApp,
    sampler: &VkSampler,
    dr: &mut DeferredRenderer,
) -> DRScene {
    let lbuffer_shader = RenderShader::new(
        PASSTHRU_VERTEX,
        "shaders/deferred/lbuffer.frag",
        vk,
    );

    let mut ds = VkDescriptorBuffer::new();

    let m = Mesh::new().set_num_vertices(3);
    let inputs = dr.get_attachments();

    for i in 0..inputs.len() {
        let mut desc = TextureSamplerDescriptor::new();
        desc.descriptor_type = DescriptorType::INPUT_ATTACHMENT;
        desc.set_shader_binding(i as u32);
        desc.set_image(*inputs[i], *sampler.raw());


        ds.add_texture_descriptor(desc);
    }

    ds.build(vk);


    let mut pipe = dr.get_composition_pipeline();
    pipe.add_descriptor_set_layout(ds.get_descriptor_layout());
    pipe.add_mesh(&m);

    dr.compile_comp_buffer(vk, app, &lbuffer_shader);

    DRScene { meshes: m, ds }
}

/// Build the mesh for the composition pass
fn build_composition_mesh(
    vk: &Vulkan,
    app: &VkApp,
    dr: &mut DeferredRenderer,
    sampler: &VkSampler,
    light_data: &StorageDescriptor,
) -> VkDescriptorBuffer {
    // build descriptors for the various attachments the composition / lighting stage needs
    let mut pos = TextureSamplerDescriptor::new();
    pos.descriptor_type = DescriptorType::INPUT_ATTACHMENT;

    let mesh = Mesh::new().set_num_vertices(3);

    let shader = RenderShader::new(PASSTHRU_VERTEX, "shaders/deferred/lbuffer.frag", vk);

    // build descriptors for input textures
    let mut ds = VkDescriptorBuffer::new();

    let mut sampler = VkSampler::new();
    sampler.build(vk);

    // add texture data to the descriptor
    let pos = dr.generate_position_descriptor(0, &sampler);
    let albedo = dr.generate_albedo_descriptor(1, &sampler);
    ds.add_texture_descriptor(pos);
    ds.add_texture_descriptor(albedo);

    ds.add_storage_descriptor(*light_data);

    // start to build out the pipeline.
    dr.get_composition_pipeline()
        .add_descriptor_set_layout(ds.get_descriptor_layout());
    dr.compile_comp_buffer(vk, app, &shader);

    ds
}

/// Build the lighting data
fn build_lights(vk: &Vulkan, binding: u32) -> LightData {
    let fmt = generate_descriptor_buffer_format::<Light>();
    let mut light_buffer = DataBuffer::create(vk, fmt);

    let colors = to_rgb(vec!["#95A3BF", "#C4D8FF", "#465B85", "#99BBBF", "#447E85"]);

    let mut lights = vec![];

    for i in 0..NUM_LIGHTS {
        let mut l = Light::new();

        // build ambient color
        let col = colors.get_at_normalized(rand_float(0.1, 1.0) * (i as f32));
        l.ambient_color4(col);

        //  build diffuse color
        let diff = colors.get_at_normalized(rand_float(0.1, 1.0) * (i as f32));
        l.diffuse_color4(diff);

        lights.push(l);
    }

    // store light data
    light_buffer.set_data(vk, &lights);

    let mut desc = StorageDescriptor::new();
    desc.set_shader_binding(binding);
    desc.set_buffer_data_with_address(
        vk,
        light_buffer.raw(),
        0,
        get_byte_size::<Light>() * (NUM_LIGHTS as u64),
    );

    LightData {
        lights,
        desc,
        buffer: light_buffer,
    }
}
