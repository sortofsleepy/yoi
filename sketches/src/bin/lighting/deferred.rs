use ash::vk;
use ash::vk::{
    AccessFlags, AttachmentDescription, AttachmentLoadOp, AttachmentReference, AttachmentStoreOp,
    ClearColorValue, ClearValue, CommandBuffer, DependencyFlags, Format, Framebuffer,
    FramebufferCreateInfo, ImageLayout, ImageTiling, ImageUsageFlags, ImageView,
    MemoryPropertyFlags, PipelineBindPoint, PipelineStageFlags, RenderPass, SampleCountFlags,
    SubpassDependency, SUBPASS_EXTERNAL,
};
use yoi_vk::render::renderpass2::{SubpassDescriptor, SubpassReference};
use yoi_vk::{
    generate_depth_texture_format, Descriptor, RenderPass2, RenderPipeline, RenderShader,
    TextureFormat, TextureSamplerDescriptor, VkApp, VkDescriptorBuffer, VkObject, VkSampler,
    VkTexture, Vulkan,
};

/// a basic Deferred Renderer implementation.
pub struct DeferredRenderer {
    dimensions: [u32; 2],
    renderpass: RenderPass2,
    format: Format,
    // Note that this only holds color attachments
    attachments: Vec<VkTexture>,
    composition_pipeline: RenderPipeline,
    geometry_pipeline: RenderPipeline,
    framebuffer: Option<Framebuffer>,
    depth_format: Format,
    depth_image_view: Option<vk::ImageView>,
}

pub enum ATTACHMENTS {
    POSITION = 0,
    ALBEDO = 1,
    NORMAL = 2,
}

impl DeferredRenderer {
    pub fn create(size: [u32; 2], format: Format) -> DeferredRenderer {
        DeferredRenderer {
            dimensions: size,
            format,
            attachments: vec![],
            framebuffer: None,
            composition_pipeline: RenderPipeline::new(),
            geometry_pipeline: RenderPipeline::new(),
            renderpass: RenderPass2::create(size[0], size[1]),
            depth_format: Format::D16_UNORM,
            depth_image_view: None,
        }
    }

    /// Returns a reference to the composition pipeline.
    pub fn get_composition_pipeline(&mut self) -> &mut RenderPipeline {
        &mut self.composition_pipeline
    }

    /// Sets up a default color attachment blend state for all pipelines.
    /// Note that you can still customize the individual blend states by getting the pipeline and setting
    /// things like normal.
    pub fn add_default_color_blend_state(&mut self) {
        for i in 0..self.attachments.len() {
            self.composition_pipeline.add_default_color_attachment_blend_state();
            self.geometry_pipeline.add_default_color_attachment_blend_state()
        }
    }

    /// Returns a reference to the geometry pipeline.
    pub fn get_geometry_pipeline(&mut self) -> &mut RenderPipeline {
        &mut self.geometry_pipeline
    }

    /// Returns the [ImageView] output from an attachment
    /// * `index` - the index of the attachment you want.
    pub fn get_output(&self, index: usize) -> &ImageView {
        &self.attachments[index].get_image_view()
    }

    /// Sets the depth texture format
    pub fn set_depth_format(&mut self, format: Format) {
        self.depth_format = format
    }

    /// Returns a reference to the [vk::Renderpass] object
    pub fn get_renderpass(&self) -> &RenderPass {
        self.renderpass.raw()
    }

    /// generates a descriptor for the position layer
    pub fn generate_position_descriptor(
        &self,
        location: u32,
        sampler: &VkSampler,
    ) -> TextureSamplerDescriptor {
        let mut pos = TextureSamplerDescriptor::new();
        pos.set_shader_binding(location);
        pos.descriptor_type = vk::DescriptorType::INPUT_ATTACHMENT;
        pos.set_image(*self.attachments[0].get_image_view(), *sampler.raw());

        pos
    }

    /// Generates a descriptor for the albedo layer
    pub fn generate_albedo_descriptor(
        &self,
        location: u32,
        sampler: &VkSampler,
    ) -> TextureSamplerDescriptor {
        let mut albedo = TextureSamplerDescriptor::new();
        albedo.set_shader_binding(location);
        albedo.descriptor_type = vk::DescriptorType::INPUT_ATTACHMENT;
        albedo.set_image(*self.attachments[1].get_image_view(), *sampler.raw());

        albedo
    }

    pub fn get_attachments(&self) -> Vec<&vk::ImageView> {
        self.attachments.iter().map(|tex| {
            tex.get_image_view()
        }).collect()
    }

    /// Sets the viewport of the renderpass. Note that the viewport is generally assumed
    /// to start from the top-left of the window.
    ///
    /// * `w` - the x width of the viewport
    /// * `h - the height of the viewport
    pub fn set_viewport(&mut self, w: f32, h: f32) {
        self.renderpass.viewport(0.0, 0.0, w, h)
    }

    /// Begins rendering into the G-Buffer
    pub fn begin(&self, vk: &Vulkan, cb: &CommandBuffer, ds: Option<&VkDescriptorBuffer>) {
        self.renderpass
            .begin(vk, cb, self.framebuffer.as_ref().unwrap());

        if ds.is_some() {
            let descriptor_buffer = ds.as_ref().unwrap();
            descriptor_buffer.bind_descriptors(
                vk,
                cb,
                &self.geometry_pipeline.get_pipeline_layout(),
            );
        }
    }

    /// Binds the G-Buffer pipeline
    ///
    /// * `vk` - the [Vulkan] instance
    /// * `cb` - the [CommandBuffer] to use for the pipeline.
    pub fn bind_gbuffer(&self, vk: &Vulkan, cb: &CommandBuffer) {
        self.geometry_pipeline.bind_pipeline(vk, *cb);
    }

    pub fn bind_lbuffer(&self, vk: &Vulkan, cb: &CommandBuffer) {
        self.composition_pipeline.bind_pipeline(vk, *cb);
    }

    /// ends the render pass
    pub fn end(&self, vk: &Vulkan, cb: &CommandBuffer) {
        self.renderpass.end(vk, cb);
    }

    /// Compiles the composition buffer.
    pub fn compile_comp_buffer(&mut self, vk: &Vulkan, app: &VkApp, shader: &RenderShader) {
        self.composition_pipeline
            .compile2(vk, app, shader, Some(self.renderpass.raw()));
    }

    /// compiles the geometry pipeline.
    pub fn compile_gbuffer(&mut self, vk: &Vulkan, app: &VkApp, shader: &RenderShader) {
        self.geometry_pipeline
            .compile2(vk, app, shader, Some(self.renderpass.raw()));
    }

    /// adds a clear value to the renderpass. The order you call this function in will determine which attachment
    /// the clear value will be applied to, ie the first time you call it, that clear value is assigned to attachment 1
    pub fn add_clear_value(&mut self, val: ClearValue) {
        if self.renderpass.num_clear_values() < self.renderpass.num_attachments() {
            self.renderpass.add_clear_value(val)
        }
    }

    /// Return the position attachment's [ImageView]
    pub fn get_position_attachment(&self) -> &ImageView {
        let idx = ATTACHMENTS::POSITION as usize;
        self.attachments[idx].get_image_view()
    }

    /// Return the albedo attachment's [ImageView]
    pub fn get_albedo_attachment(&self) -> &ImageView {
        let idx = ATTACHMENTS::ALBEDO as usize;
        self.attachments[idx].get_image_view()
    }

    /// Returns the [vk::ImageView] for the depth layer.
    pub fn get_depth_image(&self) -> &ImageView {
        self.depth_image_view.as_ref().unwrap()
    }

    /// Initializes things
    pub fn setup(&mut self, vk: &Vulkan) {
        self.renderpass.add_attachment_descriptions(vec![
            // main color / Albedo
            AttachmentDescription {
                format: self.format,
                samples: SampleCountFlags::TYPE_1,
                load_op: AttachmentLoadOp::CLEAR,
                store_op: AttachmentStoreOp::STORE,
                stencil_load_op: AttachmentLoadOp::DONT_CARE,
                stencil_store_op: AttachmentStoreOp::DONT_CARE,
                initial_layout: ImageLayout::UNDEFINED,
                final_layout: ImageLayout::PRESENT_SRC_KHR,
                ..Default::default()
            },
            // position
            AttachmentDescription {
                format: self.format,
                samples: SampleCountFlags::TYPE_1,
                load_op: AttachmentLoadOp::CLEAR,
                store_op: AttachmentStoreOp::STORE,
                stencil_load_op: AttachmentLoadOp::DONT_CARE,
                stencil_store_op: AttachmentStoreOp::DONT_CARE,
                initial_layout: ImageLayout::UNDEFINED,
                final_layout: ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                ..Default::default()
            },
            // Normals
            AttachmentDescription {
                format: self.format,
                samples: SampleCountFlags::TYPE_1,
                load_op: AttachmentLoadOp::CLEAR,
                store_op: AttachmentStoreOp::STORE,
                stencil_load_op: AttachmentLoadOp::DONT_CARE,
                stencil_store_op: AttachmentStoreOp::DONT_CARE,
                initial_layout: ImageLayout::UNDEFINED,
                final_layout: ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                ..Default::default()
            },
            // depth
            AttachmentDescription {
                format: self.depth_format,
                samples: SampleCountFlags::TYPE_1,
                load_op: AttachmentLoadOp::CLEAR,
                store_op: AttachmentStoreOp::DONT_CARE,
                stencil_load_op: AttachmentLoadOp::DONT_CARE,
                stencil_store_op: AttachmentStoreOp::DONT_CARE,
                initial_layout: ImageLayout::UNDEFINED,
                final_layout: ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                ..Default::default()
            },
        ]);

        self.renderpass.add_subpass_references(SubpassReference {
            subpass_index: 0,
            refs: vec![
                AttachmentReference {
                    attachment: 0,
                    layout: ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                },
                AttachmentReference {
                    attachment: 1,
                    layout: ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                },
                AttachmentReference {
                    attachment: 2,
                    layout: ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                },
            ],
            depth_ref: Some(AttachmentReference {
                attachment: 3,
                layout: ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
            }),
        });

        self.renderpass.add_subpass_references(SubpassReference {
            subpass_index: 1,
            refs: vec![],
            depth_ref: None,
        });

        // setup subpass descriptors
        self.renderpass.add_subpass_descriptors(vec![
            SubpassDescriptor {
                pipeline_bind_point: PipelineBindPoint::GRAPHICS,
            },
            SubpassDescriptor {
                pipeline_bind_point: PipelineBindPoint::GRAPHICS,
            },
        ]);

        self.renderpass.add_subpass_dependencies(vec![
            SubpassDependency::default()
                .src_subpass(SUBPASS_EXTERNAL)
                .dst_subpass(0)
                .src_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                .dst_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                .src_access_mask(AccessFlags::empty())
                .dst_access_mask(AccessFlags::COLOR_ATTACHMENT_WRITE)
                .dependency_flags(DependencyFlags::empty()),
            SubpassDependency::default()
                .src_subpass(0)
                .dst_subpass(1)
                .src_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                .dst_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                .src_access_mask(AccessFlags::empty())
                .dst_access_mask(AccessFlags::COLOR_ATTACHMENT_WRITE)
                .dependency_flags(DependencyFlags::empty()),
        ]);

        // ensure there are enough clear values for the number of attachments we have.
        // if not, we fill in the missing values with a default clear of black and alpha = 1
        if self.renderpass.num_attachments() != self.renderpass.num_clear_values() {
            let diff = self.renderpass.num_attachments() - self.renderpass.num_clear_values();

            for i in 0..diff {
                self.renderpass.add_clear_value(ClearValue {
                    color: ClearColorValue {
                        float32: [0.0, 0.0, 0.0, 1.0],
                    },
                })
            }
        }

        self.renderpass.compile(vk);

        // generate fbo
        for i in 0..3 {
            let mut format = TextureFormat::default();
            format.texture_format = self.format;
            format.extent.width = self.dimensions[0];
            format.extent.height = self.dimensions[1];
            format.extent.depth = 1;
            format.mip_levels = 1;
            format.array_layers = 1;
            format.sample_count = SampleCountFlags::TYPE_1;
            format.tiling_mode = ImageTiling::OPTIMAL;
            format.image_usage_flags = ImageUsageFlags::COLOR_ATTACHMENT
                | ImageUsageFlags::SAMPLED
                | ImageUsageFlags::TRANSFER_SRC;
            format.memory_property_flags = MemoryPropertyFlags::DEVICE_LOCAL;

            self.attachments.push(VkTexture::new(vk, format));
        }

        // generate depth
        let dformat = generate_depth_texture_format(
            self.dimensions[0],
            self.dimensions[1],
            self.depth_format,
        );
        let d_tex = VkTexture::new(vk, dformat);

        let mut views = vec![];

        for attach in &self.attachments {
            views.push(*attach.get_image_view());
        }

        // store depth image view
        self.depth_image_view = Some(*d_tex.get_image_view());

        views.push(*d_tex.get_image_view());

        let create_info = FramebufferCreateInfo::default()
            .width(self.dimensions[0])
            .height(self.dimensions[1])
            .render_pass(*self.renderpass.raw())
            .layers(1)
            .attachments(&views[..]);

        self.framebuffer = Some(vk.generate_framebuffer(create_info))
    }
}
