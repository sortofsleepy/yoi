use crate::raytracing::types::{BlasInput, BuildAccelerationStructure, RTAccelerationStructure};
use crate::raytracing::utils::{
    build_query_pool, build_scratch_buffer, create_accel_structure_buffer, has_flag,
};
use crate::{generate_command_buffer, BufferFormat, DataBuffer, VkObject, Vulkan};
use ash::extensions::khr::{AccelerationStructure, BufferDeviceAddress, RayTracingPipeline};
use ash::vk;
use ash::vk::{
    AccelerationStructureBuildGeometryInfoKHR, AccelerationStructureBuildRangeInfoKHR,
    AccelerationStructureBuildSizesInfoKHR, AccelerationStructureBuildTypeKHR,
    AccelerationStructureCreateFlagsKHR, AccelerationStructureCreateInfoKHR,
    AccelerationStructureGeometryInstancesDataKHR, AccelerationStructureGeometryKHR,
    AccelerationStructureInstanceKHR, AccelerationStructureKHR, AccelerationStructureMotionInfoNV,
    AccelerationStructureTypeKHR, AccessFlags, BufferDeviceAddressInfo, BufferDeviceAddressInfoKHR,
    BufferUsageFlags, BuildAccelerationStructureFlagsKHR, BuildAccelerationStructureModeKHR,
    CommandBuffer, CopyAccelerationStructureInfoKHR, DependencyFlags, DeviceAddress, DeviceSize,
    GeometryTypeKHR, Handle, MemoryBarrier, PipelineStageFlags, QueryPool, StructureType,
};
use std::cmp::max;
use std::mem::size_of;

/// Raytracing helper struct. Most of the work is based off of Nvidia's nvpro_core C++ library
/// https://github.com/nvpro-samples/nvpro_core
pub struct RayTracing {
    buffer_address: BufferDeviceAddress,
    raytrace_pipeline: RayTracingPipeline,
    acceleration_structure: AccelerationStructure,
    command_buffer: CommandBuffer,
}

impl RayTracing {
    pub fn new(instance: &Vulkan) -> Self {
        let vk_instance = instance.get_vk_instance();
        let device = instance.get_logical_device();

        let buffer_address = BufferDeviceAddress::new(vk_instance, device);
        let raytrace_pipeline = RayTracingPipeline::new(vk_instance, device);
        let acceleration_structure = AccelerationStructure::new(vk_instance, device);

        RayTracing {
            buffer_address,
            raytrace_pipeline,
            acceleration_structure,
            command_buffer: generate_command_buffer(instance),
        }
    }

    /// Builds a bottom level acceleration structure from a group of inputs.
    pub fn build_BLAS(
        &mut self,
        instance: &Vulkan,
        inputs: Vec<BlasInput>,
        flags: vk::BuildAccelerationStructureFlagsKHR,
    ) -> Vec<RTAccelerationStructure> {
        let total = inputs.len();

        let mut blas = vec![];

        let mut as_total_size: u64 = 0;
        let mut num_compactions: u32 = 0;
        let mut max_scratch_size: u64 = 0;

        let mut build_as: Vec<BuildAccelerationStructure> =
            vec![BuildAccelerationStructure::new(); inputs.len()];

        for i in 0..total {
            let mut build_info = vk::AccelerationStructureBuildGeometryInfoKHR::default()
                .flags(flags | inputs[i].flags)
                .mode(vk::BuildAccelerationStructureModeKHR::BUILD)
                .geometries(&inputs[i].as_geometry)
                .ty(vk::AccelerationStructureTypeKHR::BOTTOM_LEVEL);

            build_as[i].build_info = build_info;

            for rinfo in &inputs[i].as_build_offset_info {
                build_as[i].range_info.primitive_count = rinfo.primitive_count;
                build_as[i].range_info.transform_offset = rinfo.transform_offset;
                build_as[i].range_info.first_vertex = rinfo.first_vertex;
                build_as[i].range_info.transform_offset = rinfo.transform_offset;
            }

            // find sizes
            let mut max_prim_count = vec![0u32; inputs[i].as_build_offset_info.len()];

            for tt in 0..inputs[i].as_build_offset_info.len() {
                max_prim_count[tt] = inputs[i].as_build_offset_info[tt].primitive_count;

                unsafe {
                    self.acceleration_structure
                        .get_acceleration_structure_build_sizes(
                            AccelerationStructureBuildTypeKHR::DEVICE,
                            &build_info,
                            &max_prim_count,
                            &mut build_as[i].size_info,
                        );
                }
            }

            as_total_size += build_as[i].size_info.acceleration_structure_size;
            max_scratch_size = max(max_scratch_size, build_as[i].size_info.build_scratch_size);
            num_compactions += if has_flag(
                build_info.flags,
                vk::BuildAccelerationStructureFlagsKHR::ALLOW_COMPACTION,
            ) {
                1
            } else {
                0
            }
        }

        // build scratch buffer
        let scratch = build_scratch_buffer(instance, max_scratch_size);

        let scratch_address = instance.get_buffer_device_address(&scratch.buffer);

        // allocate query pool for storing needed size for every blas compaction
        let mut pool = QueryPool::default();

        if num_compactions > 0 {
            // Don't allow mix of on/off compaction
            assert_eq!(num_compactions, inputs.len() as u32);
            pool = build_query_pool(
                instance,
                inputs.len() as u32,
                vk::QueryType::ACCELERATION_STRUCTURE_COMPACTED_SIZE_KHR,
            );
        }

        // Batch creation / compaction of BLAS to allow staying in restricted amount of memory

        //  Indices of the BLAS to create
        let mut indices = vec![];

        let mut batch_size = 0;
        let batch_limit = 256000000;

        for i in 0..inputs.len() {
            indices.push(i as u32);

            batch_size += build_as[i].size_info.acceleration_structure_size;

            if batch_size > batch_limit || i == inputs.len() - 1 {
                self.cmd_create_blas(instance, &indices, &mut build_as, &pool, scratch_address);

                if !pool.is_null() {
                    self.cmd_compact_blas(instance, &indices, &mut build_as, &pool);
                }

                // reset
                batch_size = 0;
                indices.clear();
            }
        }

        // TODO logging?

        // store acceleration structures
        for b in build_as {
            blas.push(b.accel_struct)
        }

        blas
    }

    /// Builds a single bottom level acceleration structure (aka BLAS)
    pub fn cmd_create_blas(
        &mut self,
        instance: &Vulkan,
        indices: &[u32],
        build_as: &mut Vec<BuildAccelerationStructure>,
        pool: &QueryPool,
        scratch_address: DeviceAddress,
    ) {
        if !pool.is_null() {
            instance.reset_query_pool(&pool, indices.len() as u32);
        }

        let cb = self.command_buffer;

        for i in 0..indices.len() {
            instance.begin_command_buffer(&self.command_buffer);
            let accel = self.create_accel_structure(
                instance,
                &build_as[i].size_info,
                AccelerationStructureTypeKHR::BOTTOM_LEVEL,
            );

            unsafe {
                build_as[i].accel_struct.handle = accel.handle;
                build_as[i].build_info.dst_acceleration_structure = build_as[i].accel_struct.handle;
                build_as[i].build_info.scratch_data.device_address = scratch_address;

                self.acceleration_structure
                    .cmd_build_acceleration_structures(
                        self.command_buffer,
                        &[build_as[i].build_info],
                        &[&[build_as[i].range_info]],
                    );

                // Since the scratch buffer is reused across builds, we need a barrier to ensure one build
                // is finished before starting the next one.
                let mut mem_barrier = vk::MemoryBarrier::default();
                mem_barrier.src_access_mask = vk::AccessFlags::ACCELERATION_STRUCTURE_WRITE_KHR;
                mem_barrier.dst_access_mask = vk::AccessFlags::ACCELERATION_STRUCTURE_READ_KHR;

                instance.pipeline_barrier(
                    &self.command_buffer,
                    vk::PipelineStageFlags::ACCELERATION_STRUCTURE_BUILD_KHR,
                    vk::PipelineStageFlags::ACCELERATION_STRUCTURE_BUILD_KHR,
                    vk::DependencyFlags::empty(),
                    &[mem_barrier],
                    &[],
                    &[],
                );
            }

            if !pool.is_null() {
                // Add a query to find the 'real' amount of memory needed, use for compaction
                unsafe {
                    self.acceleration_structure
                        .cmd_write_acceleration_structures_properties(
                            self.command_buffer,
                            &[build_as[i].build_info.dst_acceleration_structure],
                            vk::QueryType::ACCELERATION_STRUCTURE_COMPACTED_SIZE_KHR,
                            *pool,
                            0,
                        )
                }
            }
        }
        instance.end_command_buffer(&self.command_buffer);
        instance.submit_command_buffer(&self.command_buffer);
    }

    pub fn cmd_compact_blas(
        &self,
        instance: &Vulkan,
        indices: &[u32],
        build_as: &mut Vec<BuildAccelerationStructure>,
        pool: &QueryPool,
    ) {
        let mut query_count: usize = 0;

        let mut compact_sizes: Vec<DeviceSize> = vec![0; indices.len()];

        // get compacted size back
        instance.get_query_pool_results(pool, &mut compact_sizes);

        for i in 0..indices.len() {
            build_as[i].cleanup_as = build_as[i].accel_struct;
            build_as[i].size_info.acceleration_structure_size = compact_sizes[query_count];

            // build compact version of acceleration structure
            let accel = self.create_accel_structure(
                instance,
                &build_as[i].size_info,
                AccelerationStructureTypeKHR::BOTTOM_LEVEL,
            );

            // copy original blas to compact version
            let mut copy = CopyAccelerationStructureInfoKHR::default();
            copy.src = build_as[i].build_info.dst_acceleration_structure;
            copy.dst = build_as[i].accel_struct.handle;
            copy.mode = vk::CopyAccelerationStructureModeKHR::COMPACT;

            unsafe {
                self.acceleration_structure
                    .cmd_copy_acceleration_structure(self.command_buffer, &copy);
            }

            query_count += 1;
        }
    }

    /// Gets the address of a buffer.
    #[deprecated = "Use get_buffer_device_address on the core Vulkan context instead."]
    pub fn get_buffer_address_with_info(&self, info: &BufferDeviceAddressInfoKHR) -> DeviceSize {
        unsafe { self.buffer_address.get_buffer_device_address(info) }
    }

    /// Gets the address of a buffer.
    #[deprecated = "Use get_buffer_device_address on the core Vulkan context instead."]
    pub fn get_buffer_address(&self, buffer: &vk::Buffer) -> DeviceSize {
        let mut info = BufferDeviceAddressInfo::default();
        info.buffer = *buffer;
        unsafe { self.buffer_address.get_buffer_device_address(&info) }
    }

    /// Get device address of BLAS structure.
    pub fn get_blas_device_address(
        &self,
        blases: &Vec<RTAccelerationStructure>,
        blas_id: u32,
    ) -> u64 {
        let blas = blases[blas_id as usize];
        let mut address = vk::AccelerationStructureDeviceAddressInfoKHR::default();
        address.acceleration_structure = blas.handle;

        unsafe {
            self.acceleration_structure
                .get_acceleration_structure_device_address(&address)
        }
    }

    /// Helper to generate an acceleration structure.
    fn create_accel_structure(
        &self,
        instance: &Vulkan,
        size_info: &AccelerationStructureBuildSizesInfoKHR,
        structure_type: AccelerationStructureTypeKHR,
    ) -> RTAccelerationStructure {
        let buffer = create_accel_structure_buffer(instance, size_info);
        let create_info = AccelerationStructureCreateInfoKHR::default()
            .buffer(*buffer.raw())
            .ty(structure_type)
            .size(size_info.acceleration_structure_size);

        unsafe {
            RTAccelerationStructure {
                handle: self
                    .acceleration_structure
                    .create_acceleration_structure(&create_info, None)
                    .unwrap(),
                buffer: None,
            }
        }
    }

    /// Builds the top level acceleration structure
    /// https://github.com/nvpro-samples/nvpro_core/blob/f7d983e5757b8f96b2499faac9886dd14391fb8c/nvvk/raytraceKHR_vk.hpp#L144
    ///
    /// AccelerationStructureInstanceKHR is usually the type but can also use VkAccelerationStructureMotionInstanceNV
    pub fn build_TLAS<T>(
        &self,
        ctx: &Vulkan,
        instances: &Vec<T>,
        flags: BuildAccelerationStructureFlagsKHR,
        update: bool,
        motion: bool,
    ) {
        let cb = self.command_buffer;
        let count_instance = instances.len();

        // Create a buffer holding the actual instance data (matrices++) for use by the AS builder
        let mut fmt = BufferFormat::default();
        fmt.size = (size_of::<AccelerationStructureInstanceKHR>() * instances.len()) as u64;
        fmt.usage_flags = BufferUsageFlags::SHADER_DEVICE_ADDRESS
            | BufferUsageFlags::ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_KHR;

        let instances_buffer = DataBuffer::create(ctx, fmt);
        let instances_buffer_address = ctx.get_buffer_device_address(&instances_buffer.raw());

        // Make sure the copy of the instance buffer are copied before triggering the acceleration structure build
        let mut barrier = MemoryBarrier::default();
        barrier.s_type = StructureType::MEMORY_BARRIER;
        barrier.src_access_mask(AccessFlags::TRANSFER_WRITE);
        barrier.dst_access_mask(AccessFlags::ACCELERATION_STRUCTURE_WRITE_KHR);

        ctx.pipeline_barrier(
            &cb,
            PipelineStageFlags::TRANSFER,
            PipelineStageFlags::ACCELERATION_STRUCTURE_BUILD_KHR,
            DependencyFlags::empty(),
            &[barrier],
            &[],
            &[],
        );

        self.cmd_create_tlas(
            ctx,
            &cb,
            count_instance as u32,
            instances_buffer_address,
            flags,
            update,
            motion,
        );
    }

    /// does the actual building of a single top level acceleration structure (aka TLAS)
    fn cmd_create_tlas(
        &self,
        instance: &Vulkan,
        cb: &CommandBuffer,
        count_instance: u32,
        instance_buffer_address: DeviceAddress,
        flags: BuildAccelerationStructureFlagsKHR,
        update: bool,
        motion: bool,
    ) {
        let mut tlas: Option<RTAccelerationStructure> = None;
        let mut instances_vk = AccelerationStructureGeometryInstancesDataKHR::default();
        instances_vk.data.device_address = instance_buffer_address;

        // Put the above into a VkAccelerationStructureGeometryKHR. We need to put the instances struct in a union and label it as instance data.
        let mut top_as_geometry = AccelerationStructureGeometryKHR::default();
        top_as_geometry.geometry_type = GeometryTypeKHR::INSTANCES;
        top_as_geometry.geometry.instances = instances_vk;

        // find sizes
        let binding = [top_as_geometry];
        let mut build_info = AccelerationStructureBuildGeometryInfoKHR::default()
            .flags(flags)
            .geometries(&binding)
            .mode(if update == true {
                BuildAccelerationStructureModeKHR::UPDATE
            } else {
                BuildAccelerationStructureModeKHR::BUILD
            })
            .ty(AccelerationStructureTypeKHR::TOP_LEVEL)
            .src_acceleration_structure(AccelerationStructureKHR::null());

        let mut size_info = AccelerationStructureBuildSizesInfoKHR::default();

        unsafe {
            self.acceleration_structure
                .get_acceleration_structure_build_sizes(
                    AccelerationStructureBuildTypeKHR::DEVICE,
                    &build_info,
                    &[count_instance],
                    &mut size_info,
                );
        }

        let mut motion_info = AccelerationStructureMotionInfoNV::default();

        if motion {
            motion_info.max_instances = count_instance;
        }

        if !update {
            let mut create_info = AccelerationStructureCreateInfoKHR::default()
                .ty(AccelerationStructureTypeKHR::TOP_LEVEL)
                .size(size_info.acceleration_structure_size);

            if motion {
                create_info.create_flags(AccelerationStructureCreateFlagsKHR::MOTION_NV);
                create_info.push_next(&mut motion_info);
            }

            tlas = Some(self.create_accel_structure(
                instance,
                &size_info,
                AccelerationStructureTypeKHR::TOP_LEVEL,
            ))
        }

        // allocate scratch memory
        let mut scratch_fmt = BufferFormat::default();
        scratch_fmt.usage_flags =
            BufferUsageFlags::STORAGE_BUFFER | BufferUsageFlags::SHADER_DEVICE_ADDRESS;
        scratch_fmt.size = size_info.build_scratch_size;
        let scratch_buffer = DataBuffer::create(instance, scratch_fmt);
        let buffer_address = instance.get_buffer_device_address(scratch_buffer.raw());

        if tlas.is_some() {
            build_info.src_acceleration_structure = tlas.unwrap().handle;
        } else {
            build_info.src_acceleration_structure = AccelerationStructureKHR::null();
        }

        build_info.dst_acceleration_structure = tlas.unwrap().handle;
        build_info.scratch_data.device_address = buffer_address;

        // Build offsets
        // TODO in the original code, another AccelerationStructureBuildRangeInfoKHR seems to be created which then takes buildOffsetInfo as a pointer
        //  which is then used in the acceleration structure building function - not yet sure why
        //  https://github.com/nvpro-samples/nvpro_core/blob/master/nvvk/raytraceKHR_vk.cpp#L375
        let mut build_offset_info = AccelerationStructureBuildRangeInfoKHR::default();
        build_offset_info.primitive_count = count_instance;

        unsafe {
            self.acceleration_structure
                .cmd_build_acceleration_structures(*cb, &[build_info], &[&[build_offset_info]])
        }
    }
}
