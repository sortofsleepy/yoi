use crate::framework::databuffer::find_memory_type_index;
use crate::raytracing::types::RTScratchBuffer;
use crate::{BufferFormat, DataBuffer, Vulkan};
use ash::vk;
use ash::vk::{
    AccelerationStructureBuildSizesInfoKHR, BufferCreateInfo, BufferUsageFlags,
    BuildAccelerationStructureFlagsKHR, DeviceOrHostAddressKHR, DeviceSize, MemoryAllocateFlags,
    MemoryAllocateFlagsInfo, MemoryAllocateInfo, MemoryPropertyFlags, QueryPool,
    QueryPoolCreateInfo, QueryType,
};

pub fn has_flag(
    itm: BuildAccelerationStructureFlagsKHR,
    flag: BuildAccelerationStructureFlagsKHR,
) -> bool {
    (itm & flag) == flag
}

pub fn build_query_pool(instance: &Vulkan, count: u32, query_type: QueryType) -> QueryPool {
    let mut pool_info = QueryPoolCreateInfo::default();
    pool_info.query_count = count;
    pool_info.query_type = query_type;

    instance.generate_query_pool(pool_info).unwrap()
}

/// Builds a scratch buffer for the ray trace process.
pub fn build_scratch_buffer(instance: &Vulkan, size: DeviceSize) -> RTScratchBuffer {
    let buffer_create_info = BufferCreateInfo::default()
        .size(size)
        .usage(BufferUsageFlags::STORAGE_BUFFER | BufferUsageFlags::SHADER_DEVICE_ADDRESS);

    let scratch_buffer = instance.generate_buffer(&buffer_create_info);

    // build buffer memory
    let scratch_buffer_memory_requirements =
        instance.get_buffer_memory_requirements(scratch_buffer);

    let mut memory_flags_alloc =
        MemoryAllocateFlagsInfo::default().flags(MemoryAllocateFlags::DEVICE_ADDRESS);

    let mem_props = instance.get_memory_properties();
    let memory_index = find_memory_type_index(
        &scratch_buffer_memory_requirements,
        &mem_props,
        MemoryPropertyFlags::DEVICE_LOCAL,
    );

    let memory_alloc = MemoryAllocateInfo::default()
        .push_next(&mut memory_flags_alloc)
        .allocation_size(scratch_buffer_memory_requirements.size)
        .memory_type_index(memory_index.unwrap());

    let buffer_mem = instance.allocate_memory(&memory_alloc);
    instance.bind_memory(&scratch_buffer, &buffer_mem, 0);

    RTScratchBuffer {
        buffer: scratch_buffer,
        memory: buffer_mem,
        device_address: DeviceOrHostAddressKHR {
            device_address: instance.get_buffer_device_address(&scratch_buffer),
        },
    }
}

/// builds an acceleration structure buffer.
pub fn create_accel_structure_buffer(
    instance: &Vulkan,
    build_sizes_info: &AccelerationStructureBuildSizesInfoKHR,
) -> DataBuffer {
    let mut fmt = BufferFormat::default();
    fmt.mem_alloc_flags = vk::MemoryAllocateFlagsKHR::DEVICE_ADDRESS;
    fmt.usage_flags = BufferUsageFlags::ACCELERATION_STRUCTURE_STORAGE_KHR
        | BufferUsageFlags::SHADER_DEVICE_ADDRESS;
    fmt.size = build_sizes_info.acceleration_structure_size;

    DataBuffer::create(instance, fmt)
}
