use crate::DataBuffer;
use ash::vk;
use ash::vk::{
    AccelerationStructureBuildGeometryInfoKHR, AccelerationStructureBuildRangeInfoKHR,
    AccelerationStructureBuildSizesInfoKHR, AccelerationStructureGeometryKHR,
    AccelerationStructureKHR, BuildAccelerationStructureFlagsKHR,
};

/// Alternative to AccelerationStructureBuildRangeInfoKHR
/// to avoid copy/clone issues
#[derive(Copy, Clone)]
pub struct AccelerationStructureBuildRangeInfo {
    pub prim_count: u32,
    pub prim_offset: u32,
    pub first_vertex: u32,
    pub transform_offset: u32,
}

impl Default for AccelerationStructureBuildRangeInfo {
    fn default() -> Self {
        AccelerationStructureBuildRangeInfo {
            prim_count: 0,
            prim_offset: 0,
            first_vertex: 0,
            transform_offset: 0,
        }
    }
}

#[derive(Copy, Clone)]
pub struct RTAccelerationStructure {
    pub handle: AccelerationStructureKHR,
    pub buffer: Option<DataBuffer>,
}
impl RTAccelerationStructure {
    pub fn new() -> Self {
        RTAccelerationStructure {
            handle: AccelerationStructureKHR::null(),
            buffer: None,
        }
    }
}
///////////////////////

///////////////////////

/// Helper for building bottom level acceleration structure (aka blas)
/// based on https://github.com/nvpro-samples/nvpro_core/blob/d1942a7bcd59068fc6f08b1b34d910a98b67c20e/nvvk/raytraceKHR_vk.hpp#L100
pub struct BlasInput<'a> {
    pub as_geometry: Vec<AccelerationStructureGeometryKHR<'a>>,
    pub as_build_offset_info: Vec<AccelerationStructureBuildRangeInfoKHR>,
    pub flags: BuildAccelerationStructureFlagsKHR,
    pub size_info: AccelerationStructureBuildSizesInfoKHR<'a>,
}

impl<'a> BlasInput<'a> {
    pub fn new() -> Self {
        BlasInput {
            as_geometry: vec![],
            as_build_offset_info: vec![],
            flags: BuildAccelerationStructureFlagsKHR::default(),
            size_info: AccelerationStructureBuildSizesInfoKHR::default(),
        }
    }
}

///////////////////////

#[derive(Copy, Clone)]
pub struct BuildAccelerationStructure<'b> {
    pub build_info: AccelerationStructureBuildGeometryInfoKHR<'b>,
    pub size_info: AccelerationStructureBuildSizesInfoKHR<'b>,
    pub range_info: AccelerationStructureBuildRangeInfoKHR,
    pub accel_struct: RTAccelerationStructure,
    pub cleanup_as: RTAccelerationStructure,
}

impl<'b> BuildAccelerationStructure<'b> {
    pub fn new() -> Self {
        BuildAccelerationStructure {
            build_info: Default::default(),
            size_info: Default::default(),
            range_info: Default::default(),
            accel_struct: RTAccelerationStructure::new(),
            cleanup_as: RTAccelerationStructure::new(),
        }
    }
}

///////////////////////

pub struct RTScratchBuffer {
    pub buffer: vk::Buffer,
    pub device_address: vk::DeviceOrHostAddressKHR,
    pub memory: vk::DeviceMemory,
}
