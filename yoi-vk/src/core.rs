#![allow(unused_imports)]
#![allow(dead_code)]

use ash::{vk, Entry};
use std::ffi::CStr;
use std::ffi::CString;
use std::os::raw::{c_char, c_void};

use ash::extensions::{
    ext::DebugUtils,
    ext::DescriptorBuffer,
    khr::{Surface, Swapchain},
};

use crate::debug::logger::enable_validation;
use crate::VkTexture;
use ash::prelude::VkResult;
use ash::vk::{
    AllocationCallbacks, ApplicationInfo, Buffer, BufferCopy, ClearAttachment, ClearRect,
    CommandBuffer, CommandBufferBeginInfo, CommandBufferUsageFlags, CommandPool,
    DebugUtilsLabelEXT, DebugUtilsMessengerEXT, DebugUtilsObjectNameInfoEXT,
    DebugUtilsObjectTagInfoEXT, DescriptorSetLayout, DeviceMemory, DeviceSize, Fence,
    FenceCreateInfo, Format, FormatFeatureFlags, FormatProperties2, Image, ImageCopy, ImageLayout,
    ImageView, MemoryAllocateInfo, MemoryMapFlags, MemoryRequirements, PhysicalDevice,
    PhysicalDeviceMemoryProperties, PhysicalDeviceProperties, Pipeline, PipelineBindPoint,
    PipelineLayout, QueryPool, QueryPoolCreateInfo, Queue, Rect2D, RenderPass, Sampler, Semaphore,
    SemaphoreSignalInfo, ShaderModule, ShaderModuleCreateInfo, ShaderStageFlags, SubpassBeginInfo,
    SubpassContents, SubpassEndInfo, SurfaceCapabilitiesKHR, SurfaceFormatKHR, SurfaceKHR,
    Viewport,
};
use ash::{Device, Instance};

/// contains all the Vulkan info you want to use for the app
/// including layers and extensions you want to enable.
///
/// For reference
/// https://github.com/KhronosGroup/Vulkan-Samples/tree/main/samples/extensions/debug_utils
#[derive(Debug)]
pub struct VkAppInfo<'a> {
    pub api_version: u32,
    pub application_version: u32,
    pub application_name: CString,
    pub engine_name: CString,
    pub engine_version: u32,
    pub layer_names: Vec<CString>,
    pub extension_names: Vec<CString>,
    pub surface_extension_names: Vec<&'a CStr>,

    pub device_extension_names: Vec<*const c_char>,
    pub window_extensions: Vec<*const c_char>,

    // the index of the physical device to use.
    pub physical_device_index: usize,
    pub enable_dynamic_render: bool,

    // Whether or not to enable nvidia mesh shader extension
    // TODO switch to general extension
    pub enable_nv_mesh_shader: bool,

    pub enable_ray_trace: bool,

    // whether or not to enable bindless descriptor
    pub enable_descriptor_buffer: bool,
}

const variant_mask: u32 = 0x7 << 29;
const major_mask: u32 = 0x7F << 22;
const minor_mask: u32 = 0xFFF << 12;
const patch_mask: u32 = 0xFFF;

pub fn print_initial_settings(info: &VkAppInfo) {
    println!("Initial settings are \n======");
    println!(
        "API: {}.{}.{}.{}",
        (info.api_version & variant_mask) >> 29,
        (info.api_version & major_mask) >> 22,
        (info.api_version & minor_mask) >> 12,
        (info.api_version & patch_mask)
    );

    println!("Application version: {}", info.application_version);
    println!(
        "Application Name: {:?}",
        info.application_name.to_str().unwrap()
    );

    // print window extensions
    let mut win_extensions = vec![];
    for ext in &info.window_extensions {
        unsafe {
            let str = CStr::from_ptr(ext.clone());
            win_extensions.push(str.to_str().unwrap());
        }
    }

    println!("Window extensions: {:?}", win_extensions);

    println!("Engine name: {}", info.engine_name.to_str().unwrap());
    println!("Engine version: {}", info.engine_version);

    // print layer names
    let mut layer_extensions = vec![];
    for ext in &info.layer_names {
        unsafe {
            let str = ext.to_str().unwrap();
            layer_extensions.push(str);
        }
    }
    println!("Layer names: {:?}", layer_extensions);

    println!("\n");
}

impl Default for VkAppInfo<'_> {
    fn default() -> Self {
        VkAppInfo {
            api_version: vk::make_api_version(0, 1, 3, 250),
            application_version: 0,
            application_name: CString::new("Hello").unwrap(),
            engine_name: CString::new("hello").unwrap(),
            engine_version: 0,
            layer_names: vec![],
            extension_names: vec![],
            physical_device_index: 0,
            surface_extension_names: vec![],
            window_extensions: vec![],
            device_extension_names: vec![],
            enable_dynamic_render: true,
            enable_ray_trace: false,
            enable_descriptor_buffer: true,
            enable_nv_mesh_shader: false,
        }
    }
}

/// Basic wrapper around the core elements of a Vulkan program like the
/// logical and physical devices as well as various queues.
/// For simplicity, also used to generate some of the common objects you might need to use.
pub struct Vulkan {
    instance: Instance,
    physical_device: PhysicalDevice,
    entry: Entry,
    logical_device: Option<Device>,
    surface: Option<SurfaceKHR>,
    surface_loader: Surface,
    graphics_queue_index: usize,
    present_queue_index: usize,
    compute_queue_index: usize,
    graphics_queue: Option<Queue>,
    present_queue: Option<Queue>,
    compute_queue: Option<Queue>,
    debugger: Option<DebugUtilsMessengerEXT>,
    device_extensions: Vec<*const c_char>,
    enable_dynamic_render: bool,
    enable_ray_trace: bool,
    enable_mesh_shader: bool,
    enable_descriptor_buffer: bool,
    debug_loader: Option<DebugUtils>,
    descriptor_buffer_ext: Option<DescriptorBuffer>,
}

impl Vulkan {
    pub fn new(info: VkAppInfo) -> Self {
        // print app info if debugging.
        #[cfg(debug_assertions)]
        {
            //println!("Vulkan settings passed in are : \n{:?}\n", info);
            print_initial_settings(&info);
        }

        let _info = vk::ApplicationInfo::default()
            .application_name(&info.application_name)
            .application_version(info.application_version)
            .api_version(info.api_version)
            .engine_name(&info.engine_name)
            .engine_version(info.engine_version);

        ///////// SETUP LAYERS //////////////
        let default_layers = vec![
            // only include validation during debug
            #[cfg(debug_assertions)]
            {
                CString::new("VK_LAYER_KHRONOS_validation").unwrap()
            },
            // if debug, include LunarG monitor layer.
            #[cfg(debug_assertions)]
            {
                CString::new("VK_LAYER_LUNARG_monitor").unwrap()
            },
        ];

        // build user layers
        let user_layers = &info.layer_names[..];

        //concat everything into one set.
        let layers = [&default_layers[..], user_layers].concat();

        let layer_names_raw: Vec<*const i8> =
            layers.iter().map(|raw_name| raw_name.as_ptr()).collect();

        ///////// SETUP SURFACE EXTENSIONS //////////////
        let mut extension_names_raw = info
            .surface_extension_names
            .iter()
            .map(|ext| ext.as_ptr())
            .collect::<Vec<_>>();

        #[cfg(debug_assertions)]
        {
            extension_names_raw.push(DebugUtils::NAME.as_ptr());
        }

        // add in any user defined instance extensions.
        for ext_name in info.extension_names {
            extension_names_raw.push(ext_name.as_ptr());
        }

        // push window extensions
        for ext_name in info.window_extensions {
            extension_names_raw.push(ext_name);
        }

        // if we want dynamic rendering we need to tick some boxes.
        // note that dynamic render is enabled by default.
        if info.enable_dynamic_render {
            extension_names_raw
                .push(ash::extensions::khr::GetPhysicalDeviceProperties2::NAME.as_ptr());
        }

        //////// parse out device extensions ///////////

        // first store any user defined extension names. Anything necessary by the system will get added in
        // [setup_logical_device]
        let device_extension = info.device_extension_names.to_vec();

        //////// BUILD INSTANCE ///////////

        let entry = Entry::linked();
        let instance_create_info = vk::InstanceCreateInfo::default()
            .application_info(&_info)
            .enabled_layer_names(&layer_names_raw)
            .enabled_extension_names(&extension_names_raw);

        let instance: Instance =
            unsafe { entry.create_instance(&instance_create_info, None).unwrap() };

        //// GET PHYSICAL DEVICE /////
        let pdevices: Vec<PhysicalDevice> = unsafe {
            instance
                .enumerate_physical_devices()
                .expect("Error loading physical device")
        };

        // if debug mode, output some possibly useful information like device name, driver etc.
        #[cfg(debug_assertions)]
        {
            let a = pdevices[info.physical_device_index];
            unsafe {
                let props = instance.get_physical_device_properties(a);
                let d_name: *const c_char = &props.device_name[0];
                let d_str = CStr::from_ptr(d_name);
                println!("Using device {:?}", d_str.to_string_lossy());
            }
        }

        //// BUILD SURFACE LOADER ///
        let surface_loader = Surface::new(&entry, &instance);

        Vulkan {
            instance,
            entry,
            surface_loader,
            logical_device: None,
            surface: None,
            debugger: None,
            graphics_queue_index: 0,
            present_queue_index: 0,
            compute_queue_index: 0,
            graphics_queue: None,
            present_queue: None,
            compute_queue: None,
            physical_device: pdevices[info.physical_device_index],
            device_extensions: device_extension,
            enable_dynamic_render: info.enable_dynamic_render,
            enable_ray_trace: info.enable_ray_trace,
            enable_mesh_shader: info.enable_nv_mesh_shader, // TODO switch to general extension later
            enable_descriptor_buffer: info.enable_descriptor_buffer,
            debug_loader: None,
            descriptor_buffer_ext: None,
        }
    }

    /// Returns the ash::Entry object for this instance.
    pub fn get_entry(&self) -> &Entry {
        &self.entry
    }

    /// returns the ash::Instance object associated with this instance
    pub fn get_vk_instance(&self) -> &Instance {
        &self.instance
    }

    /// Runs through the basic setup needed to build out the rest of the key components.
    pub fn setup(&mut self, surface: &vk::SurfaceKHR) {
        self.build_queue_indices(&surface);
        self.setup_logical_device();
        self.build_queues();

        // if we've enabled descriptor buffer support, we can now build the extension object.
        if self.enable_descriptor_buffer {
            self.descriptor_buffer_ext = Some(DescriptorBuffer::new(
                &self.instance,
                self.logical_device.as_ref().unwrap(),
            ));
        }
    }

    pub fn build_queues(&mut self) {
        if self.compute_queue.is_some()
            && self.graphics_queue.is_some()
            && self.present_queue.is_some()
        {
            return;
        }

        let device = self.logical_device.as_ref().unwrap();
        let graphics_index = self.graphics_queue_index as u32;
        let present_index = self.present_queue_index as u32;
        let compute_index = self.compute_queue_index as u32;

        let graphics_queue = unsafe { device.get_device_queue(graphics_index, 0) };
        let present_queue = unsafe { device.get_device_queue(present_index, 0) };
        let compute_queue = unsafe { device.get_device_queue(compute_index, 0) };

        self.compute_queue = Some(compute_queue);
        self.graphics_queue = Some(graphics_queue);
        self.present_queue = Some(present_queue);
    }

    /// grabs the queue indices for the device
    pub fn build_queue_indices(&mut self, surface: &vk::SurfaceKHR) {
        unsafe {
            let pdevice = self.physical_device;

            let queue_family_props = self
                .instance
                .get_physical_device_queue_family_properties(pdevice);

            // check for graphics queue first
            let graphics = queue_family_props
                .iter()
                .enumerate()
                .filter_map(|(index, info)| {
                    let graphics_support = info.queue_flags.contains(vk::QueueFlags::GRAPHICS);

                    if graphics_support {
                        Some(index)
                    } else {
                        None
                    }
                })
                .next();

            // check present support
            let present = queue_family_props
                .iter()
                .enumerate()
                .filter_map(|(index, info)| {
                    let present_support = self
                        .surface_loader
                        .get_physical_device_surface_support(
                            self.physical_device,
                            index as u32,
                            *surface,
                        )
                        .unwrap();

                    if present_support {
                        Some(index)
                    } else {
                        None
                    }
                })
                .next();

            // check compute support
            let compute = queue_family_props
                .iter()
                .enumerate()
                .filter_map(|(index, info)| {
                    let compute_support = info.queue_flags.contains(vk::QueueFlags::COMPUTE);
                    if compute_support {
                        Some(index)
                    } else {
                        None
                    }
                })
                .next();

            self.compute_queue_index = compute.unwrap();
            self.graphics_queue_index = graphics.unwrap();
            self.present_queue_index = present.unwrap();
        }
    }

    /// get surface capabilities
    pub fn get_physical_device_capabilities(&self, surface: &SurfaceKHR) -> SurfaceCapabilitiesKHR {
        let pdevice = self.physical_device;

        unsafe {
            self.surface_loader
                .get_physical_device_surface_capabilities(pdevice, *surface)
                .unwrap()
        }
    }

    /// get surface capabilities
    pub fn get_physical_device_capabilities_for_surface(
        &self,
        surface: &vk::SurfaceKHR,
    ) -> SurfaceCapabilitiesKHR {
        let pdevice = self.physical_device;

        unsafe {
            self.surface_loader
                .get_physical_device_surface_capabilities(pdevice, *surface)
                .unwrap()
        }
    }

    /// Return graphics queue index.
    pub fn get_graphics_queue_index(&self) -> usize {
        self.graphics_queue_index
    }

    /// sets up the logical device for the instance.
    pub fn setup_logical_device(&mut self) {
        // don't go through creation process if device exists.
        if self.logical_device.is_some() {
            return;
        }

        // swapchain, descriptor buffer and dynamic rendering extensions included by default.
        let mut device_extension_names_raw = vec![
            Swapchain::NAME.as_ptr(),
            ash::extensions::khr::DynamicRendering::NAME.as_ptr(),
            ash::extensions::khr::BufferDeviceAddress::NAME.as_ptr(),
        ];

        let priorities = [1.0];
        let p_device = self.physical_device;
        let instance = &self.instance;
        let mut queue_info = vec![];

        // apply user defined extensions.
        let mut device_extensions = &mut self.device_extensions;
        for ext in device_extensions {
            device_extension_names_raw.push(*ext);
        }

        // next apply any other key extensions that may be needed depending on settings
        if self.enable_ray_trace {
            device_extension_names_raw
                .push(ash::extensions::khr::RayTracingPipeline::NAME.as_ptr());
            device_extension_names_raw
                .push(ash::extensions::khr::AccelerationStructure::NAME.as_ptr());
            device_extension_names_raw
                .push(ash::extensions::khr::DeferredHostOperations::NAME.as_ptr());
        }

        // we try to see if the graphics and present queue indices are the same, if so we just need one value.
        if self.present_queue_index == self.graphics_queue_index {
            queue_info.push(
                vk::DeviceQueueCreateInfo::default()
                    .queue_family_index(self.graphics_queue_index as u32)
                    .queue_priorities(&priorities),
            );
        } else {
            queue_info.push(
                vk::DeviceQueueCreateInfo::default()
                    .queue_family_index(self.graphics_queue_index as u32)
                    .queue_priorities(&priorities),
            );
            queue_info.push(
                vk::DeviceQueueCreateInfo::default()
                    .queue_family_index(self.present_queue_index as u32)
                    .queue_priorities(&priorities),
            );
        }

        //////////// ENABLE EXTENSION FEATURES //////////////////
        // TODO make this user-setable
        let features = vk::PhysicalDeviceFeatures {
            shader_clip_distance: 1,
            sampler_anisotropy: vk::Bool32::from(true),
            ..Default::default()
        };

        // dynamic rendering feature
        let mut dyna_rendering = vk::PhysicalDeviceDynamicRenderingFeaturesKHR::default()
            .dynamic_rendering(self.enable_dynamic_render);

        // newer physical device features.
        let mut physical_device_12_features = vk::PhysicalDeviceVulkan12Features::default();

        // acceleration structure features
        let mut accel_structure_feature =
            vk::PhysicalDeviceAccelerationStructureFeaturesKHR::default();

        // ray trace pipeline features
        let mut ray_trace_pipeline = vk::PhysicalDeviceRayTracingPipelineFeaturesKHR::default();

        // Nvidia mesh shader features
        // TODO eventually switch to KHR version once support is able to be tested.
        let mut mesh_shader_nv = vk::PhysicalDeviceMeshShaderFeaturesNV::default();

        // Descriptor indexing features
        let mut physical_device_descriptor_indexing_features =
            vk::PhysicalDeviceDescriptorIndexingFeatures::default();

        // descriptor buffer features
        let mut descriptor_buffer_features =
            vk::PhysicalDeviceDescriptorBufferFeaturesEXT::default();

        // buffer address features
        let mut buffer_address_features_ext = vk::PhysicalDeviceBufferAddressFeaturesEXT::default();
        let mut buffer_address_features_khr =
            vk::PhysicalDeviceBufferDeviceAddressFeaturesKHR::default();

        // enable features as needed for ray tracing
        if self.enable_ray_trace {
            physical_device_12_features.buffer_device_address = vk::Bool32::from(true);
            accel_structure_feature.acceleration_structure = vk::Bool32::from(true);
            ray_trace_pipeline.ray_tracing_pipeline = vk::Bool32::from(true);
        }

        // enable mesh shader features
        if self.enable_mesh_shader {
            println!("Mesh Shader enabled : note currently using Nvidia specific extension; will switch to common extension when available.");
            mesh_shader_nv.mesh_shader = vk::Bool32::from(true);
            mesh_shader_nv.task_shader = vk::Bool32::from(true);
        }

        // enable descriptor buffer features
        if self.enable_descriptor_buffer {
            println!("NOTICE: If using RenderDoc - at the moment, you cannot use Descriptor Buffers. The instance launched by RenderDoc will immediately crash otherwise.");

            // TODO note that as of 10.18.2023 - RenderDoc appears to lack support for descriptor buffers and enabling this extension will cause
            //  the launch of an app to fail.
            device_extension_names_raw.push(DescriptorBuffer::NAME.as_ptr());

            descriptor_buffer_features.descriptor_buffer = vk::Bool32::from(true);
            buffer_address_features_ext.buffer_device_address = vk::Bool32::from(true);
            buffer_address_features_khr.buffer_device_address = vk::Bool32::from(true);

            // Enable partially bound descriptor bindings
            physical_device_descriptor_indexing_features.descriptor_binding_partially_bound =
                vk::Bool32::from(true);

            // Enable non-uniform indexing and update after bind
            // binding flags for textures, uniforms, and buffers
            // TODO add other possible data types like texel buffers, etc.
            //  https://registry.khronos.org/vulkan/specs/1.3-extensions/man/html/VkPhysicalDeviceDescriptorIndexingFeatures.html#_description
            physical_device_descriptor_indexing_features
                .shader_sampled_image_array_non_uniform_indexing = vk::Bool32::from(true);
            physical_device_descriptor_indexing_features
                .descriptor_binding_sampled_image_update_after_bind = vk::Bool32::from(true);
            physical_device_descriptor_indexing_features
                .shader_uniform_buffer_array_non_uniform_indexing = vk::Bool32::from(true);
            physical_device_descriptor_indexing_features
                .descriptor_binding_uniform_buffer_update_after_bind = vk::Bool32::from(true);
            physical_device_descriptor_indexing_features
                .shader_storage_buffer_array_non_uniform_indexing = vk::Bool32::from(true);
            physical_device_descriptor_indexing_features
                .descriptor_binding_storage_buffer_update_after_bind = vk::Bool32::from(true);
        }

        ////////// BUILD AND APPEND FEATURE SETTINGS AS NEEDED /////////////////

        let mut device_create_info = vk::DeviceCreateInfo::default()
            .queue_create_infos(&queue_info[..])
            .enabled_extension_names(&device_extension_names_raw[..])
            .push_next(&mut dyna_rendering);

        if self.enable_ray_trace {
            #[cfg(debug_assertions)]
            {
                println!("Raytracing enabled");
            }

            device_create_info = device_create_info
                .push_next(&mut accel_structure_feature)
                .push_next(&mut ray_trace_pipeline);
        }

        if self.enable_mesh_shader {
            #[cfg(debug_assertions)]
            {
                println!("Mesh shader enabled");
            }
            device_create_info = device_create_info.push_next(&mut mesh_shader_nv);
        }

        if self.enable_descriptor_buffer {
            device_create_info = device_create_info
                .push_next(&mut buffer_address_features_khr)
                .push_next(&mut buffer_address_features_ext)
                .push_next(&mut descriptor_buffer_features);
        }

        ////////// BUILD DEVICE /////////////////
        let device = unsafe {
            instance
                .create_device(
                    p_device,
                    &device_create_info.enabled_features(&features),
                    None,
                )
                .unwrap()
        };

        #[cfg(debug_assertions)]
        {
            let mut win_extensions = vec![];
            for ext in &device_extension_names_raw {
                unsafe {
                    let str = CStr::from_ptr(ext.clone());
                    win_extensions.push(str.to_str().unwrap());
                }
            }
            println!(
                "Device extensions are {:?} \n==========================\n",
                win_extensions
            );
        }

        self.logical_device = Some(device);
    }

    /// returns the logical device for this instance.
    pub fn get_logical_device(&self) -> &Device {
        self.logical_device.as_ref().unwrap()
    }

    ///////// COMMAND BUFFERS /////////////

    /// starts the specified command buffer
    pub fn begin_command_buffer(&self, command_buffer: &vk::CommandBuffer) {
        let device = self.logical_device.as_ref().unwrap();

        unsafe {
            let cb_info = vk::CommandBufferBeginInfo::default()
                .flags(vk::CommandBufferUsageFlags::RENDER_PASS_CONTINUE);
            device
                .reset_command_buffer(
                    *command_buffer,
                    vk::CommandBufferResetFlags::RELEASE_RESOURCES,
                )
                .unwrap();

            device
                .begin_command_buffer(*command_buffer, &cb_info)
                .unwrap();
        }
    }

    pub fn begin_command_buffer_with_flags(
        &self,
        command_buffer: &CommandBuffer,
        flags: CommandBufferUsageFlags,
    ) {
        let device = self.logical_device.as_ref().unwrap();

        unsafe {
            let cb_info = vk::CommandBufferBeginInfo::default().flags(flags);
            device
                .reset_command_buffer(
                    *command_buffer,
                    vk::CommandBufferResetFlags::RELEASE_RESOURCES,
                )
                .unwrap();

            device
                .begin_command_buffer(*command_buffer, &cb_info)
                .unwrap();
        }
    }

    pub fn end_command_buffer(&self, command_buffer: &vk::CommandBuffer) {
        let device = self.logical_device.as_ref().unwrap();

        unsafe {
            device.end_command_buffer(*command_buffer).unwrap();
            //  device.reset_command_buffer(*command_buffer,vk::CommandBufferResetFlags::RELEASE_RESOURCES).unwrap();
        }
    }

    /// calls reset on a command buffer
    pub fn reset_command_buffer(&self, cb: &vk::CommandBuffer) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device
                .reset_command_buffer(*cb, vk::CommandBufferResetFlags::empty())
                .unwrap()
        }
    }

    /// starts the specified command buffer. Can pass in a command buffer begin info.
    pub fn begin_command_buffer_with_info(
        &self,
        command_buffer: &vk::CommandBuffer,
        cb_info: &vk::CommandBufferBeginInfo,
    ) {
        let device = self.logical_device.as_ref().unwrap();

        unsafe {
            device
                .begin_command_buffer(*command_buffer, cb_info)
                .expect("Unable to start command buffer. Check begin_command_buffer_with_info");
        }
    }

    ///////// MEMORY /////////

    /// gets the memory requirements for a image object.
    pub fn get_memory_requirements(&self, image: vk::Image) -> vk::MemoryRequirements {
        let device = &self.logical_device.as_ref().unwrap();
        unsafe { device.get_image_memory_requirements(image) }
    }

    /// get memory requirements for a buffer
    pub fn get_buffer_memory_requirements(&self, buffer: vk::Buffer) -> vk::MemoryRequirements {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.get_buffer_memory_requirements(buffer) }
    }

    /// Returns the memory properties of the physical device.
    pub fn get_memory_properties(&self) -> PhysicalDeviceMemoryProperties {
        let pdevice = self.physical_device;
        unsafe { self.instance.get_physical_device_memory_properties(pdevice) }
    }

    /// allocates memory based on the info provided.
    pub fn allocate_memory(&self, alloc_info: &MemoryAllocateInfo) -> vk::DeviceMemory {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.allocate_memory(alloc_info, None).unwrap() }
    }

    /// binds device memory
    pub fn bind_memory(&self, buffer: &vk::Buffer, memory: &vk::DeviceMemory, offset: u64) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device
                .bind_buffer_memory(*buffer, *memory, offset)
                .expect("Unable to bind buffer memory.");
        }
    }

    pub fn map_memory(
        &self,
        mem: &DeviceMemory,
        offset: DeviceSize,
        size: DeviceSize,
        flags: MemoryMapFlags,
    ) -> *mut c_void {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.map_memory(*mem, offset, size, flags).unwrap() }
    }

    pub fn unmap_memory(&self, mem: &DeviceMemory) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.unmap_memory(*mem);
        }
    }

    /// binds image memory at the specified offset.
    pub fn bind_image_memory(
        &self,
        image: vk::Image,
        memory: vk::DeviceMemory,
        offset: vk::DeviceSize,
    ) {
        let device = &self.logical_device.as_ref().unwrap();
        unsafe {
            device
                .bind_image_memory(image, memory, offset)
                .expect("unable to bind image memory");
        }
    }
    ///////// BUFFERS ///////////////////////

    pub fn bind_index_buffer(
        &self,
        cb: CommandBuffer,
        vb: &vk::Buffer,
        offset: u64,
        index_type: vk::IndexType,
    ) {
        let device = &self.logical_device.as_ref().unwrap();
        unsafe {
            device.cmd_bind_index_buffer(cb, *vb, offset, index_type);
        }
    }

    pub fn bind_vertex_buffer(
        &self,
        cb: CommandBuffer,
        first_binding: u32,
        vb: &vk::Buffer,
        offsets: vk::DeviceSize,
    ) {
        let device = &self.logical_device.as_ref().unwrap();
        unsafe {
            device.cmd_bind_vertex_buffers(cb, first_binding, &[*vb], &[offsets]);
        }
    }

    ///////// SWAPCHAIN / IMAGING /////////////

    /// Returns possible surface formats for the current physical device.
    pub fn get_surface_formats(&self, surface: &SurfaceKHR) -> Vec<SurfaceFormatKHR> {
        let pdevice = self.physical_device;

        unsafe {
            self.surface_loader
                .get_physical_device_surface_formats(pdevice, *surface)
                .unwrap()
        }
    }

    /// creates an image view using the logical device.
    pub fn generate_image_view(&self, info: vk::ImageViewCreateInfo) -> VkResult<ImageView> {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.create_image_view(&info, None) }
    }

    /// creates an image object using the logical device.
    pub fn generate_image(&self, info: vk::ImageCreateInfo) -> Image {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.create_image(&info, None).unwrap() }
    }

    /// generates a new swapchain loader.
    pub fn generate_swapchain_loader(&self) -> Swapchain {
        let device = self.logical_device.as_ref().unwrap();
        let instance = &self.instance;

        Swapchain::new(instance, device)
    }

    /// generates a graphics pipeline based on the shader info utilized.
    pub fn generate_graphics_pipeline(
        &self,
        info: vk::GraphicsPipelineCreateInfo,
        pc: vk::PipelineCache,
    ) -> Vec<Pipeline> {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.create_graphics_pipelines(pc, &[info], None).unwrap() }
    }

    /// creates a vk::Buffer object based on the info provided.
    pub fn generate_buffer(&self, buffer_info: &vk::BufferCreateInfo) -> vk::Buffer {
        let device = self.logical_device.as_ref().unwrap();

        unsafe { device.create_buffer(buffer_info, None).unwrap() }
    }

    /// generate a pipeline layout
    pub fn generate_pipeline_layout(
        &self,
        info: vk::PipelineLayoutCreateInfo,
    ) -> vk::PipelineLayout {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.create_pipeline_layout(&info, None).unwrap() }
    }

    ///////// COMPUTE ///////

    /// generates a compute pipeline based on the shader info utilized.
    pub fn generate_compute_pipeline(
        &self,
        info: vk::ComputePipelineCreateInfo,
        pc: vk::PipelineCache,
    ) -> Vec<Pipeline> {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.create_compute_pipelines(pc, &[info], None).unwrap() }
    }

    ///////// BARRIERS /////////////

    /// Sets up a pipeline barrier with a fence - Meant to be run after a command buffer has been bound.
    pub fn pipeline_barrier(
        &self,
        command_buffer: &vk::CommandBuffer,
        src_stage_mask: vk::PipelineStageFlags,
        dst_stage_mask: vk::PipelineStageFlags,
        dependency_flags: vk::DependencyFlags,
        memory_barriers: &[vk::MemoryBarrier],
        buffer_memory_barriers: &[vk::BufferMemoryBarrier],
        image_memory_barriers: &[vk::ImageMemoryBarrier],
    ) {
        let device = self.logical_device.as_ref().unwrap();

        unsafe {
            device.cmd_pipeline_barrier(
                *command_buffer,
                src_stage_mask,
                dst_stage_mask,
                dependency_flags,
                memory_barriers,
                buffer_memory_barriers,
                image_memory_barriers,
            );
        }
    }

    ////////// DESTRUCTORS ////////////

    /// Destroys a Framebuffer object.
    pub fn destroy_framebuffer(&self, framebuffer: vk::Framebuffer) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.destroy_framebuffer(framebuffer, None);
        }
    }

    /// Destroys a Renderpass object.
    pub fn destroy_renderpass(&self, rp: vk::RenderPass) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.destroy_render_pass(rp, None);
        }
    }

    pub fn destroy_texture(&self, img: vk::Image, view: vk::ImageView, memory: vk::DeviceMemory) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.destroy_image(img, None);
            device.destroy_image_view(view, None);
            device.free_memory(memory, None);
        }
    }

    pub fn destroy_vk_texture(&self, tex: &VkTexture) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.destroy_image(*tex.get_image(), None);
            device.destroy_image_view(*tex.get_image_view(), None);
            device.free_memory(*tex.get_memory(), None);
        }
    }

    /// destroys a shader module.
    pub fn destroy_shader(&self, shader: vk::ShaderModule) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.destroy_shader_module(shader, None);
        }
    }

    /// destroys a buffer object
    pub fn destroy_buffer(&self, buffer: vk::Buffer) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.destroy_buffer(buffer, None) }
    }

    pub fn destroy_image(&self, img: vk::Image) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.destroy_image(img, None);
        }
    }

    pub fn destroy_image_view(&self, img_view: vk::ImageView) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.destroy_image_view(img_view, None);
        }
    }

    /// Destroys a semaphore object.
    pub fn destroy_semaphore(&self, semaphore: vk::Semaphore) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.destroy_semaphore(semaphore, None);
        }
    }

    /// Destroys a command buffer pool
    pub fn destroy_command_pool(&self, pool: vk::CommandPool) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.destroy_command_pool(pool, None);
        }
    }

    /// destroys a pipeline
    pub fn destroy_pipeline(&self, pipeline: vk::Pipeline) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.destroy_pipeline(pipeline, None);
        }
    }

    /// destroys the specified pipeline layout.
    pub fn destroy_pipeline_layout(&self, pipeline: vk::PipelineLayout) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.destroy_pipeline_layout(pipeline, None);
        }
    }

    /// destroys the specified descriptor set layout
    pub fn destroy_descriptorset_layout(&self, ds_layout: vk::DescriptorSetLayout) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.destroy_descriptor_set_layout(ds_layout, None) }
    }

    ///////// UTILS /////////////
    /// Constructs a command pool based on the pool create info passed in.
    pub fn generate_command_pool(
        &self,
        command_pool_info: vk::CommandPoolCreateInfo,
    ) -> CommandPool {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device
                .create_command_pool(&command_pool_info, None)
                .unwrap()
        }
    }

    /// generates command buffers based on the allocation info.
    pub fn generate_commmand_buffers(
        &self,
        command_buffer_info: vk::CommandBufferAllocateInfo,
    ) -> Vec<vk::CommandBuffer> {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device
                .allocate_command_buffers(&command_buffer_info)
                .unwrap()
        }
    }

    /// Generates a fence based on the allocation info specified.
    pub fn generate_fence(&self, fence_info: vk::FenceCreateInfo) -> Fence {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.create_fence(&fence_info, None).unwrap() }
    }

    /// generates a semaphore
    pub fn generate_semaphore(&self, semaphore_info: vk::SemaphoreCreateInfo) -> vk::Semaphore {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.create_semaphore(&semaphore_info, None).unwrap() }
    }

    /// Returns the size of a [vk::DescriptorSetLayout] object.
    pub fn generate_descriptor_layout_size(&self, layout: &DescriptorSetLayout) -> vk::DeviceSize {
        let descriptor = self.descriptor_buffer_ext.as_ref().unwrap();
        unsafe { descriptor.get_descriptor_set_layout_size(*layout) }
    }

    /// Binds a descriptor set.
    pub fn bind_descriptor_set(
        &self,
        cb: CommandBuffer,
        bind_point: PipelineBindPoint,
        pipeline_layout: PipelineLayout,
        first_set: u32,
        descriptor_sets: &[vk::DescriptorSet],
        dynamic_offsets: &[u32],
    ) {
        let device = &self.logical_device.as_ref().unwrap();
        unsafe {
            device.cmd_bind_descriptor_sets(
                cb,
                bind_point,
                pipeline_layout,
                first_set,
                descriptor_sets,
                dynamic_offsets,
            );
        }
    }

    /// updates a descriptor set.
    pub fn update_descriptor_sets(&self, descriptor_writes: Vec<vk::WriteDescriptorSet>) {
        let device = self.logical_device.as_ref().unwrap();

        unsafe {
            device.update_descriptor_sets(&descriptor_writes[..], &[]);
        }
    }

    /// generates a framebuffer
    pub fn generate_framebuffer(
        &self,
        framebuffer_info: vk::FramebufferCreateInfo,
    ) -> vk::Framebuffer {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.create_framebuffer(&framebuffer_info, None).unwrap() }
    }

    /// generates a sampler based on the provided info
    pub fn generate_sampler(&self, sampler_info: vk::SamplerCreateInfo) -> VkResult<Sampler> {
        let device = self.logical_device.as_ref().unwrap();

        unsafe { device.create_sampler(&sampler_info, None) }
    }

    /// allocates a descriptor set
    pub fn generate_descriptor(
        &self,
        info: vk::DescriptorSetAllocateInfo,
    ) -> Vec<vk::DescriptorSet> {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.allocate_descriptor_sets(&info).unwrap() }
    }

    /// creates a descriptor pool.
    pub fn generate_descriptor_pool(
        &self,
        pool_info: vk::DescriptorPoolCreateInfo,
        allocation: Option<&AllocationCallbacks>,
    ) -> vk::DescriptorPool {
        let device = self.logical_device.as_ref().unwrap();

        unsafe {
            device
                .create_descriptor_pool(&pool_info, allocation)
                .unwrap()
        }
    }

    pub fn generate_descriptor_layout(
        &self,
        create_info: vk::DescriptorSetLayoutCreateInfo,
        allocation: Option<&AllocationCallbacks>,
    ) -> VkResult<DescriptorSetLayout> {
        let device = self.logical_device.as_ref().unwrap();

        unsafe { device.create_descriptor_set_layout(&create_info, allocation) }
    }

    /// generates a shader module based on the shader info utilized.
    pub fn generate_shader_module(&self, shader_info: vk::ShaderModuleCreateInfo) -> ShaderModule {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.create_shader_module(&shader_info, None).unwrap() }
    }

    /// generates a renderpass based on the renderpass info used.
    pub fn generate_renderpass(
        &self,
        renderpass_info: vk::RenderPassCreateInfo,
    ) -> VkResult<RenderPass> {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.create_render_pass(&renderpass_info, None) }
    }

    ///////////////////////////////////////////////////////////

    /// creates a fence.
    pub fn create_fence(&self) -> Fence {
        let device = self.logical_device.as_ref().unwrap();

        unsafe {
            device
                .create_fence(
                    &vk::FenceCreateInfo {
                        ..Default::default()
                    },
                    None,
                )
                .unwrap()
        }
    }

    /// creates a fence but allows you to pass your own CreateInfo object.
    pub fn create_fence_with_info(&self, info: &FenceCreateInfo) -> Fence {
        let device = self.logical_device.as_ref().unwrap();

        unsafe { device.create_fence(info, None).unwrap() }
    }

    /// Sets up a pipeline barrier with a fence - meant to be run on it's own so will bind/unbind/submit
    /// everything.
    pub fn onetime_pipeline_barrier(
        &self,
        command_buffer: vk::CommandBuffer,
        src_stage_mask: vk::PipelineStageFlags,
        dst_stage_mask: vk::PipelineStageFlags,
        dependency_flags: vk::DependencyFlags,
        memory_barriers: &[vk::MemoryBarrier],
        buffer_memory_barriers: &[vk::BufferMemoryBarrier],
        image_memory_barriers: &[vk::ImageMemoryBarrier],
    ) {
        let device = self.logical_device.as_ref().unwrap();

        unsafe {
            let fence = device
                .create_fence(
                    &vk::FenceCreateInfo {
                        ..Default::default()
                    },
                    None,
                )
                .unwrap();

            device.begin_command_buffer(command_buffer, &vk::CommandBufferBeginInfo::default()).expect("Unable to begin command buffer for pipeline barrier. Check call to onetime_pipeline_barrier");
            device.cmd_pipeline_barrier(
                command_buffer,
                src_stage_mask,
                dst_stage_mask,
                dependency_flags,
                memory_barriers,
                buffer_memory_barriers,
                image_memory_barriers,
            );
            device.end_command_buffer(command_buffer).expect(
                "Unable to end command buffer for pipeline barrier. check onetime_pipeline_barrier",
            );

            // submit the command into the queue
            let command_buffers = [command_buffer];
            let cb_submit = vk::SubmitInfo::default().command_buffers(&command_buffers);

            self.submit_graphics(cb_submit, fence);
            device
                .wait_for_fences(&[fence], true, 100000)
                .expect("Unable to wait for fences. check call to onetime_pipeline_barrier.");
        }
    }

    /// starts dynamic rendering
    pub fn begin_dynamic_render(&self, cb: &vk::CommandBuffer, render_info: &vk::RenderingInfoKHR) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.cmd_begin_rendering(*cb, render_info) };
    }

    /// ends dynamic rendering
    pub fn end_dynamic_render(&self, cb: &vk::CommandBuffer) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.cmd_end_rendering(*cb) };
    }

    /// sets the viewport for the command buffer.
    pub fn set_viewport(&self, cb: CommandBuffer, firstvp: u32, viewports: &[Viewport]) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.cmd_set_viewport(cb, firstvp, viewports) };
    }

    pub fn clear_attachment(
        &self,
        cb: CommandBuffer,
        attachment: ClearAttachment,
        rect: ClearRect,
    ) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.cmd_clear_attachments(cb, &[attachment], &[rect]) };
    }

    /// Binds a pipeline to the specified command buffer and pipeline bind point.
    pub fn bind_pipeline(&self, cb: CommandBuffer, bp: PipelineBindPoint, p: Pipeline) {
        let device = &self.logical_device.as_ref().unwrap();
        unsafe { device.cmd_bind_pipeline(cb, bp, p) };
    }

    pub fn get_physical_device(&self) -> &vk::PhysicalDevice {
        &self.physical_device
    }

    /// triggers wait/idle command on present queue
    pub fn present_wait_idle(&self) {
        let device = self.logical_device.as_ref().unwrap();
        let present = self.present_queue.unwrap();
        unsafe {
            device
                .queue_wait_idle(present)
                .expect("Unable to wait idle for present queue. ")
        };
    }

    /// triggers wait/idle command on compute queue
    pub fn compute_wait_idle(&self) {
        let device = self.logical_device.as_ref().unwrap();
        let present = self.compute_queue.unwrap();

        unsafe {
            device
                .queue_wait_idle(present)
                .expect("Unable to wait for compute queue idle")
        };
    }

    /// Resets the specified fences.
    pub fn reset_fences(&self, fences: &[vk::Fence]) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.reset_fences(fences).unwrap();
        }
    }

    /// Waits for the specified fences to clear
    pub fn wait_for_fences(&self, fences: &[vk::Fence]) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.wait_for_fences(fences, true, u64::MAX).unwrap();
        }
    }

    /// waits for the device idle state.
    pub fn wait_idle(&self) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.device_wait_idle().unwrap();
        }
    }

    /// Returns the presentation queue.
    pub fn get_present_queue(&self) -> &vk::Queue {
        self.present_queue.as_ref().unwrap()
    }

    pub fn get_compute_queue_index(&self) -> usize {
        self.compute_queue_index
    }

    /// returns instance associated with current vulkan instance.
    pub fn get_instance(&self) -> &Instance {
        &self.instance
    }

    /// builds debug functionality
    pub fn build_debug(&mut self) {
        unsafe {
            self.debug_loader = Some(DebugUtils::new(&self.entry, &self.instance));
            //   self.debugger = Some(enable_validation(&self.debug_loader.as_ref().unwrap(), &self.entry, &self.instance));
            self.debugger = Some(enable_validation(&self.debug_loader.as_ref().unwrap()));
        }
    }

    /// set the scissor value for a command buffer.
    pub fn set_scissor(&self, cb: CommandBuffer, firstscissor: u32, scissors: &[Rect2D]) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.cmd_set_scissor(cb, firstscissor, scissors) };
    }

    /// Submits information onto the graphics queue.
    pub fn submit_graphics(&self, submit_info: vk::SubmitInfo, fence: vk::Fence) {
        let device = self.logical_device.as_ref().unwrap();
        let graphics_queue = self.graphics_queue.unwrap();
        unsafe {
            device
                .queue_submit(graphics_queue, &[submit_info], fence)
                .unwrap();
            device.queue_wait_idle(graphics_queue).unwrap();
        }
    }

    /// Turns on/off depth writing for command buffers
    pub fn set_command_buffer_depth_write(&self, cb: &CommandBuffer, enable: bool) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.cmd_set_depth_write_enable(*cb, enable) }
    }

    /// Turns on/off depth testing for command buffers
    pub fn set_command_buffer_depth_test(&self, cb: &CommandBuffer, enable: bool) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.cmd_set_depth_test_enable(*cb, enable) }
    }

    /// Complementary function to [Vulkan::submit_graphics], meant to submit a single command buffer
    /// on the graphics queue. Will automatically construct submit info.
    pub fn submit_command_buffer(&self, cb: &CommandBuffer) {
        let buffers = &[*cb];
        let submit_info = vk::SubmitInfo::default().command_buffers(buffers);
        self.submit_graphics(submit_info, Fence::null());
    }

    /// Same as [Vulkan::submit_command_buffer] but submits on the compute queue instead.
    pub fn submit_command_buffer_on_compute(&self, cb: &CommandBuffer) {
        let buffers = &[*cb];
        let submit_info = vk::SubmitInfo::default().command_buffers(buffers);
        self.submit_compute(submit_info, Fence::null());
    }

    /// submits a command on the compute queue.
    pub fn submit_compute(&self, submit_info: vk::SubmitInfo, fence: vk::Fence) {
        let device = self.logical_device.as_ref().unwrap();
        let compute_queue = self.compute_queue.unwrap();

        unsafe {
            device
                .queue_submit(compute_queue, &[submit_info], fence)
                .unwrap();
            device.queue_wait_idle(compute_queue).unwrap();
        };
    }

    /// Binds descriptor buffer using the specified slice of [vk::DescriptorBufferBindingInfoEXT] objects.
    pub fn bind_descriptor_buffers(
        &self,
        cb: &vk::CommandBuffer,
        binding_info: &[vk::DescriptorBufferBindingInfoEXT],
    ) {
        let ext = self.descriptor_buffer_ext.as_ref().unwrap();
        unsafe {
            ext.cmd_bind_descriptor_buffers(*cb, binding_info);
        }
    }

    /// sets the offset for the currently bound descriptor buffer.
    pub fn set_descriptor_buffer_offset(
        &self,
        cb: CommandBuffer,
        bind_point: PipelineBindPoint,
        pipeline_layout: PipelineLayout,
        first_set: u32,
        buffer_indices: &[u32],
        offsets: &[vk::DeviceSize],
    ) {
        let ext = self.descriptor_buffer_ext.as_ref().unwrap();

        unsafe {
            ext.cmd_set_descriptor_buffer_offsets(
                cb,
                bind_point,
                pipeline_layout,
                first_set,
                buffer_indices,
                offsets,
            )
        }
    }

    pub fn draw(
        &self,
        cb: CommandBuffer,
        vertex_count: u32,
        instance_count: u32,
        first_vertex: u32,
        first_instance: u32,
    ) {
        let device = &self.logical_device.as_ref().unwrap();
        unsafe {
            device.cmd_draw(
                cb,
                vertex_count,
                instance_count,
                first_vertex,
                first_instance,
            );
        }
    }

    /// draws indexed
    pub fn draw_indexed(
        &self,
        cb: CommandBuffer,
        index_count: u32,
        instance_count: u32,
        first_index: u32,
        vertex_offset: i32,
        first_instance: u32,
    ) {
        let device = &self.logical_device.as_ref().unwrap();
        unsafe {
            device.cmd_draw_indexed(
                cb,
                index_count,
                instance_count,
                first_index,
                vertex_offset,
                first_instance,
            )
        }
    }

    /// Gets a descriptor object in order to place in a buffer.
    pub fn get_descriptor_ext(
        &self,
        descriptor_info: &vk::DescriptorGetInfoEXT,
        descriptor: &mut [u8],
    ) {
        if !self.enable_descriptor_buffer {
            println!(
                "Call to get_descriptor_ext - descriptor buffer extension has not been enabled"
            );
            return;
        }

        let descriptor_buffer = self.descriptor_buffer_ext.as_ref().unwrap();
        unsafe {
            descriptor_buffer.get_descriptor(descriptor_info, descriptor);
        }
    }

    /// Returns the binding offset value for the specified [vk::DescriptorSetLayout] and desired binding value.
    pub fn get_descriptor_layout_binding_offset(
        &self,
        layout: &DescriptorSetLayout,
        binding: u32,
    ) -> vk::DeviceSize {
        let descriptor = self.descriptor_buffer_ext.as_ref().unwrap();
        unsafe { descriptor.get_descriptor_set_layout_binding_offset(*layout, binding) }
    }

    /// returns the device address for the buffer
    pub fn get_buffer_device_address(&self, buffer: &vk::Buffer) -> vk::DeviceAddress {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device
                .get_buffer_device_address(&vk::BufferDeviceAddressInfo::default().buffer(*buffer))
        }
    }

    /// Generates a vk::QueryPool
    pub fn generate_query_pool(&self, pool_info: QueryPoolCreateInfo) -> VkResult<QueryPool> {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.create_query_pool(&pool_info, None) }
    }

    /// returns device properties of physical device from PhysicalDeviceProperties2
    pub fn get_physical_device_properties2(&self, props: &mut vk::PhysicalDeviceProperties2) {
        let pdevice = self.physical_device;
        let instance = &self.instance;

        unsafe { instance.get_physical_device_properties2(pdevice, props) }
    }

    /// starts a renderpass with inline subpass contents.
    pub fn start_renderpass(&self, buff: &vk::CommandBuffer, begin_info: &vk::RenderPassBeginInfo) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.cmd_begin_render_pass(*buff, begin_info, vk::SubpassContents::INLINE) }
    }

    /// ends the currently bound renderpass
    pub fn end_renderpass(&self, buff: &vk::CommandBuffer) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.cmd_end_render_pass(*buff);
        }
    }

    /// Fills a region of a buffer with a fixed value.
    pub fn fill_buffer(
        &self,
        cb: &vk::CommandBuffer,
        buff: &Buffer,
        offset: u64,
        size: u64,
        data: u32,
    ) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.cmd_fill_buffer(*cb, *buff, offset, size, data) }
    }

    /// Resets a vk::QueryPool
    pub fn reset_query_pool(&self, pool: &QueryPool, count: u32) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe { device.reset_query_pool(*pool, 0, count) }
    }

    /// Returns query pool results. Writes to data prop.
    pub fn get_query_pool_results<T>(&self, pool: &QueryPool, data: &mut [T]) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device
                .get_query_pool_results(*pool, 0, data, vk::QueryResultFlags::WAIT)
                .expect("Unable to get query pool results");
        }
    }

    /// Copies one image to another.
    pub fn copy_image(
        &self,
        command_buffer: &CommandBuffer,
        src_image: &Image,
        src_layout: ImageLayout,
        dst_image: &Image,
        dst_layout: ImageLayout,
        regions: &[ImageCopy],
    ) {
        let device = self.logical_device.as_ref().unwrap();
        unsafe {
            device.cmd_copy_image(
                *command_buffer,
                *src_image,
                src_layout,
                *dst_image,
                dst_layout,
                regions,
            );
        }
    }

    /// Copies data from one buffer to another. Pass in the command buffer to use, the src / destination as well as the number
    /// of bytes to copy. Submits using the graphics queue.
    /// Note that this is NOT meant to be run within an existing command buffer recording
    pub fn one_time_copy_buffer(
        &self,
        command_buffer: &CommandBuffer,
        src: &Buffer,
        dst: &Buffer,
        size: DeviceSize,
    ) {
        let device = self.logical_device.as_ref().unwrap();

        let cb_info = CommandBufferBeginInfo {
            flags: CommandBufferUsageFlags::ONE_TIME_SUBMIT,
            ..Default::default()
        };

        let copy_region = BufferCopy {
            size,
            ..Default::default()
        };
        unsafe {
            device
                .begin_command_buffer(*command_buffer, &cb_info)
                .expect("Unable to start command buffer. check copy_buffer_to_buffer");
            device.cmd_copy_buffer(*command_buffer, *src, *dst, &[copy_region]);
            device
                .end_command_buffer(*command_buffer)
                .expect("Unable to end command buffer. check copy_buffer_to_buffer");
            self.submit_command_buffer(command_buffer);
        }
    }

    /// Signals the specified semaphore. Note that semaphore must have been created with the
    /// VK_SEMAPHORE_TYPE_TIMELINE type.
    pub fn signal_semaphore(&self, semaphore: &Semaphore, value: u64) {
        let device = self.logical_device.as_ref().unwrap();

        let mut signal_info = SemaphoreSignalInfo::default();
        signal_info.semaphore = *semaphore;
        signal_info.value = value;

        unsafe {
            device.signal_semaphore(&signal_info).unwrap();
        }
    }

    /// Begin command buffer label
    /// Note that this only works in Debug and not release
    pub fn begin_command_label(&self, cmd: &CommandBuffer, label: &str) {
        let debug_utils = self.debug_loader.as_ref().unwrap();

        let mut txt = DebugUtilsLabelEXT::default();
        //  txt.label_name(String::from(label).as_c_str());

        #[cfg(debug_assertions)]
        {
            unsafe {
                debug_utils.cmd_begin_debug_utils_label(*cmd, &txt);
            }
        }
    }

    /// Ends command buffer label
    /// Note that this only works in Debug and not release
    pub fn end_command_label(&self, cmd: &CommandBuffer) {
        let debug_utils = self.debug_loader.as_ref().unwrap();

        #[cfg(debug_assertions)]
        {
            unsafe {
                debug_utils.cmd_end_debug_utils_label(*cmd);
            }
        }
    }

    /// Begin queue label
    /// Note that this only works in Debug and not release
    pub fn begin_queue_label(&self, queue: &Queue, label: &str) {
        let debug_utils = self.debug_loader.as_ref().unwrap();

        let mut txt = DebugUtilsLabelEXT::default();
        // txt.label_name(CString::from(label).as_c_str());

        #[cfg(debug_assertions)]
        {
            unsafe {
                debug_utils.queue_begin_debug_utils_label(*queue, &txt);
            }
        }
    }

    /// Ends queue label
    /// Note that this only works in Debug and not release
    pub fn end_queue_label(&self, queue: &Queue) {
        let debug_utils = self.debug_loader.as_ref().unwrap();

        #[cfg(debug_assertions)]
        {
            unsafe {
                debug_utils.queue_end_debug_utils_label(*queue);
            }
        }
    }

    pub fn set_push_constants(
        &self,
        cb: &CommandBuffer,
        layout: &PipelineLayout,
        flags: ShaderStageFlags,
        offsets: u32,
        constants: &[u8],
    ) {
        let device = self.logical_device.as_ref().unwrap();

        unsafe {
            device.cmd_push_constants(*cb, *layout, flags, offsets, constants);
        }
    }

    /// Adds an object label
    pub fn add_object_label(&self, obj_name: &DebugUtilsObjectNameInfoEXT) {
        let debug_utils = self.debug_loader.as_ref().unwrap();
        let device = self.logical_device.as_ref().unwrap();
        let handle = device.handle();

        #[cfg(debug_assertions)]
        {
            unsafe {
                debug_utils
                    .set_debug_utils_object_name(handle, obj_name)
                    .expect("Unable to add object label");
            }
        }
    }

    /// Adds an object tag
    pub fn add_object_tag(&self, obj_name: &DebugUtilsObjectTagInfoEXT) {
        let debug_utils = self.debug_loader.as_ref().unwrap();
        let device = self.logical_device.as_ref().unwrap();
        let handle = device.handle();

        #[cfg(debug_assertions)]
        {
            unsafe {
                debug_utils
                    .set_debug_utils_object_tag(handle, obj_name)
                    .expect("Unable to add object tag");
            }
        }
    }

    /// Runs vkCmdNextSubpass
    pub fn next_subpass(&self, cb: &CommandBuffer, contents: SubpassContents) {
        let device = self.logical_device.as_ref().unwrap();

        unsafe { device.cmd_next_subpass(*cb, contents) }
    }

    /// Runs vkCmdNextSubpass2
    pub fn next_subpass2(
        &self,
        cb: &CommandBuffer,
        begin: &SubpassBeginInfo,
        end: &SubpassEndInfo,
    ) {
        let device = self.logical_device.as_ref().unwrap();

        unsafe {
            device.cmd_next_subpass2(*cb, begin, end);
        }
    }

    /// Prints a list of all the device extensions available.
    pub fn print_device_extensions(&self) {
        unsafe {
            let ext = self
                .instance
                .enumerate_device_extension_properties(self.physical_device)
                .unwrap();
            for e in ext {
                let name = CStr::from_ptr(e.extension_name.as_ptr());
                let id = name.to_owned();
                println!("{:?}", id);
            }
        }
    }
}
