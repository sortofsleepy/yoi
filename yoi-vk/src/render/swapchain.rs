use crate::core::Vulkan;
use crate::types::traits::VkObject;
use ash::extensions::khr::Swapchain;
use ash::prelude::VkResult;
use ash::vk;
use ash::vk::{
    ComponentMapping, ComponentSwizzle, CompositeAlphaFlagsKHR, Extent2D, Fence, Format, Image,
    ImageAspectFlags, ImageSubresourceRange, ImageUsageFlags, ImageView, ImageViewCreateInfo,
    ImageViewType, PresentInfoKHR, PresentModeKHR, Queue, Semaphore, SharingMode,
    SurfaceCapabilitiesKHR, SurfaceFormatKHR, SurfaceTransformFlagsKHR, SwapchainCreateInfoKHR,
    SwapchainKHR,
};
use image::ImageFormat;
use std::ffi::CString;

/// A wrapper around the basics of a swapchain
pub struct VkSwapChain {
    swapchain: Option<SwapchainKHR>,
    swapchain_loader: Option<Swapchain>,
    surface_capabilities: Option<SurfaceCapabilitiesKHR>,
    surface_formats: Vec<SurfaceFormatKHR>,
    surface_format: Option<SurfaceFormatKHR>,
    image_views: Vec<ImageView>,
    images: Vec<Image>,
    desired_image_count: u32,
    present_mode: PresentModeKHR,
    extent: Extent2D,
    color_format: Option<Format>,
}

impl VkSwapChain {
    pub fn new() -> Self {
        VkSwapChain {
            swapchain: None,
            swapchain_loader: None,
            images: vec![],
            image_views: vec![],
            surface_formats: vec![],
            color_format: None,
            surface_format: None,
            surface_capabilities: None,
            desired_image_count: 0,
            extent: Extent2D {
                width: 256,
                height: 256,
            },
            // FIFO is guaranteed to work on a majority of setups so just set it as default.
            // It will automatically tell your app to use the max refresh rate your monitor supports.
            // For more info https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkPresentModeKHR.html
            present_mode: PresentModeKHR::FIFO,
        }
    }

    /// initializes the swapchain.
    pub fn init(&mut self, instance: &Vulkan, surface: &vk::SurfaceKHR) {
        let formats = instance.get_surface_formats(surface);
        let capabilities = instance.get_physical_device_capabilities(surface);

        // Get Surface format to use - pick the first one from the list of preferred formats
        let mut selected: Option<SurfaceFormatKHR> = None;
        let preferred_formats: Vec<Format> = vec![
            Format::B8G8R8A8_UNORM,
            Format::R8G8B8A8_UNORM,
            Format::A8B8G8R8_UINT_PACK32,
        ];

        for format in &formats {
            if preferred_formats.contains(&format.format) {
                selected = Some(*format);
                break;
            }
        }

        // generally want +1 image compared to the min image count. If that happens to be
        // greater than max, take the max, otherwise take the additional image.
        let image_count = if (capabilities.min_image_count + 1) > capabilities.max_image_count {
            capabilities.max_image_count
        } else {
            capabilities.min_image_count + 1
        };

        let surface_resolution = match capabilities.current_extent.width {
            std::u32::MAX => Extent2D {
                width: 0,
                height: 0,
            },

            _ => capabilities.current_extent,
        };

        let pre_transform = if capabilities
            .supported_transforms
            .contains(SurfaceTransformFlagsKHR::IDENTITY)
        {
            SurfaceTransformFlagsKHR::IDENTITY
        } else {
            capabilities.current_transform
        };

        // TODO incorporate a user set-able present mode

        let swapchain_loader = instance.generate_swapchain_loader();

        let swapchain_create_info = SwapchainCreateInfoKHR::default()
            .surface(*surface)
            .min_image_count(image_count)
            //.image_color_space(selected.unwrap().color_space)
            .image_color_space(formats[0].color_space)
            .image_extent(surface_resolution)
            .image_usage(
                ImageUsageFlags::COLOR_ATTACHMENT
                    | ImageUsageFlags::TRANSFER_SRC
                    | ImageUsageFlags::INPUT_ATTACHMENT
                    | ImageUsageFlags::TRANSFER_DST,
            )
            .image_sharing_mode(SharingMode::EXCLUSIVE)
            .pre_transform(pre_transform)
            .composite_alpha(CompositeAlphaFlagsKHR::OPAQUE)
            .present_mode(self.present_mode)
            .clipped(true)
            //.image_format(selected.unwrap().format)
            .image_format(formats[0].format)
            .image_array_layers(1);

        let swapchain = unsafe {
            swapchain_loader
                .create_swapchain(&swapchain_create_info, None)
                .unwrap()
        };

        let images = unsafe { swapchain_loader.get_swapchain_images(swapchain).unwrap() };

        // build image views
        let component_mapping = ComponentMapping {
            r: ComponentSwizzle::R,
            g: ComponentSwizzle::G,
            b: ComponentSwizzle::B,
            a: ComponentSwizzle::A,
        };

        let subresource_range = ImageSubresourceRange {
            aspect_mask: ImageAspectFlags::COLOR,
            base_mip_level: 0,
            level_count: 1,
            base_array_layer: 0,
            layer_count: 1,
        };

        // build image views
        for img in images.iter() {
            let imageview_create_info = ImageViewCreateInfo {
                image: *img,
                view_type: ImageViewType::TYPE_2D,
                format: formats[0].format,

                components: component_mapping,
                subresource_range,
                ..Default::default()
            };

            let imageview = instance.generate_image_view(imageview_create_info);

            if imageview.is_ok() {
                self.image_views.push(imageview.unwrap());
            } else if imageview.is_err() {
                panic!("{}", imageview.unwrap_err());
            }
        }

        self.color_format = Some(selected.unwrap().format);
        self.extent = surface_resolution;
        self.swapchain_loader = Some(swapchain_loader);
        self.images = images;
        self.swapchain = Some(swapchain);
        self.desired_image_count = image_count;
        self.surface_format = Some(formats[0]);
        self.surface_formats = formats;
        self.surface_capabilities = Some(capabilities);
    }

    /// Returns the color format of the swap chain
    pub fn get_color_format(&self) -> Format {
        self.color_format.unwrap()
    }

    /// Used to re-initialize swapchain - pretty much same as init but clears image and image_views
    /// plus doesn't save the other settings.
    pub fn re_init(&mut self, instance: &Vulkan, surface: &vk::SurfaceKHR) {
        let formats = instance.get_surface_formats(surface);
        let capabilities = instance.get_physical_device_capabilities(surface);

        self.image_views.clear();
        self.images.clear();

        // generally want +1 image compared to the min image count. If that happens to be
        // greater than max, take the max, otherwise take the additional image.
        let image_count = if (capabilities.min_image_count + 1) > capabilities.max_image_count {
            capabilities.max_image_count
        } else {
            capabilities.min_image_count + 1
        };

        let surface_resolution = match capabilities.current_extent.width {
            std::u32::MAX => Extent2D {
                width: 0,
                height: 0,
            },

            _ => capabilities.current_extent,
        };

        let pre_transform = if capabilities
            .supported_transforms
            .contains(SurfaceTransformFlagsKHR::IDENTITY)
        {
            SurfaceTransformFlagsKHR::IDENTITY
        } else {
            capabilities.current_transform
        };

        // TODO incorporate a user set-able present mode

        let swapchain_loader = instance.generate_swapchain_loader();

        let swapchain_create_info = SwapchainCreateInfoKHR::default()
            .surface(*surface)
            .min_image_count(image_count)
            .image_color_space(formats[0].color_space)
            .image_extent(surface_resolution)
            .image_usage(
                ImageUsageFlags::COLOR_ATTACHMENT
                    | ImageUsageFlags::TRANSFER_SRC
                    | ImageUsageFlags::INPUT_ATTACHMENT
                    | ImageUsageFlags::TRANSFER_DST,
            )
            .image_sharing_mode(SharingMode::EXCLUSIVE)
            .pre_transform(pre_transform)
            .composite_alpha(CompositeAlphaFlagsKHR::OPAQUE)
            .present_mode(self.present_mode)
            .clipped(true)
            .image_format(formats[0].format)
            .image_array_layers(1);

        let swapchain = unsafe {
            swapchain_loader
                .create_swapchain(&swapchain_create_info, None)
                .unwrap()
        };
        let images = unsafe { swapchain_loader.get_swapchain_images(swapchain).unwrap() };

        // build image views
        let component_mapping = ComponentMapping {
            r: ComponentSwizzle::R,
            g: ComponentSwizzle::G,
            b: ComponentSwizzle::B,
            a: ComponentSwizzle::A,
        };

        let subresource_range = ImageSubresourceRange {
            aspect_mask: ImageAspectFlags::COLOR,
            base_mip_level: 0,
            level_count: 1,
            base_array_layer: 0,
            layer_count: 1,
        };

        // build image views
        for img in images.iter() {
            let imageview_create_info = ImageViewCreateInfo {
                image: *img,
                view_type: ImageViewType::TYPE_2D,
                format: formats[0].format, // TODO get compatible formats instead of just taking the first one.
                components: component_mapping,
                subresource_range,
                ..Default::default()
            };

            let imageview = instance.generate_image_view(imageview_create_info);

            if imageview.is_ok() {
                self.image_views.push(imageview.unwrap());
            } else if imageview.is_err() {
                panic!("{}", imageview.unwrap_err());
            }
        }
        self.extent = surface_resolution;
        self.images = images;
        self.swapchain = Some(swapchain);
    }

    /// Acquires the next images in the swapchain
    pub fn next_image_khr(
        &self,
        timeout: u64,
        semaphore: &Semaphore,
        fence: &Fence,
    ) -> VkResult<(u32, bool)> {
        let loader = self.swapchain_loader.as_ref().unwrap();
        let swapchain = self.swapchain.as_ref().unwrap();

        unsafe { loader.acquire_next_image(*swapchain, timeout, *semaphore, *fence) }
    }

    /// Submits information onto the presentation queue.
    pub fn submit_present(
        &self,
        submit_info: &PresentInfoKHR,
        present_queue: Queue,
    ) -> VkResult<bool> {
        let swapchain = self.swapchain_loader.as_ref().unwrap();
        unsafe { swapchain.queue_present(present_queue, submit_info) }
    }

    pub fn destroy_swapchain(&mut self, instance: &Vulkan) {
        let swapchain = self.swapchain_loader.as_ref().unwrap();
        let image_views = &self.image_views;
        unsafe {
            for imgv in image_views {
                instance.destroy_image_view(*imgv);
            }

            swapchain.destroy_swapchain(self.swapchain.unwrap(), None);
        }
    }

    /// returns the extent of this swapchain
    pub fn get_extent(&self) -> Extent2D {
        self.extent
    }

    /// returns the number of swapchain images.
    pub fn get_num_images(&self) -> usize {
        self.images.len()
    }

    /// Returns the current image objects
    pub fn get_images(&self) -> &Vec<Image> {
        &self.images
    }

    /// Returns the current image views for the swapchain
    pub fn get_image_views(&self) -> &Vec<ImageView> {
        &self.image_views
    }

    /// returns the swapchain image at the specified index. Note that
    /// there are no checks to ensure that the index exists.
    pub fn get_image_view_at(&self, idx: usize) -> &ImageView {
        &self.image_views[idx]
    }

    /// Returns the image at the specified index.
    pub fn get_image_at(&self, idx: usize) -> &Image {
        &self.images[idx]
    }
}

impl VkObject for VkSwapChain {
    type Raw = SwapchainKHR;

    fn raw(&self) -> &SwapchainKHR {
        &self.swapchain.as_ref().unwrap()
    }

    fn add_label(&self, ctx: &Vulkan, name: String) {
        #[cfg(debug_assertions)]
        {
            let raw = self.swapchain.unwrap();
            let name_info = vk::DebugUtilsObjectNameInfoEXT::default();
            name_info.object_handle(raw);
            let msg = format!("unable to convert {} to c_string for label", name);
            name_info.object_name(CString::new(name).expect(msg.as_str()).as_c_str());

            ctx.add_object_label(&name_info);
        }
    }
}
