use crate::framebuffer::create_framebuffer;
use crate::render::renderpass2::{RenderPass2, SubpassDescriptor, SubpassReference};
use crate::{FramebufferFormat, TextureFormat, VkObject, VkTexture, Vulkan};
use ash::vk;
use ash::vk::{
    AccessFlags, AttachmentDescription, AttachmentLoadOp, AttachmentReference, AttachmentStoreOp,
    ClearColorValue, ClearRect, ClearValue, CommandBuffer, DependencyFlags, Extent2D, Format,
    FramebufferCreateInfo, ImageAspectFlags, ImageLayout, ImageTiling, ImageUsageFlags,
    MemoryPropertyFlags, Offset2D, PipelineBindPoint, PipelineStageFlags, Rect2D, SampleCountFlags,
    SubpassContents, SubpassDependency, SUBPASS_EXTERNAL,
};

/// A wrapper around a [Renderpass] and a [Framebuffer]
/// Note that for simplicity, this generally assumes just one attachment per render target.
pub struct RenderTarget2 {
    viewport: vk::Viewport,
    attachments: Vec<VkTexture>,
    render_pass: RenderPass2,
    framebuffer: Option<vk::Framebuffer>,
    clear_color: [f32; 4],
    clear_values: Vec<vk::ClearValue>,
    depth_stencil_value: vk::ClearDepthStencilValue,
}

impl RenderTarget2 {
    pub fn new(width: u32, height: u32) -> Self {
        let rt = RenderTarget2 {
            viewport: vk::Viewport {
                x: 0.0,
                y: 0.0,
                width: width as f32,
                height: height as f32,
                ..Default::default()
            },
            attachments: vec![],
            render_pass: RenderPass2::create(width, height),
            framebuffer: None,
            clear_color: [0.0, 0.0, 0.0, 1.0],
            clear_values: vec![],
            depth_stencil_value: vk::ClearDepthStencilValue {
                depth: 1.0,
                stencil: 0,
            },
        };

        rt
    }

    /// Calls destructors on objects
    pub fn destroy(&self, instance: &Vulkan) {
        instance.destroy_renderpass(*self.render_pass.raw());
        instance.destroy_framebuffer(self.framebuffer.unwrap());
    }

    /// Updates a attachment descriptor to allow for additive writing onto the attachment
    ///
    /// * `vk` - the [Vulkan] instance
    /// * `attachment_index` - the index of the attachment to alter
    /// * `cb` - a command buffer to use for the image layout transition.
    pub fn should_clear_image(&mut self, attachment_index: usize) {
        // alter the attachment description of the specified attachment
        let mut desc = self
            .render_pass
            .get_attachment_description(attachment_index);
        desc.initial_layout = ImageLayout::GENERAL;
        desc.final_layout = ImageLayout::GENERAL;
        desc.load_op = AttachmentLoadOp::LOAD;
    }

    pub fn compile(&mut self, vk: &Vulkan) {
        let mut views = vec![];
        for attachment in self.attachments.iter() {
            views.push(*attachment.get_image_view());
        }
        self.render_pass.compile(vk);

        let fbo = create_framebuffer(
            vk,
            FramebufferFormat {
                width: self.viewport.width as u32,
                height: self.viewport.height as u32,
                layers: 1,
                attachments: views,
            },
            self.render_pass.raw(),
        );

        self.framebuffer = Some(fbo)
    }

    pub fn get_color_view(&self) -> &vk::ImageView {
        self.attachments[0].get_image_view()
    }
    pub fn get_color_view_at(&self, idx: usize) -> &vk::ImageView {
        self.attachments[idx].get_image_view()
    }

    pub fn begin(&self, vk: &Vulkan, cb: &CommandBuffer) {
        self.render_pass
            .begin(vk, cb, self.framebuffer.as_ref().unwrap());
    }

    /// Updates the viewport. Note that unlike [RenderTarget2::set_viewport], this is meant to be used
    /// outside of a render loop.
    ///
    /// Note that this also updates the scissor extent.
    pub fn update_viewport(&mut self, x: f32, y: f32, w: f32, h: f32) {
        self.render_pass.viewport(x, y, w, h);
    }

    /// sets the viewport during rendering.
    pub fn set_viewport(&self, vk: &Vulkan, cb: &CommandBuffer) {
        vk.set_viewport(
            *cb,
            0,
            &[vk::Viewport {
                x: 0.0,
                y: 0.0,
                width: self.viewport.width,
                height: self.viewport.height,
                min_depth: 0.0,
                max_depth: 1.0,
            }],
        );
    }

    pub fn end(&self, vk: &Vulkan, cb: &CommandBuffer) {
        vk.end_renderpass(cb);
    }

    pub fn get_renderpass(&self) -> &RenderPass2 {
        &self.render_pass
    }

    pub fn get_attachment(&self) -> VkTexture {
        self.attachments[0]
    }

    pub fn get_attachment_at(&self, idx: usize) -> VkTexture {
        self.attachments[idx]
    }

    /// Clears regions within currently bound framebuffer attachments.
    pub fn clear_attachments(&self, vk: &Vulkan, cb: &CommandBuffer, color: [f32; 4]) {
        vk.clear_attachment(
            *cb,
            vk::ClearAttachment {
                aspect_mask: ImageAspectFlags::COLOR,
                color_attachment: 0,
                clear_value: ClearValue {
                    color: ClearColorValue { float32: color },
                },
            },
            ClearRect {
                rect: Rect2D {
                    offset: Offset2D { x: 0, y: 0 },
                    extent: Extent2D {
                        width: self.viewport.width as u32,
                        height: self.viewport.height as u32,
                    },
                },
                base_array_layer: 0,
                layer_count: 1,
            },
        );
    }

    /// adds an attachment to the Render target
    pub fn add_attachment(&mut self, vk: &Vulkan, texture: &VkTexture) {
        self.attachments.push(*texture);

        self.render_pass
            .add_attachment_description(AttachmentDescription {
                format: texture.get_format(),
                samples: SampleCountFlags::TYPE_1,
                load_op: AttachmentLoadOp::CLEAR,
                store_op: AttachmentStoreOp::STORE,
                stencil_load_op: AttachmentLoadOp::DONT_CARE,
                stencil_store_op: AttachmentStoreOp::DONT_CARE,
                initial_layout: ImageLayout::UNDEFINED,
                final_layout: ImageLayout::PRESENT_SRC_KHR,
                ..Default::default()
            });

        self.add_default_attachment_reference();
        self.add_default_clear_value();
    }

    pub fn add_attachment_description(&mut self, description: AttachmentDescription) {
        self.render_pass.add_attachment_description(description);
    }

    pub fn add_subpass_dependency(&mut self, subpass_dep: SubpassDependency) {
        self.render_pass.add_subpass_dependency(subpass_dep)
    }

    pub fn add_subpass_descriptor(&mut self, desc: SubpassDescriptor) {
        self.render_pass.add_subpass_descriptor(desc);
    }

    pub fn add_default_attachment_reference(&mut self) {
        self.render_pass.add_subpass_references(SubpassReference {
            subpass_index: 0,
            refs: vec![vk::AttachmentReference {
                attachment: 0,
                layout: ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
            }],
            depth_ref: None,
        })
    }

    pub fn add_attachment_reference(&mut self, subpass_index: usize, _ref: AttachmentReference) {
        self.render_pass.add_subpass_references(SubpassReference {
            subpass_index,
            refs: vec![_ref],
            depth_ref: None,
        })
    }

    pub fn add_default_clear_value(&mut self) {
        self.render_pass.add_clear_value(ClearValue {
            color: ClearColorValue {
                float32: [0.0, 0.0, 0.0, 1.0],
            },
        });
    }

    /// Adds a subpass input
    ///
    /// * `idx` the attachment index to add the input for
    /// * `dep_index` the index of the attachment to use as the input.
    ///
    /// TODO maybe add Vec of references too
    pub fn add_subpass_input(&mut self, idx: usize, dep_index: u32) {
        self.render_pass.add_input_attachment_ref(SubpassReference {
            subpass_index: idx,
            refs: vec![vk::AttachmentReference {
                attachment: dep_index,
                layout: ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            }],
            depth_ref: None,
        });
    }

    /// Adds a default subpass dependency
    pub fn add_default_subpass_dependency(&mut self) {
        self.render_pass.add_subpass_dependency(
            SubpassDependency::default()
                .src_subpass(SUBPASS_EXTERNAL)
                .dst_subpass(0)
                .src_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                .dst_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
                .src_access_mask(AccessFlags::empty())
                .dst_access_mask(AccessFlags::COLOR_ATTACHMENT_WRITE)
                .dependency_flags(DependencyFlags::empty()),
        );
    }

    /// Adds a default subpass descriptor.
    pub fn add_default_subpass_descriptor(&mut self) {
        self.render_pass.add_subpass_descriptor(SubpassDescriptor {
            pipeline_bind_point: PipelineBindPoint::GRAPHICS,
        })
    }

    /// Adds the necessary components for one attachment.
    pub fn add_default_attachment(&mut self, vk: &Vulkan, texture_fmt: Option<Format>) {
        let mut format = TextureFormat::default();

        let mut texture_type = format.texture_format;

        if texture_fmt.is_some() {
            texture_type = texture_fmt.unwrap();
        }

        format.texture_format = texture_type;
        format.extent.width = self.viewport.width as u32;
        format.extent.height = self.viewport.height as u32;
        format.extent.depth = 1;
        format.mip_levels = 1;
        format.array_layers = 1;
        format.sample_count = SampleCountFlags::TYPE_1;
        format.tiling_mode = ImageTiling::OPTIMAL;
        format.image_usage_flags = ImageUsageFlags::COLOR_ATTACHMENT
            | ImageUsageFlags::SAMPLED
            | ImageUsageFlags::TRANSFER_SRC
            | ImageUsageFlags::INPUT_ATTACHMENT;
        format.memory_property_flags = MemoryPropertyFlags::DEVICE_LOCAL;

        let attachment = VkTexture::new(vk, format);

        self.attachments.push(attachment);

        self.render_pass
            .add_attachment_description(AttachmentDescription {
                format: texture_type,
                samples: SampleCountFlags::TYPE_1,
                load_op: AttachmentLoadOp::CLEAR,
                store_op: AttachmentStoreOp::STORE,
                stencil_load_op: AttachmentLoadOp::DONT_CARE,
                stencil_store_op: AttachmentStoreOp::DONT_CARE,
                initial_layout: ImageLayout::UNDEFINED,
                final_layout: ImageLayout::PRESENT_SRC_KHR,
                ..Default::default()
            });

        self.add_default_attachment_reference();
        self.add_default_clear_value();
    }
}
