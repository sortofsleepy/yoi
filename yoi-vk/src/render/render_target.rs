use crate::core::Vulkan;
use crate::descriptors::renderpass_descriptor::RenderPassDescriptor;
use crate::framebuffer::create_framebuffer;
use crate::texture::VkTexture;
use crate::utils::{
    generate_color_attachment_format, generate_default_color_attachment_descriptor,
    generate_default_color_attachment_descriptor_without_clear,
};
use crate::{FramebufferFormat, VkObject, VkRenderPass};
use ash::vk;
use ash::vk::{
    AttachmentDescription, ClearColorValue, ClearRect, ClearValue, CommandBuffer, Extent2D,
    ImageAspectFlags, Offset2D, Rect2D, SubpassDescription,
};

/// A wrapper around the idea of a Framebuffer object. calling it RenderTarget as Framebuffer is a slightly
/// different concept than what you might be used to if coming from OpenGL.
pub struct RenderTarget {
    viewport: vk::Viewport,
    attachments: Vec<VkTexture>,
    render_pass: VkRenderPass,
    framebuffer: Option<vk::Framebuffer>,
    clear_color: [f32; 4],
    clear_values: Vec<vk::ClearValue>,
    depth_stencil_value: vk::ClearDepthStencilValue,
}

impl RenderTarget {
    pub fn new(width: u32, height: u32) -> Self {
        let rt = RenderTarget {
            viewport: vk::Viewport {
                x: 0.0,
                y: 0.0,
                width: width as f32,
                height: height as f32,
                ..Default::default()
            },
            attachments: vec![],
            render_pass: VkRenderPass::new(),
            framebuffer: None,
            clear_color: [0.0, 0.0, 0.0, 1.0],
            clear_values: vec![],
            depth_stencil_value: vk::ClearDepthStencilValue {
                depth: 1.0,
                stencil: 0,
            },
        };

        rt
    }

    /// Calls destructors on objects
    pub fn destroy(&self, instance: &Vulkan) {
        instance.destroy_renderpass(*self.render_pass.raw());
        instance.destroy_framebuffer(self.framebuffer.unwrap());
    }

    /// Sets the clear color for the render target.
    pub fn set_clear_color(&mut self, r: f32, g: f32, b: f32, a: f32) {
        self.clear_color[0] = r;
        self.clear_color[1] = g;
        self.clear_color[2] = b;
        self.clear_color[3] = a;
    }

    /// Sets a new viewport object overwriting the current.
    pub fn set_viewport(&mut self, viewport: vk::Viewport) {
        self.viewport = viewport;
    }

    /// Sets viewport render position for the render target.
    pub fn set_viewport_position(&mut self, x: f32, y: f32) {
        self.viewport.x = x;
        self.viewport.y = y;
    }

    /// Clears all attachments.
    pub fn empty_attachments(&mut self) {
        self.attachments.clear()
    }

    pub fn clear_attachments(&self, vk: &Vulkan, cb: &CommandBuffer) {
        vk.clear_attachment(
            *cb,
            vk::ClearAttachment {
                aspect_mask: ImageAspectFlags::COLOR,
                color_attachment: 0,
                clear_value: ClearValue {
                    color: ClearColorValue {
                        float32: [1.0, 0.0, 0.0, 1.],
                    },
                },
            },
            ClearRect {
                rect: Rect2D {
                    offset: Offset2D { x: 0, y: 0 },
                    extent: Extent2D {
                        width: self.viewport.width as u32,
                        height: self.viewport.height as u32,
                    },
                },
                base_array_layer: 0,
                layer_count: 1,
            },
        );
    }

    /// Generates a default color attachment with default settings for the [RenderTarget]
    pub fn generate_attachment(&mut self, instance: &Vulkan) {
        let mut tfmt = generate_color_attachment_format(
            self.viewport.width as u32,
            self.viewport.height as u32,
            None,
        );
        let texture = VkTexture::new(instance, tfmt);
        self.add_attachment(
            texture,
            generate_default_color_attachment_descriptor(tfmt.texture_format),
        );
    }

    /// Does the same thing as [generate_attachment] but creates a [vk::AttachmentDescriptor] that doesn't clear itself.
    pub fn generate_attachment_with_descriptor_without_clear(&mut self, instance: &Vulkan) {
        let mut tfmt = generate_color_attachment_format(
            self.viewport.width as u32,
            self.viewport.height as u32,
            None,
        );
        let texture = VkTexture::new(instance, tfmt);

        self.attachments.push(texture);
        self.render_pass.add_attachment_description(
            generate_default_color_attachment_descriptor_without_clear(tfmt.texture_format),
        );
    }

    /// Does the same thing as [generate_attachment] but allows you to pass in your own vk::AttachmentDescriptor.
    pub fn generate_attachment_with_descriptor(
        &mut self,
        instance: &Vulkan,
        desc: AttachmentDescription,
    ) {
        let tfmt = generate_color_attachment_format(
            self.viewport.width as u32,
            self.viewport.height as u32,
            None,
        );
        let texture = VkTexture::new(instance, tfmt);

        self.attachments.push(texture);
        self.render_pass.add_attachment_description(desc);
    }

    /// Adds attachment to render target.
    pub fn add_attachment(
        &mut self,
        texture: VkTexture,
        attachment_description: vk::AttachmentDescription,
    ) {
        self.attachments.push(texture);
        self.render_pass
            .add_attachment_description(attachment_description);

        let color_ref = vk::AttachmentReference::default()
            .attachment((self.attachments.len() - 1) as u32)
            .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL);
        self.render_pass.add_color_attachment_reference(color_ref);
    }

    /// Adds subpass dependency to the render target.
    pub fn add_subpass_dependency(&mut self, dependency: vk::SubpassDependency) {
        self.render_pass.add_subpass_dependency(dependency);
    }

    /// get reference to renderpass.
    pub fn get_renderpass(&self) -> &VkRenderPass {
        &self.render_pass
    }

    pub fn get_framebuffer(&self) -> &vk::Framebuffer {
        &self.framebuffer.as_ref().unwrap()
    }

    pub fn add_color_attachment_description(&mut self, desc: RenderPassDescriptor) {
        self.render_pass.add_color_attachment_description(desc);
    }

    /// Starts rendering onto a [RenderTarget]
    pub fn begin(&self, instance: &Vulkan, cb: &vk::CommandBuffer) {
        let rp = &self.render_pass;
        let fb = self.framebuffer.unwrap();

        let width = self.viewport.width;
        let height = self.viewport.height;

        let extent = vk::Extent2D {
            width: width as u32,
            height: height as u32,
        };
        let scissor = vk::Rect2D {
            offset: vk::Offset2D { x: 0, y: 0 },
            extent,
        };

        // Setup clear values. If no values set, pass in default.
        let mut clear_values = vec![];

        // generate the same number of clear values vs number of attachments if no custom values have been
        // defined
        if self.clear_values.is_empty() {
            for i in 0..self.attachments.len() {
                clear_values.push(vk::ClearValue {
                    color: vk::ClearColorValue {
                        float32: self.clear_color,
                    },
                })
            }
        } else {
            if self.clear_values.len() != self.attachments.len() {
                // ensure there's enough clear values for all attachments
                let num_attach = self.clear_values.len();

                for i in num_attach..self.attachments.len() {
                    clear_values.push(vk::ClearValue {
                        color: vk::ClearColorValue {
                            float32: self.clear_color,
                        },
                    })
                }
            }
        }

        let renderpass_info = vk::RenderPassBeginInfo::default()
            .render_area(scissor)
            .render_pass(*rp.raw())
            .clear_values(&clear_values[..])
            .framebuffer(fb);

        instance.set_viewport(*cb, 0, &[self.viewport]);
        instance.set_scissor(*cb, 0, &[scissor]);
        instance.start_renderpass(cb, &renderpass_info);
    }

    /// Ends rendering onto the [RenderTarget]
    pub fn end(&self, instance: &Vulkan, cb: &vk::CommandBuffer) {
        instance.end_renderpass(cb);
    }

    /// Returns the number of attachments currently associated with the RenderTarget
    pub fn get_num_attachments(&self) -> usize {
        self.attachments.len()
    }

    /// Compiles the render target.
    pub fn compile(&mut self, instance: &Vulkan) {
        // if no attachments have been added add at least one attachment.
        if self.attachments.len() < 1 {
            self.generate_attachment(instance);
        }

        // construct renderpass
        self.render_pass.build(instance, vec![]);

        let mut views = vec![];
        for attachment in self.attachments.iter() {
            views.push(*attachment.get_image_view());
        }

        let fbo = create_framebuffer(
            instance,
            FramebufferFormat {
                width: self.viewport.width as u32,
                height: self.viewport.height as u32,
                layers: 1,
                attachments: views,
            },
            &self.render_pass.raw(),
        );

        self.framebuffer = Some(fbo);
    }

    pub fn compile_with_subpass_descriptions(
        &mut self,
        instance: &Vulkan,
        subpass_descriptions: Vec<SubpassDescription>,
    ) {
        // if no attachments have been added add at least one attachment.
        if self.attachments.len() < 1 {
            self.generate_attachment(instance);
        }

        // construct renderpass
        self.render_pass.build(instance, subpass_descriptions);

        let mut views = vec![];
        for attachment in self.attachments.iter() {
            views.push(*attachment.get_image_view());
        }

        let fbo = create_framebuffer(
            instance,
            FramebufferFormat {
                width: self.viewport.width as u32,
                height: self.viewport.height as u32,
                layers: 1,
                attachments: views,
            },
            &self.render_pass.raw(),
        );

        self.framebuffer = Some(fbo);
    }

    pub fn get_color_view(&self) -> &vk::ImageView {
        self.attachments[0].get_image_view()
    }

    pub fn get_attachment(&self) -> VkTexture {
        self.attachments[0]
    }

    pub fn get_color_view_at(&self, idx: usize) -> &vk::ImageView {
        self.attachments[idx].get_image_view()
    }
}
