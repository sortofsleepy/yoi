use crate::core::Vulkan;
use crate::render::swapchain::VkSwapChain;
use crate::types::traits::VkObject;
use crate::{generate_subresource_range, insert_image_memory_barrier, VkApp};
use ash::prelude::VkResult;
use ash::vk;
use ash::vk::{
    AccessFlags, AttachmentLoadOp, AttachmentStoreOp, ClearValue, CommandBuffer,
    CommandBufferAllocateInfo, CommandBufferBeginInfo, CommandBufferLevel, CommandBufferResetFlags,
    CommandBufferUsageFlags, CommandPoolCreateFlags, CommandPoolCreateInfo, Fence, FenceCreateInfo,
    ImageLayout, PipelineStageFlags, RenderingAttachmentInfoKHR, RenderingInfoKHR, Semaphore,
    SemaphoreCreateInfo,
};
use std::panic::panic_any;

/// Wraps some common things and settings usually used when rendering.
pub struct VkRenderer {
    stage_wait_semaphores: Vec<Semaphore>,
    //custom_semaphores: Vec<Semaphore>,
    //stage_signal_semaphores: Vec<Semaphore>,
    wait_semaphores: Vec<Semaphore>,
    render_finished_semaphores: Vec<Semaphore>,
    command_buffers: Vec<CommandBuffer>,
    max_frames_in_flight: usize,
    fences: Vec<Fence>,
    wait_dst_stage_masks: Vec<PipelineStageFlags>,
    fence_timeout: u32,
    current_frame: usize,
    recorded: bool,
    clear_color: Option<ClearValue>,
}

impl VkRenderer {
    pub fn new() -> Self {
        VkRenderer {
            wait_semaphores: vec![],
            //custom_semaphores: vec![],
            render_finished_semaphores: vec![],
            fences: vec![],
            command_buffers: vec![],
            stage_wait_semaphores: vec![],
            //stage_signal_semaphores: vec![],
            max_frames_in_flight: 3,
            wait_dst_stage_masks: vec![],
            fence_timeout: 100000,
            current_frame: 0,
            recorded: false,

            // we default to [VkApp] specified clear value
            clear_color: None,
        }
    }

    pub fn set_clear_color(&mut self, r: f32, g: f32, b: f32, a: f32) {
        self.clear_color = Some(ClearValue {
            color: vk::ClearColorValue {
                float32: [r, g, b, a],
            },
        })
    }

    pub fn set_max_frames_in_flight(&mut self, num: usize) {
        self.max_frames_in_flight = num;
    }

    pub fn get_max_frames_in_flight(&self) -> usize {
        self.max_frames_in_flight
    }

    /// Initializes renderer.
    pub fn init(&mut self, instance: &Vulkan, swapchain: &VkSwapChain) {
        ///// SETUP COMMAND BUFFERS //////
        // build command pool
        let command_pool_info = CommandPoolCreateInfo::default()
            .queue_family_index(instance.get_graphics_queue_index() as u32)
            .flags(CommandPoolCreateFlags::RESET_COMMAND_BUFFER);

        let command_pool = instance.generate_command_pool(command_pool_info);

        let size = swapchain.get_num_images();
        let buffer_allocate_info = CommandBufferAllocateInfo::default()
            .command_pool(command_pool)
            .level(CommandBufferLevel::PRIMARY)
            .command_buffer_count(size as u32);

        self.command_buffers = instance.generate_commmand_buffers(buffer_allocate_info);

        ///// SETUP SEMAPHORES AND FENCES /////
        for _i in 0..self.max_frames_in_flight {
            let semaphore_info_image = SemaphoreCreateInfo {
                ..Default::default()
            };

            let semaphore_info_render = SemaphoreCreateInfo {
                ..Default::default()
            };

            let fence_info = FenceCreateInfo {
                flags: vk::FenceCreateFlags::SIGNALED,
                ..Default::default()
            };

            // by default, we push a wait semaphore for each possible frame image.
            self.wait_semaphores
                .push(instance.generate_semaphore(semaphore_info_image));
            self.render_finished_semaphores
                .push(instance.generate_semaphore(semaphore_info_render));
            self.fences.push(instance.generate_fence(fence_info));
        }
    }

    pub fn record_commands_with_renderpass<F: Fn(&CommandBuffer)>(
        &mut self,
        instance: &Vulkan,
        meta: &VkApp,
        draw: F,
    ) {
        let swapchain = meta.get_swapchain();
        let depth = meta.get_depth_image();
        //let depth_image = depth.get_image();
        //let depth_image_view = depth.get_image_view();

        for i in 0..swapchain.get_num_images() {
            let current_buffer = self.command_buffers[i];

            self.bind_command_buffer(i, instance);

            draw(&current_buffer);

            self.unbind_command_buffer(instance, &current_buffer);

            instance.submit_command_buffer(&current_buffer);
        }
    }

    /// Same as [Renderer::record_commands_with_renderpass] but also sends the current command buffer index in addition
    /// to the command buffer object.
    pub fn record_commands_with_renderpass_and_index<F: Fn(&CommandBuffer, usize)>(
        &mut self,
        instance: &Vulkan,
        draw: F,
    ) {
        for i in 0..self.max_frames_in_flight {
            let current_buffer = self.command_buffers[i];

            self.bind_command_buffer(i, instance);

            draw(&current_buffer, i);

            self.unbind_command_buffer(instance, &current_buffer);

            // TODO this is a bit of a mystery - sometimes you don't have to submit; I believe I found another point where this was being called
            //  and that was why I removed this not too long ago, but for some reason, some commands still need this? Should investigate.
            instance.submit_command_buffer(&current_buffer);
        }
    }
    pub fn get_render_semaphore(&self) -> &Semaphore {
        &self.render_finished_semaphores[0]
    }

    /// Records commands onto the main command buffer for rendering
    pub fn record_commands<F: Fn(&CommandBuffer)>(
        &mut self,
        instance: &Vulkan,
        meta: &VkApp,
        draw: F,
    ) {
        let swapchain = meta.get_swapchain();
        let depth = meta.get_depth_image();
        let depth_image = depth.get_image();
        let depth_image_view = depth.get_image_view();

        for i in 0..swapchain.get_num_images() {
            let swapchain_image = swapchain.get_image_at(i);
            let current_buffer = self.command_buffers[i];

            self.bind_command_buffer(i, instance);

            // restore to presentation.
            insert_image_memory_barrier(
                instance,
                &current_buffer,
                swapchain_image,
                AccessFlags::empty(),
                AccessFlags::COLOR_ATTACHMENT_WRITE,
                ImageLayout::UNDEFINED,
                ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                generate_subresource_range(false),
                PipelineStageFlags::TOP_OF_PIPE,
                PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
            );

            insert_image_memory_barrier(
                instance,
                &current_buffer,
                depth_image,
                AccessFlags::empty(),
                AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE,
                ImageLayout::UNDEFINED,
                ImageLayout::DEPTH_ATTACHMENT_OPTIMAL_KHR,
                generate_subresource_range(true),
                PipelineStageFlags::EARLY_FRAGMENT_TESTS | PipelineStageFlags::LATE_FRAGMENT_TESTS,
                PipelineStageFlags::EARLY_FRAGMENT_TESTS | PipelineStageFlags::LATE_FRAGMENT_TESTS,
            );

            let mut clear_val = *meta.get_clear();
            if self.clear_color.is_some() {
                clear_val = self.clear_color.unwrap();
            }

            let color_attachment = RenderingAttachmentInfoKHR::default()
                .image_view(*swapchain.get_image_view_at(i))
                .image_layout(ImageLayout::ATTACHMENT_OPTIMAL)
                .load_op(AttachmentLoadOp::CLEAR)
                .store_op(AttachmentStoreOp::STORE)
                .clear_value(clear_val);

            let depth_stencil_attachment = RenderingAttachmentInfoKHR::default()
                .image_view(*depth_image_view)
                .image_layout(ImageLayout::DEPTH_ATTACHMENT_OPTIMAL)
                .load_op(AttachmentLoadOp::CLEAR)
                .store_op(AttachmentStoreOp::STORE)
                .clear_value(*meta.get_depth_stencil());

            let c_attachments = &[color_attachment];
            let rendering_info = RenderingInfoKHR::default()
                .render_area(*meta.get_render_area())
                .layer_count(1)
                .color_attachments(c_attachments)
                .depth_attachment(&depth_stencil_attachment);

            instance.begin_dynamic_render(&current_buffer, &rendering_info);

            instance.set_viewport(current_buffer, 0, meta.get_viewports());
            instance.set_scissor(current_buffer, 0, meta.get_scissors());

            draw(&current_buffer);

            instance.end_dynamic_render(&current_buffer);

            // transition color image back to a presentation format
            insert_image_memory_barrier(
                instance,
                &current_buffer,
                swapchain_image,
                AccessFlags::COLOR_ATTACHMENT_WRITE,
                AccessFlags::empty(),
                ImageLayout::COLOR_ATTACHMENT_OPTIMAL,
                ImageLayout::PRESENT_SRC_KHR,
                generate_subresource_range(false),
                PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                PipelineStageFlags::BOTTOM_OF_PIPE,
            );

            instance.end_command_buffer(&current_buffer);
        }
    }

    /// Adds a wait stage to the rendering stack.
    pub fn add_wait_stage(mut self, stage: PipelineStageFlags) -> Self {
        self.wait_dst_stage_masks.push(stage);

        self
    }

    /// increment frame count.
    pub fn finish_present(&mut self) {
        self.current_frame = (self.current_frame + 1) % self.max_frames_in_flight;
    }

    /// Binds the specified command buffer.
    pub fn bind_command_buffer(&self, idx: usize, instance: &Vulkan) -> &CommandBuffer {
        let buffer = self.command_buffers[idx];
        let command_buffer_begin_info = CommandBufferBeginInfo::default();
        //.flags(CommandBufferUsageFlags::ONE_TIME_SUBMIT);

        instance.reset_command_buffer(&buffer);
        instance.begin_command_buffer_with_info(&buffer, &command_buffer_begin_info);
        &self.command_buffers[idx]
    }

    pub fn one_time_bind_command_buffer(&self, idx: usize, instance: &Vulkan) -> &CommandBuffer {
        let buffer = self.command_buffers[idx];
        let command_buffer_begin_info =
            CommandBufferBeginInfo::default().flags(CommandBufferUsageFlags::ONE_TIME_SUBMIT);

        instance.reset_command_buffer(&buffer);
        instance.begin_command_buffer_with_info(&buffer, &command_buffer_begin_info);
        &self.command_buffers[idx]
    }

    /// Unbinds the specified command buffer.
    pub fn unbind_command_buffer(&self, instance: &Vulkan, buffer: &CommandBuffer) {
        let device = instance.get_logical_device();
        unsafe {
            let res = device.end_command_buffer(*buffer);

            #[cfg(debug_assertions)]
            {
                if (res.is_err()) {
                    panic!("Issue unbinding command buffer")
                }
            }
        };
    }

    /// ends the command buffer at the specified index.
    pub fn unbind_command_buffer_at(&self, idx: usize, instance: &Vulkan) {
        let device = instance.get_logical_device();
        let buffer = self.command_buffers[idx];

        unsafe { device.end_command_buffer(buffer).unwrap() };
    }

    /// Gets the command buffer at the specified index.
    pub fn get_command_buffer(&self, idx: usize) -> &CommandBuffer {
        &self.command_buffers[idx]
    }

    pub fn get_current_frame(&self) -> usize {
        self.current_frame
    }

    /// Default presentation method to the screen. Takes renderer settings to compose the presentation.
    pub fn present(&mut self, instance: &Vulkan, swapchain: &VkSwapChain) -> vk::Result {
        return self.custom_present(instance, swapchain, Some(&[]), Some(&[]), Some(&[]));
    }
    pub fn present_with_compute(
        &self,
        instance: &Vulkan,
        swapchain: &VkSwapChain,
        compute_semaphore: &Semaphore,
        graphics_semaphore: &Semaphore,
    ) -> vk::Result {
        return self.custom_present(
            instance,
            swapchain,
            Some(&[vk::PipelineStageFlags::VERTEX_INPUT]),
            Some(&[*compute_semaphore]),
            Some(&[*graphics_semaphore]),
        );
    }

    pub fn debug_present(&self, vk: &Vulkan, swapchain: &VkSwapChain) {
        let frame = self.current_frame;
        let fence = self.fences[frame];

        vk.wait_for_fences(&[fence]);
        vk.reset_fences(&[fence]);

        let fence_timeout = self.fence_timeout;
        let image_semaphore = self.wait_semaphores[frame];
        let img_index = swapchain
            .next_image_khr(fence_timeout as u64, &image_semaphore, &Fence::null())
            .unwrap();

        ////// PREPARE TO SUBMIT //////
        let wait_stage = &[vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT];
        let present_complete_semaphore = &[self.wait_semaphores[frame]];
        let render_complete_semaphore = &[self.render_finished_semaphores[frame]];
        let command_buffer = &[self.command_buffers[img_index.0 as usize]];

        let submit_info = vk::SubmitInfo::default()
            .wait_dst_stage_mask(wait_stage)
            .signal_semaphores(render_complete_semaphore)
            .wait_semaphores(present_complete_semaphore)
            .command_buffers(command_buffer);

        vk.submit_graphics(submit_info, fence);
        let render_complete_semaphore = &[self.render_finished_semaphores[frame]];
        let schain = &[*swapchain.raw()];
        let img_idx = &[img_index.0];
        let present_info = vk::PresentInfoKHR::default()
            .wait_semaphores(render_complete_semaphore)
            .swapchains(schain)
            .image_indices(img_idx);

        swapchain.submit_present(&present_info, *vk.get_present_queue());

        vk.present_wait_idle();
    }

    /// A custom presentation function allowing you to specify the
    /// - wait stages
    /// - wait semaphores
    /// - signal semaphores
    ///
    /// Note that this does make some assumptions by automatically adding the COLOR_ATTACHMENT_OUTPUT stage to wait stages
    /// as well as automatically inserting one of the renderer's wait semaphores.
    pub fn custom_present(
        &self,
        instance: &Vulkan,
        swapchain: &VkSwapChain,
        wait_stages: Option<&[PipelineStageFlags]>,
        wait_semaphores: Option<&[Semaphore]>,
        signal_semaphores: Option<&[Semaphore]>,
    ) -> vk::Result {
        let fence = self.fences[self.current_frame];
        instance.wait_for_fences(&[fence]);
        instance.reset_fences(&[fence]);

        let fence_timeout = self.fence_timeout;
        let image_semaphore = self.wait_semaphores[self.current_frame];

        let current_buffer =
            swapchain.next_image_khr(fence_timeout as u64, &image_semaphore, &Fence::null());
        let buffer = if current_buffer.is_ok() {
            current_buffer.unwrap()
        } else {
            return current_buffer.unwrap_err();
        };

        let command_buffer = self.command_buffers[buffer.0 as usize];

        //////////// BUILD WAIT STAGES //////////////////
        let wait_stages = &[
            wait_stages.unwrap(),
            &[PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT],
        ]
        .concat();

        //////////// BUILD WAIT SEMAPHORES //////////////////
        let stage_wait_semaphores = &[wait_semaphores.unwrap(), &[image_semaphore]].concat();

        //////////// BUILD SIGNAL SEMAPHORES //////////////////
        let signal_semaphores = &[
            signal_semaphores.unwrap(),
            &[self.render_finished_semaphores[self.current_frame]],
        ]
        .concat();

        //////////// BUILD SUBMIT //////////////////
        let binding = [command_buffer];
        let submit_info = vk::SubmitInfo::default()
            .wait_semaphores(stage_wait_semaphores)
            .signal_semaphores(signal_semaphores)
            .wait_dst_stage_mask(wait_stages)
            .command_buffers(&binding);

        instance.submit_graphics(submit_info, fence);

        //// SETUP PRESENT /////
        let wait_sema = [self.render_finished_semaphores[self.current_frame]];
        let swap = [*swapchain.raw()];
        let img_idx = [buffer.0];
        let present_info = vk::PresentInfoKHR::default()
            .wait_semaphores(&wait_sema)
            .swapchains(&swap)
            .image_indices(&img_idx);

        swapchain.submit_present(&present_info, *instance.get_present_queue());

        //// wait for fences ///
        //instance.wait_for_fences(&[fence]);
        //instance.reset_fences(&[fence]);
        instance.present_wait_idle();

        return vk::Result::SUCCESS;
    }
}
