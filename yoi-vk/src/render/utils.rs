use crate::framework::databuffer::find_memory_type_index;
use crate::types::formats::TextureFormat;
use crate::Vulkan;
use ash::vk;
use ash::vk::{
    AccessFlags, CommandBuffer, DependencyFlags, Format, Image, ImageAspectFlags, ImageCreateInfo,
    ImageLayout, ImageMemoryBarrier, ImageSubresourceRange, ImageTiling, ImageType,
    ImageUsageFlags, MemoryAllocateInfo, MemoryPropertyFlags, PipelineStageFlags, SampleCountFlags,
};

/// generates a default subresource range.
pub fn generate_subresource_range(for_depth: bool) -> ImageSubresourceRange {
    if for_depth {
        ImageSubresourceRange {
            aspect_mask: ImageAspectFlags::DEPTH,
            base_mip_level: 0,
            level_count: 1,
            base_array_layer: 0,
            layer_count: 1,
        }
    } else {
        ImageSubresourceRange {
            aspect_mask: ImageAspectFlags::COLOR,
            base_mip_level: 0,
            level_count: 1,
            base_array_layer: 0,
            layer_count: 1,
        }
    }
}

/// Sets the layout on a vk::Image while providing access flags to use. Meant to be run
/// within a recording process with a command buffer already bound
#[allow(unused)]
pub fn insert_image_memory_barrier(
    instance: &Vulkan,
    command_buffer: &CommandBuffer,
    image: &Image,
    src_access_mask: AccessFlags,
    dst_access_mask: AccessFlags,
    old_layout: ImageLayout,
    new_layout: ImageLayout,
    subresource_range: ImageSubresourceRange,
    src_stage_mask: PipelineStageFlags,
    dst_stage_mask: PipelineStageFlags,
) {
    instance.pipeline_barrier(
        command_buffer,
        src_stage_mask,
        dst_stage_mask,
        DependencyFlags::default(),
        &[],
        &[],
        &[ImageMemoryBarrier::default()
            .old_layout(old_layout)
            .new_layout(new_layout)
            .image(*image)
            .src_access_mask(src_access_mask)
            .dst_access_mask(dst_access_mask)
            .subresource_range(subresource_range)],
    );
}

#[allow(unused)]
pub fn set_image_layout(
    instance: &Vulkan,
    cb: &CommandBuffer,
    image: &Image,
    old_layout: ImageLayout,
    new_layout: ImageLayout,
    subresource_range: ImageSubresourceRange,
    src_stage_mask: PipelineStageFlags,
    dst_stage_mask: PipelineStageFlags,
) {
    let mut src_access_mask: AccessFlags = AccessFlags::default();

    match old_layout {
        ImageLayout::UNDEFINED => {
            // no change,, mask will be 0
        }

        ImageLayout::PREINITIALIZED => src_access_mask = AccessFlags::HOST_WRITE,

        ImageLayout::COLOR_ATTACHMENT_OPTIMAL => {
            src_access_mask = AccessFlags::COLOR_ATTACHMENT_WRITE;
        }

        ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL => {
            src_access_mask = AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE;
        }

        ImageLayout::TRANSFER_SRC_OPTIMAL => {
            src_access_mask = AccessFlags::TRANSFER_READ;
        }

        ImageLayout::TRANSFER_DST_OPTIMAL => {
            src_access_mask = AccessFlags::TRANSFER_WRITE;
        }

        ImageLayout::SHADER_READ_ONLY_OPTIMAL => {
            src_access_mask = AccessFlags::SHADER_READ;
        }

        _ => {}
    };

    let mut dst_access_mask = AccessFlags::default();

    match new_layout {
        ImageLayout::TRANSFER_DST_OPTIMAL => {
            dst_access_mask = AccessFlags::TRANSFER_WRITE;
        }

        ImageLayout::TRANSFER_SRC_OPTIMAL => dst_access_mask = AccessFlags::TRANSFER_READ,

        ImageLayout::COLOR_ATTACHMENT_OPTIMAL => {
            dst_access_mask = AccessFlags::COLOR_ATTACHMENT_WRITE;
        }

        ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL => {
            dst_access_mask = AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE;
        }

        ImageLayout::SHADER_READ_ONLY_OPTIMAL => {
            if src_access_mask == AccessFlags::empty() {
                src_access_mask = AccessFlags::HOST_WRITE | AccessFlags::TRANSFER_WRITE;
            }

            dst_access_mask = AccessFlags::SHADER_READ;
        }

        _ => {}
    }

    let barrier = ImageMemoryBarrier::default()
        .old_layout(old_layout)
        .new_layout(new_layout)
        .image(*image)
        .src_access_mask(src_access_mask)
        .dst_access_mask(dst_access_mask)
        .subresource_range(subresource_range);

    unsafe {
        instance.get_logical_device().cmd_pipeline_barrier(
            *cb,
            src_stage_mask,
            dst_stage_mask,
            DependencyFlags::default(),
            &[],
            &[],
            &[barrier],
        );
    }
}

/// generate a default ImageCreateInfo
pub(crate) fn generate_image_create_info(format: &TextureFormat) -> ImageCreateInfo {
    ImageCreateInfo {
        image_type: format.image_type,
        format: format.texture_format,
        extent: format.extent,
        mip_levels: format.mip_levels,
        array_layers: format.array_layers,
        samples: format.sample_count,
        tiling: format.tiling_mode,
        usage: format.image_usage_flags,
        sharing_mode: format.share_mode,
        initial_layout: format.initial_layout,
        ..Default::default()
    }
}

/// Returns a MemoryAllocateInfo object based on the image and flags passed in.
///
/// * 'instance' - the main Vulkan instance that contains all core methods
/// * 'image' - The image object to query
/// * 'memory_property_flags' - The property flags to check against in order to find the memory type index.
pub fn get_image_memory_allocate_info(
    instance: &Vulkan,
    image: Image,
    memory_property_flags: MemoryPropertyFlags,
) -> MemoryAllocateInfo {
    let memory_flags = memory_property_flags;
    let memory_requirements = instance.get_memory_requirements(image);
    let memory_props = instance.get_memory_properties();

    let memory_type_index =
        find_memory_type_index(&memory_requirements, &memory_props, memory_flags).unwrap();
    let attachment_allocate_info = MemoryAllocateInfo::default()
        .allocation_size(memory_requirements.size)
        .memory_type_index(memory_type_index);

    attachment_allocate_info
}

/// Generate a default [TextureFormat] suitable for use as a depth attachment.
#[allow(unused)]
#[deprecated = "use generate_depth_texture_format instead"]
pub fn generate_depth_attachment_format(width: u32, height: u32) -> TextureFormat {
    let mut dformat = TextureFormat::default();
    dformat.image_type = ImageType::TYPE_2D;
    //dformat.texture_format = Format::D16_UNORM;
    dformat.texture_format = Format::D32_SFLOAT;

    dformat.extent.width = width;
    dformat.extent.height = height;
    dformat.extent.depth = 1;
    dformat.mip_levels = 1;
    dformat.array_layers = 1;
    dformat.sample_count = SampleCountFlags::TYPE_1;
    dformat.tiling_mode = ImageTiling::OPTIMAL;
    dformat.image_subresource_range.aspect_mask = vk::ImageAspectFlags::DEPTH;
    dformat.initial_layout = vk::ImageLayout::UNDEFINED;
    dformat.image_usage_flags =
        ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT | ImageUsageFlags::SAMPLED;
    dformat.memory_property_flags = MemoryPropertyFlags::DEVICE_LOCAL;
    dformat
}

/// Generate a [TextureFormat] object for creating a new texture.
///
/// * `width` - width of the texture
/// * `height - height of the texture
/// * `format` - the [Format] for the texture.
///
pub fn generate_depth_texture_format(width: u32, height: u32, format: Format) -> TextureFormat {
    let mut dformat = TextureFormat::default();
    dformat.image_type = ImageType::TYPE_2D;
    dformat.texture_format = format;
    dformat.extent.width = width;
    dformat.extent.height = height;
    dformat.extent.depth = 1;
    dformat.mip_levels = 1;
    dformat.array_layers = 1;
    dformat.sample_count = SampleCountFlags::TYPE_1;
    dformat.tiling_mode = ImageTiling::OPTIMAL;
    dformat.image_subresource_range.aspect_mask = vk::ImageAspectFlags::DEPTH;
    dformat.initial_layout = vk::ImageLayout::UNDEFINED;
    dformat.image_usage_flags =
        ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT | ImageUsageFlags::SAMPLED;
    dformat.memory_property_flags = MemoryPropertyFlags::DEVICE_LOCAL;
    dformat
}
