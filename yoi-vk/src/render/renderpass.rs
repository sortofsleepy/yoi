use crate::core::Vulkan;
use crate::descriptors::renderpass_descriptor::RenderPassDescriptor;
use crate::VkObject;
use ash::vk;
use ash::vk::SubpassDescription;
use std::ffi::CString;

#[deprecated = "Deprecated in favor of cleaner version in [RenderPass2]"]
pub struct VkRenderPass {
    rp: Option<vk::RenderPass>,
    color_reference: Vec<vk::AttachmentReference>,
    depth_reference: Option<vk::AttachmentReference>,
    attachment_descriptions: Vec<vk::AttachmentDescription>,
    subpass_dependencies: Vec<vk::SubpassDependency>,
    //subpass_descriptions: Vec<vk::SubpassDescription>,
}

impl VkRenderPass {
    pub fn new() -> Self {
        VkRenderPass {
            rp: None,
            color_reference: vec![],
            depth_reference: None,
            attachment_descriptions: vec![],
            subpass_dependencies: vec![],
            //subpass_descriptions: vec![],
        }
    }

    pub fn new_with_descriptors(
        color_desc: RenderPassDescriptor,
        depth_desc: RenderPassDescriptor,
    ) -> Self {
        let color = vk::AttachmentDescription {
            flags: Default::default(),
            format: color_desc.color_format,
            load_op: color_desc.load_op,
            store_op: color_desc.store_op,
            samples: vk::SampleCountFlags::TYPE_1,
            stencil_load_op: color_desc.stencil_load_op,
            stencil_store_op: color_desc.stencil_store_op,
            initial_layout: color_desc.color_initial_layout,
            final_layout: color_desc.color_final_layout,
            ..Default::default()
        };

        let depth = vk::AttachmentDescription {
            flags: Default::default(),
            format: depth_desc.depth_format,
            load_op: depth_desc.depth_load_op,
            samples: vk::SampleCountFlags::TYPE_1,
            store_op: depth_desc.depth_store_op,
            stencil_load_op: depth_desc.depth_stencil_load_op,
            stencil_store_op: depth_desc.depth_stencil_store_op,
            initial_layout: depth_desc.depth_initial_layout,
            final_layout: depth_desc.depth_final_layout,
            ..Default::default()
        };

        VkRenderPass {
            rp: None,
            color_reference: vec![],
            depth_reference: None,
            attachment_descriptions: vec![color, depth],
            subpass_dependencies: vec![],
            // subpass_descriptions: vec![],
        }
    }

    /// Generates a color attachment description object based on the settings you pass in.
    pub fn generate_color_attachment_description(
        desc: RenderPassDescriptor,
    ) -> vk::AttachmentDescription {
        vk::AttachmentDescription {
            format: desc.color_format,
            samples: vk::SampleCountFlags::TYPE_1,
            load_op: desc.load_op,
            store_op: desc.store_op,
            stencil_load_op: desc.stencil_load_op,
            stencil_store_op: desc.stencil_store_op,
            initial_layout: desc.color_initial_layout,
            final_layout: desc.color_final_layout,
            ..Default::default()
        }
    }

    /// Generates a depth oriented attachment description object based on the settings you pass in.
    pub fn generate_depth_attachment_description(
        desc: RenderPassDescriptor,
    ) -> vk::AttachmentDescription {
        vk::AttachmentDescription {
            format: desc.depth_format,
            load_op: desc.depth_load_op,
            samples: vk::SampleCountFlags::TYPE_1,
            store_op: desc.depth_store_op,
            stencil_load_op: desc.depth_stencil_load_op,
            stencil_store_op: desc.depth_stencil_store_op,
            initial_layout: desc.depth_initial_layout,
            final_layout: desc.depth_final_layout,
            ..Default::default()
        }
    }

    pub fn add_color_attachment_description(&mut self, desc: RenderPassDescriptor) {
        self.attachment_descriptions
            .push(vk::AttachmentDescription {
                format: desc.color_format,
                samples: vk::SampleCountFlags::TYPE_1,
                load_op: desc.load_op,
                store_op: desc.store_op,
                stencil_load_op: desc.stencil_load_op,
                stencil_store_op: desc.stencil_store_op,
                initial_layout: desc.color_initial_layout,
                final_layout: desc.color_final_layout,
                ..Default::default()
            })
    }

    /// Adds an attachment description to the stack.
    pub fn add_attachment_description(&mut self, desc: vk::AttachmentDescription) {
        self.attachment_descriptions.push(desc);
    }

    pub fn add_subpass_dependency(&mut self, dependency: vk::SubpassDependency) {
        self.subpass_dependencies.push(dependency);
    }

    pub fn add_color_attachment_reference(&mut self, reference: vk::AttachmentReference) {
        self.color_reference.push(reference);
    }

    pub fn add_depth_attachment_reference(&mut self, reference: vk::AttachmentReference) {
        self.depth_reference = Some(reference);
    }

    pub fn build(&mut self, instance: &Vulkan, subpass_descriptions: Vec<SubpassDescription>) {
        if !self.depth_reference.is_some() {
            let depth_ref = vk::AttachmentReference::default()
                .attachment(1)
                .layout(vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

            self.depth_reference = Some(depth_ref);
        }

        let mut subpasses = vec![];
        if subpass_descriptions.is_empty() {
            let description = vk::SubpassDescription::default()
                .pipeline_bind_point(vk::PipelineBindPoint::GRAPHICS)
                .color_attachments(&self.color_reference[..]);
            subpasses.push(description);
        } else {
            subpasses.extend(subpass_descriptions)
        }

        let renderpass_create_info = vk::RenderPassCreateInfo::default()
            .attachments(&self.attachment_descriptions[..])
            .subpasses(&subpasses[..])
            .dependencies(&self.subpass_dependencies[..]);

        let raw_rp = instance.generate_renderpass(renderpass_create_info);
        self.rp = Some(raw_rp.unwrap());
    }

    #[cfg(debug_assertions)]
    pub fn print_attachment_count(&self) {
        println!(
            "\nAttachment count is {}",
            self.attachment_descriptions.len()
        );
        //  println!("Subpass description count is {}", self.subpass_descriptions.len());
        println!(
            "Subpass dependency count is {}\n",
            self.subpass_dependencies.len()
        );
    }
}

impl VkObject for VkRenderPass {
    type Raw = vk::RenderPass;

    fn raw(&self) -> &Self::Raw {
        self.rp.as_ref().unwrap()
    }

    fn add_label(&self, ctx: &Vulkan, name: String) {
        #[cfg(debug_assertions)]
        {
            let raw = self.rp.unwrap();
            let name_info = vk::DebugUtilsObjectNameInfoEXT::default();
            name_info.object_handle(raw);
            let msg = format!("unable to convert {} to c_string for label", name);
            name_info.object_name(CString::new(name).expect(msg.as_str()).as_c_str());

            ctx.add_object_label(&name_info);
        }
    }
}
