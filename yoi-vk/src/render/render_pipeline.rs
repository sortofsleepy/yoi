use crate::core::Vulkan;
use crate::framework::vkapp::VkApp;
use crate::shaders::shader::RenderShader;
use crate::types::graphics::GraphicsPipelineFormat;
use crate::{Mesh, VkDescriptorBuffer, VkObject, VkRenderPass};
use ash::vk;
use ash::vk::{
    Bool32, CommandBuffer, DescriptorSetLayout, DynamicState, Format, GraphicsPipelineCreateInfo,
    Pipeline, PipelineCache, PipelineColorBlendAttachmentState, PipelineColorBlendStateCreateInfo,
    PipelineCreateFlags, PipelineDepthStencilStateCreateInfo, PipelineDynamicStateCreateInfo,
    PipelineInputAssemblyStateCreateInfo, PipelineLayout, PipelineLayoutCreateInfo,
    PipelineMultisampleStateCreateInfo, PipelineRasterizationStateCreateInfo,
    PipelineRenderingCreateInfoKHR, PipelineVertexInputStateCreateInfo, PolygonMode,
    PrimitiveTopology, PushConstantRange, RenderPass, SampleCountFlags,
    VertexInputAttributeDescription, VertexInputBindingDescription,
};
use std::ffi::CString;

/// Basic wrapper around a [vk::Pipeline] object for rendering things to screen.
/// Provides support for descriptor buffers and push constants.
pub struct RenderPipeline {
    pipeline_cache: Option<PipelineCache>,
    subpass_index: u32,
    pipeline_format: GraphicsPipelineFormat,
    pipeline_layout: Option<PipelineLayout>,
    pipeline: Option<Pipeline>,
    topology: PrimitiveTopology,
    descriptor_sets: Vec<DescriptorSetLayout>,
    input_attrib_descriptions: Vec<VertexInputAttributeDescription>,
    input_binding_descriptions: Vec<VertexInputBindingDescription>,
    pipeline_color_attachment_blend_states: Vec<PipelineColorBlendAttachmentState>,
    pipeline_flags: PipelineCreateFlags,
    push_constants: Vec<PushConstantRange>,
}

impl RenderPipeline {
    /// Initializes pipeline with a [VkDescriptorSet]
    #[deprecated = "Semi-deprecated in favor of descriptor buffers. Kept around for legacy / special situations."]
    pub fn new_with_descriptor_set() -> Self {
        RenderPipeline {
            push_constants: vec![],
            pipeline_cache: None,
            subpass_index: 0,
            input_attrib_descriptions: vec![],
            input_binding_descriptions: vec![],
            pipeline: None,
            pipeline_layout: None,
            topology: PrimitiveTopology::TRIANGLE_LIST,
            pipeline_format: GraphicsPipelineFormat::default(),
            pipeline_color_attachment_blend_states: vec![],
            pipeline_flags: PipelineCreateFlags::default(),
            descriptor_sets: vec![],
        }
    }

    /// Initializes a new pipeline for use with a descriptor buffer.
    pub fn new() -> Self {
        RenderPipeline {
            pipeline_cache: None,
            push_constants: vec![],
            subpass_index: 0,
            input_attrib_descriptions: vec![],
            input_binding_descriptions: vec![],
            pipeline: None,
            pipeline_layout: None,
            topology: PrimitiveTopology::TRIANGLE_LIST,
            pipeline_format: GraphicsPipelineFormat::default(),
            pipeline_color_attachment_blend_states: vec![],
            pipeline_flags: PipelineCreateFlags::DESCRIPTOR_BUFFER_EXT
                | PipelineCreateFlags::default(),
            descriptor_sets: vec![],
        }
    }

    /// Initializes a new pipeline for use with a descriptor buffer.
    #[deprecated = "Just use new instead"]
    pub fn new_with_descriptor_buffer() -> Self {
        RenderPipeline {
            pipeline_cache: None,
            push_constants: vec![],
            subpass_index: 0,
            input_attrib_descriptions: vec![],
            input_binding_descriptions: vec![],
            pipeline: None,
            pipeline_layout: None,
            topology: PrimitiveTopology::TRIANGLE_LIST,
            pipeline_format: GraphicsPipelineFormat::default(),
            pipeline_color_attachment_blend_states: vec![],
            pipeline_flags: PipelineCreateFlags::DESCRIPTOR_BUFFER_EXT
                | PipelineCreateFlags::default(),
            descriptor_sets: vec![],
        }
    }

    pub fn add_push_constant(&mut self, range: PushConstantRange) {
        self.push_constants.push(range);
    }

    /// Sets topology for the pipeline.
    pub fn set_topology(&mut self, top: PrimitiveTopology) {
        self.topology = top
    }

    /// binds the pipeline.
    pub fn bind_pipeline(&self, instance: &Vulkan, cb: CommandBuffer) {
        instance.bind_pipeline(cb, vk::PipelineBindPoint::GRAPHICS, self.pipeline.unwrap());
    }

    /// Set vertex attribute descriptions
    pub fn set_attrib_descriptions(&mut self, desc: Vec<VertexInputAttributeDescription>) {
        self.input_attrib_descriptions = desc;
    }

    /// sets vertex binding descriptions.
    pub fn set_binding_descriptions(&mut self, desc: Vec<VertexInputBindingDescription>) {
        self.input_binding_descriptions = desc;
    }

    /// adds a descriptor set for use with the pipeline.
    pub fn add_descriptor_set_layout(&mut self, ds: vk::DescriptorSetLayout) {
        self.descriptor_sets.push(ds);
    }

    pub fn add_mesh(&mut self, m: &Mesh) {
        self.set_attrib_descriptions(m.get_attrib_descriptions());
        self.set_binding_descriptions(m.get_binding_descriptions());
    }

    pub fn get_pipeline_layout(&self) -> vk::PipelineLayout {
        self.pipeline_layout.unwrap()
    }

    /// Adds a [vk::PipelineCreateFlag] for use during pipeline creation.
    pub fn add_pipeline_create_flag(&mut self, flag: vk::PipelineCreateFlags) {
        self.pipeline_flags = self.pipeline_flags | flag;
    }

    /// Enables alpha blending on the pipeline
    pub fn enable_alpha_blending(&mut self) {
        self.pipeline_format.blend_enable = Bool32::from(true);
        self.pipeline_format.src_color_blend_factor = vk::BlendFactor::SRC_ALPHA;
        self.pipeline_format.dst_color_blend_factor = vk::BlendFactor::ONE_MINUS_SRC_ALPHA;
        self.pipeline_format.color_blend_op = vk::BlendOp::ADD;
        self.pipeline_format.src_alpha_blend_factor = vk::BlendFactor::ONE;
        self.pipeline_format.dst_alpha_blend_factor = vk::BlendFactor::ZERO;
        self.pipeline_format.alpha_blend_op = vk::BlendOp::ADD;
    }

    pub fn enable_additive_blending(&mut self) {
        self.pipeline_format.blend_enable = Bool32::from(true);
        self.pipeline_format.src_color_blend_factor = vk::BlendFactor::SRC_ALPHA;
        self.pipeline_format.dst_color_blend_factor = vk::BlendFactor::ONE;
        self.pipeline_format.color_blend_op = vk::BlendOp::ADD;
        self.pipeline_format.src_alpha_blend_factor = vk::BlendFactor::ONE;
        self.pipeline_format.dst_alpha_blend_factor = vk::BlendFactor::ONE;
        self.pipeline_format.alpha_blend_op = vk::BlendOp::ADD;
    }

    pub fn set_cull_mode(&mut self, mode: vk::CullModeFlags) {
        self.pipeline_format.cull_mode = mode;
    }

    /// Enables depth testing on the pipeline
    pub fn enable_depth_test(&mut self) {
        self.pipeline_format.depth_test_enable = Bool32::from(true);
    }

    /// Disables depth test on the pipeline
    pub fn disable_depth_test(&mut self) {
        self.pipeline_format.depth_test_enable = Bool32::from(false);
    }

    /// Enables depth write on the pipeline
    pub fn enable_depth_write(&mut self) {
        self.pipeline_format.depth_write_enable = Bool32::from(true);
    }

    /// Enables depth write on the pipeline
    pub fn disable_depth_write(&mut self) {
        self.pipeline_format.depth_write_enable = Bool32::from(false);
    }

    pub fn is_depth_test_enabled(&self) -> bool {
        if self.pipeline_format.depth_test_enable == Bool32::from(true) {
            true
        } else {
            false
        }
    }

    pub fn is_depth_write_enabled(&self) -> bool {
        if self.pipeline_format.depth_write_enable == Bool32::from(true) {
            true
        } else {
            false
        }
    }

    pub fn has_push_constants(&self) -> bool {
        !self.push_constants.is_empty()
    }

    /// adds a default color attachment blend state
    pub fn add_default_color_attachment_blend_state(&mut self) {
        self.pipeline_color_attachment_blend_states
            .push(PipelineColorBlendAttachmentState {
                blend_enable: self.pipeline_format.blend_enable,
                src_color_blend_factor: self.pipeline_format.src_color_blend_factor,
                dst_color_blend_factor: self.pipeline_format.dst_color_blend_factor,
                color_blend_op: self.pipeline_format.color_blend_op,
                src_alpha_blend_factor: self.pipeline_format.src_alpha_blend_factor,
                dst_alpha_blend_factor: self.pipeline_format.dst_alpha_blend_factor,
                alpha_blend_op: self.pipeline_format.alpha_blend_op,
                color_write_mask: self.pipeline_format.color_write_mask,
            });
    }
    /// sets the subpass index for the Pipeline.
    pub fn set_subpass_index(&mut self, idx: u32) {
        self.subpass_index = idx;
    }

    /// adds a color attachment blend state.
    pub fn add_color_attachment_blend_state(&mut self, state: PipelineColorBlendAttachmentState) {
        self.pipeline_color_attachment_blend_states.push(state);
    }

    /// Builds the pipeline. If no renderpass is passed in, it will switch to building for dynamic rendering
    /// and assumes you want the default B8G8R8A8_UNORM and D32_SFLOAT settings
    pub fn compile2(
        &mut self,
        instance: &Vulkan,
        app: &VkApp,
        shader: &RenderShader,
        rp: Option<&RenderPass>,
    ) {
        // if no renderpass, assume we're doing default dynamic compile
        if rp.is_none() {
            self.compile_dynamic(
                instance,
                &app,
                &shader,
                Format::B8G8R8A8_UNORM,
                Format::D32_SFLOAT,
            );
        } else {
            self.compile_with_renderpass(instance, app, shader, rp.unwrap())
        }
    }

    /// Shorthand for [RenderPipeline::compile_dynamic] - assumes you want the default
    /// B8G8R8A8_UNORM and D32_SFLOAT settings
    #[deprecated = "start to use compile2 which will eventually replace this"]
    pub fn compile(&mut self, instance: &Vulkan, app: &VkApp, shader: &RenderShader) {
        self.compile_dynamic(
            instance,
            &app,
            &shader,
            Format::B8G8R8A8_UNORM,
            Format::D32_SFLOAT,
        );
    }

    /// Compiles the pipeline for use with multiple descriptor sets.
    pub fn compile_dynamic(
        &mut self,
        instance: &Vulkan,
        app: &VkApp,
        shader: &RenderShader,
        color_format: Format,
        depth_format: Format,
    ) {
        ///// BUILD PIPELINE LAYOUT //////
        let pipeline_create_info = PipelineLayoutCreateInfo::default()
            .push_constant_ranges(&self.push_constants[..])
            .set_layouts(&self.descriptor_sets[..]);

        let pipeline_layout = instance.generate_pipeline_layout(pipeline_create_info);

        ////////// BUILD VERTEX INPUT DESCRIPTIONS /////////////
        let input_create_info = PipelineVertexInputStateCreateInfo::default()
            .vertex_attribute_descriptions(&self.input_attrib_descriptions)
            .vertex_binding_descriptions(&self.input_binding_descriptions);

        ////// BUILD INPUT ASSEMBLY STATE //////
        let pipeline_input_assembly_create_info = PipelineInputAssemblyStateCreateInfo {
            topology: self.topology,
            ..Default::default()
        };

        /////// BUILD RASTER INFO ///////////////
        let pipeline_raster_state_create_info = PipelineRasterizationStateCreateInfo {
            depth_clamp_enable: self.pipeline_format.depth_clamp_enable,
            rasterizer_discard_enable: self.pipeline_format.raster_discard_enable,
            polygon_mode: PolygonMode::FILL,
            cull_mode: self.pipeline_format.cull_mode,
            front_face: self.pipeline_format.front_face,
            depth_bias_enable: self.pipeline_format.depth_bias_enable,
            depth_bias_constant_factor: self.pipeline_format.depth_bias_constant_factor,
            depth_bias_clamp: self.pipeline_format.depth_bias_clamp,
            depth_bias_slope_factor: self.pipeline_format.depth_bias_slope_factor,
            line_width: self.pipeline_format.line_width,
            ..Default::default()
        };

        ///// BUILD MULTI-SAMPLE STATE //////
        let pipeline_multisample_state_create_info = PipelineMultisampleStateCreateInfo {
            rasterization_samples: SampleCountFlags::TYPE_1,
            ..Default::default()
        };

        ////// SETUP DEPTH STENCIL STATE ////////
        let depth_stencil_state_create_info = PipelineDepthStencilStateCreateInfo {
            depth_test_enable: self.pipeline_format.depth_test_enable,
            depth_write_enable: self.pipeline_format.depth_write_enable,
            depth_compare_op: self.pipeline_format.depth_compare_op,
            depth_bounds_test_enable: self.pipeline_format.depth_bound_testable,
            stencil_test_enable: self.pipeline_format.stencil_test_enable,
            front: self.pipeline_format.stencil_op_state_front,
            back: self.pipeline_format.stencil_op_state_back,
            ..Default::default()
        };

        //////// SETUP COLOR COMPONENT //////////////
        if self.pipeline_color_attachment_blend_states.is_empty() {
            let pipeline_color_blend_attachment_state = PipelineColorBlendAttachmentState {
                blend_enable: self.pipeline_format.blend_enable,
                src_color_blend_factor: self.pipeline_format.src_color_blend_factor,
                dst_color_blend_factor: self.pipeline_format.dst_color_blend_factor,
                color_blend_op: self.pipeline_format.color_blend_op,
                src_alpha_blend_factor: self.pipeline_format.src_alpha_blend_factor,
                dst_alpha_blend_factor: self.pipeline_format.dst_alpha_blend_factor,
                alpha_blend_op: self.pipeline_format.alpha_blend_op,
                color_write_mask: self.pipeline_format.color_write_mask,
            };

            self.pipeline_color_attachment_blend_states
                .push(pipeline_color_blend_attachment_state)
        }

        let pipeline_color_blend_state_create_info = PipelineColorBlendStateCreateInfo::default()
            .logic_op_enable(self.pipeline_format.logic_op_enable)
            .logic_op(self.pipeline_format.logic_op)
            .attachments(&self.pipeline_color_attachment_blend_states[..])
            .blend_constants(self.pipeline_format.blend_constants);

        ///////// DYNAMIC STATE ////////////
        let mut dynamic_states = vec![DynamicState::VIEWPORT, DynamicState::SCISSOR];

        if self.pipeline_format.depth_write_enable == 1 {
            dynamic_states.push(DynamicState::DEPTH_WRITE_ENABLE);
        }

        if self.pipeline_format.depth_test_enable == 1 {
            dynamic_states.push(DynamicState::DEPTH_TEST_ENABLE)
        }

        let pipeline_dynamic_state_create_info =
            PipelineDynamicStateCreateInfo::default().dynamic_states(&dynamic_states);

        //// SETUP SHADER STAGES ////
        // If pipeline stages isn't populated, check to see if there's a shader
        // specified and get that information instead.
        let mut pipeline_shader_stages = vec![];
        pipeline_shader_stages.push(shader.generate_vertex_pipeline_info());

        pipeline_shader_stages.push(shader.generate_fragment_pipeline_info());

        /*
        let pipeline_cache_create_info = PipelineCacheCreateInfo {
            ..Default::default()
        };
         */

        let pipeline_cache = PipelineCache::null();

        // define the color, depth and stencil attachments which are engaged at run time
        let binding = [color_format];
        let mut rendering_create_info = PipelineRenderingCreateInfoKHR::default()
            .color_attachment_formats(&binding)
            .depth_attachment_format(depth_format);

        let viewport = vk::PipelineViewportStateCreateInfo::default()
            .viewports(app.get_viewports())
            .scissors(app.get_scissors());

        ///// FINALLY START TO CONSTRUCT PIPELINE /////
        let graphics_pipeline_create_info = GraphicsPipelineCreateInfo::default()
            .subpass(self.subpass_index)
            .stages(&pipeline_shader_stages[..])
            .vertex_input_state(&input_create_info)
            .input_assembly_state(&pipeline_input_assembly_create_info)
            .viewport_state(&viewport)
            .rasterization_state(&pipeline_raster_state_create_info)
            .multisample_state(&pipeline_multisample_state_create_info)
            .depth_stencil_state(&depth_stencil_state_create_info)
            .color_blend_state(&pipeline_color_blend_state_create_info)
            .dynamic_state(&pipeline_dynamic_state_create_info)
            .layout(pipeline_layout)
            .flags(self.pipeline_flags)
            .push_next(&mut rendering_create_info);

        let gp = instance.generate_graphics_pipeline(graphics_pipeline_create_info, pipeline_cache);

        self.pipeline_layout = Some(pipeline_layout);
        self.pipeline = Some(gp[0]);
        self.pipeline_cache = Some(pipeline_cache);
    }

    /// Compiles the pipeline for use with a normal render pass [VkRenderPass]
    pub fn compile_with_renderpass(
        &mut self,
        instance: &Vulkan,
        app: &VkApp,
        shader: &RenderShader,
        rp: &RenderPass,
    ) {
        ///// BUILD PIPELINE LAYOUT //////
        let pipeline_create_info = PipelineLayoutCreateInfo::default()
            .push_constant_ranges(&self.push_constants[..])
            .set_layouts(&self.descriptor_sets[..]);

        let pipeline_layout = instance.generate_pipeline_layout(pipeline_create_info);

        ////////// BUILD VERTEX INPUT DESCRIPTIONS /////////////
        let input_create_info = PipelineVertexInputStateCreateInfo::default()
            .vertex_attribute_descriptions(&self.input_attrib_descriptions)
            .vertex_binding_descriptions(&self.input_binding_descriptions);

        ////// BUILD INPUT ASSEMBLY STATE //////
        let pipeline_input_assembly_create_info = PipelineInputAssemblyStateCreateInfo {
            topology: self.topology,
            ..Default::default()
        };

        /////// BUILD RASTER INFO ///////////////
        let pipeline_raster_state_create_info = PipelineRasterizationStateCreateInfo {
            depth_clamp_enable: self.pipeline_format.depth_clamp_enable,
            rasterizer_discard_enable: self.pipeline_format.raster_discard_enable,
            polygon_mode: PolygonMode::FILL,
            cull_mode: self.pipeline_format.cull_mode,
            front_face: self.pipeline_format.front_face,
            depth_bias_enable: self.pipeline_format.depth_bias_enable,
            depth_bias_constant_factor: self.pipeline_format.depth_bias_constant_factor,
            depth_bias_clamp: self.pipeline_format.depth_bias_clamp,
            depth_bias_slope_factor: self.pipeline_format.depth_bias_slope_factor,
            line_width: self.pipeline_format.line_width,
            ..Default::default()
        };

        ///// BUILD MULTI-SAMPLE STATE //////
        let pipeline_multisample_state_create_info = PipelineMultisampleStateCreateInfo {
            rasterization_samples: SampleCountFlags::TYPE_1,
            ..Default::default()
        };

        ////// SETUP DEPTH STENCIL STATE ////////
        let depth_stencil_state_create_info = PipelineDepthStencilStateCreateInfo {
            depth_test_enable: self.pipeline_format.depth_test_enable,
            depth_write_enable: self.pipeline_format.depth_write_enable,
            depth_compare_op: self.pipeline_format.depth_compare_op,
            depth_bounds_test_enable: self.pipeline_format.depth_bound_testable,
            stencil_test_enable: self.pipeline_format.stencil_test_enable,
            front: self.pipeline_format.stencil_op_state_front,
            back: self.pipeline_format.stencil_op_state_back,
            ..Default::default()
        };

        //////// SETUP COLOR COMPONENT //////////////
        if self.pipeline_color_attachment_blend_states.is_empty() {
            let pipeline_color_blend_attachment_state = PipelineColorBlendAttachmentState {
                blend_enable: self.pipeline_format.blend_enable,
                src_color_blend_factor: self.pipeline_format.src_color_blend_factor,
                dst_color_blend_factor: self.pipeline_format.dst_color_blend_factor,
                color_blend_op: self.pipeline_format.color_blend_op,
                src_alpha_blend_factor: self.pipeline_format.src_alpha_blend_factor,
                dst_alpha_blend_factor: self.pipeline_format.dst_alpha_blend_factor,
                alpha_blend_op: self.pipeline_format.alpha_blend_op,
                color_write_mask: self.pipeline_format.color_write_mask,
            };

            self.pipeline_color_attachment_blend_states
                .push(pipeline_color_blend_attachment_state)
        }

        let pipeline_color_blend_state_create_info = PipelineColorBlendStateCreateInfo::default()
            .logic_op_enable(self.pipeline_format.logic_op_enable)
            .logic_op(self.pipeline_format.logic_op)
            .attachments(&self.pipeline_color_attachment_blend_states[..])
            .blend_constants(self.pipeline_format.blend_constants);

        ///////// DYNAMIC STATE ////////////
        let mut dynamic_states = vec![DynamicState::VIEWPORT, DynamicState::SCISSOR];

        if self.pipeline_format.depth_write_enable == 1 {
            dynamic_states.push(DynamicState::DEPTH_WRITE_ENABLE);
        }

        if self.pipeline_format.depth_test_enable == 1 {
            dynamic_states.push(DynamicState::DEPTH_TEST_ENABLE)
        }

        let pipeline_dynamic_state_create_info =
            PipelineDynamicStateCreateInfo::default().dynamic_states(&dynamic_states);

        //// SETUP SHADER STAGES ////
        // If pipeline stages isn't populated, check to see if there's a shader
        // specified and get that information instead.
        let mut pipeline_shader_stages = vec![];
        pipeline_shader_stages.push(shader.generate_vertex_pipeline_info());

        pipeline_shader_stages.push(shader.generate_fragment_pipeline_info());

        /*
        let pipeline_cache_create_info = PipelineCacheCreateInfo {
            ..Default::default()
        };
         */

        let pipeline_cache = PipelineCache::null();

        let viewport = vk::PipelineViewportStateCreateInfo::default()
            .viewports(app.get_viewports())
            .scissors(app.get_scissors());

        ///// FINALLY START TO CONSTRUCT PIPELINE /////
        let graphics_pipeline_create_info = GraphicsPipelineCreateInfo::default()
            .subpass(self.subpass_index)
            .stages(&pipeline_shader_stages[..])
            .vertex_input_state(&input_create_info)
            .input_assembly_state(&pipeline_input_assembly_create_info)
            .viewport_state(&viewport)
            .rasterization_state(&pipeline_raster_state_create_info)
            .multisample_state(&pipeline_multisample_state_create_info)
            .depth_stencil_state(&depth_stencil_state_create_info)
            .color_blend_state(&pipeline_color_blend_state_create_info)
            .dynamic_state(&pipeline_dynamic_state_create_info)
            .layout(pipeline_layout)
            .flags(self.pipeline_flags)
            .render_pass(*rp);

        let gp = instance.generate_graphics_pipeline(graphics_pipeline_create_info, pipeline_cache);

        self.pipeline_layout = Some(pipeline_layout);
        self.pipeline = Some(gp[0]);
        self.pipeline_cache = Some(pipeline_cache);
    }
}

impl VkObject for RenderPipeline {
    type Raw = Pipeline;

    fn raw(&self) -> &Self::Raw {
        self.pipeline.as_ref().unwrap()
    }

    fn add_label(&self, ctx: &Vulkan, name: String) {
        #[cfg(debug_assertions)]
        {
            let raw = self.pipeline.unwrap();
            let name = CString::new("test").expect("BNAD");

            let test = "Bester tester";
            let mut name_info = vk::DebugUtilsObjectTagInfoEXT::default()
                .object_handle(raw)
                .tag(test.as_bytes());

            //ctx.add_object_tag(&name_info);
        }
    }
}
