use crate::{VkObject, Vulkan};
use ash::vk;
use ash::vk::{
    AccessFlags, AttachmentDescription, AttachmentLoadOp, AttachmentReference, AttachmentStoreOp,
    ClearColorValue, ClearRect, ClearValue, CommandBuffer, DependencyFlags, Extent2D, Format,
    Framebuffer, ImageAspectFlags, ImageLayout, Offset2D, PipelineBindPoint, PipelineStageFlags,
    Rect2D, RenderPass, SampleCountFlags, SubpassContents, SubpassDependency, SubpassDescription,
    Viewport, SUBPASS_EXTERNAL,
};
use glam::vec4;

pub struct SubpassDescriptor {
    pub pipeline_bind_point: PipelineBindPoint,
}

/// A set of [AttachmentReference] objects related to items being input into a subpass either as an attachment dependency or as an
/// input..
///
/// * `subpass_index` : the index of the subpass the attachment references should be associated with
/// * `refs` : a vector of [AttachmentReference] objects to use as the inputs to the subpass.
/// * `depth_ref` : a [AttachmentReference] representing your depth layer; note that this is optional.
pub struct SubpassReference {
    pub subpass_index: usize,
    pub refs: Vec<AttachmentReference>,
    pub depth_ref: Option<AttachmentReference>,
}

pub struct RenderPass2 {
    attachment_descriptions: Vec<AttachmentDescription>,
    attachment_references: Vec<AttachmentReference>,
    subpass_dependencies: Vec<SubpassDependency>,

    // to avoid lifetime issues, store reference to attachment ref + bind point in separate struct.
    subpass_descriptions: Vec<SubpassDescriptor>,
    subpass_input_attachments: Vec<SubpassReference>,
    subpass_references: Vec<SubpassReference>,

    rp: Option<vk::RenderPass>,

    scissor: Rect2D,
    viewport: Viewport,
    clear_values: Vec<ClearValue>,
    width: u32,
    height: u32,
    is_running: bool,
}

impl RenderPass2 {
    pub fn new() -> Self {
        RenderPass2 {
            is_running: false,
            attachment_descriptions: vec![],
            attachment_references: vec![],
            subpass_descriptions: vec![],
            subpass_dependencies: vec![],
            subpass_input_attachments: vec![],
            subpass_references: vec![],
            rp: None,
            scissor: vk::Rect2D {
                offset: Offset2D { x: 0, y: 0 },
                extent: Extent2D {
                    width: 1280,
                    height: 1280,
                },
            },
            viewport: vk::Viewport {
                x: 0.0,
                y: 0.0,
                width: 1280.0,
                height: 1280.0,
                min_depth: 0.0,
                max_depth: 1.0,
            },
            clear_values: vec![],
            width: 0,
            height: 0,
        }
    }

    pub fn create(width: u32, height: u32) -> Self {
        RenderPass2 {
            is_running: false,
            attachment_descriptions: vec![],
            attachment_references: vec![],
            subpass_descriptions: vec![],
            subpass_dependencies: vec![],
            subpass_input_attachments: vec![],
            subpass_references: vec![],
            rp: None,
            scissor: vk::Rect2D {
                offset: Offset2D { x: 0, y: 0 },
                extent: Extent2D { width, height },
            },
            viewport: vk::Viewport {
                x: 0.0,
                y: 0.0,
                width: width as f32,
                height: height as f32,
                min_depth: 0.0,
                max_depth: 1.0,
            },
            clear_values: vec![],
            width: 0,
            height: 0,
        }
    }
    pub fn clear_attachments(&self, vk: &Vulkan, cb: &CommandBuffer, color: [f32; 4]) {
        vk.clear_attachment(
            *cb,
            vk::ClearAttachment {
                aspect_mask: ImageAspectFlags::COLOR,
                color_attachment: 0,
                clear_value: ClearValue {
                    color: ClearColorValue { float32: color },
                },
            },
            ClearRect {
                rect: Rect2D {
                    offset: Offset2D { x: 0, y: 0 },
                    extent: Extent2D {
                        width: self.viewport.width as u32,
                        height: self.viewport.height as u32,
                    },
                },
                base_array_layer: 0,
                layer_count: 1,
            },
        );
    }

    pub fn num_attachment_refs(&self) -> usize {
        self.attachment_references.len()
    }

    pub fn num_attachments(&self) -> usize {
        self.attachment_descriptions.len()
    }

    pub fn begin(&self, vk: &Vulkan, cb: &CommandBuffer, fb: &Framebuffer) {
        let rp = self.rp.unwrap();

        let renderpass_info = vk::RenderPassBeginInfo::default()
            .render_area(self.scissor)
            .render_pass(rp)
            .clear_values(&self.clear_values[..])
            .framebuffer(*fb);

        vk.set_viewport(*cb, 0, &[self.viewport]);
        vk.set_scissor(*cb, 0, &[self.scissor]);

        vk.start_renderpass(cb, &renderpass_info);
    }

    pub fn next_subpass(&mut self, vk: Vulkan, cb: &CommandBuffer) {
        if self.is_running {
            vk.next_subpass(cb, SubpassContents::INLINE);
        }
    }

    pub fn end(&self, vk: &Vulkan, cb: &CommandBuffer) {
        vk.end_renderpass(cb);
    }

    pub fn viewport(&mut self, x: f32, y: f32, w: f32, h: f32) {
        self.viewport.width = w;
        self.viewport.height = h;
        self.viewport.x = x;
        self.viewport.y = y;

        self.scissor.extent.width = w as u32;
        self.scissor.extent.height = h as u32;
    }
    /// Generates a default subpass dependency that is suitable for most situations
    pub fn generate_default_subpass_dependency() -> SubpassDependency {
        SubpassDependency::default()
            .src_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
            .dst_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
            .src_access_mask(AccessFlags::empty())
            .dst_access_mask(AccessFlags::COLOR_ATTACHMENT_WRITE)
            .dependency_flags(DependencyFlags::BY_REGION)
    }
    pub fn get_attachment_description(&mut self, index: usize) -> &mut AttachmentDescription {
        &mut self.attachment_descriptions[index]
    }

    /// Adds an attachment description to the renderpass.
    pub fn add_attachment_description(&mut self, desc: AttachmentDescription) {
        self.attachment_descriptions.push(desc);
    }

    pub fn add_attachment_descriptions(&mut self, descs: Vec<AttachmentDescription>) {
        for desc in descs {
            self.attachment_descriptions.push(desc);
        }
    }

    pub fn add_subpass_dependency(&mut self, desc: SubpassDependency) {
        self.subpass_dependencies.push(desc);
    }

    pub fn add_subpass_dependencies(&mut self, descs: Vec<SubpassDependency>) {
        for desc in descs {
            self.subpass_dependencies.push(desc);
        }
    }

    pub fn add_subpass_descriptor(&mut self, desc: SubpassDescriptor) {
        self.subpass_descriptions.push(desc);
    }

    pub fn add_subpass_descriptors(&mut self, descs: Vec<SubpassDescriptor>) {
        for desc in descs {
            self.subpass_descriptions.push(desc);
        }
    }

    pub fn add_subpass_references(&mut self, desc: SubpassReference) {
        self.subpass_references.push(desc)
    }

    pub fn add_clear_value(&mut self, val: ClearValue) {
        self.clear_values.push(val)
    }

    /// return the number of clear values currently set.
    pub fn num_clear_values(&self) -> usize {
        self.clear_values.len()
    }

    pub fn add_input_attachment_ref(&mut self, desc: SubpassReference) {
        self.subpass_input_attachments.push(desc);
    }

    /// Compiles the renderpass.
    pub fn compile(&mut self, vk: &Vulkan) {
        self.pre_check();
        let mut subpass_desc = vec![];

        // Build a list of subpass descriptions.
        for i in 0..self.subpass_descriptions.len() {
            let mut desc =
                vk::SubpassDescription::default().pipeline_bind_point(PipelineBindPoint::GRAPHICS);

            // associate any attachment references with the subpass.
            for re in &self.subpass_references {
                if re.subpass_index == i {
                    desc = desc.color_attachments(&re.refs);

                    // if a depth layer is specified, set that up.
                    if re.depth_ref.is_some() {
                        desc = desc.depth_stencil_attachment(re.depth_ref.as_ref().unwrap());
                    }
                }
            }

            // check for inputs - remember when adding inputs there is an index value that refers
            // to the subpass descriptor to add the inputs to.
            for sinput in &self.subpass_input_attachments {
                if i == sinput.subpass_index {
                    desc = desc.input_attachments(&sinput.refs)
                }
            }

            subpass_desc.push(desc)
        }

        let rp_info = vk::RenderPassCreateInfo::default()
            .attachments(&self.attachment_descriptions[..])
            .subpasses(&subpass_desc[..])
            .dependencies(&self.subpass_dependencies[..]);

        let res = vk.generate_renderpass(rp_info);
        if res.is_err() {
            println!("Error creating renderpass");
            return;
        }

        self.rp = Some(res.unwrap());
    }
    /// Checks to ensure that all minimally required values are available.
    fn pre_check(&mut self) {
        if self.subpass_descriptions.is_empty() {
            self.subpass_descriptions.push(SubpassDescriptor {
                pipeline_bind_point: PipelineBindPoint::GRAPHICS,
            })
        }

        // if no attachment descriptions, add default for a color attachment
        if self.attachment_descriptions.is_empty() {
            self.attachment_descriptions.push(AttachmentDescription {
                format: Format::B8G8R8A8_UNORM,
                samples: SampleCountFlags::TYPE_1,
                load_op: vk::AttachmentLoadOp::CLEAR,
                store_op: vk::AttachmentStoreOp::STORE,
                stencil_load_op: AttachmentLoadOp::DONT_CARE,
                stencil_store_op: AttachmentStoreOp::DONT_CARE,
                initial_layout: ImageLayout::UNDEFINED,
                final_layout: vk::ImageLayout::PRESENT_SRC_KHR,
                ..Default::default()
            })
        }
        ///// CLEAR VALUE ////////
        if self.clear_values.is_empty() {
            self.clear_values.push(vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: [0.0, 0.0, 0.0, 1.0],
                },
            })
        }
    }
}

impl VkObject for RenderPass2 {
    type Raw = RenderPass;

    fn raw(&self) -> &Self::Raw {
        self.rp.as_ref().unwrap()
    }

    fn add_label(&self, ctx: &Vulkan, name: String) {
        todo!()
    }
}
