use ash::vk::{
    BlendFactor, BlendOp, Bool32, ColorComponentFlags, CompareOp, CullModeFlags, FrontFace,
    LogicOp, PolygonMode, SampleCountFlags, StencilOp, StencilOpState,
};
///////// GRAPHICS PIPELINE FORMAT /////////////
pub struct GraphicsPipelineFormat {
    // pub pipeline_flags: PipelineRasterizationStateCreateFlags,
    pub depth_clamp_enable: Bool32,
    pub raster_discard_enable: Bool32,
    pub polygon_mode: PolygonMode,
    pub line_width: f32,
    pub cull_mode: CullModeFlags,
    pub front_face: FrontFace,
    pub depth_bias_enable: Bool32,
    pub depth_bias_constant_factor: f32,
    pub depth_bias_clamp: f32,
    pub depth_bias_slope_factor: f32,
    pub logic_op_enable: bool,
    pub logic_op: LogicOp,
    pub blend_constants: [f32; 4],

    //multi sampling
    pub sample_shading_enable: bool,
    pub sample_count_flags: SampleCountFlags,

    // depth stencil
    // pub depth_stencil_flags: PipelineDepthStencilStateCreateFlags,
    pub depth_test_enable: Bool32,
    pub depth_write_enable: Bool32,
    pub depth_compare_op: CompareOp,
    pub depth_bound_testable: Bool32,
    pub stencil_test_enable: Bool32,
    pub stencil_op_state_front: StencilOpState,
    pub stencil_op_state_back: StencilOpState,

    // color
    pub blend_enable: Bool32,
    pub src_color_blend_factor: BlendFactor,
    pub dst_color_blend_factor: BlendFactor,
    pub color_blend_op: BlendOp,
    pub src_alpha_blend_factor: BlendFactor,
    pub dst_alpha_blend_factor: BlendFactor,
    pub alpha_blend_op: BlendOp,

    pub color_write_mask: ColorComponentFlags,
}

impl Default for GraphicsPipelineFormat {
    fn default() -> Self {
        GraphicsPipelineFormat {
            depth_clamp_enable: Bool32::from(false),
            raster_discard_enable: Bool32::from(false),
            polygon_mode: PolygonMode::FILL,
            line_width: 1.0,
            cull_mode: CullModeFlags::NONE,
            front_face: FrontFace::CLOCKWISE,
            depth_bias_enable: Bool32::from(false),
            depth_bias_constant_factor: 0.0,
            depth_bias_clamp: 0.0,
            depth_bias_slope_factor: 0.0,
            logic_op_enable: false,
            logic_op: LogicOp::NO_OP,
            blend_constants: [1.0, 1.0, 1.0, 1.0],
            sample_shading_enable: false,
            sample_count_flags: SampleCountFlags::TYPE_1,
            depth_test_enable: Bool32::from(false),
            depth_write_enable: Bool32::from(false),
            depth_compare_op: CompareOp::LESS_OR_EQUAL,
            depth_bound_testable: Bool32::from(false),
            stencil_test_enable: Bool32::from(false),
            stencil_op_state_front: StencilOpState {
                fail_op: StencilOp::KEEP,
                pass_op: StencilOp::KEEP,
                depth_fail_op: StencilOp::KEEP,
                compare_op: CompareOp::ALWAYS,
                ..Default::default()
            },
            stencil_op_state_back: StencilOpState {
                fail_op: StencilOp::KEEP,
                pass_op: StencilOp::KEEP,
                depth_fail_op: StencilOp::KEEP,
                compare_op: CompareOp::ALWAYS,
                ..Default::default()
            },
            blend_enable: Bool32::from(false),
            src_color_blend_factor: BlendFactor::ZERO,
            dst_color_blend_factor: BlendFactor::ZERO,
            color_blend_op: BlendOp::ADD,
            src_alpha_blend_factor: BlendFactor::ZERO,
            dst_alpha_blend_factor: BlendFactor::ZERO,
            alpha_blend_op: BlendOp::ADD,
            color_write_mask: ColorComponentFlags::R
                | ColorComponentFlags::G
                | ColorComponentFlags::B
                | ColorComponentFlags::A,
        }
    }
}
