use crate::Vulkan;
use ash::vk::{DescriptorSetLayoutBinding, DescriptorType, ShaderStageFlags};

/// A trait containing some functions that all object wrappers ought to enforce.
pub trait VkObject {
    type Raw;

    /// Returns the underlying object of the wrapper, ie for VkSwapChain, it returns it's
    /// vk::SwapchainKHR object.
    fn raw(&self) -> &Self::Raw;

    fn add_label(&self, ctx: &Vulkan, name: String);
}

pub trait Descriptor {
    /// sets the shader stage that this descriptor runs on
    fn set_shader_stage(&mut self, flags: ShaderStageFlags);

    fn get_layout(&self) -> DescriptorSetLayoutBinding;

    /// sets the binding location within the shader.
    fn set_shader_binding(&mut self, binding: u32);

    /// returns the number of descriptors contained in the binding
    fn get_count(&self) -> u32;

    /// returns the type of descriptor
    fn get_type(&self) -> DescriptorType;

    /// returns the shader flags for the descriptor
    fn get_stage_flags(&self) -> ShaderStageFlags;

    //
    fn get_binding(&self) -> u32;
    fn get_dst_array_element(&self) -> u32;

    /// returns the size of the Descriptor
    fn get_range_size(&self) -> u64;

    /// Returns device address of a buffer associated with the descriptor.
    fn get_address(&self) -> u64;
}
