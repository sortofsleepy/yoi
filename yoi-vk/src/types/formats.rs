use crate::framework::databuffer::DataBuffer;
use ash::vk;
use ash::vk::{ImageView, MemoryAllocateFlags};

pub struct FramebufferFormat {
    pub width: u32,
    pub height: u32,
    pub layers: u32,
    pub attachments: Vec<ImageView>,
}

/// Represents general common settings for a Texture / Attachment
#[derive(Copy, Clone)]
pub struct TextureFormat {
    pub texture_format: vk::Format,
    pub extent: vk::Extent3D,
    pub sample_count: vk::SampleCountFlags,
    pub share_mode: vk::SharingMode,
    pub image_type: vk::ImageType,
    pub tiling_mode: vk::ImageTiling,
    pub image_usage_flags: vk::ImageUsageFlags,
    pub initial_layout: vk::ImageLayout,
    pub memory_offset: u32,
    pub mip_levels: u32,
    pub array_layers: u32,
    pub memory_property_flags: vk::MemoryPropertyFlags,
    pub min_filter: vk::Filter,
    pub mag_filter: vk::Filter,

    pub unnormalized_coordinates: vk::Bool32,

    /// sampler settings
    pub mipmap_mode: vk::SamplerMipmapMode,
    pub address_mode_u: vk::SamplerAddressMode,
    pub address_mode_v: vk::SamplerAddressMode,
    pub address_mode_w: vk::SamplerAddressMode,

    pub image_subresource_range: vk::ImageSubresourceRange,
    pub mip_lod_bias: f32,
    pub anisotropy_enable: vk::Bool32,
    pub max_anisotropy: f32,
    pub compare_enable: vk::Bool32,
    pub min_lod: f32,
    pub max_lod: f32,
    pub border_color: vk::BorderColor,
    pub compare_op: vk::CompareOp,
    pub sampler_flags: vk::SamplerCreateFlags,
    pub view_type: vk::ImageViewType,
}

impl Default for TextureFormat {
    fn default() -> Self {
        TextureFormat {
            texture_format: vk::Format::B8G8R8A8_UNORM,
            extent: vk::Extent3D {
                width: 640,
                height: 480,
                depth: 1,
            },
            sample_count: vk::SampleCountFlags::TYPE_1,
            share_mode: vk::SharingMode::EXCLUSIVE,
            image_type: vk::ImageType::TYPE_2D,
            tiling_mode: vk::ImageTiling::LINEAR,
            image_usage_flags: vk::ImageUsageFlags::SAMPLED | vk::ImageUsageFlags::COLOR_ATTACHMENT,
            initial_layout: vk::ImageLayout::PREINITIALIZED,
            memory_offset: 0,
            mip_levels: 1,
            array_layers: 1,
            memory_property_flags: vk::MemoryPropertyFlags::HOST_VISIBLE
                | vk::MemoryPropertyFlags::HOST_COHERENT,
            min_filter: vk::Filter::NEAREST,
            mag_filter: vk::Filter::NEAREST,
            unnormalized_coordinates: vk::Bool32::from(false),
            mipmap_mode: vk::SamplerMipmapMode::NEAREST,
            address_mode_u: vk::SamplerAddressMode::CLAMP_TO_EDGE,
            address_mode_v: vk::SamplerAddressMode::CLAMP_TO_EDGE,
            address_mode_w: vk::SamplerAddressMode::CLAMP_TO_EDGE,
            image_subresource_range: vk::ImageSubresourceRange {
                aspect_mask: vk::ImageAspectFlags::COLOR,
                base_mip_level: 0,
                level_count: 1,
                base_array_layer: 0,
                layer_count: 1,
            },
            mip_lod_bias: 0.0,
            anisotropy_enable: vk::Bool32::from(false),
            max_anisotropy: 16.0,
            compare_enable: vk::Bool32::from(false),
            min_lod: 0.0,
            max_lod: 0.0,
            compare_op: vk::CompareOp::NEVER,
            border_color: vk::BorderColor::FLOAT_OPAQUE_WHITE,
            sampler_flags: vk::SamplerCreateFlags::default(),
            view_type: vk::ImageViewType::TYPE_2D,
        }
    }
}

impl TextureFormat {
    /// sets the texture format for the texture.
    pub fn format(mut self, fmt: vk::Format) -> Self {
        self.texture_format = fmt;
        self
    }

    /// sets the tiling mode for the texture.
    pub fn tiling_mode(mut self, mode: vk::ImageTiling) -> Self {
        self.tiling_mode = mode;
        self
    }

    /// Sets the extent for the texture.
    pub fn dimensions(mut self, width: u32, height: u32) -> Self {
        self.extent.width = width;
        self.extent.height = height;
        self
    }
}

/////////////////////////////////

/// Represents general settings around how to construct a buffer.
/// Provides some commonly used presets via [default] which you can
/// further adjust as necessary.
///
/// Also provides chainable helper functions for common things you might
/// want to change.
#[derive(Copy, Clone)]
pub struct BufferFormat {
    pub size: vk::DeviceSize,
    pub alignment: vk::DeviceSize,
    pub share_mode: vk::SharingMode,
    pub usage_flags: vk::BufferUsageFlags,
    pub create_flags: vk::BufferCreateFlags,
    pub mem_prop_flags: vk::MemoryPropertyFlags,
    pub mem_alloc_flags: vk::MemoryAllocateFlags,
}

impl BufferFormat {
    #[deprecated = "use (size) instead"]
    pub fn set_size(mut self, size: u64) -> Self {
        self.size = size;
        self
    }

    /// Changes the size of the buffer.
    /// Note that the default size is 0 so you will almost
    /// alwayss have to change this
    pub fn size(mut self, size: u64) -> Self {
        self.size = size;
        self
    }

    /// changes buffer usage flags
    pub fn usage_flags(mut self, flags: vk::BufferUsageFlags) -> Self {
        self.usage_flags = flags;
        self
    }

    /// changes the alignment value for the buffer.
    pub fn alignment(mut self, a: vk::DeviceSize) -> Self {
        self.alignment = a;
        self
    }
}

impl Default for BufferFormat {
    fn default() -> Self {
        BufferFormat {
            create_flags: vk::BufferCreateFlags::empty(),
            mem_prop_flags: vk::MemoryPropertyFlags::HOST_VISIBLE
                | vk::MemoryPropertyFlags::HOST_COHERENT,
            size: vk::DeviceSize::from(0 as u64),
            alignment: vk::DeviceSize::from(0 as u64),
            share_mode: vk::SharingMode::EXCLUSIVE,
            usage_flags: vk::BufferUsageFlags::VERTEX_BUFFER | vk::BufferUsageFlags::UNIFORM_BUFFER,
            mem_alloc_flags: MemoryAllocateFlags::empty(),
        }
    }
}

/////////////////////////

pub struct MeshAttribFormat {
    pub data_format: vk::Format,
    pub stride: u32,
    pub offset: u32,
}

/// object that defines a mesh buffer and it's location.
pub struct MeshAttrib {
    pub buffer: DataBuffer,
    pub location: u32,
}

impl Default for MeshAttribFormat {
    fn default() -> Self {
        MeshAttribFormat {
            data_format: vk::Format::R32G32B32A32_SFLOAT,
            stride: 0,
            offset: 0,
        }
    }
}
