use crate::types::formats::TextureFormat;
use crate::{core::Vulkan, VkObject};
use ash::vk;
use std::ffi::CString;

#[derive(Copy, Clone)]
pub struct VkSampler<'a> {
    sampler: Option<vk::Sampler>,
    sampler_info: vk::SamplerCreateInfo<'a>,
}

impl<'a> VkSampler<'a> {
    pub fn new() -> Self {
        let format = TextureFormat::default();

        let sampler_info = vk::SamplerCreateInfo {
            mag_filter: format.mag_filter,
            min_filter: format.min_filter,
            mipmap_mode: format.mipmap_mode,
            address_mode_u: format.address_mode_u,
            address_mode_v: format.address_mode_v,
            address_mode_w: format.address_mode_w,
            mip_lod_bias: format.mip_lod_bias,
            anisotropy_enable: format.anisotropy_enable,
            max_anisotropy: format.max_anisotropy,
            compare_enable: format.compare_enable,
            compare_op: format.compare_op,
            min_lod: format.min_lod,
            max_lod: format.max_lod,
            border_color: format.border_color,
            unnormalized_coordinates: format.unnormalized_coordinates,
            ..Default::default()
        };

        VkSampler {
            sampler: None,
            sampler_info,
        }
    }

    /// Makes a new sampler with repeat wrapping vs default clamp-to-edge.
    pub fn new_repeat_sampler() -> Self {
        let format = TextureFormat::default();

        let sampler_info = vk::SamplerCreateInfo {
            mag_filter: format.mag_filter,
            min_filter: format.min_filter,
            mipmap_mode: format.mipmap_mode,
            address_mode_u: vk::SamplerAddressMode::REPEAT,
            address_mode_v: vk::SamplerAddressMode::REPEAT,
            address_mode_w: vk::SamplerAddressMode::REPEAT,
            mip_lod_bias: format.mip_lod_bias,
            anisotropy_enable: format.anisotropy_enable,
            max_anisotropy: format.max_anisotropy,
            compare_enable: format.compare_enable,
            compare_op: format.compare_op,
            min_lod: format.min_lod,
            max_lod: format.max_lod,
            border_color: format.border_color,
            unnormalized_coordinates: format.unnormalized_coordinates,
            ..Default::default()
        };

        VkSampler {
            sampler: None,
            sampler_info,
        }
    }

    pub fn new_with_format(format: TextureFormat) -> Self {
        let sampler_info = vk::SamplerCreateInfo {
            mag_filter: format.mag_filter,
            min_filter: format.min_filter,
            mipmap_mode: format.mipmap_mode,
            address_mode_u: format.address_mode_u,
            address_mode_v: format.address_mode_v,
            address_mode_w: format.address_mode_w,
            mip_lod_bias: format.mip_lod_bias,
            anisotropy_enable: format.anisotropy_enable,
            max_anisotropy: format.max_anisotropy,
            compare_enable: format.compare_enable,
            compare_op: format.compare_op,
            min_lod: format.min_lod,
            max_lod: format.max_lod,
            border_color: format.border_color,
            unnormalized_coordinates: format.unnormalized_coordinates,
            ..Default::default()
        };

        VkSampler {
            sampler: None,
            sampler_info,
        }
    }

    /// Builds the sampler object.
    pub fn build(&mut self, instance: &Vulkan) {
        let sampler = instance.generate_sampler(self.sampler_info);
        if sampler.is_ok() {
            self.sampler = Some(sampler.unwrap());
        } else {
            panic!("{}", sampler.unwrap_err());
        }
    }
}

impl VkObject for VkSampler<'_> {
    type Raw = vk::Sampler;

    fn raw(&self) -> &Self::Raw {
        self.sampler.as_ref().unwrap()
    }

    fn add_label(&self, ctx: &Vulkan, name: String) {
        #[cfg(debug_assertions)]
        {
            let raw = self.sampler.unwrap();
            let name_info = vk::DebugUtilsObjectNameInfoEXT::default();
            name_info.object_handle(raw);
            let msg = format!("unable to convert {} to c_string for label", name);
            name_info.object_name(CString::new(name).expect(msg.as_str()).as_c_str());

            ctx.add_object_label(&name_info);
        }
    }
}
