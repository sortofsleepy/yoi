use crate::descriptors::descriptor_buffer::VkDescriptorBuffer;
use crate::descriptors::descriptor_set::VkDescriptorSet;
use crate::{DataBuffer, Vulkan};
use ash::vk;
use ash::vk::{
    CommandBuffer, CommandBufferAllocateInfo, CommandBufferBeginInfo, CommandBufferLevel,
    CommandPool, CommandPoolCreateFlags, CommandPoolCreateInfo, DescriptorSetLayout, Pipeline,
    PipelineBindPoint, PipelineCreateFlags, PipelineLayout, PipelineLayoutCreateInfo,
    PipelineShaderStageCreateInfo, PushConstantRange, Semaphore, SemaphoreCreateInfo,
    ShaderStageFlags,
};
use std::mem::size_of;

/// Describes a basic one pass Compute pipeline. Includes the pipeline as well as the buffer associated with the pipeline.
pub struct ComputeStack {
    pub pipeline: ComputePipeline,
    pub buffer: DataBuffer,
}

pub struct ComputePipeline {
    compute_pipeline: Option<Pipeline>,
    dispatch_size: [u32; 3],
    workgroup_count: u32,
    command_pool: CommandPool,
    compute_semaphore: Semaphore,
    pub command_buffers: Vec<CommandBuffer>,
    pipeline_layout: Option<PipelineLayout>,
    push_constants: Vec<PushConstantRange>,
    pipeline_flags: PipelineCreateFlags,
    //descriptor_set: Option<VkDescriptorSet>,
    //layout_create_info: PipelineLayoutCreateInfo,
}

impl ComputePipeline {
    pub fn new(instance: &Vulkan) -> Self {
        /////// INIT COMMAND BUFFERS ///////////
        let info = CommandPoolCreateInfo::default()
            .queue_family_index(instance.get_compute_queue_index() as u32)
            .flags(CommandPoolCreateFlags::RESET_COMMAND_BUFFER);

        let pool = instance.generate_command_pool(info);

        let cb = CommandBufferAllocateInfo {
            command_pool: pool,
            level: CommandBufferLevel::PRIMARY,
            command_buffer_count: 1,
            ..Default::default()
        };

        let buffers = instance.generate_commmand_buffers(cb);

        /////// BUILD SEMAPHORE ///////////
        let semaphore = instance.generate_semaphore(SemaphoreCreateInfo {
            ..Default::default()
        });

        ComputePipeline {
            compute_pipeline: None,
            dispatch_size: [100, 1, 1],
            workgroup_count: 256,
            command_buffers: buffers,
            command_pool: pool,
            compute_semaphore: semaphore,
            pipeline_layout: None,
            push_constants: vec![],
            pipeline_flags: PipelineCreateFlags::default(),
        }
    }
    pub fn new_with_descriptor_buffer(instance: &Vulkan) -> Self {
        /////// INIT COMMAND BUFFERS ///////////
        let info = CommandPoolCreateInfo::default()
            .queue_family_index(instance.get_compute_queue_index() as u32)
            .flags(CommandPoolCreateFlags::RESET_COMMAND_BUFFER);

        let pool = instance.generate_command_pool(info);

        let cb = CommandBufferAllocateInfo {
            command_pool: pool,
            level: CommandBufferLevel::PRIMARY,
            command_buffer_count: 1,
            ..Default::default()
        };

        let buffers = instance.generate_commmand_buffers(cb);

        /////// BUILD SEMAPHORE ///////////
        let semaphore = instance.generate_semaphore(SemaphoreCreateInfo {
            ..Default::default()
        });

        ComputePipeline {
            compute_pipeline: None,
            dispatch_size: [100, 1, 1],
            workgroup_count: 256,
            command_buffers: buffers,
            command_pool: pool,
            compute_semaphore: semaphore,
            pipeline_layout: None,
            push_constants: vec![],
            pipeline_flags: PipelineCreateFlags::DESCRIPTOR_BUFFER_EXT
                | PipelineCreateFlags::default(),
        }
    }

    pub fn get_pipeline_layout(&self) -> &PipelineLayout {
        self.pipeline_layout.as_ref().unwrap()
    }

    pub fn get_semaphore(&self) -> &vk::Semaphore {
        &self.compute_semaphore
    }

    pub fn compile_with_descriptors(
        &mut self,
        instance: &Vulkan,
        shader: PipelineShaderStageCreateInfo,
        descriptors: DescriptorSetLayout,
    ) {
        let binding = [self.compute_semaphore];
        let submit_info = vk::SubmitInfo::default().signal_semaphores(&binding);

        instance.submit_compute(submit_info, vk::Fence::null());
        instance.wait_idle();

        let mut ds_layouts = vec![];
        ds_layouts.push(descriptors);

        let pipeline_create_info = PipelineLayoutCreateInfo::default()
            .push_constant_ranges(&self.push_constants[..])
            .set_layouts(&ds_layouts[..]);

        let pipeline_layout = instance.generate_pipeline_layout(pipeline_create_info);

        let cp_create_info = vk::ComputePipelineCreateInfo::default()
            .layout(pipeline_layout)
            .flags(self.pipeline_flags)
            .stage(shader);

        let pipelines =
            instance.generate_compute_pipeline(cp_create_info, vk::PipelineCache::null());

        self.pipeline_layout = Some(pipeline_layout);
        self.compute_pipeline = Some(pipelines[0]);
    }

    pub fn set_dispatch_size(&mut self, dispatch: [u32; 3]) {
        self.dispatch_size = dispatch;
    }

    pub fn add_push_constant(&mut self, range: PushConstantRange) {
        self.push_constants.push(range);
    }

    pub fn compile(
        &mut self,
        instance: &Vulkan,
        shader: PipelineShaderStageCreateInfo,
        descriptor_layouts: Vec<DescriptorSetLayout>,
    ) {
        let binding = [self.compute_semaphore];
        let submit_info = vk::SubmitInfo::default().signal_semaphores(&binding);

        instance.submit_compute(submit_info, vk::Fence::null());
        instance.wait_idle();

        let pipeline_create_info = vk::PipelineLayoutCreateInfo::default()
            .push_constant_ranges(&self.push_constants[..])
            .set_layouts(&descriptor_layouts[..]);

        let pipeline_layout = instance.generate_pipeline_layout(pipeline_create_info);

        let cp_create_info = vk::ComputePipelineCreateInfo::default()
            .layout(pipeline_layout)
            .flags(self.pipeline_flags)
            .stage(shader);

        let pipelines =
            instance.generate_compute_pipeline(cp_create_info, vk::PipelineCache::null());

        self.pipeline_layout = Some(pipeline_layout);
        self.compute_pipeline = Some(pipelines[0]);
    }

    /// The primary function for running the compute pipeline. Uses internal command buffers and settings to bind and
    /// dispatch the pipeline.
    pub fn run(&self, instance: &Vulkan, descriptor: &VkDescriptorBuffer) {
        let device = instance.get_logical_device();
        let cb = self.command_buffers[0];
        let pipeline = self.compute_pipeline.unwrap();
        let layout = self.pipeline_layout.unwrap();
        let dispatch_size = self.dispatch_size;

        unsafe {
            device
                .begin_command_buffer(cb, &CommandBufferBeginInfo::default())
                .unwrap();
            device.cmd_bind_pipeline(cb, PipelineBindPoint::COMPUTE, pipeline);
        };

        descriptor.bind_descriptors_compute(instance, &cb, &layout);

        unsafe {
            device.cmd_dispatch(cb, dispatch_size[0], dispatch_size[1], dispatch_size[2]);
            device.end_command_buffer(cb).unwrap();
        }
    }

    /// The primary function for running the compute pipeline. Uses internal command buffers and settings to bind and
    /// dispatch the pipeline. Allows for push constants.
    pub fn run_with_push_constants<T>(
        &self,
        instance: &Vulkan,
        descriptor: &VkDescriptorBuffer,
        push_constants: Vec<T>,
    ) {
        let device = instance.get_logical_device();
        let cb = self.command_buffers[0];
        let pipeline = self.compute_pipeline.unwrap();
        let layout = self.pipeline_layout.unwrap();
        let dispatch_size = self.dispatch_size;

        unsafe {
            device
                .begin_command_buffer(cb, &CommandBufferBeginInfo::default())
                .unwrap();
            device.cmd_bind_pipeline(cb, PipelineBindPoint::COMPUTE, pipeline);
        };

        descriptor.bind_descriptors_compute(instance, &cb, &layout);

        unsafe {
            let slice =
                std::slice::from_raw_parts(push_constants.as_ptr() as *const u8, size_of::<T>());
            instance.set_push_constants(&cb, &layout, ShaderStageFlags::COMPUTE, 0, slice);

            device.cmd_dispatch(cb, dispatch_size[0], dispatch_size[1], dispatch_size[2]);
            device.end_command_buffer(cb).unwrap();
        }
    }

    /// The primary function for running the compute pipeline. Uses internal command buffers and settings to bind and
    /// dispatch the pipeline.
    pub fn run_with_descriptor_set(&self, instance: &Vulkan, descriptor: &VkDescriptorSet) {
        let device = instance.get_logical_device();
        let cb = self.command_buffers[0];
        let pipeline = self.compute_pipeline.unwrap();
        let layout = self.pipeline_layout.unwrap();
        let dispatch_size = self.dispatch_size;

        unsafe {
            device
                .begin_command_buffer(cb, &CommandBufferBeginInfo::default())
                .unwrap();
            device.cmd_bind_pipeline(cb, PipelineBindPoint::COMPUTE, pipeline);
        };

        instance.bind_descriptor_set(
            cb,
            vk::PipelineBindPoint::COMPUTE,
            layout,
            0,
            &descriptor.get_descriptors()[..],
            &[],
        );

        unsafe {
            device.cmd_dispatch(cb, dispatch_size[0], dispatch_size[1], dispatch_size[2]);
            device.end_command_buffer(cb).unwrap();
        }
    }

    /// Standard bind function. Doesn't use internal command buffers.
    pub fn bind(&self, instance: &Vulkan, cb: &CommandBuffer) {
        let device = instance.get_logical_device();
        let pipeline = self.compute_pipeline.unwrap();

        unsafe {
            device.cmd_bind_pipeline(*cb, PipelineBindPoint::COMPUTE, pipeline);
        };
    }

    /// Complimentary function to [ComputePipeline::bind]. Dispatches the compute pipeline.
    pub fn dispatch(&self, instance: &Vulkan, cb: &CommandBuffer, dispatch_size: [u32; 3]) {
        let device = instance.get_logical_device();

        unsafe {
            device.cmd_dispatch(*cb, dispatch_size[0], dispatch_size[1], dispatch_size[2]);
        };
    }

    pub fn submit_compute_test(&self, instance: &Vulkan) {
        let binding = [self.command_buffers[0]];
        let submit_info = vk::SubmitInfo::default().command_buffers(&binding);

        instance.submit_compute(submit_info, vk::Fence::null());
        instance.compute_wait_idle();
    }
    pub fn submit_compute(&self, instance: &Vulkan, wait_semaphore: &vk::Semaphore) {
        let wait_binding = [*wait_semaphore];
        let signal_binding = [self.compute_semaphore];
        let buffers = [self.command_buffers[0]];

        let submit_info = vk::SubmitInfo::default()
            .wait_semaphores(&wait_binding)
            .wait_dst_stage_mask(&[vk::PipelineStageFlags::COMPUTE_SHADER])
            .signal_semaphores(&signal_binding)
            .command_buffers(&buffers);

        instance.submit_compute(submit_info, vk::Fence::null());
        instance.compute_wait_idle();

        instance.wait_idle();
    }
}
