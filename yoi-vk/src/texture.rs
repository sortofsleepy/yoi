use crate::render::utils::generate_image_create_info;
use crate::{generate_subresource_range, get_image_memory_allocate_info, TextureFormat, Vulkan};
use ash::util::Align;
use ash::vk;
use ash::vk::{CommandBuffer, Extent3D, ImageLayout};
use image::DynamicImage;
use std::mem::{align_of, size_of};

#[derive(Copy, Clone)]
pub struct VkTexture {
    image_view: Option<vk::ImageView>,
    image: Option<vk::Image>,
    image_memory: Option<vk::DeviceMemory>,
    description: Option<vk::AttachmentDescription>,
    needs_staging: bool,
    // image_memory_barrier: Option<vk::ImageMemoryBarrier>,
    aspect_mask: vk::ImageAspectFlags,
    format: TextureFormat,
    //sampler: Option<VkSampler>,
}

impl VkTexture {
    /// Destroys the texture.
    pub fn destroy(&self, ctx: &Vulkan) {
        ctx.destroy_texture(
            self.image.unwrap(),
            self.image_view.unwrap(),
            self.image_memory.unwrap(),
        );
    }

    /// Creates a texture from a [DynamicImage] object.
    pub fn create_with_image(
        instance: &Vulkan,
        img: &[u8],
        dimensions: [u32; 2],
        format: TextureFormat,
    ) -> Self {
        // load image from file
        let size = dimensions[0] * dimensions[1] * 4;
        let device = instance.get_logical_device();

        let texture_create_info = vk::ImageCreateInfo {
            image_type: format.image_type,
            format: format.texture_format,
            extent: Extent3D {
                width: dimensions[0],
                height: dimensions[1],
                depth: 1,
            },
            mip_levels: format.mip_levels,
            array_layers: format.array_layers,
            samples: format.sample_count,
            tiling: format.tiling_mode,
            usage: format.image_usage_flags,
            sharing_mode: format.share_mode,
            initial_layout: format.initial_layout,
            ..Default::default()
        };

        let image = instance.generate_image(texture_create_info);

        let memory_flags = format.memory_property_flags;
        let attachment_allocate_info =
            get_image_memory_allocate_info(instance, image, memory_flags);

        let memory = instance.allocate_memory(&attachment_allocate_info);

        instance.bind_image_memory(image, memory, 0);

        /////////// BUILD IMAGE VIEW //////////////////
        let view_subresource = generate_subresource_range(false);

        let view_create_info = vk::ImageViewCreateInfo::default()
            .format(format.texture_format)
            .subresource_range(view_subresource)
            .image(image)
            .view_type(vk::ImageViewType::TYPE_2D);

        let image_view = instance.generate_image_view(view_create_info);

        //// WRITE IMAGE INFO //////
        let ptr = unsafe {
            device
                .map_memory(memory, 0, size as u64, vk::MemoryMapFlags::empty())
                .unwrap()
        };

        unsafe {
            let mut slice = Align::new(ptr, align_of::<u8>() as u64, size as u64);
            slice.copy_from_slice(img);

            device.unmap_memory(memory);
        };

        VkTexture {
            image_view: Some(image_view.unwrap()),
            image: Some(image),
            image_memory: Some(memory),
            description: None,
            needs_staging: false,
            //image_memory_barrier: None,
            aspect_mask: Default::default(),
            //sampler: None,
            format,
        }
    }

    pub fn new(instance: &Vulkan, format: TextureFormat) -> Self {
        let mut texture_create_info = generate_image_create_info(&format);

        let image = instance.generate_image(texture_create_info);

        let memory_flags = format.memory_property_flags;
        let attachment_allocate_info =
            get_image_memory_allocate_info(instance, image, memory_flags);

        let memory = instance.allocate_memory(&attachment_allocate_info);

        instance.bind_image_memory(image, memory, 0);

        /////////// BUILD IMAGE VIEW //////////////////
        let view_subresource = vk::ImageSubresourceRange::default()
            .aspect_mask(format.image_subresource_range.aspect_mask)
            .base_mip_level(0)
            .level_count(1)
            .base_array_layer(0)
            .layer_count(1);

        let view_create_info = vk::ImageViewCreateInfo::default()
            .format(format.texture_format)
            .subresource_range(view_subresource)
            .image(image)
            .view_type(vk::ImageViewType::TYPE_2D);

        let image_view = instance.generate_image_view(view_create_info);

        VkTexture {
            image_view: Some(image_view.unwrap()),
            image: Some(image),
            image_memory: Some(memory),
            description: None,
            needs_staging: false,
            //image_memory_barrier: None,
            aspect_mask: Default::default(),
            //sampler: None,
            format,
        }
    }
    /// Returns the format for the texture.
    pub fn get_format(&self) -> vk::Format {
        self.format.texture_format
    }

    /// Sets some data on the texture.
    /// Note that this is an unsafe operation.
    pub fn set_data<T: std::marker::Copy>(
        &mut self,
        app: &Vulkan,
        cb: &vk::CommandBuffer,
        data: &[T],
    ) {
        if self.image.is_none() || self.image_view.is_none() {
            println!("Unable to set data - you may not have initialized the texture yet by calling initiailize_empty_image");
            return;
        }

        let memory = self.image_memory.unwrap();
        let device = app.get_logical_device();
        let size = size_of::<T>() * data.len();

        //// WRITE IMAGE INFO //////
        unsafe {
            let ptr = device
                .map_memory(memory, 0, size as u64, vk::MemoryMapFlags::empty())
                .unwrap();
            let mut slice = Align::new(ptr, align_of::<T>() as u64, size as u64);
            slice.copy_from_slice(data);
            device.unmap_memory(memory);
        };

        self.set_image_layout(cb, app);
    }

    pub fn initialize_empty_image(&mut self, instance: &Vulkan, width: u32, height: u32) {
        // load image from file
        let dimensions = [width, height];
        let device = instance.get_logical_device();

        self.format.extent.width = dimensions[0];
        self.format.extent.height = dimensions[1];

        if self.needs_staging {
            todo!()
        }

        let texture_create_info = vk::ImageCreateInfo {
            image_type: self.format.image_type,
            format: self.format.texture_format,
            extent: self.format.extent,
            mip_levels: self.format.mip_levels,
            array_layers: self.format.array_layers,
            samples: self.format.sample_count,
            tiling: self.format.tiling_mode,
            usage: self.format.image_usage_flags,
            sharing_mode: self.format.share_mode,
            initial_layout: self.format.initial_layout,
            ..Default::default()
        };

        let image = unsafe { device.create_image(&texture_create_info, None).unwrap() };

        /////////// BUILD MEMORY //////////////////

        let memory_flags =
            self.format.memory_property_flags | vk::MemoryPropertyFlags::DEVICE_LOCAL;
        let attachment_allocate_info =
            get_image_memory_allocate_info(instance, image, memory_flags);

        let memory = unsafe {
            device
                .allocate_memory(&attachment_allocate_info, None)
                .unwrap()
        };

        unsafe {
            device
                .bind_image_memory(image, memory, 0)
                .expect("unable to bind image memory");
        }
        /////////// BUILD IMAGE VIEW //////////////////
        let view_subresource = generate_subresource_range(false);

        let view_create_info = vk::ImageViewCreateInfo {
            view_type: vk::ImageViewType::TYPE_2D,
            format: self.format.texture_format,
            subresource_range: view_subresource,
            image,
            ..Default::default()
        };

        let image_view = unsafe { device.create_image_view(&view_create_info, None).unwrap() };

        self.image = Some(image);
        self.image_view = Some(image_view);
    }

    /// Returns the image view for the texture.
    pub fn get_image_view(&self) -> &vk::ImageView {
        self.image_view.as_ref().unwrap()
    }

    /// returns the image for the texture.
    pub fn get_image(&self) -> &vk::Image {
        self.image.as_ref().unwrap()
    }

    /// loads an image into a texture.
    pub fn load_image(&mut self, img: DynamicImage, instance: &Vulkan) {
        // load image from file
        let dimensions = [img.width(), img.height()];
        let size = dimensions[0] * dimensions[1] * 4;
        let device = instance.get_logical_device();

        self.format.extent.width = dimensions[0];
        self.format.extent.height = dimensions[1];

        if self.needs_staging {
            todo!()
        }

        let texture_create_info = vk::ImageCreateInfo {
            image_type: self.format.image_type,
            format: self.format.texture_format,
            extent: self.format.extent,
            mip_levels: self.format.mip_levels,
            array_layers: self.format.array_layers,
            samples: self.format.sample_count,
            tiling: self.format.tiling_mode,
            usage: self.format.image_usage_flags,
            sharing_mode: self.format.share_mode,
            initial_layout: self.format.initial_layout,
            ..Default::default()
        };

        let image = unsafe { device.create_image(&texture_create_info, None).unwrap() };

        /////////// BUILD MEMORY //////////////////

        let memory_flags =
            self.format.memory_property_flags | vk::MemoryPropertyFlags::DEVICE_LOCAL;
        let attachment_allocate_info =
            get_image_memory_allocate_info(instance, image, memory_flags);

        let memory = unsafe {
            device
                .allocate_memory(&attachment_allocate_info, None)
                .unwrap()
        };

        unsafe {
            device
                .bind_image_memory(image, memory, 0)
                .expect("unable to bind image memory");
        }
        /////////// BUILD IMAGE VIEW //////////////////
        let view_subresource = generate_subresource_range(false);

        let view_create_info = vk::ImageViewCreateInfo {
            view_type: vk::ImageViewType::TYPE_2D,
            format: self.format.texture_format,
            subresource_range: view_subresource,
            image,
            ..Default::default()
        };

        let image_view = unsafe { device.create_image_view(&view_create_info, None).unwrap() };

        //// WRITE IMAGE INFO //////
        let ptr = unsafe {
            device
                .map_memory(memory, 0, size as u64, vk::MemoryMapFlags::empty())
                .unwrap()
        };
        unsafe {
            let mut slice = Align::new(ptr, align_of::<u8>() as u64, size as u64);

            // TODO should make extraction value adjustable depending on image.
            slice.copy_from_slice(img.into_rgba8().as_raw());

            device.unmap_memory(memory);
        };

        self.image_memory = Some(memory);
        self.image = Some(image);
        self.image_view = Some(image_view);
    }

    pub fn set_image_layout(&mut self, cb: &CommandBuffer, instance: &Vulkan) {
        let img = self.image.unwrap();
        let fmt = self.format.texture_format;
        let layout = self.format.initial_layout;

        self.set_image_layout_detail(
            instance,
            *cb,
            img,
            fmt,
            layout,
            vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
        );
    }
    pub fn update_image_layout(
        &mut self,
        cb: &CommandBuffer,
        instance: &Vulkan,
        new_layout: ImageLayout,
    ) {
        let img = self.image.unwrap();
        let fmt = self.format.texture_format;
        let old_layout = self.format.initial_layout;

        self.set_image_layout_detail(instance, *cb, img, fmt, old_layout, new_layout);
    }
    pub fn get_memory(&self) -> &vk::DeviceMemory {
        self.image_memory.as_ref().unwrap()
    }

    /// sets up the image layout.
    pub fn set_image_layout_detail(
        &mut self,
        instance: &Vulkan,
        cb: CommandBuffer,
        img: vk::Image,
        format: vk::Format,
        old_layout: vk::ImageLayout,
        new_layout: vk::ImageLayout,
    ) {
        //// SETUP SOURCE MASK  //////

        let mut source_access_mask = vk::AccessFlags::default();
        match old_layout {
            vk::ImageLayout::TRANSFER_DST_OPTIMAL => {
                source_access_mask = vk::AccessFlags::TRANSFER_WRITE
            }
            vk::ImageLayout::PREINITIALIZED => {
                source_access_mask = vk::AccessFlags::HOST_WRITE;
            }
            vk::ImageLayout::GENERAL => {}
            vk::ImageLayout::UNDEFINED => {}
            _ => {}
        }

        //// SETUP PIPELINE STAGE FLAGS //////
        let mut source_stage = vk::PipelineStageFlags::default();

        match old_layout {
            vk::ImageLayout::GENERAL => source_stage = vk::PipelineStageFlags::HOST,
            vk::ImageLayout::PREINITIALIZED => source_stage = vk::PipelineStageFlags::HOST,

            vk::ImageLayout::TRANSFER_DST_OPTIMAL => {
                source_stage = vk::PipelineStageFlags::TRANSFER;
            }

            vk::ImageLayout::UNDEFINED => {
                source_stage = vk::PipelineStageFlags::TOP_OF_PIPE;
            }
            _ => {}
        }

        ////// SETUP DESTINATION MASK ///////////
        let mut destination_access_mask = vk::AccessFlags::default();

        match new_layout {
            vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL => {
                destination_access_mask = vk::AccessFlags::COLOR_ATTACHMENT_WRITE;
            }

            vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL => {
                destination_access_mask = vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_READ
                    | vk::AccessFlags::DEPTH_STENCIL_ATTACHMENT_WRITE;
            }

            vk::ImageLayout::GENERAL
            | vk::ImageLayout::PRESENT_SRC_KHR
            | vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL => {
                destination_access_mask = vk::AccessFlags::SHADER_READ;
            }

            vk::ImageLayout::TRANSFER_SRC_OPTIMAL => {
                destination_access_mask = vk::AccessFlags::TRANSFER_READ;
            }

            vk::ImageLayout::TRANSFER_DST_OPTIMAL => {
                destination_access_mask = vk::AccessFlags::TRANSFER_WRITE;
            }

            _ => {}
        }

        ///////// SETUP DESTINATION STAGE ////////////
        let mut destination_stage = vk::PipelineStageFlags::default();

        match new_layout {
            vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL => {
                destination_stage = vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT;
            }

            vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL => {
                destination_stage = vk::PipelineStageFlags::EARLY_FRAGMENT_TESTS;
            }

            vk::ImageLayout::GENERAL => {
                destination_stage = vk::PipelineStageFlags::FRAGMENT_SHADER;
            }
            vk::ImageLayout::PRESENT_SRC_KHR => {
                destination_stage = vk::PipelineStageFlags::BOTTOM_OF_PIPE;
            }

            vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL => {
                destination_stage = vk::PipelineStageFlags::FRAGMENT_SHADER;
            }

            vk::ImageLayout::TRANSFER_DST_OPTIMAL | vk::ImageLayout::TRANSFER_SRC_OPTIMAL => {
                destination_stage = vk::PipelineStageFlags::TRANSFER;
            }

            _ => {}
        }

        let mut aspect_mask = vk::ImageAspectFlags::default();

        if new_layout == vk::ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL {
            aspect_mask = vk::ImageAspectFlags::DEPTH;

            if format == vk::Format::D32_SFLOAT_S8_UINT || format == vk::Format::D24_UNORM_S8_UINT {
                aspect_mask |= vk::ImageAspectFlags::STENCIL;
            }
        } else {
            aspect_mask = vk::ImageAspectFlags::COLOR;
        }

        let subresource_range = vk::ImageSubresourceRange {
            aspect_mask,
            base_mip_level: 0,
            level_count: 1,
            base_array_layer: 0,
            layer_count: 1,
        };

        let memory_barrier = vk::ImageMemoryBarrier {
            src_access_mask: source_access_mask,
            dst_access_mask: destination_access_mask,
            old_layout,
            new_layout,
            image: img,
            subresource_range,
            ..Default::default()
        };

        instance.onetime_pipeline_barrier(
            cb,
            source_stage,
            destination_stage,
            vk::DependencyFlags::empty(),
            &[],
            &[],
            &[memory_barrier],
        );
    }
}
