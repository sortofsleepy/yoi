use crate::core::Vulkan;
use crate::descriptors::storage_descriptor::StorageDescriptor;
use crate::descriptors::uniform_descriptor::UniformDescriptor;
use crate::types::traits::Descriptor;
use ash::vk::{
    CommandBuffer, DescriptorPool, DescriptorPoolCreateInfo, DescriptorPoolSize, DescriptorSet,
    DescriptorSetAllocateInfo, DescriptorSetLayout, DescriptorSetLayoutBinding,
    DescriptorSetLayoutCreateInfo, DescriptorType, PipelineBindPoint, PipelineLayout,
    WriteDescriptorSet,
};

pub struct VkDescriptorSet {
    /// the maximum number of descriptor sets that can be derived from the descriptor pool
    max_sets: u32,

    /// pool sizes for deriving descriptor sets
    pool_sizes: Vec<DescriptorPoolSize>,

    /// layout bindings for all the descriptors this set will use.
    //layout_bindings: Vec<DescriptorSetLayoutBinding<'a>>,

    /// descriptor layouts
    descriptor_layout: Option<DescriptorSetLayout>,

    /// descriptor pool
    descriptor_pool: Option<DescriptorPool>,

    /// descriptor sets
    descriptor_set: Option<Vec<DescriptorSet>>,

    //acceleration_descriptors: Vec<RayTraceAccelerationDescriptor>,
    //storage_image_descriptors: Vec<StorageImageDescriptor>,
    storage_descriptors: Vec<StorageDescriptor>,
    uniform_descriptors: Vec<UniformDescriptor>,
    //texture_descriptors: Vec<TextureSamplerDescriptor>,
    //dynamic_descriptors: Vec<DynamicUniformDescriptor>,
}

impl VkDescriptorSet {
    pub fn new(max_sets: u32) -> Self {
        VkDescriptorSet {
            pool_sizes: vec![],
            descriptor_layout: None,
            descriptor_pool: None,
            descriptor_set: None,
            uniform_descriptors: vec![],
            //  layout_bindings: vec![],
            //acceleration_descriptors: vec![],
            //storage_image_descriptors: vec![],
            //texture_descriptors: vec![],
            storage_descriptors: vec![],
            //dynamic_descriptors: vec![],
            max_sets,
        }
    }

    /// add descriptor set for uniform buffers.
    pub fn add_uniform_buffer_descriptor(&mut self, desc: UniformDescriptor) {
        self.pool_sizes.push(
            DescriptorPoolSize::default()
                .ty(DescriptorType::UNIFORM_BUFFER)
                .descriptor_count(1),
        );

        /*
         self.layout_bindings.push(  DescriptorSetLayoutBinding {
            binding: desc.get_binding(),
            descriptor_type: desc.get_type(),
            descriptor_count: desc.get_count(),
            stage_flags: desc.get_stage_flags(),
            ..Default::default()
        });
         */
        self.uniform_descriptors.push(desc);
    }

    /// Adds descriptor set for storage buffers
    pub fn add_storage_buffer_descriptor(&mut self, desc: StorageDescriptor) {
        self.pool_sizes.push(
            DescriptorPoolSize::default()
                .ty(DescriptorType::STORAGE_BUFFER)
                .descriptor_count(1),
        );

        self.storage_descriptors.push(desc);
    }

    /// Builds the descriptor set
    pub fn build(&mut self, instance: &Vulkan) {
        let pool_info = DescriptorPoolCreateInfo::default()
            .max_sets(self.max_sets)
            .pool_sizes(&self.pool_sizes[..]);

        // build a list of layout bindings based on all available descriptors
        let mut layout_bindings = vec![];
        let uni = self.get_uniform_layout_bindings();
        let store = self.get_storage_layout_bindings();

        layout_bindings.extend(uni);
        layout_bindings.extend(store);

        // build descriptor set layout.
        let descriptor_layout_create_info =
            DescriptorSetLayoutCreateInfo::default().bindings(&layout_bindings[..]);

        let layout = instance
            .generate_descriptor_layout(descriptor_layout_create_info, None)
            .expect("Unable to unwrap descriptor set layout");

        let descriptor_layout = [layout];

        let descriptor_pool = instance.generate_descriptor_pool(pool_info, None);
        let descriptor_allocate = DescriptorSetAllocateInfo::default()
            .descriptor_pool(descriptor_pool)
            .set_layouts(&descriptor_layout);

        let descriptor_sets = instance.generate_descriptor(descriptor_allocate);

        self.descriptor_layout = Some(descriptor_layout[0]);
        self.descriptor_set = Some(descriptor_sets);

        // make sure descriptor information gets written.
        self.update_descriptors(instance);
    }

    pub fn bind_descriptors(
        &self,
        instance: &Vulkan,
        cb: &CommandBuffer,
        pipeline_layout: PipelineLayout,
        first_set: u32,
    ) {
        if self.descriptor_set.is_some() {
            let ds = self.descriptor_set.as_ref().unwrap();
            instance.bind_descriptor_set(
                *cb,
                PipelineBindPoint::GRAPHICS,
                pipeline_layout,
                first_set,
                &ds[..],
                &[],
            )
        }
    }

    pub fn get_descriptor_layout(&self) -> &DescriptorSetLayout {
        self.descriptor_layout.as_ref().unwrap()
    }

    /// alias for get_descriptors
    pub fn get_descriptors(&self) -> &Vec<DescriptorSet> {
        self.descriptor_set.as_ref().unwrap()
    }

    pub fn update_descriptors(&self, instance: &Vulkan) {
        for ds in self.descriptor_set.as_ref().unwrap().iter() {
            let mut write_sets = vec![];
            // apply uniform descriptors if present
            if !self.uniform_descriptors.is_empty() {
                for desc in self.uniform_descriptors.iter() {
                    let write_data = WriteDescriptorSet {
                        dst_set: *ds,
                        descriptor_count: 1,
                        dst_binding: desc.get_binding(),
                        descriptor_type: DescriptorType::UNIFORM_BUFFER,
                        p_buffer_info: desc.get_buffer_info(),
                        ..Default::default()
                    };

                    write_sets.push(write_data);
                }
            }

            // apply storage descriptors if present
            if !self.storage_descriptors.is_empty() {
                for store in &self.storage_descriptors {
                    // TODO don't know why but writing it out like this appears to be more correct than using a builder object
                    let write_data = WriteDescriptorSet {
                        dst_set: *ds,
                        descriptor_count: 1,
                        dst_binding: store.get_binding(),
                        descriptor_type: DescriptorType::STORAGE_BUFFER,
                        p_buffer_info: store.get_buffer_info(),
                        ..Default::default()
                    };
                    write_sets.push(write_data);
                }
            }

            // TODO add dynamic descriptors
            instance.update_descriptor_sets(write_sets);
        }
    }

    /// Grabs all of the descriptor layout bindings for uniform descriptors.
    fn get_uniform_layout_bindings(&self) -> Vec<DescriptorSetLayoutBinding> {
        let mut bindings = vec![];
        for uni in &self.uniform_descriptors {
            bindings.push(uni.get_layout());
        }

        bindings
    }

    /// Grabs all of the descriptor layout bindings for uniform descriptors.
    fn get_storage_layout_bindings(&self) -> Vec<DescriptorSetLayoutBinding> {
        let mut bindings = vec![];
        for uni in &self.storage_descriptors {
            bindings.push(uni.get_layout());
        }

        bindings
    }
}
