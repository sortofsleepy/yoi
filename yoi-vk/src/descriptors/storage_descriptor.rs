use crate::types::traits::Descriptor;
use crate::Vulkan;
use ash::vk::{
    Buffer, DescriptorBufferInfo, DescriptorSetLayoutBinding, DescriptorType, DeviceSize,
    ShaderStageFlags,
};

////////// STORAGE DESCRIPTOR ///////////////
#[derive(Copy, Clone)]
pub struct StorageDescriptor {
    buffer_address: u64,
    binding: u32,
    buffer_info: DescriptorBufferInfo,
    dst_array_element: u32,
    stage_flags: ShaderStageFlags,
    descriptor_type: DescriptorType,
    descriptor_count: u32,
}

impl StorageDescriptor {
    pub fn new() -> Self {
        StorageDescriptor {
            buffer_address: 0,
            binding: 0,
            dst_array_element: 0,
            descriptor_count: 1,
            descriptor_type: DescriptorType::STORAGE_BUFFER,
            buffer_info: DescriptorBufferInfo {
                buffer: Default::default(),
                offset: 0,
                range: 0,
            },
            stage_flags: ShaderStageFlags::COMPUTE,
        }
    }

    pub fn new_with_layout(layout: DescriptorSetLayoutBinding) -> Self {
        StorageDescriptor {
            binding: 0,
            buffer_address: 0,
            dst_array_element: 0,
            descriptor_count: 1,
            descriptor_type: DescriptorType::STORAGE_BUFFER,
            buffer_info: DescriptorBufferInfo {
                buffer: Default::default(),
                offset: 0,
                range: 0,
            },

            stage_flags: ShaderStageFlags::COMPUTE,
        }
    }

    /// Returns device address of the buffer.
    pub fn get_address(&self) -> u64 {
        self.buffer_address
    }

    pub fn set_buffer_data_with_address(
        &mut self,
        ctx: &Vulkan,
        buffer: &Buffer,
        offset: DeviceSize,
        range: DeviceSize,
    ) {
        self.buffer_address = ctx.get_buffer_device_address(buffer);
        self.buffer_info.buffer = *buffer;
        self.buffer_info.offset = offset;
        self.buffer_info.range = range;
    }

    /// returns the buffer information for the descriptor
    pub fn get_buffer_info(&self) -> &DescriptorBufferInfo {
        &self.buffer_info
    }

    pub fn set_buffer(&mut self, buffer: &Buffer, offset: DeviceSize, range: DeviceSize) {
        self.buffer_info.buffer = *buffer;
        self.buffer_info.offset = offset;
        self.buffer_info.range = range;
    }

    pub fn set_buffer_debug(&mut self, buffer: &Buffer, offset: DeviceSize, range: DeviceSize) {
        self.buffer_info.buffer = *buffer;
        self.buffer_info.offset = offset;
        self.buffer_info.range = range;
    }
}

impl Descriptor for StorageDescriptor {
    /// sets the shader stage to use with the descriptor
    fn set_shader_stage(&mut self, flags: ShaderStageFlags) {
        self.stage_flags = flags;
    }

    fn set_shader_binding(&mut self, binding: u32) {
        self.binding = binding
    }
    fn get_count(&self) -> u32 {
        self.descriptor_count
    }
    fn get_type(&self) -> DescriptorType {
        self.descriptor_type
    }

    fn get_layout(&self) -> DescriptorSetLayoutBinding {
        DescriptorSetLayoutBinding {
            binding: self.binding,
            descriptor_type: self.descriptor_type,
            descriptor_count: self.descriptor_count,
            stage_flags: self.stage_flags,
            ..Default::default()
        }
    }

    fn get_address(&self) -> u64 {
        self.buffer_address
    }
    fn get_binding(&self) -> u32 {
        self.binding
    }
    fn get_dst_array_element(&self) -> u32 {
        self.dst_array_element
    }

    fn get_range_size(&self) -> u64 {
        self.buffer_info.range
    }

    fn get_stage_flags(&self) -> ShaderStageFlags {
        self.stage_flags
    }
}
