use ash::vk::{
    AttachmentLoadOp, AttachmentStoreOp, ColorSpaceKHR, Extent3D, Flags, Format, ImageLayout,
    SampleCountFlags,
};

// defines some common basic settings for use when creating
// render passes
// TODO add samples parameter
pub struct RenderPassDescriptor {
    pub attachment_dimensions: Extent3D,

    /// color settings //
    pub flags: Flags,
    pub flag_bits: SampleCountFlags,
    pub load_op: AttachmentLoadOp,
    pub store_op: AttachmentStoreOp,
    pub stencil_load_op: AttachmentLoadOp,
    pub stencil_store_op: AttachmentStoreOp,
    pub color_initial_layout: ImageLayout,
    pub color_final_layout: ImageLayout,
    pub color_format: Format,
    pub color_space: ColorSpaceKHR,

    /// depth settings ///
    pub depth_format: Format,
    pub depth_flag_bits: SampleCountFlags,
    pub depth_load_op: AttachmentLoadOp,
    pub depth_store_op: AttachmentStoreOp,
    pub depth_stencil_load_op: AttachmentLoadOp,
    pub depth_stencil_store_op: AttachmentStoreOp,
    pub depth_initial_layout: ImageLayout,
    pub depth_final_layout: ImageLayout,
}

impl RenderPassDescriptor {
    /// initializes a default set of settings suitable for a depth image.
    pub fn default_depth_settings() -> RenderPassDescriptor {
        RenderPassDescriptor {
            attachment_dimensions: Default::default(),
            flags: 0,
            flag_bits: SampleCountFlags::TYPE_1,
            load_op: AttachmentLoadOp::CLEAR,
            store_op: AttachmentStoreOp::STORE,
            stencil_load_op: AttachmentLoadOp::DONT_CARE,
            stencil_store_op: AttachmentStoreOp::DONT_CARE,
            color_initial_layout: ImageLayout::UNDEFINED,
            color_final_layout: ImageLayout::PRESENT_SRC_KHR,
            color_format: Format::D16_UNORM,
            color_space: ColorSpaceKHR::SRGB_NONLINEAR,
            depth_format: Format::D16_UNORM,
            depth_flag_bits: SampleCountFlags::TYPE_1,
            depth_load_op: AttachmentLoadOp::CLEAR,
            depth_store_op: AttachmentStoreOp::DONT_CARE,
            depth_stencil_load_op: AttachmentLoadOp::DONT_CARE,
            depth_stencil_store_op: AttachmentStoreOp::DONT_CARE,
            depth_initial_layout: ImageLayout::UNDEFINED,
            depth_final_layout: ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        }
    }
}

impl Default for RenderPassDescriptor {
    fn default() -> Self {
        RenderPassDescriptor {
            attachment_dimensions: Default::default(),
            flags: 0,
            flag_bits: SampleCountFlags::TYPE_1,
            load_op: AttachmentLoadOp::CLEAR,
            store_op: AttachmentStoreOp::STORE,
            stencil_load_op: AttachmentLoadOp::DONT_CARE,
            stencil_store_op: AttachmentStoreOp::DONT_CARE,
            color_initial_layout: ImageLayout::UNDEFINED,
            color_final_layout: ImageLayout::PRESENT_SRC_KHR,
            color_format: Format::B8G8R8A8_UNORM,
            color_space: ColorSpaceKHR::SRGB_NONLINEAR,
            depth_format: Format::D16_UNORM,
            depth_flag_bits: SampleCountFlags::TYPE_1,
            depth_load_op: AttachmentLoadOp::CLEAR,
            depth_store_op: AttachmentStoreOp::DONT_CARE,
            depth_stencil_load_op: AttachmentLoadOp::DONT_CARE,
            depth_stencil_store_op: AttachmentStoreOp::DONT_CARE,
            depth_initial_layout: ImageLayout::UNDEFINED,
            depth_final_layout: ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        }
    }
}
