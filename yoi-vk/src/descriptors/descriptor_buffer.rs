use crate::descriptors::storage_descriptor::StorageDescriptor;
use crate::descriptors::texture_sampler_descriptor::TextureSamplerDescriptor;
use crate::descriptors::uniform_descriptor::UniformDescriptor;
use crate::types::formats::BufferFormat;
use crate::types::traits::Descriptor;
use crate::{DataBuffer, VkObject, Vulkan};
use ash::vk::{
    BufferUsageFlags, CommandBuffer, DescriptorAddressInfoEXT, DescriptorBindingFlags,
    DescriptorBufferBindingInfoEXT, DescriptorDataEXT, DescriptorGetInfoEXT, DescriptorSetLayout,
    DescriptorSetLayoutBinding, DescriptorSetLayoutCreateFlags, DescriptorSetLayoutCreateInfo,
    DescriptorType, Format, MemoryAllocateFlags, MemoryPropertyFlags,
    PhysicalDeviceDescriptorBufferPropertiesEXT, PhysicalDeviceProperties2KHR, PipelineBindPoint,
    PipelineLayout,
};
use std::slice;

/// storage type for a Uniform resource when using descriptor buffers.
/// Stores the buffer itself + it's address.
#[derive(Clone)]
struct UniformResource {
    pub buffer: DataBuffer,
    pub address: u64,
}

#[derive(Clone)]
pub struct VkDescriptorBuffer {
    /// descriptor layouts
    descriptor_layout: Option<DescriptorSetLayout>,

    /// The uniform descriptors added to the overall set
    uniform_descriptors: Vec<UniformDescriptor>,

    /// Storage descriptors to add
    storage_descriptors: Vec<StorageDescriptor>,

    /// Texture descriptors to add .
    texture_descriptors: Vec<TextureSamplerDescriptor>,

    /// Stores any flags needed to create the buffer
    flags: Vec<DescriptorBindingFlags>,

    /// Vec that stores buffers that contain the descriptor data
    layout_buffers: Vec<UniformResource>,

    uniform_buffer_descriptor_size: usize,
    storage_buffer_descriptor_size: usize,
    combined_image_sampler_descriptor_size: usize,
    buffer_offset_alignment: u64,
}

impl VkDescriptorBuffer {
    pub fn new() -> Self {
        VkDescriptorBuffer {
            descriptor_layout: None,
            uniform_descriptors: vec![],
            texture_descriptors: vec![],
            flags: vec![],
            layout_buffers: vec![],
            storage_descriptors: vec![],
            buffer_offset_alignment: 0,
            uniform_buffer_descriptor_size: 0,
            storage_buffer_descriptor_size: 0,
            combined_image_sampler_descriptor_size: 0,
        }
    }

    /// Returns whether or not there are descriptors that are a part of the set.
    pub fn has_descriptors(&self) -> bool {
        if !self.uniform_descriptors.is_empty() {
            return true;
        }
        return false;
    }

    /// Adds a Bindless Uniform buffer descriptor.
    pub fn add_uniform_buffer_descriptor(&mut self, desc: UniformDescriptor) {
        self.uniform_descriptors.push(desc);
    }

    pub fn add_texture_descriptor(&mut self, desc: TextureSamplerDescriptor) {
        self.texture_descriptors.push(desc)
    }

    /// Adds a storage descriptor.
    pub fn add_storage_descriptor(&mut self, desc: StorageDescriptor) {
        self.storage_descriptors.push(desc);
    }

    /// Binds the descriptors
    pub fn bind_descriptors(
        &self,
        instance: &Vulkan,
        command_buffer: &CommandBuffer,
        pipeline_layout: &PipelineLayout,
    ) {
        // Bind uniform layout buffers first.
        let mut i = 0;
        //let alignment = self.buffer_offset_alignment;

        for buffer in &self.layout_buffers {
            let mut binding_infos = vec![];

            let uniform_binding_info = DescriptorBufferBindingInfoEXT::default()
                .address(buffer.address)
                .usage(BufferUsageFlags::RESOURCE_DESCRIPTOR_BUFFER_EXT);

            binding_infos.push(uniform_binding_info);

            //let offset = i * alignment;
            instance.bind_descriptor_buffers(command_buffer, &binding_infos[..]);
            instance.set_descriptor_buffer_offset(
                *command_buffer,
                PipelineBindPoint::GRAPHICS,
                *pipeline_layout,
                0,
                &[0],
                &[0],
            );

            i += 1;
        }
    }

    pub fn bind_descriptors_compute(
        &self,
        instance: &Vulkan,
        command_buffer: &CommandBuffer,
        pipeline_layout: &PipelineLayout,
    ) {
        // Bind uniform layout buffers first.
        let mut i = 0;
        let alignment = self.buffer_offset_alignment;

        for buffer in &self.layout_buffers {
            let mut binding_infos = vec![];

            let uniform_binding_info = DescriptorBufferBindingInfoEXT::default()
                .address(buffer.address)
                .usage(BufferUsageFlags::RESOURCE_DESCRIPTOR_BUFFER_EXT);

            binding_infos.push(uniform_binding_info);

            //let offset = i * alignment;
            instance.bind_descriptor_buffers(command_buffer, &binding_infos[..]);
            instance.set_descriptor_buffer_offset(
                *command_buffer,
                PipelineBindPoint::COMPUTE,
                *pipeline_layout,
                0,
                &[0],
                &[0],
            );

            i += 1;
        }
    }

    /// returns descriptor set layout.
    pub fn get_descriptor_layout(&self) -> DescriptorSetLayout {
        self.descriptor_layout.unwrap()
    }

    /// Builds the descriptor buffer.
    pub fn build(&mut self, instance: &Vulkan) {
        let mut descriptor_props = PhysicalDeviceDescriptorBufferPropertiesEXT::default();

        // Load general properties of what we can do with Descriptor buffers from the physical device.
        let mut device_props_2 =
            PhysicalDeviceProperties2KHR::default().push_next(&mut descriptor_props);

        instance.get_physical_device_properties2(&mut device_props_2);

        self.buffer_offset_alignment = descriptor_props.descriptor_buffer_offset_alignment;
        self.uniform_buffer_descriptor_size = descriptor_props.uniform_buffer_descriptor_size;
        self.storage_buffer_descriptor_size = descriptor_props.storage_buffer_descriptor_size;
        self.combined_image_sampler_descriptor_size =
            descriptor_props.combined_image_sampler_descriptor_size;

        self.setup_uniforms(instance);
    }

    /// Setup descriptors for uniforms
    fn setup_uniforms(&mut self, instance: &Vulkan) {
        ///////// GENERAL SETUP ////////////////////
        let uniforms = &self.uniform_descriptors;
        let storage = &self.storage_descriptors;
        let texture = &self.texture_descriptors;

        // get layout bindings for all the uniforms
        let mut layout_bindings: Vec<DescriptorSetLayoutBinding> = vec![];
        uniforms
            .iter()
            .for_each(|desc| layout_bindings.push(desc.get_layout()));
        storage
            .iter()
            .for_each(|desc| layout_bindings.push(desc.get_layout()));

        texture.iter().for_each(|desc| {
            layout_bindings.push(desc.get_layout());
        });

        // prepare descriptor layout
        let descriptor_layout_create_info = DescriptorSetLayoutCreateInfo::default()
            .bindings(&layout_bindings[..])
            .flags(DescriptorSetLayoutCreateFlags::DESCRIPTOR_BUFFER_EXT);

        let descriptor_layout_res = instance
            .generate_descriptor_layout(descriptor_layout_create_info, None)
            .expect("Unable to setup descriptor buffer uniforms");

        let descriptor_layout = descriptor_layout_res;

        // descriptor size
        let layout_size = instance.generate_descriptor_layout_size(&descriptor_layout);

        ///// BUILD UNIFORM DESCRIPTOR BUFFER /////////
        // build the buffer for this descriptor set
        let mut fmt = BufferFormat::default();
        fmt.usage_flags = BufferUsageFlags::RESOURCE_DESCRIPTOR_BUFFER_EXT
            | BufferUsageFlags::SHADER_DEVICE_ADDRESS;
        fmt.mem_prop_flags = MemoryPropertyFlags::HOST_VISIBLE | MemoryPropertyFlags::HOST_COHERENT;
        fmt.mem_alloc_flags = MemoryAllocateFlags::DEVICE_ADDRESS;
        fmt.size = layout_size;

        // build buffer for uniforms
        let buffer = DataBuffer::create(instance, fmt);

        let uniform_address = instance.get_buffer_device_address(buffer.raw());

        self.update_uniforms(instance, &descriptor_layout, &buffer);
        self.update_storage(instance, &descriptor_layout, &buffer);
        self.update_texture_sampler(instance, &descriptor_layout, &buffer);

        self.layout_buffers.push(UniformResource {
            address: uniform_address,
            buffer,
        });

        self.descriptor_layout = Some(descriptor_layout);
    }

    fn update_texture_sampler(
        &self,
        ctx: &Vulkan,
        descriptor_layout: &DescriptorSetLayout,
        buffer: &DataBuffer,
    ) {
        // loop through uniforms and set up descriptors.
        for desc in self.texture_descriptors.iter() {
            // TODO not sure if this is being used right.
            let descriptor_offset = ctx
                .get_descriptor_layout_binding_offset(&descriptor_layout, desc.get_binding())
                as usize;
            let ptr = unsafe {
                let mut ptr_data = buffer.map_memory(ctx) as *mut u8;
                ptr_data = ptr_data.add(descriptor_offset);

                slice::from_raw_parts_mut(ptr_data, self.combined_image_sampler_descriptor_size)
            };

            let img_info = desc.get_image_info();

            let mut data = DescriptorDataEXT::default();
            data.p_combined_image_sampler = &img_info;

            let desc_info = DescriptorGetInfoEXT::default()
                .ty(DescriptorType::COMBINED_IMAGE_SAMPLER)
                .data(data);
            ctx.get_descriptor_ext(&desc_info, ptr);

            buffer.unmap_memory(ctx);
        }
    }

    /// Applies storage descriptors to the descriptor buffer.
    fn update_storage(
        &self,
        instance: &Vulkan,
        descriptor_layout: &DescriptorSetLayout,
        buffer: &DataBuffer,
    ) {
        // loop through uniforms and set up descriptors.
        for desc in self.storage_descriptors.iter() {
            // TODO not sure if this is being used right.
            let descriptor_offset = instance
                .get_descriptor_layout_binding_offset(&descriptor_layout, desc.get_binding())
                as usize;
            let ptr = unsafe {
                let mut ptr_data = buffer.map_memory(instance) as *mut u8;
                ptr_data = ptr_data.add(descriptor_offset);

                slice::from_raw_parts_mut(ptr_data, self.storage_buffer_descriptor_size)
            };

            let descriptor_address = DescriptorAddressInfoEXT::default()
                .range(desc.get_range_size())
                .address(desc.get_address())
                .format(Format::UNDEFINED);

            let mut data = DescriptorDataEXT::default();
            data.p_storage_buffer = &descriptor_address;
            let desc_info = DescriptorGetInfoEXT::default()
                .ty(DescriptorType::STORAGE_BUFFER)
                .data(data);

            instance.get_descriptor_ext(&desc_info, ptr);

            buffer.unmap_memory(instance);
        }
    }

    /// Updates buffer with uniform descriptors if available.
    fn update_uniforms(
        &self,
        instance: &Vulkan,
        descriptor_layout: &DescriptorSetLayout,
        buffer: &DataBuffer,
    ) {
        // loop through uniforms and set up descriptors.
        for desc in self.uniform_descriptors.iter() {
            // TODO not sure if this is being used right.
            let descriptor_offset = instance
                .get_descriptor_layout_binding_offset(&descriptor_layout, desc.get_binding())
                as usize;
            let mut ptr = unsafe {
                let mut ptr_data = buffer.map_memory(instance) as *mut u8;
                ptr_data = ptr_data.add(descriptor_offset);

                slice::from_raw_parts_mut(ptr_data, self.uniform_buffer_descriptor_size)
            };

            let descriptor_address = DescriptorAddressInfoEXT::default()
                .range(desc.get_range_size())
                .address(desc.get_address())
                .format(Format::UNDEFINED);

            let mut data = DescriptorDataEXT::default();
            data.p_uniform_buffer = &descriptor_address;
            let desc_info = DescriptorGetInfoEXT::default()
                .ty(DescriptorType::UNIFORM_BUFFER)
                .data(data);

            instance.get_descriptor_ext(&desc_info, ptr);

            buffer.unmap_memory(instance);
        }
    }
}
