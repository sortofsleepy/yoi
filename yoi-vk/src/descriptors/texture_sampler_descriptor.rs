use crate::types::traits::Descriptor;
use ash::vk::{
    DescriptorImageInfo, DescriptorSetLayoutBinding, DescriptorType, ImageLayout, ImageView,
    Sampler, ShaderStageFlags,
};

#[derive(Copy, Clone)]
pub struct TextureSamplerDescriptor {
    dst_array_element: u32,
    binding: u32,
    pub descriptor_type: DescriptorType,
    descriptor_count: u32,
    stage_flags: ShaderStageFlags,
    sampler: Sampler,
    image_view: ImageView,
    image_layout: ImageLayout,
}

impl TextureSamplerDescriptor {
    pub fn new() -> Self {
        TextureSamplerDescriptor {
            image_layout: ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            sampler: Sampler::default(),
            image_view: ImageView::default(),
            dst_array_element: 0,
            binding: 0,
            descriptor_type: DescriptorType::COMBINED_IMAGE_SAMPLER,
            descriptor_count: 1,
            stage_flags: ShaderStageFlags::FRAGMENT,
        }
    }

    pub fn set_image(&mut self, view: ImageView, sampler: Sampler) {
        self.image_view = view;
        self.sampler = sampler;
    }

    pub fn set_image_layout(&mut self, layout: ImageLayout) {
        self.image_layout = layout;
    }

    pub fn get_image_info(&self) -> DescriptorImageInfo {
        DescriptorImageInfo {
            sampler: self.sampler,
            image_view: self.image_view,
            image_layout: self.image_layout,
        }
    }
}

impl Descriptor for TextureSamplerDescriptor {
    /// sets the shader stage to use with the descriptor
    fn set_shader_stage(&mut self, flags: ShaderStageFlags) {
        self.stage_flags = flags;
    }

    fn set_shader_binding(&mut self, binding: u32) {
        self.binding = binding
    }

    fn get_count(&self) -> u32 {
        self.descriptor_count
    }

    fn get_type(&self) -> DescriptorType {
        self.descriptor_type
    }

    fn get_layout(&self) -> DescriptorSetLayoutBinding {
        DescriptorSetLayoutBinding {
            binding: self.binding,
            descriptor_type: self.descriptor_type,
            descriptor_count: self.descriptor_count,
            stage_flags: self.stage_flags,
            ..Default::default()
        }
    }

    fn get_address(&self) -> u64 {
        todo!()
    }
    fn get_stage_flags(&self) -> ShaderStageFlags {
        self.stage_flags
    }
    fn get_binding(&self) -> u32 {
        self.binding
    }

    fn get_dst_array_element(&self) -> u32 {
        self.dst_array_element
    }

    // Since it's an image, just return 0
    fn get_range_size(&self) -> u64 {
        return 0;
    }
}
