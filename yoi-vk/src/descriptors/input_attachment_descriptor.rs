use crate::types::traits::Descriptor;
use ash::vk::{
    DescriptorImageInfo, DescriptorSetLayoutBinding, DescriptorType, ImageLayout, ImageView,
    Sampler, ShaderStageFlags,
};

////////////// INPUT ATTACHMENT DESCRIPTORS ///////////////////////
#[derive(Copy, Clone)]
pub struct InputAttachmentDescriptor {
    image_info: DescriptorImageInfo,
    dst_array_element: u32,
    binding: u32,
    descriptor_type: DescriptorType,
    descriptor_count: u32,
    stage_flags: ShaderStageFlags,
}

impl InputAttachmentDescriptor {
    pub fn new() -> Self {
        InputAttachmentDescriptor {
            dst_array_element: 0,
            image_info: DescriptorImageInfo {
                image_view: Default::default(),
                image_layout: ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                ..Default::default()
            },
            binding: 0,
            descriptor_type: DescriptorType::INPUT_ATTACHMENT,
            descriptor_count: 1,
            stage_flags: ShaderStageFlags::FRAGMENT,
        }
    }

    pub fn set_image(mut self, view: ImageView, sampler: Sampler) -> Self {
        self.image_info.image_view = view;
        self.image_info.sampler = sampler;
        self
    }

    /// sets image information for the descriptor
    pub fn set_image_info(mut self, view: ImageView, sampler: Sampler) -> Self {
        self.image_info.image_view = view;
        self.image_info.sampler = sampler;
        self
    }

    pub fn set_image_layout(mut self, layout: ImageLayout) -> Self {
        self.image_info.image_layout = layout;
        self
    }

    pub fn set_binding(mut self, binding: u32) -> Self {
        self.binding = binding;
        self
    }

    pub fn get_image_info(&self) -> &DescriptorImageInfo {
        &self.image_info
    }
}

impl Descriptor for InputAttachmentDescriptor {
    /// sets the shader stage to use with the descriptor
    fn set_shader_stage(&mut self, flags: ShaderStageFlags) {
        self.stage_flags = flags;
    }
    fn get_layout(&self) -> DescriptorSetLayoutBinding {
        DescriptorSetLayoutBinding {
            binding: self.binding,
            descriptor_type: self.descriptor_type,
            descriptor_count: self.descriptor_count,
            stage_flags: self.stage_flags,
            ..Default::default()
        }
    }
    fn set_shader_binding(&mut self, binding: u32) {
        self.binding = binding
    }

    fn get_count(&self) -> u32 {
        self.descriptor_count
    }

    fn get_type(&self) -> DescriptorType {
        self.descriptor_type
    }

    fn get_address(&self) -> u64 {
        todo!()
    }

    fn get_stage_flags(&self) -> ShaderStageFlags {
        self.stage_flags
    }
    fn get_binding(&self) -> u32 {
        self.binding
    }

    fn get_dst_array_element(&self) -> u32 {
        self.dst_array_element
    }

    // Since it's an image, just return 0
    fn get_range_size(&self) -> u64 {
        return 0;
    }
}
////////////////////////////////
