use crate::{
    generate_subresource_range, insert_image_memory_barrier, BufferFormat, DataBuffer,
    TextureFormat, VkObject, VkTexture, Vulkan,
};
use ash::vk;
use ash::vk::{
    AccessFlags, AttachmentDescription, AttachmentLoadOp, AttachmentStoreOp, BufferUsageFlags,
    CommandBuffer, CommandBufferAllocateInfo, CommandBufferLevel, CommandBufferUsageFlags,
    CommandPoolCreateFlags, CommandPoolCreateInfo, Extent3D, Format, ImageAspectFlags, ImageCopy,
    ImageLayout, ImageSubresourceLayers, ImageTiling, ImageUsageFlags, MemoryBarrier,
    MemoryPropertyFlags, PipelineStageFlags, SampleCountFlags,
};

/// generates a default color attachment [TextureFormat] object suitable to be used as a
/// color attachment.
#[allow(unused)]
pub fn generate_color_attachment_format(
    width: u32,
    height: u32,
    tex_fmt: Option<Format>,
) -> TextureFormat {
    let mut format = TextureFormat::default();

    if tex_fmt.is_none() {
        format.texture_format = Format::R32G32B32A32_SFLOAT;
    } else {
        format.texture_format = tex_fmt.unwrap();
    }

    format.extent.width = width;
    format.extent.height = height;
    format.extent.depth = 1;
    format.mip_levels = 1;
    format.array_layers = 1;
    format.sample_count = SampleCountFlags::TYPE_1;
    format.tiling_mode = ImageTiling::OPTIMAL;
    format.image_usage_flags = ImageUsageFlags::COLOR_ATTACHMENT
        | ImageUsageFlags::SAMPLED
        | ImageUsageFlags::TRANSFER_SRC;
    format.memory_property_flags = MemoryPropertyFlags::DEVICE_LOCAL;
    format
}

/// generates a default attachment descriptor suitable for a color attachment. By default,
/// we assume the attachment should clear itself on every frame.
#[allow(unused)]
pub fn generate_default_color_attachment_descriptor(format: Format) -> AttachmentDescription {
    AttachmentDescription {
        format,
        samples: SampleCountFlags::TYPE_1,
        load_op: AttachmentLoadOp::CLEAR,
        store_op: AttachmentStoreOp::STORE,
        stencil_load_op: AttachmentLoadOp::DONT_CARE,
        stencil_store_op: AttachmentStoreOp::DONT_CARE,
        initial_layout: ImageLayout::UNDEFINED,
        final_layout: ImageLayout::SHADER_READ_ONLY_OPTIMAL,
        ..Default::default()
    }
}

/// Does the same as [generate_default_color_attachment_descriptor] and setting things up to not clear the attachment.
#[allow(unused)]
pub fn generate_default_color_attachment_descriptor_without_clear(
    format: Format,
) -> AttachmentDescription {
    AttachmentDescription {
        format,
        samples: SampleCountFlags::TYPE_1,
        load_op: AttachmentLoadOp::DONT_CARE,
        store_op: AttachmentStoreOp::STORE,
        stencil_load_op: AttachmentLoadOp::DONT_CARE,
        stencil_store_op: AttachmentStoreOp::DONT_CARE,
        initial_layout: ImageLayout::UNDEFINED,
        final_layout: ImageLayout::SHADER_READ_ONLY_OPTIMAL,
        ..Default::default()
    }
}

/// Generates a command buffer.
pub fn generate_command_buffer(instance: &Vulkan) -> CommandBuffer {
    let pool_info = CommandPoolCreateInfo {
        queue_family_index: instance.get_graphics_queue_index() as u32,
        flags: CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
        ..Default::default()
    };

    let pool = instance.generate_command_pool(pool_info);

    // allocate a command buffer
    let buffer_info = CommandBufferAllocateInfo {
        command_pool: pool,
        level: CommandBufferLevel::PRIMARY,
        command_buffer_count: 1,
        ..Default::default()
    };

    let buffers = instance.generate_commmand_buffers(buffer_info);

    // TODO do we need to destroy if memory gets released anyways?
    //instance.destroy_command_pool(pool);

    buffers[0]
}

// Generates a buffer suitable for transferring from.
pub fn generate_staging_buffer(instance: &Vulkan, size: u64) -> DataBuffer {
    // setup staging buffer
    let mut stage_fmt = BufferFormat::default();
    stage_fmt.usage_flags = BufferUsageFlags::TRANSFER_SRC;
    stage_fmt.size = size;

    DataBuffer::create(instance, stage_fmt)
}

/// Fills a [DataBuffer] with empty values.
pub fn fill_buffer(vk: &Vulkan, device_size: usize, cb: &CommandBuffer, buff: &DataBuffer) {
    // build buffer
    let fence = vk.create_fence();

    vk.begin_command_buffer_with_flags(cb, CommandBufferUsageFlags::ONE_TIME_SUBMIT);

    // record fill command
    vk.fill_buffer(cb, &buff.raw(), 0, device_size as u64, 0.5 as u32);

    let barrier = MemoryBarrier::default()
        .src_access_mask(vk::AccessFlags::TRANSFER_WRITE)
        .dst_access_mask(vk::AccessFlags::HOST_READ);

    vk.pipeline_barrier(
        cb,
        vk::PipelineStageFlags::TRANSFER,
        vk::PipelineStageFlags::HOST,
        vk::DependencyFlags::empty(),
        &[barrier],
        &[],
        &[],
    );
    vk.end_command_buffer(cb);

    // submit the command into the queue
    let command_buffers = [*cb];
    let cb_submit = vk::SubmitInfo::default().command_buffers(&command_buffers);

    vk.submit_graphics(cb_submit, fence);
    vk.wait_for_fences(&[fence]);
    vk.wait_idle();
}

/// Helper to generate a [DataBuffer] for storage purposes
pub fn generate_storage_buffer(vk: &Vulkan, device_size: usize) -> DataBuffer {
    let mut fmt = BufferFormat::default();
    fmt.size = device_size as u64;
    fmt.usage_flags = vk::BufferUsageFlags::STORAGE_BUFFER | vk::BufferUsageFlags::TRANSFER_DST;
    fmt.mem_prop_flags = MemoryPropertyFlags::HOST_VISIBLE
        | MemoryPropertyFlags::HOST_CACHED
        | MemoryPropertyFlags::HOST_COHERENT;

    let buff = DataBuffer::create(vk, fmt);
    buff
}

/// Copies data from one buffer to another. It's recommended that you use this
/// vs vkCmdUpdateBuffer as that command is limited to 65536 bytes and can't be used for bigger
/// objects without fancy manipulation.
pub fn copy_buffer_to_buffer(
    instance: &Vulkan,
    command_buffer: &CommandBuffer,
    src: &vk::Buffer,
    dst: &vk::Buffer,
    size: u64,
) {
    let device = instance.get_logical_device();

    let cb_info = vk::CommandBufferBeginInfo {
        flags: vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT,
        ..Default::default()
    };

    let copy_region = vk::BufferCopy {
        size: size,
        ..Default::default()
    };

    let submit_info = vk::SubmitInfo {
        command_buffer_count: 1,
        p_command_buffers: [*command_buffer].as_ptr(),
        ..Default::default()
    };

    unsafe {
        device
            .begin_command_buffer(*command_buffer, &cb_info)
            .expect("Unable to start command buffer. check copy_buffer_to_buffer");
        device.cmd_copy_buffer(*command_buffer, *src, *dst, &[copy_region]);
        device
            .end_command_buffer(*command_buffer)
            .expect("Unable to end command buffer. check copy_buffer_to_buffer");
        instance.submit_graphics(submit_info, vk::Fence::null());
    }
}

/// Copies the contents of one texture to another.
/// Note that this assumes both textures are color textures.
/// Also note that this is meant to be run within an existing command buffer recording.
pub fn copy_image_to_image(
    vk: &Vulkan,
    cb: &CommandBuffer,
    tex1: &VkTexture,
    tex2: &VkTexture,
    width: u32,
    height: u32,
) {
    let img1 = tex1.get_image();
    let img2 = tex2.get_image();

    let mut a = ImageCopy::default()
        .src_subresource(ImageSubresourceLayers {
            aspect_mask: ImageAspectFlags::COLOR,
            mip_level: 0,
            base_array_layer: 0,
            layer_count: 1,
        })
        .dst_subresource(ImageSubresourceLayers {
            aspect_mask: ImageAspectFlags::COLOR,
            mip_level: 0,
            base_array_layer: 0,
            layer_count: 1,
        })
        .extent(Extent3D {
            width,
            height,
            depth: 1,
        });

    // setup src for transfer
    insert_image_memory_barrier(
        vk,
        cb,
        img1,
        AccessFlags::empty(),
        AccessFlags::TRANSFER_READ,
        ImageLayout::UNDEFINED,
        ImageLayout::TRANSFER_SRC_OPTIMAL,
        generate_subresource_range(false),
        PipelineStageFlags::HOST,
        PipelineStageFlags::TRANSFER,
    );

    // setup dst for receiving
    insert_image_memory_barrier(
        vk,
        cb,
        img2,
        AccessFlags::COLOR_ATTACHMENT_WRITE,
        AccessFlags::TRANSFER_WRITE,
        ImageLayout::UNDEFINED,
        ImageLayout::TRANSFER_DST_OPTIMAL,
        generate_subresource_range(false),
        PipelineStageFlags::ALL_GRAPHICS,
        PipelineStageFlags::ALL_GRAPHICS,
    );

    /////
    vk.copy_image(
        cb,
        img1,
        ImageLayout::TRANSFER_SRC_OPTIMAL,
        img2,
        ImageLayout::TRANSFER_DST_OPTIMAL,
        &[a],
    );

    ////

    // reset src to original
    insert_image_memory_barrier(
        vk,
        cb,
        img1,
        AccessFlags::empty(),
        AccessFlags::SHADER_READ,
        ImageLayout::TRANSFER_SRC_OPTIMAL,
        ImageLayout::PRESENT_SRC_KHR,
        generate_subresource_range(false),
        PipelineStageFlags::TRANSFER,
        PipelineStageFlags::FRAGMENT_SHADER,
    );

    // reset dst to original layout.
    insert_image_memory_barrier(
        vk,
        cb,
        img2,
        AccessFlags::COLOR_ATTACHMENT_WRITE,
        AccessFlags::NONE,
        ImageLayout::TRANSFER_DST_OPTIMAL,
        //ImageLayout::SHADER_READ_ONLY_OPTIMAL,
        ImageLayout::PRESENT_SRC_KHR,
        generate_subresource_range(false),
        PipelineStageFlags::ALL_GRAPHICS,
        PipelineStageFlags::ALL_GRAPHICS,
    );
}

/// Generates a texture that is suitable for a variety of purposes, such as serving as an additive surface for a renderpass.
/// Will automatically do the image transition to [ImageLayout::GENERAL]
pub fn generate_general_layout_texture(
    vk: &Vulkan,
    texture_format: Format,
    width: u32,
    height: u32,
    cb: &CommandBuffer,
) -> VkTexture {
    let mut format = TextureFormat::default();
    format.texture_format = texture_format;
    format.extent.width = width;
    format.extent.height = height;
    format.extent.depth = 1;
    format.mip_levels = 1;
    format.array_layers = 1;
    format.sample_count = SampleCountFlags::TYPE_1;
    format.tiling_mode = ImageTiling::OPTIMAL;
    format.image_usage_flags = ImageUsageFlags::COLOR_ATTACHMENT
        | ImageUsageFlags::SAMPLED
        | ImageUsageFlags::TRANSFER_SRC;
    format.memory_property_flags = MemoryPropertyFlags::DEVICE_LOCAL;

    let mut tex = VkTexture::new(vk, format);
    tex.update_image_layout(cb, vk, ImageLayout::GENERAL);

    tex
}
