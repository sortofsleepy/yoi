use crate::core::Vulkan;
use crate::types::formats::BufferFormat;
use crate::VkObject;
use ash::util::Align;
use ash::vk;
use ash::vk::{DeviceSize, MemoryAllocateFlagsInfo};
use core::ffi::c_void;
use std::ffi::CString;
use std::mem::{align_of, size_of};
use std::rc::Rc;

///Helps find the type index for the memory property you specify
#[allow(unused)]
pub fn find_memory_type_index(
    memory_req: &vk::MemoryRequirements,
    memory_props: &vk::PhysicalDeviceMemoryProperties,
    flags: vk::MemoryPropertyFlags,
) -> Option<u32> {
    memory_props.memory_types[..memory_props.memory_type_count as _]
        .iter()
        .enumerate()
        .find(|(index, memory_type)| {
            (1 << index) & memory_req.memory_type_bits != 0
                && memory_type.property_flags & flags == flags
        })
        .map(|(index, _memory_type)| index as _)
}

/// Represents a block of data for use in Vulkan.
/// Provides some wrapper functions to make things a bit easier.
#[derive(Clone, Copy)]
pub struct DataBuffer {
    memory_bound: bool,
    format: BufferFormat,
    vbo: Option<vk::Buffer>,
    memory: Option<vk::DeviceMemory>,
    id: i32, // TODO see if there's a clean enough way to work in a string / str
}

impl DataBuffer {
    /// Creates a new [DataBuffer]. Pass in a Vulkan instance and a [BufferFormat] object.
    pub fn create(instance: &Vulkan, fmt: BufferFormat) -> Self {
        let create_info = vk::BufferCreateInfo::default()
            .size(fmt.size)
            .flags(fmt.create_flags)
            .usage(fmt.usage_flags)
            .sharing_mode(fmt.share_mode);

        let vbo = instance.generate_buffer(&create_info);
        let memory_req = instance.get_buffer_memory_requirements(vbo);
        let memory_props = instance.get_memory_properties();
        let mem_flags = fmt.mem_prop_flags;
        let memory_index = find_memory_type_index(&memory_req, &memory_props, mem_flags);

        let mut mem_alloc = MemoryAllocateFlagsInfo::default().flags(fmt.mem_alloc_flags);

        let memory = instance.allocate_memory(
            &vk::MemoryAllocateInfo::default()
                .push_next(&mut mem_alloc)
                .memory_type_index(memory_index.unwrap())
                .allocation_size(memory_req.size),
        );

        // bind memory
        instance.bind_memory(&vbo, &memory, 0);

        DataBuffer {
            memory_bound: true,
            format: fmt,
            vbo: Some(vbo),
            memory: Some(memory),
            id: 0,
        }
    }

    /// Creates buffer with specified size. All other settings are defaults of [BufferFormat]
    pub fn create_with_size(instance: &Rc<Vulkan>, size: u64) -> Self {
        let fmt = BufferFormat::default().size(size);
        let create_info = vk::BufferCreateInfo::default()
            .size(size)
            .flags(fmt.create_flags)
            .usage(fmt.usage_flags)
            .sharing_mode(fmt.share_mode);

        let vbo = instance.generate_buffer(&create_info);
        let memory_req = instance.get_buffer_memory_requirements(vbo);
        let memory_props = instance.get_memory_properties();
        let mem_flags = fmt.mem_prop_flags;
        let memory_index = find_memory_type_index(&memory_req, &memory_props, mem_flags);

        let memory = instance.allocate_memory(
            &vk::MemoryAllocateInfo::default()
                .memory_type_index(memory_index.unwrap())
                .allocation_size(memory_req.size),
        );

        // bind memory
        instance.bind_memory(&vbo, &memory, 0);

        DataBuffer {
            memory_bound: true,
            format: fmt,
            vbo: Some(vbo),
            memory: Some(memory),
            id: 0,
        }
    }

    /// destroys the buffer.
    pub fn destroy(&self, app: &Vulkan) {
        app.destroy_buffer(self.vbo.unwrap())
    }

    /// returns the size of the buffer
    pub fn get_size(&self) -> u64 {
        self.format.size
    }

    /// sets object information within the buffer.
    pub fn set_object_data<T: std::marker::Copy>(&mut self, instance: &Vulkan, data: T) {
        let device = instance.get_logical_device();
        let memory = self.memory.unwrap();
        let buffer_size = self.format.size;

        // if no memory available, return and note to user
        if self.memory.is_none() {
            println!(
                "Error! Attempting to set data on DataBuffer:{} without any memory",
                self.id
            );
            return;
        }

        unsafe {
            let ptr = device
                .map_memory(memory, 0, buffer_size, vk::MemoryMapFlags::empty())
                .unwrap();
            let mut slice = Align::new(ptr, align_of::<T>() as u64, buffer_size);
            slice.copy_from_slice(&[data]);
            device.unmap_memory(memory);
        };
    }

    /// Same as [DataBuffer::set_object_data] but allows you to specify an offset.
    pub fn set_object_data_at_offset<T: std::marker::Copy>(
        &mut self,
        instance: &Vulkan,
        data: T,
        offset: DeviceSize,
    ) {
        let device = instance.get_logical_device();
        let memory = self.memory.unwrap();
        let buffer_size = self.format.size;

        // if no memory available, return and note to user
        if self.memory.is_none() {
            println!(
                "Error! Attempting to set data on DataBuffer:{} without any memory",
                self.id
            );
            return;
        }

        unsafe {
            let ptr = device
                .map_memory(
                    memory,
                    offset,
                    size_of::<T>() as u64,
                    vk::MemoryMapFlags::empty(),
                )
                .unwrap();
            let mut slice = Align::new(ptr, align_of::<T>() as u64, buffer_size);
            slice.copy_from_slice(&[data]);
            device.unmap_memory(memory);
        };
    }

    /// Same as [DataBuffer::set_object_data] but allows you to specify an offset with a range.
    pub fn set_object_data_at_offset_with_range<T: std::marker::Copy>(
        &mut self,
        instance: &Vulkan,
        data: T,
        offset: DeviceSize,
        update_range: usize,
    ) {
        let device = instance.get_logical_device();
        let memory = self.memory.unwrap();
        let buffer_size = self.format.size;

        // if no memory available, return and note to user
        if self.memory.is_none() {
            println!(
                "Error! Attempting to set data on DataBuffer:{} without any memory",
                self.id
            );
            return;
        }

        unsafe {
            let ptr = device
                .map_memory(
                    memory,
                    offset,
                    (size_of::<T>() * update_range) as u64,
                    vk::MemoryMapFlags::empty(),
                )
                .unwrap();
            let mut slice = Align::new(ptr, align_of::<T>() as u64, buffer_size);
            slice.copy_from_slice(&[data]);
            device.unmap_memory(memory);
        };
    }

    /// Maps the buffer's memory. Returns a pointer to the memory.
    /// Make sure to call [unmap_memory] once you're done.
    pub fn map_memory(&self, instance: &Vulkan) -> *mut c_void {
        let device = instance.get_logical_device();
        let buffer_size = self.format.size;

        let memory = self.memory.unwrap();
        unsafe {
            device
                .map_memory(memory, 0, buffer_size, vk::MemoryMapFlags::empty())
                .unwrap()
        }
    }

    /// Unmaps the buffer's memory.
    pub fn unmap_memory(&self, instance: &Vulkan) {
        let memory = self.memory.unwrap();
        let device = instance.get_logical_device();

        unsafe {
            device.unmap_memory(memory);
        }
    }

    /// sets data on the buffer.
    pub fn set_data<T: std::marker::Copy>(&mut self, instance: &Vulkan, data: &[T]) {
        // if no memory available, return and note to user
        if self.memory.is_none() {
            println!(
                "Error! Attempting to set data on DataBuffer:{} without any memory",
                self.id
            );
            return;
        }

        let device = instance.get_logical_device();
        let memory = self.memory.unwrap();
        let buffer_size = self.format.size;

        unsafe {
            let ptr = device
                .map_memory(memory, 0, buffer_size, vk::MemoryMapFlags::empty())
                .unwrap();
            let mut slice = Align::new(ptr, align_of::<T>() as u64, buffer_size);
            slice.copy_from_slice(&data[..]);
            device.unmap_memory(memory);
        };
    }

    /// Sets buffer memory.
    /// Note that it will also bind memory at the same time.
    pub fn set_memory(&mut self, app: &Vulkan, memory: vk::DeviceMemory) {
        self.memory = Some(memory);

        // bind memory
        app.bind_memory(self.vbo.as_ref().unwrap(), self.memory.as_ref().unwrap(), 0);

        self.memory_bound = true;
    }
}

impl VkObject for DataBuffer {
    type Raw = vk::Buffer;

    fn raw(&self) -> &Self::Raw {
        self.vbo.as_ref().unwrap()
    }

    fn add_label(&self, ctx: &Vulkan, name: String) {
        #[cfg(debug_assertions)]
        {
            let raw = self.vbo.unwrap();
            let mut name_info = vk::DebugUtilsObjectNameInfoEXT::default();
            name_info.object_handle(raw);
            let msg = format!("unable to convert {} to c_string for label", name);
            name_info.object_name(CString::new(name).expect(msg.as_str()).as_c_str());

            ctx.add_object_label(&name_info);
        }
    }
}
