use crate::render::swapchain::VkSwapChain;
use crate::texture::VkTexture;
use crate::{generate_depth_attachment_format, Vulkan};
use ash::vk;
use ash::vk::{Extent2D, Offset2D, Rect2D};

/// Construct around general app settings for vulkan like viewport, clear etc.
/// Also holds a VkSwapChain and default depth texture as well.
pub struct VkApp {
    viewport: Vec<vk::Viewport>,
    clear_value: vk::ClearValue,
    depth_stencil: vk::ClearValue,
    render_area: vk::Rect2D,
    scissor: Vec<Rect2D>,
    swapchain: VkSwapChain,
    depth_texture: VkTexture,
}

impl VkApp {
    pub fn new(
        width: f32,
        height: f32,
        extent: vk::Extent2D,
        swapchain: VkSwapChain,
        depth_texture: VkTexture,
    ) -> Self {
        VkApp {
            swapchain,
            depth_texture,
            viewport: vec![vk::Viewport {
                x: 0.0,
                y: 0.0,
                width,
                height,
                min_depth: 0.0,
                max_depth: 1.0,
            }],
            clear_value: vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: [0.0, 0.0, 0.0, 1.0],
                },
            },
            depth_stencil: vk::ClearValue {
                depth_stencil: vk::ClearDepthStencilValue {
                    depth: 1.0,
                    stencil: 0,
                },
            },
            render_area: vk::Rect2D {
                offset: vk::Offset2D { x: 0, y: 0 },
                extent: vk::Extent2D {
                    width: width as u32,
                    height: height as u32,
                },
            },
            scissor: vec![vk::Rect2D {
                offset: vk::Offset2D { x: 0, y: 0 },
                extent,
            }],
        }
    }

    pub fn get_swapchain(&self) -> &VkSwapChain {
        &self.swapchain
    }

    pub fn get_depth_image(&self) -> &VkTexture {
        &self.depth_texture
    }

    /// resets aspects
    pub fn reset(&mut self, instance: &Vulkan, surface: &vk::SurfaceKHR, window_size: [u32; 2]) {
        instance.wait_idle();
        instance.destroy_vk_texture(&self.depth_texture);

        // re-init swapchain
        self.swapchain.destroy_swapchain(instance);
        self.swapchain.re_init(instance, surface);

        let mut dtfmt = generate_depth_attachment_format(window_size[0], window_size[1]);
        dtfmt.image_subresource_range.aspect_mask = vk::ImageAspectFlags::DEPTH;

        self.depth_texture = VkTexture::new(&instance, dtfmt);

        // re-size render area, viewports, etc.
        self.render_area.extent = self.swapchain.get_extent();

        for i in 0..self.viewport.len() {
            self.viewport[i].width = window_size[0] as f32;
            self.viewport[i].height = window_size[1] as f32;
            self.scissor[i].extent = self.swapchain.get_extent();
        }
    }

    pub fn get_depth_stencil(&self) -> &vk::ClearValue {
        &self.depth_stencil
    }
    pub fn get_clear(&self) -> &vk::ClearValue {
        &self.clear_value
    }

    pub fn get_render_area(&self) -> &vk::Rect2D {
        &self.render_area
    }

    pub fn get_viewports(&self) -> &[vk::Viewport] {
        &self.viewport
    }

    pub fn get_scissors(&self) -> &[vk::Rect2D] {
        &self.scissor
    }

    pub fn set_scissor(&mut self, offset: Offset2D, extent: Extent2D) {
        self.scissor[0].extent = extent;
        self.scissor[0].offset = offset;
    }

    pub fn set_scissor_at(&mut self, idx: usize, offset: Offset2D, extent: Extent2D) {
        self.scissor[idx].extent = extent;
        self.scissor[idx].offset = offset;
    }

    pub fn set_viewport(&mut self, x: f32, y: f32, width: f32, height: f32) {
        self.viewport[0].x = x;
        self.viewport[0].y = y;
        self.viewport[0].width = width;
        self.viewport[0].height = height;
    }

    pub fn set_viewport_at(&mut self, idx: usize, x: f32, y: f32, width: f32, height: f32) {
        self.viewport[idx].x = x;
        self.viewport[idx].y = y;
        self.viewport[idx].width = width;
        self.viewport[idx].height = height;
    }
}
