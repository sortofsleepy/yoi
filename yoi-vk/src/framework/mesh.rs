use crate::core::Vulkan;
use crate::framework::databuffer::DataBuffer;
use crate::types::formats::{BufferFormat, MeshAttrib, MeshAttribFormat};
use crate::VkObject;
use ash::vk::{
    BufferUsageFlags, CommandBuffer, DeviceSize, IndexType, VertexInputAttributeDescription,
    VertexInputBindingDescription, VertexInputRate,
};
use std::mem::size_of;

/// This is a basic wrapper around the concept of a mesh.
pub struct Mesh {
    input_binding_descriptions: Vec<VertexInputBindingDescription>,
    input_attrib_descriptions: Vec<VertexInputAttributeDescription>,
    index_buffer: Option<DataBuffer>,
    attributes: Vec<MeshAttrib>,
    read_offsets: Vec<DeviceSize>,
    num_vertices: u32,
    first_vertex: u32,
    first_instance: u32,
    first_index: u32,
    vertex_offset: i32,
    index_offset: u64,
    instance_count: u32,
    num_indices: usize,
    index_type: IndexType,
    // descriptor_set: Option<VkDescriptorSet>,
}

impl Mesh {
    pub fn new() -> Mesh {
        Mesh {
            input_binding_descriptions: vec![],
            input_attrib_descriptions: vec![],
            index_buffer: None,
            attributes: vec![],
            read_offsets: vec![],
            num_vertices: 1, // should always draw at least 1 vertex point.
            first_vertex: 0,
            first_instance: 0,
            first_index: 0,
            vertex_offset: 0,
            index_offset: 0,
            num_indices: 0,
            instance_count: 1,
            index_type: IndexType::UINT32,
            //descriptor_set: None,
        }
    }

    /// Adds an attribute to the mesh based on a vector of data.
    pub fn add_attribute<T: std::marker::Copy>(
        mut self,
        instance: &Vulkan,
        location: u32,
        data: Vec<T>,
    ) -> Self {
        let format = MeshAttribFormat::default();

        // for simplicity, buffer binding is implicitly whatever position we're att in the binding description index.
        let binding = self.input_binding_descriptions.len();

        let mut buffer = DataBuffer::create(
            instance,
            BufferFormat::default().size((size_of::<T>() * data.len()) as u64),
        );
        buffer.set_data(instance, &data);

        let input_binding_desc = VertexInputBindingDescription::default()
            .binding(binding as u32)
            .stride((size_of::<T>()) as u32)
            .input_rate(VertexInputRate::VERTEX);

        let input_attrib_desc = VertexInputAttributeDescription::default()
            .binding(binding as u32)
            .location(location)
            .format(format.data_format)
            .offset(0);

        self.read_offsets.push(0);
        self.attributes.push(MeshAttrib { buffer, location });

        self.input_attrib_descriptions.push(input_attrib_desc);
        self.input_binding_descriptions.push(input_binding_desc);
        self
    }

    /// Adds an attribute to the mesh based on a vector of data.
    pub fn add_instanced_attribute<T: std::marker::Copy>(
        mut self,
        instance: &Vulkan,
        location: u32,
        data: Vec<T>,
    ) -> Self {
        let format = MeshAttribFormat::default();

        // for simplicity, buffer binding is implicitly whatever position we're att in the binding description index.
        let binding = self.input_binding_descriptions.len();

        let mut buffer = DataBuffer::create(
            instance,
            BufferFormat::default().size((size_of::<T>() * data.len()) as u64),
        );
        buffer.set_data(instance, &data);

        let input_binding_desc = VertexInputBindingDescription::default()
            .binding(binding as u32)
            .stride((size_of::<T>()) as u32)
            .input_rate(VertexInputRate::INSTANCE);

        let input_attrib_desc = VertexInputAttributeDescription::default()
            .binding(binding as u32)
            .location(location)
            .format(format.data_format)
            .offset(0);

        self.read_offsets.push(0);
        self.attributes.push(MeshAttrib { buffer, location });

        self.input_attrib_descriptions.push(input_attrib_desc);
        self.input_binding_descriptions.push(input_binding_desc);
        self
    }

    /// Adds a new attribute by adding a buffer of data instead of a Vec
    pub fn add_attrib_buffer(mut self, location: u32, buffer: &DataBuffer, stride: u32) -> Self {
        let format = MeshAttribFormat::default();

        // for simplicity, buffer binding is implicitly whatever position we're att in the binding description index.
        let binding = self.input_binding_descriptions.len();

        let input_binding_desc = VertexInputBindingDescription::default()
            .binding(binding as u32)
            .stride(stride)
            .input_rate(VertexInputRate::VERTEX);

        let input_attrib_desc = VertexInputAttributeDescription::default()
            .binding(binding as u32)
            .location(location)
            .format(format.data_format)
            .offset(0);

        self.read_offsets.push(0);
        self.attributes.push(MeshAttrib {
            buffer: *buffer,
            location,
        });

        self.input_attrib_descriptions.push(input_attrib_desc);
        self.input_binding_descriptions.push(input_binding_desc);
        self
    }

    pub fn add_instanced_attrib_buffer(
        mut self,
        location: u32,
        buffer: &DataBuffer,
        stride: u32,
    ) -> Self {
        let format = MeshAttribFormat::default();

        // for simplicity, buffer binding is implicitly whatever position we're att in the binding description index.
        let binding = self.input_binding_descriptions.len();

        let input_binding_desc = VertexInputBindingDescription::default()
            .binding(binding as u32)
            .stride(stride)
            .input_rate(VertexInputRate::INSTANCE);

        let input_attrib_desc = VertexInputAttributeDescription::default()
            .binding(binding as u32)
            .location(location)
            .format(format.data_format)
            .offset(0);

        self.read_offsets.push(0);
        self.attributes.push(MeshAttrib {
            buffer: *buffer,
            location,
        });

        self.input_attrib_descriptions.push(input_attrib_desc);
        self.input_binding_descriptions.push(input_binding_desc);
        self
    }

    pub fn set_num_vertices(mut self, num: u32) -> Self {
        self.num_vertices = num;
        self
    }

    pub fn set_instance_count(&mut self, count: u32) {
        self.instance_count = count;
    }

    /// Adds indices to the mesh.
    pub fn add_index_data<T: Copy>(mut self, instance: &Vulkan, data: Vec<T>) -> Self {
        let mut format = BufferFormat::default();
        format.usage_flags = BufferUsageFlags::TRANSFER_DST
            | BufferUsageFlags::INDEX_BUFFER
            | BufferUsageFlags::STORAGE_BUFFER;
        format.size = (size_of::<T>() * data.len()) as u64;
        let mut buffer = DataBuffer::create(instance, format);
        buffer.set_data(instance, &data);

        self.num_indices = data.len();
        self.index_buffer = Some(buffer);

        self
    }

    /// sets the number of vertices
    pub fn set_num_verts(mut self, num: u32) -> Self {
        self.num_vertices = num;
        self
    }

    pub fn update_num_verts(&mut self, num: u32) {
        self.num_vertices = num;
    }

    /// Sets the number of instances.
    pub fn set_num_instances(mut self, num: u32) -> Self {
        self.instance_count = num;
        self
    }

    /// Basically same as [set_num_instances] but usable when you need to take a ref of Self
    pub fn update_num_instances(&mut self, num: u32) {
        self.instance_count = num;
    }

    /// sets the first index value to use for drawing.
    pub fn set_first_index(&mut self, num: u32) {
        self.first_index = num;
    }

    /// sets the vertex offset value to use for drawing
    pub fn set_vertex_offset(&mut self, num: i32) {
        self.vertex_offset = num;
    }

    /// Updates attribute on the buffer.
    pub fn update_attribute<T: std::marker::Copy>(
        &mut self,
        location: u32,
        instance: &Vulkan,
        data: &[T],
    ) {
        let attrib = self
            .attributes
            .iter()
            .find(|attrib| attrib.location == location)
            .expect("Mesh::update_attribute - could not find attribute at specified location");

        let mut buff = attrib.buffer;
        buff.set_data(instance, data);
    }

    /// draws a mesh. Assumes command buffer and renderpass are currently bound.
    pub fn draw(&self, instance: &Vulkan, cb: CommandBuffer) {
        // bind attribute buffers
        for i in 0..self.attributes.len() {
            let itm = self.attributes.get(i);
            if itm.is_some() {
                let buff = itm.unwrap().buffer;
                //instance.bind_vertex_buffer(cb,i as u32,&buff,self.read_offsets[i]);
                instance.bind_vertex_buffer(cb, i as u32, &buff.raw(), self.read_offsets[i]);
            }
        }

        // if there is an index buffer value, bind it and drawIndexed.
        // otherwise draw normally.
        if self.index_buffer.is_some() {
            //instance.bind_index_buffer(cb,self.index_buffer.as_ref().unwrap(),self.index_offset,self.index_type);
            instance.bind_index_buffer(
                cb,
                self.index_buffer.as_ref().unwrap().raw(),
                self.index_offset,
                self.index_type,
            );
            instance.draw_indexed(
                cb,
                self.num_indices as u32,
                self.instance_count,
                self.first_index,
                self.vertex_offset,
                self.first_instance,
            );
        } else {
            instance.draw(
                cb,
                self.num_vertices,
                self.instance_count,
                self.first_vertex,
                self.first_instance,
            );
        }
    }
    pub fn get_attrib_descriptions(&self) -> Vec<VertexInputAttributeDescription> {
        self.input_attrib_descriptions.clone()
    }

    pub fn get_binding_descriptions(&self) -> Vec<VertexInputBindingDescription> {
        self.input_binding_descriptions.clone()
    }

    /// Renders the mesh information. Only applies binding and draw calls related to the mesh;
    /// as for other necessary components to render - assumes [crate::render::renderer::VkRenderer::record_commands] was called or
    /// assumes that the other necessary components were bound manually.
    pub fn render(&self, instance: &Vulkan, cb: CommandBuffer) {
        // bind attribute buffers
        for i in 0..self.attributes.len() {
            let itm = self.attributes.get(i);
            if itm.is_some() {
                let buff = itm.unwrap().buffer;
                //instance.bind_vertex_buffer(cb,i as u32,&buff,self.read_offsets[i]);
                instance.bind_vertex_buffer(cb, i as u32, &buff.raw(), self.read_offsets[i]);
            }
        }

        // if there is an index buffer value, bind it and drawIndexed.
        // otherwise draw normally.
        if self.index_buffer.is_some() {
            instance.bind_index_buffer(
                cb,
                self.index_buffer.as_ref().unwrap().raw(),
                self.index_offset,
                self.index_type,
            );
            instance.draw_indexed(
                cb,
                self.num_indices as u32,
                self.instance_count,
                self.first_index,
                self.vertex_offset,
                self.first_instance,
            );
        } else {
            instance.draw(
                cb,
                self.num_vertices,
                self.instance_count,
                self.first_vertex,
                self.first_instance,
            );
        }
    }
}
