use ash::extensions::ext::DebugUtils;
use ash::vk;
use ash::vk::DebugUtilsMessengerEXT;
use std::borrow::Cow;
use std::ffi::CStr;

/// debug callback for system
unsafe extern "system" fn vulkan_debug_callback(
    message_severity: vk::DebugUtilsMessageSeverityFlagsEXT,
    message_type: vk::DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const vk::DebugUtilsMessengerCallbackDataEXT,
    _user_data: *mut std::os::raw::c_void,
) -> vk::Bool32 {
    let callback_data = *p_callback_data;
    let message_id_number: i32 = callback_data.message_id_number as i32;

    let message_id_name = if callback_data.p_message_id_name.is_null() {
        Cow::from("")
    } else {
        CStr::from_ptr(callback_data.p_message_id_name).to_string_lossy()
    };

    let message = if callback_data.p_message.is_null() {
        Cow::from("")
    } else {
        CStr::from_ptr(callback_data.p_message).to_string_lossy()
    };

    println!(
        "{:?}: {:?} [{} ({})] : {} \n\n",
        message_severity,
        message_type,
        message_id_name,
        &message_id_number.to_string(),
        message,
    );

    vk::FALSE
}

/// builds the debugger / validator
//pub unsafe fn enable_validation(loader: &DebugUtils, entry: &Entry, instance: &Instance) -> DebugUtilsMessengerEXT {
pub unsafe fn enable_validation(loader: &DebugUtils) -> DebugUtilsMessengerEXT {
    let debug_info = vk::DebugUtilsMessengerCreateInfoEXT::default()
        .message_severity(
            vk::DebugUtilsMessageSeverityFlagsEXT::ERROR
                | vk::DebugUtilsMessageSeverityFlagsEXT::WARNING
                | vk::DebugUtilsMessageSeverityFlagsEXT::INFO,
        )
        .message_type(
            vk::DebugUtilsMessageTypeFlagsEXT::GENERAL
                | vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION
                | vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE,
        )
        .pfn_user_callback(Some(vulkan_debug_callback));

    loader
        .create_debug_utils_messenger(&debug_info, None)
        .unwrap()
}

////////////////

/*

/// Wrapper around debug marker to try and make it easier to identify Vulkan objects.
/// Note that currently, this only seems to work in conjunction with tools like RenderDoc, validation layers
/// do not appear to output the information correctly.
pub struct DebugLayer {
    utils: DebugUtils,
}

impl DebugLayer {
    pub fn new(app: &VkApp) -> Self {
        let core = app.get_instance();
        DebugLayer {
            utils: DebugUtils::new(core.get_entry(), core.get_instance())
        }
    }

    /// Starts a new grouping of commands under a new label within your debug utility
    pub fn begin(&self, command_buffer: &vk::CommandBuffer, label: &str, color: Vec4) {
        unsafe {
            self.utils.cmd_begin_debug_utils_label(*command_buffer, &DebugUtilsLabelEXT::builder()
                .label_name(&&CString::new(label).unwrap())
                .color([color.x, color.y, color.z, color.w])
                .build(),
            )
        }
    }

    /// Ends grouping of commands under previously started label.
    pub fn end(&self, command_buffer: &vk::CommandBuffer) {
        unsafe {
            self.utils.cmd_end_debug_utils_label(*command_buffer)
        }
    }

    /// Sets some information to be associated with a Vulkan object
    pub fn set_object_name(&self, device: &vk::Device, info: &DebugUtilsObjectNameInfoEXT) {
        unsafe {
            self.utils.debug_utils_set_object_name(*device, info).unwrap();
        }
    }

    /// Sets tag information to be associated with a Vulkan object.
    pub fn set_object_tag_info(&self, device: &vk::Device, info: &DebugUtilsObjectTagInfoEXT) {
        unsafe {
            self.utils.debug_utils_set_object_tag(*device, info).unwrap()
        }
    }
}

 */
