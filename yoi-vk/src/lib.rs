pub mod compute;
pub mod core;
pub mod debug;
pub mod descriptors;
pub mod framebuffer;
pub mod framework;
pub mod render;
pub mod sampler;
pub mod shaders;
pub mod texture;

pub mod raytracing;
mod types;
mod utils;

pub use ash::vk::{
    AttachmentDescription, BufferUsageFlags, ClearColorValue, ClearDepthStencilValue, ClearValue,
    CommandBuffer, Extent2D, Framebuffer, ImageView, Offset2D, Rect2D, RenderPassBeginInfo,
    SubpassDependency, Viewport,
};
pub use std::mem::size_of;

pub use crate::{
    compute::*,
    core::{VkAppInfo, Vulkan},
    descriptors::{
        descriptor_buffer::VkDescriptorBuffer, descriptor_set::VkDescriptorSet,
        storage_descriptor::StorageDescriptor,
        texture_sampler_descriptor::TextureSamplerDescriptor,
        uniform_descriptor::UniformDescriptor,
    },
    framework::vkapp::VkApp,
    framework::{databuffer::DataBuffer, mesh::Mesh},
    render::{
        render_pipeline::*, render_target::RenderTarget, renderer::*, renderpass::VkRenderPass,
        renderpass2::RenderPass2, rendertarget2::RenderTarget2, swapchain::VkSwapChain, utils::*,
    },
    sampler::VkSampler,
    shaders::shader::{ComputeShader, RenderShader},
    texture::VkTexture,
    types::{
        formats::{BufferFormat, FramebufferFormat, MeshAttribFormat, TextureFormat},
        traits::{Descriptor, VkObject},
    },
    utils::{
        copy_buffer_to_buffer, copy_image_to_image, fill_buffer, generate_color_attachment_format,
        generate_command_buffer, generate_default_color_attachment_descriptor,
        generate_general_layout_texture, generate_storage_buffer,
    },
};
