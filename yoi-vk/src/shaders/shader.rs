use crate::core::Vulkan;
use ash::vk::{
    PipelineShaderStageCreateInfo, ShaderModule, ShaderModuleCreateInfo, ShaderStageFlags,
};
use std::ffi::CString;
use std::fs;
use std::path::Path;

/// Defines a Shader object for the purposes of rendering something
pub struct RenderShader {
    vertex: Option<ShaderModule>,
    vertex_main: CString,
    fragment: Option<ShaderModule>,
    fragment_main: CString,
}

impl RenderShader {
    pub fn debug(source: String) {
        let compiler = shaderc::Compiler::new().unwrap();

        ////////// BUILD VERTEX //////////////
        let vertex_result = compiler
            .compile_into_spirv(
                &source,
                shaderc::ShaderKind::Fragment,
                "shader-vertex.glsl",
                "main",
                None,
            )
            .unwrap();
    }

    /// Initializes a new shader. Pass in path to vertex/fragment shaders + Vulkan instance.
    /// - "main" is assumed to be the entry point name for each shader.
    /// - You can pass in either the shader path or source code.
    pub fn new(vertex: &str, fragment: &str, instance: &Vulkan) -> Self {
        let v_path = Path::new(vertex);
        let f_path = Path::new(fragment);

        let mut vert_source = String::new();

        if v_path.exists() {
            //vert_source = load_file(vertex);
            let (parent, shader) = vertex.split_once("/").unwrap();
            resolve_includes(shader, Path::new(parent), &mut vert_source);
        } else {
            vert_source = String::from(vertex);
        }

        let mut frag_source = String::new();
        if f_path.exists() {
            //frag_source = load_file(fragment);
            let (parent, shader) = fragment.split_once("/").unwrap();
            resolve_includes(shader, Path::new(parent), &mut frag_source);
        } else {
            frag_source = String::from(fragment);
        }

        let compiler = shaderc::Compiler::new().unwrap();

        ////////// BUILD VERTEX //////////////
        let vertex_path = format!("{}", vertex);
        let vertex_result = compiler
            .compile_into_spirv(
                &vert_source,
                shaderc::ShaderKind::Vertex,
                vertex_path.as_str(),
                "main",
                None,
            )
            .unwrap();

        let vertex_shader_info = ShaderModuleCreateInfo::default().code(&vertex_result.as_binary());
        let vertex_module = instance.generate_shader_module(vertex_shader_info);
        ////////// BUILD FRAGMENT //////////////
        let fragment_path = format!("{}", fragment);
        let frag_result = compiler
            .compile_into_spirv(
                &frag_source,
                shaderc::ShaderKind::Fragment,
                fragment_path.as_str(),
                "main",
                None,
            )
            .unwrap();

        let frag_shader_info = ShaderModuleCreateInfo::default().code(&frag_result.as_binary());
        let frag_module = instance.generate_shader_module(frag_shader_info);

        RenderShader {
            vertex: Some(vertex_module),
            vertex_main: CString::new("main").unwrap(),
            fragment: Some(frag_module),
            fragment_main: CString::new("main").unwrap(),
        }
    }

    /// Initializes a new shader. Pass in path to vertex/fragment shaders + Vulkan instance.
    /// - "main" is assumed to be the entry point name for each shader.
    /// - You can pass in either the shader path or source code.
    pub fn new_debug(vert_source: &str, frag_source: &str, instance: &Vulkan) -> Self {
        let compiler = shaderc::Compiler::new().unwrap();

        ////////// BUILD VERTEX //////////////
        let vertex_result = compiler
            .compile_into_spirv(
                &vert_source,
                shaderc::ShaderKind::Vertex,
                "shader-vertex.glsl",
                "main",
                None,
            )
            .unwrap();

        let vertex_shader_info = ShaderModuleCreateInfo::default().code(&vertex_result.as_binary());
        let vertex_module = instance.generate_shader_module(vertex_shader_info);
        ////////// BUILD FRAGMENT //////////////
        let frag_result = compiler
            .compile_into_spirv(
                &frag_source,
                shaderc::ShaderKind::Fragment,
                "shader-fragment.glsl",
                "main",
                None,
            )
            .unwrap();

        let frag_shader_info = ShaderModuleCreateInfo::default().code(&frag_result.as_binary());
        let frag_module = instance.generate_shader_module(frag_shader_info);

        RenderShader {
            vertex: Some(vertex_module),
            vertex_main: CString::new("main").unwrap(),
            fragment: Some(frag_module),
            fragment_main: CString::new("main").unwrap(),
        }
    }

    pub fn generate_vertex_pipeline_info(&self) -> PipelineShaderStageCreateInfo {
        PipelineShaderStageCreateInfo {
            stage: ShaderStageFlags::VERTEX,
            module: self.vertex.unwrap(),
            p_name: self.vertex_main.as_ptr(),
            ..Default::default()
        }
    }

    /// Generates pipeline info for fragment stage.
    pub fn generate_fragment_pipeline_info(&self) -> PipelineShaderStageCreateInfo {
        PipelineShaderStageCreateInfo {
            stage: ShaderStageFlags::FRAGMENT,
            module: self.fragment.unwrap(),
            p_name: self.fragment_main.as_ptr(),
            ..Default::default()
        }
    }

    /// Destroys shader modules.
    pub fn destroy(&self, instance: &Vulkan) {
        if self.vertex.is_some() {
            instance.destroy_shader(self.vertex.unwrap());
        }

        if self.fragment.is_some() {
            instance.destroy_shader(self.fragment.unwrap());
        }
    }
}

/////////// COMPUTE SHADER //////////////

pub struct ComputeShader {
    compute: Option<ShaderModule>,
    main: CString,
}

impl ComputeShader {
    pub fn new(path: &str, instance: &Vulkan) -> Self {
        let c_path = Path::new(path);

        let mut source = String::new();
        if c_path.exists() {
            let (parent, shader) = path.split_once("/").unwrap();
            resolve_includes(shader, Path::new(parent), &mut source);
        } else {
            source = String::from(path);
        }

        let binding = source.as_str();
        let compiler = shaderc::Compiler::new().unwrap();
        let result = compiler
            .compile_into_spirv(
                binding,
                shaderc::ShaderKind::Compute,
                "shader.compute",
                "main",
                None,
            )
            .unwrap();
        let shader_info = ShaderModuleCreateInfo::default().code(&result.as_binary());

        ComputeShader {
            compute: Some(instance.generate_shader_module(shader_info)),
            main: CString::new("main").unwrap(),
        }
    }

    pub fn generate_pipeline_info(&self) -> PipelineShaderStageCreateInfo {
        PipelineShaderStageCreateInfo {
            stage: ShaderStageFlags::COMPUTE,
            module: self.compute.unwrap(),
            p_name: self.main.as_ptr(),
            ..Default::default()
        }
    }
}

fn load_file(src: &str) -> String {
    let s = format!("Something went wrong trying to read file at {}", src);
    fs::read_to_string(src).expect(s.as_str())
}

/// Recursively iterates through a file looking for #include statements and appending the contents line-by-line to
/// [output]
fn resolve_includes(path: &str, current_dir: &Path, output: &mut String) {
    let resolved_path = current_dir.join(path);
    let resolved_path = resolved_path.canonicalize().unwrap();

    let content = load_file(resolved_path.to_str().unwrap());

    let mut lines = content.lines();
    while let Some(line) = lines.next() {
        if line.starts_with("#include") {
            let included_path = line.trim_start_matches("#include").trim().replace("\"", "");
            let dir = resolved_path.parent().unwrap();
            resolve_includes(included_path.as_str(), dir, output);
        } else {
            output.push_str(line);
            output.push_str("\n");
        }
    }
}
