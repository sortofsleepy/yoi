use crate::core::Vulkan;
use crate::render::swapchain::VkSwapChain;
use crate::types::formats::FramebufferFormat;
use ash::vk;
use ash::vk::{Framebuffer, FramebufferCreateInfo};

/// Helper function to quickly generate a framebuffer.
pub fn create_framebuffer(
    instance: &Vulkan,
    format: FramebufferFormat,
    rp: &vk::RenderPass,
) -> Framebuffer {
    let create_info = FramebufferCreateInfo::default()
        .width(format.width)
        .height(format.height)
        .render_pass(*rp)
        .layers(format.layers)
        .attachments(&format.attachments[..]);

    instance.generate_framebuffer(create_info)
}

/// Generates a framebuffer for the purposes of rendering directly to the swapchain.
pub fn create_swapchain_framebuffer(
    instance: &Vulkan,
    format: FramebufferFormat,
    swapchain: &VkSwapChain,
    rp: &vk::RenderPass,
) -> Framebuffer {
    let views = swapchain.get_image_views();

    let mut attachments = vec![];

    // build a framebuffer for each swapchain image.
    for view in views {
        attachments.push(*view);
    }

    let create_info = FramebufferCreateInfo::default()
        .width(format.width)
        .height(format.height)
        .render_pass(*rp)
        .layers(format.layers)
        .attachments(&attachments[..]);

    instance.generate_framebuffer(create_info)
}
