yoi-vk
===

This is a set of helpers for working with Vulkan. It is all brought together via the [Ash](https://github.com/ash-rs/ash) crate.

General Notes
===
* The core of this is the `Vulkan` struct which contains the core commands and functions that are utilized throughout the library.
  * The idea is to pass an instance of this around whenever you need to do something like say, create a buffer.
  * This is where most of the `unsafe` calls will be localized to as well.
* It tries to make use of as many new features as possible so there may be some things that break or may not be available on your particular GPU.

Structure
===
* `debug` - general debug helpers
* `descriptors` - helpers and structs around descriptor related things both in regard to Descriptor buffers and normal descriptor sets.
* `framework` - this contains wrappers around various concepts such as buffers, meshes and general application information such as viewport and scissor settings.
* `raytracing` - this contains helpers for utilizing ray tracing hardware and related extensions. Note that I currently do not have an AMD GPU - while I will try to remember to use either native or KHR
extensions, things may slip my mind and some NVidia specific extensions may make their way in there.
* `render` - this contains helpers in regard to general rendering such has dealing with Swapchains, pipelines etc
* `shaders` - this contains helpers in regard to working with shaders. This library makes use of `shaderc` which should come bundled in your Vulkan SDK installation.
* `types` - this holds some types like buffer and texture formats and provides defaults for the various possible values. 
* `compute.rs` - deals with compute pipelines
* `core.rs` - this is kind of the heart of everything. Contains all the core Vulkan commands you might have to use including application initialization. 
* `framebuffer.rs` - deals with creating framebuffers
* `sampler.rs` - deals with creating samplers
* `texture.rs` - deals with creating textures
* `utils.rs` - some general purpose helper functions 